package br.pasifae.ariadne.remote;

import java.util.List;

import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.RespostaDoVoluntario;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;

/**
 * Created by JosueResende on 17/03/2018.
 */

public interface RespostaService {
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public List<RespostaDoVoluntario> respostas;
    }
// ------------------------------------------------------------------
    @GET("/resposta/voluntario/")
    Call<RespostaService.Response> respostas(@Field("usuario") String usuario);
// ------------------------------------------------------------------

}
