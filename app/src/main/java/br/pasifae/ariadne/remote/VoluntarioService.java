package br.pasifae.ariadne.remote;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by JosueResende on 18/03/2018.
 */

public interface VoluntarioService {
    public static class DadosDeResposta {
//        formulario_id":"1","questao_id":"1","resposta_id":"1","resposta_conteudo":"DATA","resposta_texto":null,"marcada_id":null,"marcada_texto":null,
        public long formulario_id = 0;
        public long formulario_id_resposta_referencia = 0;
        public long questao_id = 0;
        public long resposta_id = 0;
        public long marcada_id = 0;
        public String resposta_conteudo;
        public String resposta_texto;
        public String resposta_tag;
        public String marcada_texto;
    }
    public static class DadosDaQuestao{
        public long formulario_id = 0;
        public long questao_id = 0;
        public String questao_tipo;
        public String questao_texto;
        public int questao_limite_marcadas;
    }
    public static class DadosDoFormulario {
        public long formulario_id = 0;
        public String formulario_titulo;
        public String formulario_texto;
        public long resposta_referencia_id = 0;
        public long questao_referencia_id = 0;
    }
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public List<VoluntarioService.DadosDoFormulario> formularios;
        public List<VoluntarioService.DadosDaQuestao> questoes;
        public List<VoluntarioService.DadosDeResposta> respostas;
    }
// ------------------------------------------------------------------
    @FormUrlEncoded @POST("/voluntario/marcar/")
    Call<VoluntarioService.Response> marcar(@Field("usuario") String usuario,
                                            @Field("idQuestao") Long idQuestao,
                                            @Field("idResposta") Long idResposta,
                                            @Field("texto") String texto);
    @GET("/voluntario/formularios/")
    Call<VoluntarioService.Response> formularios(@Query("usuario") String usuario);
    @GET("/voluntario/questoes/")
    Call<VoluntarioService.Response> questoes(@Query("usuario") String usuario);
    @GET("/voluntario/respostas/")
    Call<VoluntarioService.Response> respostas(@Query("usuario") String usuario);
// ------------------------------------------------------------------
}
