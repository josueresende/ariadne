package br.pasifae.ariadne.remote;

import java.util.List;

import br.pasifae.ariadne.dados.modelo.Formulario;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;

/**
 * Created by JosueResende on 17/03/2018.
 */

public interface FormularioService {
    public static class DadosDoFormulario {
        public long formulario_id = 0;
        public String formulario_titulo;
        public String formulario_texto;
    }
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public List<DadosDoFormulario> formularios;
    }
// ------------------------------------------------------------------
    @GET("/formulario/voluntario/")
    Call<FormularioService.Response> formularios(@Field("usuario") String usuario);
// ------------------------------------------------------------------
}
