package br.pasifae.ariadne.remote;

import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Logger;

import br.pasifae.ariadne.security.Keyset;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;
import retrofit2.http.Header;

/**
 * Created by JosueResende on 16/03/2018.
 */

public interface AriadneHttpClient {
    final static OkHttpClient INSTANCE = new OkHttpClient().newBuilder()
            .addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    Keyset keyset = Keyset.load();
                    String querySign = null;
                    String bodySign = null;
                    if (request.url().query() != null) {
                        querySign = keyset.sign(request.url().query());
                    }
                    if (request.body() != null) {
                        BufferedSink bufferedSink = Okio.buffer(Okio.sink(new ByteArrayOutputStream()));
                        request.body().writeTo(bufferedSink);
                        String unsigned = bufferedSink.buffer().readUtf8();
                        Logger.getAnonymousLogger().info(String.format("<<%s>>", unsigned));
                        bodySign = keyset.sign(unsigned);
                    }
                    Request.Builder builder = request.newBuilder()
                            .header("Manufacturer", Build.MANUFACTURER)
                            .header("Model", Build.MODEL)
                            .header("UID", keyset.getUID())
                    ;
                    if (querySign != null) builder.addHeader("SIGN_Q", querySign);
                    if (bodySign != null) builder.addHeader("SIGN_B", bodySign);
                    request = builder.build();
                    for (String name : request.headers().names()) {
                        Logger.getAnonymousLogger().info(String.format("%s=%s", name, request.header(name)));
                    }
                    return chain.proceed(request);
                }
            })
            .build();
}

