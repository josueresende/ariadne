package br.pasifae.ariadne.remote;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.dados.modelo.Pesquisador;
import br.pasifae.ariadne.dados.modelo.Usuario;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by JosueResende on 14/04/2018.
 */

public interface MensagemService {
    public static class DadosDaMensagem implements Serializable {
        public long mensagem_id = 0;
        public String mensagem_uid;
        public long mensagem_direta_id = 0;
        public long mensagem_id_referencia = 0;
        public long anuncio_id = 0;
        public long busca_id = 0;
        public long autor_id = 0;
        public String autor_nome;
        public String autor_tipo;
        public long leitor_id = 0;
        public String leitor_nome;
        public String leitor_tipo;
        public String mensagem_conteudo;
        public Date mensagem_data_criacao;
        public String mensagem_usuario_nome;
        public Date mensagem_data_entregue;
        public Date mensagem_data_visualizada;
        public Date mensagem_data_respondida;
        public Date anuncio_data_expiracao;
        public UsuarioService.DadosDoUsuario remetente;
        public UsuarioService.DadosDoUsuario destinatario;
        public PesquisadorService.DadosDaBusca busca;
        public String mensagem_extra;
        public boolean hasJSONOnExtra() {
            return (this.mensagem_extra == null) ? false : (this.mensagem_extra.trim().startsWith("{") && this.mensagem_extra.trim().endsWith("}"));
        }
    }
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public List<DadosDaMensagem> mensagens = new LinkedList<>();
        public List<PesquisadorService.DadosDeResposta> respostas = new LinkedList<>();
    }
// ------------------------------------------------------------------
    @DELETE("/mensagem/anuncio/")
    Call<MensagemService.Response> removerAnuncio(
            @Query("idAnuncio") Long idAnuncio
    );
    @FormUrlEncoded @POST("/mensagem/anuncio/")
    Call<MensagemService.Response> anunciarNaBusca(
            @Field("uid") String uid,
            @Field("idBusca") Long idBusca,
            @Field("expiracao") String dataDeExpiracao,
            @Field("conteudo") String conteudo,
            @Field("extra") String extra
    );
    @FormUrlEncoded @POST("/mensagem/anuncio/")
    Call<MensagemService.Response> anunciarSemBusca(
            @Field("uid") String uid,
            @Field("criterios") String jsonCriterios,
            @Field("expiracao") String dataDeExpiracao,
            @Field("conteudo") String conteudo,
            @Field("extra") String extra
    );
    @GET("/mensagem/lista/")
    Call<MensagemService.Response> listarMensagens(
            @Query("idMensagem") Long idMensagem
    );
    @FormUrlEncoded @POST("/mensagem/direta/")
    Call<MensagemService.Response> enviarMensagemDireta(
            @Field("uid") String uid,
            @Field("usuario") String usuario,
            @Field("conteudo") String conteudo,
            @Field("extra") String extra
    );
// ------------------------------------------------------------------

}
