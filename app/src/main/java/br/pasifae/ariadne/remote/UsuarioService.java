package br.pasifae.ariadne.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.dados.modelo.Usuario;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by JosueResende on 16/03/2018.
 */

public interface UsuarioService {
    public static class DadosDoUsuario {
        public String tipo;
        public String login;
        public String senha;
        public String nome;
        public String sobrenome;
        public String sexo;
        public String data_nascimento;
        public String foto_base64;
        public String termo_ciencia;
        public String regiao;
        public String estado;
        public String municipio;
    }
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public Usuario usuario;
        //
        public List<VoluntarioService.DadosDoFormulario> formularios = new LinkedList<>();
        public List<VoluntarioService.DadosDaQuestao> questoes = new LinkedList<>();
        public List<VoluntarioService.DadosDeResposta> respostas = new LinkedList<>();
        public List<PesquisadorService.DadosDaBusca> buscas = new LinkedList<>();
        public List<MensagemService.DadosDaMensagem> mensagens = new LinkedList<>();

    }
// ------------------------------------------------------------------
    @FormUrlEncoded @POST("/usuario/senha/") Call<Response> atualizarSenha(@Field("password") String password);
    @FormUrlEncoded @POST("/usuario/foto/") Call<Response> atualizarFoto(@Field("usuario") String username, @Field("foto_base64") String base64);
    @Deprecated @FormUrlEncoded @POST("/usuario/registrar/") Call<Response> registrar(@Field("tipo") String tipo, @Field("nome") String username, @Field("senha") String password);
    @FormUrlEncoded @POST("/usuario/registrar/") Call<Response> registrarCriptografado(@Field("tipo") String tipo, @Field("username") String username, @Field("password") String password);
    @Deprecated @GET("/usuario/login/") Call<Response> login(@Query("nome") String nome, @Query("senha") String senha);
    @GET("/usuario/login/") Call<Response> loginCriptografado(@Query("username") String username, @Query("password") String password);
// ------------------------------------------------------------------
}
