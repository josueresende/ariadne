package br.pasifae.ariadne.remote;

import com.google.gson.GsonBuilder;

import br.pasifae.ariadne.dados.modelo.Usuario;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by JosueResende on 17/03/2018.
 */

public interface TeseuService {

    public static MyRetrofit retrofit = new MyRetrofit();

    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public String pubk;
    }
    // ------------------------------------------------------------------
    @FormUrlEncoded @POST("/location/set/") Call<TeseuService.Response> setLocation(@Field("usuario") String usuario, @Field("accuracy") double accuracy, @Field("altitude") double altitude, @Field("latitude") double latitude, @Field("longitude") double longitude);
    @FormUrlEncoded @POST("/session/open/") Call<TeseuService.Response> openSession(@Field("publickey") String publicKey);
    // ------------------------------------------------------------------
    //
    public static class MyRetrofit {
        Retrofit retrofit = null;
        public void setHost(String host) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(host)
                    .addConverterFactory(
                            GsonConverterFactory.create(
                                    new GsonBuilder()
                                            .setDateFormat("yyyy-MM-dd HH:mm:ss")
                                            .setLenient().create()
                            )
                    )
                    .client(AriadneHttpClient.INSTANCE)
                    .build();
        }
        public boolean hasHost() {
            return retrofit != null;
        }
        public <T> T create(Class<T> service) {
            if (retrofit != null) return retrofit.create(service);
            return null;
        }
    }
}
