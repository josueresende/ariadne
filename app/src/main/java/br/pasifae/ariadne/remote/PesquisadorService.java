package br.pasifae.ariadne.remote;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by JosueResende on 18/03/2018.
 */

public interface PesquisadorService {

    public  static class DadosDeQuestaoFormulario {
        public long questaoformulario_id = 0;
        public long questaoformulario_id_questao = 0;
        public long questaoformulario_id_formulario = 0;
        public String questaoformulario_ordem_exibicao = "";
        public int _deleted;
        public int _versao;
    }

    public  static class DadosDeFormulario {
        public long formulario_id = 0;
        public long formulario_id_resposta = 0;
        public String formulario_titulo;
        public int _deleted;
        public int _versao;
    }
    public static class DadosDoEstudo {
        public long estudo_id = 0;
        public String estudo_nome;
        public int _deleted;
    }

    public static class DadosDoVoluntario {
        public long voluntario_id = 0;
        public String voluntario_nome;
        public double distancia = 0;
        public List<VoluntarioService.DadosDeResposta> respostas;
    }

    public static class DadosDeResposta {
        public long estudo_id = 0;
        public long busca_id = 0;
        public String busca_deleted;
        public long buscada_id = 0;
        public long questao_id = 0;
        public long resposta_id = 0;
        public long anuncio_id = 0;
        public String resposta_conteudo;
        public String resposta_texto;
        public String resposta_tag;
        public String buscada_texto;
        public String buscada_deleted;
        public int _deleted;
        public int _versao;
    }
    public static class DadosDaQuestao{
        public long questao_id = 0;
        public String questao_tipo;
        public String questao_texto;
        public String questao_especifico;
        public int questao_iscriterio;
        public String questao_tag;
        public String questao_config;
        public int questao_limite_marcadas;
        public int _deleted;
        public int _versao;
    }
    public static class DadosDaBusca {
        public long busca_id = 0;
        public String busca_nome;
        public String busca_deleted;
    }
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public List<PesquisadorService.DadosDoVoluntario> voluntarios;
        public List<PesquisadorService.DadosDaBusca> buscas;
        public List<PesquisadorService.DadosDeResposta> respostas;
        public List<PesquisadorService.DadosDaQuestao> questoes;
        public List<PesquisadorService.DadosDoEstudo> estudos;
        public List<PesquisadorService.DadosDeFormulario> formularios;
        public List<PesquisadorService.DadosDeQuestaoFormulario> questoesformularios;
    }
// ------------------------------------------------------------------
    @DELETE("/pesquisador/resposta/")
    Call<PesquisadorService.Response> removerResposta(@Query("idBusca") Long idBusca, @Query("idQuestao") Long idQuestao, @Query("idResposta") Long idResposta);
    @DELETE("/pesquisador/busca/")
    Call<PesquisadorService.Response> removerBusca(@Query("idBusca") Long idBusca);
    @FormUrlEncoded @POST("/pesquisador/busca/")
    Call<PesquisadorService.Response> novaBusca(@Field("usuario") String usuario, @Field("idEstudo") Long idEstudo, @Field("nome") String nome);
    @FormUrlEncoded @POST("/pesquisador/resposta/")
    Call<PesquisadorService.Response> novaResposta(@Field("usuario") String usuario, @Field("idBusca") Long idBusca, @Field("idQuestao") Long idQuestao, @Field("idResposta") Long idResposta, @Field("expressao") String expressao);
    @GET("/pesquisador/voluntarios/")
    Call<PesquisadorService.Response> trazerVoluntarios(@Query("usuario") String usuario, @Query("idBusca") Long idBusca);
    @GET("/pesquisador/buscas/")
    Call<PesquisadorService.Response> trazerBusca(@Query("idBusca") Long idBusca);
    @GET("/pesquisador/buscadas/")
    Call<PesquisadorService.Response> trazerBuscadas(@Query("idBusca") Long idBusca);
    @GET("/pesquisador/questoes/")
    Call<PesquisadorService.Response> trazerQuestoes(@Query("idBusca") Long idBusca);
    @GET("/pesquisador/estudos/")
    Call<PesquisadorService.Response> estudos(@Query("usuario") String usuario);
    @GET("/pesquisador/buscas/update/")
    Call<PesquisadorService.Response> updateBuscas(@Query("idBusca") Long idBusca);
    @GET("/pesquisador/buscas/")
    Call<PesquisadorService.Response> buscas(@Query("usuario") String usuario);
    @GET("/pesquisador/buscadas/")
    Call<PesquisadorService.Response> buscadas(@Query("usuario") String usuario);
    @GET("/pesquisador/questoes/")
    Call<PesquisadorService.Response> questoes(@Query("idVersao") Integer idVersao);
    @GET("/pesquisador/respostas/")
    Call<PesquisadorService.Response> respostas(@Query("idVersao") Integer idVersao);
    @GET("/pesquisador/formularios/")
    Call<PesquisadorService.Response> formularios(@Query("idVersao") Integer idVersao);
// ------------------------------------------------------------------
}
