package br.pasifae.ariadne.remote;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by JosueResende on 16/04/2018.
 */

public interface QuestaoService {
    public static class Response {
        public int error_code = 0;
        public String error_description;
        public String debug;
        public String pubk;
        // TODO colocar questoes e respostas
    }
// ------------------------------------------------------------------
    // TODO implementar no projeto Teseu
    @GET("/questao/update") Call<Response> update(@Query("lastUpdate") Date lastUpdate);
// ------------------------------------------------------------------
}
