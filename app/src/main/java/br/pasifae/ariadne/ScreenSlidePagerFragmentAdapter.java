package br.pasifae.ariadne;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.voluntario.Questionario_FinalFragment;
import br.pasifae.ariadne.voluntario.Questionario_QuestaoFragment;

/**
 * Created by JosueResende on 05/04/2018.
 *
 * TODO Utilizar os formularios/questoes/respostas baixadas
 *
 */

public class ScreenSlidePagerFragmentAdapter extends FragmentStatePagerAdapter {

    private Map<Long, List<Formulario>> questoesVinculadas = new LinkedHashMap<>();
    private List<Questao> questoes = new LinkedList<Questao>();
    private Map<Long, Resposta> respostas = new LinkedHashMap<Long, Resposta>();

    public ScreenSlidePagerFragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        this.questoes.clear();
        this.respostas.clear();
    }

    @Override
    public Fragment getItem(int position) {
        Logger.getAnonymousLogger().info("getItem: " + position);
        if (position < questoes.size()) {
            return (new Questionario_QuestaoFragment())
                    .setPagerAdapter(this)
                    .setPosition(position)
                    .setQuestao(questoes.get(position))
            ;
        } else {
            return (new Questionario_FinalFragment())
                    .setPagerAdapter(this)
                    .setPosition(position)
            ;
        }
    }

    public void add(Formulario formulario) {
        if (formulario.getResposta() == null) return;
        if (questoesVinculadas.containsKey(formulario.getResposta().getId()) == false) {
            questoesVinculadas.put(formulario.getResposta().getId(), new LinkedList<Formulario>());
        }
        questoesVinculadas.get(formulario.getResposta().getId()).add(formulario);
    }

    public void add(Questao questao) {
        this.questoes.add(questao);
    }

    public void insere(Questao aposQuestao, Questao questao) {
        this.questoes.add(this.questoes.indexOf(aposQuestao) + 1, questao);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return questoes.size() + 1;
    }

    public void checarQuestao(int position) {
        if ((position < questoes.size()) &&
            (position > 0) &&
            (questoes.get(position - 1).isRespondida())) {
            int index = position;
            for (Resposta resposta : questoes.get(position - 1).getRespondidas()) {
                if (questoesVinculadas.containsKey(resposta.getId())) {
                    for (Formulario formulario : questoesVinculadas.get(resposta.getId())) {
                        for (Questao questao : formulario.getQuestoes()) {
                            if (questoes.contains(questao)) continue;
                            questoes.add(index++, questao);
                        }
                    }
                }
            }
            List<Long> respondidas = new LinkedList<>();
            for(Long id : questoesVinculadas.keySet()) {
                for (Questao questao : questoes) {
                    if (questao.getRespondidas().contains(new Resposta(id))) respondidas.add(id);
                }
            }
            for(Long id : questoesVinculadas.keySet()) {
                if (respondidas.contains(id)) continue;
                Logger.getAnonymousLogger().info("SPOILED " + id);
                for (Formulario formulario : questoesVinculadas.get(id)) {
                    for (Questao questao : formulario.getQuestoes()) {
                        if (questoes.contains(questao)) {
                            questao.desmarcar();
                            questoes.remove(questao);
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    public void before() {

    }

    public void next() {

    }
}