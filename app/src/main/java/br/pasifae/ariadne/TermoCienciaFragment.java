package br.pasifae.ariadne;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.MultiAutoCompleteTextView;

import br.pasifae.ariadne.dados.Acesso;


/**
 */
public class TermoCienciaFragment extends DialogFragment {

    static public interface TermoCienciaDialogListener {
        public void OnUserSelectPositive();
        public void OnUserSelectNegative();
    }

    private TermoCienciaDialogListener listener;
    private View layout;
    private Acesso acesso;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String termo = "";

    public TermoCienciaFragment() {
        // Required empty public constructor
    }

    public void setTextTermo(String termo) {
        this.termo = termo;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (listener != null) listener.OnUserSelectNegative();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        acesso = new Acesso(getActivity());

        layout = inflater.inflate(R.layout.fragment_termo_ciencia, null);

//        setCancelable(false);

        MultiAutoCompleteTextView multiAutoCompleteTextView = layout.findViewById(R.id.multiAutoCompleteTextView);
        multiAutoCompleteTextView.setEnabled(false);
        multiAutoCompleteTextView.setText(termo);

        builder.setView(layout)
                // Add action buttons
//                .setNegativeButton("REJEITAR", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        if (listener != null) listener.OnUserSelectNegative();
//                        TermoCienciaFragment.this.getDialog().cancel();
//                    }
//                })
                .setPositiveButton("EU CONCORDO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener != null) listener.OnUserSelectPositive();
                    }
                });
        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TermoCienciaDialogListener) {
            this.listener = (TermoCienciaDialogListener) context;
//        } else
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TermoCienciaDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
        listener = null;
    }

}
