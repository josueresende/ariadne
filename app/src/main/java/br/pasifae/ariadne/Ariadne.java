package br.pasifae.ariadne;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 14/02/2018.
 */

public class Ariadne extends Application {

    static
    private Context APP_CONTEXT;

    static
    final public String CHANNEL_ID = "PASIFAE_ARIADNE_CHANNEL_v1";
    static
    final public String ARIADNE_NAME = "Voluntário v1.0";
    static
    final public String ARIADNE_DESCRIPTION = "Aplicação para voluntários de pesquisas";

    static
    private int notification_id = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        APP_CONTEXT = getApplicationContext();
    }

    final static public Context getAppContext() {
        return APP_CONTEXT;
    }

    static
    public void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, ARIADNE_NAME, importance);
            channel.setDescription(ARIADNE_DESCRIPTION);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getAppContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private static void notify(NotificationCompat.Builder mBuilder) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getAppContext());
        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify("Ariadne", R.string.app_name, notification);
        try {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone ringtone = RingtoneManager.getRingtone(getAppContext(), uri);
            ringtone.play();
        } catch (Exception e) {

        }
    }

    static public class NotificacaoConteudo implements Serializable {
        long timestamp;
        String conteudo;

        public NotificacaoConteudo(long timestamp, String conteudo) {
            this.timestamp = timestamp;
            this.conteudo = conteudo;
        }
    }

    static public class Notificacao implements Serializable {
        long autor_id;
        String autor_nome;
        long timestamp;
        List<NotificacaoConteudo> conteudo = new LinkedList<>();
        public Notificacao(long autor_id, String autor_nome) {
            this.autor_id = autor_id;
            this.autor_nome = autor_nome;
        }
        public void add(long timestamp, String mensagem_conteudo) {
            if (mensagem_conteudo.length() > 20) {
                conteudo.add(new NotificacaoConteudo(timestamp, mensagem_conteudo.substring(0, 19) + "..."));
            } else {
                conteudo.add(new NotificacaoConteudo(timestamp, mensagem_conteudo));
            }
        }
    }

    public static void bundleNotification(Notificacao notificacao) {
        NotificationManagerCompat notificationManager = null;
        NotificationCompat.Builder summaryBuilder = null;
        NotificationCompat.InboxStyle inboxStyle = null;
        NotificationCompat.Builder builder = null;
        NotificationCompat.MessagingStyle messagingStyle = null;
        {
            notificationManager = NotificationManagerCompat.from(getAppContext());
        }
        {   // SUMMMARY
            {
                //Build and issue the group summary. Use inbox style so that all messages are displayed
                summaryBuilder = new NotificationCompat.Builder(getAppContext())
                        .setContentTitle("Mensagens")
                        .setSmallIcon(R.drawable.ic_trials)
                        .setGroupSummary(true)
                        .setGroup("REVOLUTION_GROUP");
            }
            {
                summaryBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            }
            {
                String line = (notificacao.conteudo.size() == 1) ? "1 mensagem nova" : String.format("%d mensagens novas", notificacao.conteudo.size());
                inboxStyle = new NotificationCompat.InboxStyle();
                inboxStyle.setBigContentTitle(notificacao.autor_nome);
                inboxStyle.addLine(line);

                summaryBuilder.setStyle(inboxStyle);
            }
            {
                notificationManager.notify(R.string.app_name, summaryBuilder.build());
            }
        }
        {
            messagingStyle = new NotificationCompat.MessagingStyle(notificacao.autor_nome);
            for (NotificacaoConteudo notificacaoConteudo : notificacao.conteudo) {
                messagingStyle.addMessage(notificacaoConteudo.conteudo, notificacaoConteudo.timestamp, notificacao.autor_nome);
            }
        }
        {
            //issue the Bundled notification. Since there is a summary notification, this will only display
            //on systems with Nougat or later
            builder = new NotificationCompat.Builder(getAppContext())
                    .setContentTitle(notificacao.autor_nome)
                    .setContentText(
                            (notificacao.conteudo.size() == 1) ? "1 mensagem nova" : String.format("%d mensagens novas", notificacao.conteudo.size())
                    )
                    .setSmallIcon(R.drawable.ic_trials)
                    .setStyle(messagingStyle)
                    .setGroup("REVOLUTION_GROUP");
        }
        int requestCode = R.string.app_name + new Long(notificacao.autor_id).intValue();
        {   /**
            //Add an action that simply starts the main activity. This is not very useful it is mainly for demonstration
            Intent intent = new Intent(getAppContext(), NotificationActivity.class);
            //Each notification needs a unique request code, so that each pending intent is unique. It does not matter
            //in this simple case, but is important if we need to take action on a specific notification, such as
            //deleting a message
            PendingIntent pendingIntent = PendingIntent.getActivity(getAppContext(), requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(android.R.drawable.ic_input_get,
                            "Do Something", pendingIntent)
                            .build();
            builder.addAction(action);
            //*/
        }
        {   /**
            Intent intent = new Intent(getAppContext(), NotificationActivity.class);
            intent.setAction("FROM_NOTIFICATION");
            intent.putExtra(notificacao.getClass().getName(), notificacao);

            PendingIntent pendingIntent = PendingIntent.getActivity(
                    getAppContext(),
                    requestCode,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            // **/
        }
        {
            Intent intent = new Intent(getAppContext(), NotificationActivity.class);
            intent.setAction("FROM_NOTIFICATION");
            intent.putExtra(notificacao.getClass().getName(), notificacao);
            PendingIntent pendingIntent = PendingIntent.getActivity(
                    getAppContext(),
                    requestCode,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Action action = new NotificationCompat.Action.Builder(android.R.drawable.ic_input_get,
                    "Abrir Conversa", pendingIntent)
                    .build();
            builder.addAction(action);
        }
        {
            notificationManager.notify(requestCode, builder.build());
        }
    }
}
