package br.pasifae.ariadne.security;

import java.io.Serializable;

/**
 * Created by JosueResende on 14/02/2018.
 */

public class Keys implements Serializable {
    byte[] privateKey;
    byte[] publicKey;
}
