package br.pasifae.ariadne.security;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Base64;

import org.json.JSONObject;

import java.io.Serializable;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.UUID;
import java.util.logging.Logger;

import javax.crypto.Cipher;

import br.pasifae.ariadne.dados.Acesso;

/**
 * Created by JosueResende on 14/02/2018.
 */

public class Keyset implements Serializable {

    String uid;
    Keys local = new Keys();
    Keys remote = new Keys();

    static public Keyset load() {
        return new Keyset();
    }

    private Keyset() {
        Cursor cursor = Acesso.Database.execSelect(
            "SELECT uid, local_privateKey, local_publicKey, remote_publicKey FROM keyset ORDER BY id DESC LIMIT 1",
            null
        );
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            uid = cursor.getString(0);
            local.privateKey = cursor.getBlob(1);
            local.publicKey = cursor.getBlob(2);
            remote.publicKey = cursor.getBlob(3);
        }
    }

    public void loadRemotePublicKey(String pem) {
        try {
            pem = pem
                .replaceAll(" ","")
            ;
            StringBuffer stringBuffer = new StringBuffer();
            for (String line : pem.split("\n")) {
                if (line.contains("PUBLICKEY")) continue;
                stringBuffer.append(line);
            }
            Logger.getAnonymousLogger().info("base64: " + stringBuffer.toString());
            remote.publicKey = Base64.decode(stringBuffer.toString(), Base64.DEFAULT);
            ContentValues contentValues = new ContentValues();
            contentValues.put("remote_publicKey", remote.publicKey);
            Acesso.Database.execUpdate("keyset", contentValues, "uid = ?", new String[]{uid});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getUID() {
        return new String(uid.getBytes());
    }
    public String getPublicKey() { return Base64.encodeToString(local.publicKey, Base64.NO_WRAP | Base64.URL_SAFE); }
    public boolean noVerify() {
        return (remote.publicKey == null || remote.publicKey.length == 0);
    }
    public boolean noSign() {
        return (local.privateKey == null || local.privateKey.length == 0);
    }
    public boolean noEncrypt() {
        return (remote.publicKey == null || remote.publicKey.length == 0);
    }
    public boolean noDecrypt() {
        return (local.privateKey == null || local.privateKey.length == 0);
    }
    public void generate() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            local.privateKey = keyPair.getPrivate().getEncoded();
            local.publicKey = keyPair.getPublic().getEncoded();
            uid = UUID.randomUUID().toString();
            ContentValues contentValues = new ContentValues();
            contentValues.put("uid", uid);
            contentValues.put("local_privateKey", local.privateKey);
            contentValues.put("local_publicKey", local.publicKey);
            Acesso.Database.execInsert("keyset", contentValues);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String encrypt(String decrypted) {
        try {
            if (noEncrypt()) return null;

//            Logger.getAnonymousLogger().info("REMOTE.PUBLICKEY: >" + (new String(remote.publicKey)) + "<");

            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(remote.publicKey);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);

//            Logger.getAnonymousLogger().info("PUBLIC KEY: " + new String(Base64.encode(publicKey.getEncoded(), Base64.NO_WRAP)));

//            OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", new MGF1ParameterSpec("SHA-256"), PSource.PSpecified.DEFAULT);

            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] encrypted = cipher.doFinal(decrypted.getBytes());
//            Logger.getAnonymousLogger().info("RAW: " +decrypted + " >" + new String(encrypted) + "<");
//            Logger.getAnonymousLogger().info("DEFAULT: " +decrypted + " >" + Base64.encodeToString(encrypted, Base64.DEFAULT) + "<");
//            Logger.getAnonymousLogger().info("NOWRAP AND URLSAFE (b64): " +decrypted + " >" + Base64.encodeToString(encrypted, Base64.NO_WRAP) + "<");
//            Logger.getAnonymousLogger().info("NOWRAP AND URLSAFE (hex): " +decrypted + " >" + new String(Hex.encodeHex(encrypted)) + "<");
            return Base64.encodeToString(encrypted, Base64.NO_WRAP | Base64.URL_SAFE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public String sign(String unsigned) {
        try {
            if (noSign()) return null;
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(local.privateKey);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);

            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(privateKey);
            signature.update(unsigned.getBytes());
            return Base64.encodeToString(signature.sign(), Base64.NO_WRAP | Base64.URL_SAFE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public boolean verify(String signed, String signatureInBase64) {
        try {
            if (noVerify()) return false;
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(remote.publicKey);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);

            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(publicKey);
            signature.update(signed.getBytes());
            return signature.verify(Base64.decode(signatureInBase64, Base64.NO_WRAP | Base64.URL_SAFE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("keyset.uid", uid);
            if (local.privateKey != null) jsonObject.put("keyset.local.privateKey", Base64.encodeToString(local.privateKey, Base64.DEFAULT));
            if (local.publicKey != null) jsonObject.put("keyset.local.publicKey", Base64.encodeToString(local.publicKey, Base64.DEFAULT));
            if (remote.privateKey != null) jsonObject.put("keyset.remote.privateKey", Base64.encodeToString(remote.privateKey, Base64.DEFAULT));
            if (remote.publicKey != null) jsonObject.put("keyset.remote.publicKey", Base64.encodeToString(remote.publicKey, Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public void fromJSONObject(JSONObject jsonObject) {
        try {
            if (jsonObject != null && jsonObject.has("keyset.uid")) {
                uid = jsonObject.getString("keyset.uid");
                if (jsonObject.has("keyset.local.privateKey"))
                    local.privateKey = Base64.decode(jsonObject.getString("keyset.local.privateKey"), Base64.NO_WRAP | Base64.URL_SAFE);
                if (jsonObject.has("keyset.local.publicKey"))
                    local.publicKey = Base64.decode(jsonObject.getString("keyset.local.publicKey"), Base64.NO_WRAP | Base64.URL_SAFE);
                if (jsonObject.has("keyset.remote.privateKey"))
                    remote.privateKey = Base64.decode(jsonObject.getString("keyset.remote.privateKey"), Base64.NO_WRAP | Base64.URL_SAFE);
                if (jsonObject.has("keyset.remote.publicKey"))
                    remote.publicKey = Base64.decode(jsonObject.getString("keyset.remote.publicKey"), Base64.NO_WRAP | Base64.URL_SAFE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
