package br.pasifae.ariadne;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import java.util.logging.Logger;

public class NotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        {   /** ATUA EM CONJUNTO COM Main_Activity.onNewIntent **/
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancel(R.string.app_name);
                finish();
                }
            }, new IntentFilter("LOGOUT"));
        }

        if (getIntent().getAction() != null && getIntent().getAction().equals("FROM_NOTIFICATION")) {

            for(String key : getIntent().getExtras().keySet()) {
                Logger.getAnonymousLogger().info("key = " + key);
            }

            if (getIntent().hasExtra(Ariadne.Notificacao.class.getName())) {
                Ariadne.Notificacao notificacao = (Ariadne.Notificacao) getIntent().getSerializableExtra(Ariadne.Notificacao.class.getName());

                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE))
                        .cancel(R.string.app_name + (new Long(notificacao.autor_id).intValue()));

                LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                        new Intent("TAB_CHANGE")
                                .putExtra("TAB", "MENSAGENS")
                                .putExtra("MESSAGE_AUTOR_ID", notificacao.autor_id)
                );
            }
            finish();
        }

    }
}
