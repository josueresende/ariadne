package br.pasifae.ariadne.pesquisador;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import br.pasifae.ariadne.R;

public class Perfil_SliderFragmentActivity extends FragmentActivity {
    ViewPager viewPager;
    Perfil_FragmentStatePagerAdapter pagerAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        {   /** ATUA EM CONJUNTO COM Main_Activity.onNewIntent **/
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    nm.cancel(R.string.app_name);
                    finish();
                }
            }, new IntentFilter("LOGOUT"));
        }
        viewPager = new ViewPager(this);
        pagerAdapter = new Perfil_FragmentStatePagerAdapter(getSupportFragmentManager(), viewPager);
        viewPager.setId(View.generateViewId());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setLongClickable(true);
        viewPager.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        setContentView(viewPager);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        } else {
            getParent().onBackPressed();
        }
    }
}
