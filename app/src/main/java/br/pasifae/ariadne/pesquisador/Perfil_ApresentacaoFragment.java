package br.pasifae.ariadne.pesquisador;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.Usuario;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class Perfil_ApresentacaoFragment extends Perfil_Fragment {
//    LinearLayout linearLayout_0;
    LinearLayout linearLayout_1;
    LinearLayout linearLayout_2;
    LinearLayout linearLayout_3;
    LinearLayout linearLayout_4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        String email =
                Acesso.usuarioLogadoComSucesso.email.principal;
        String sistema =
                Acesso.usuarioLogadoComSucesso.getNomeDeExibicao();
        String nome =
                Acesso.usuarioLogadoComSucesso.getNome();
//        String sobrenome =
//                Acesso.usuarioLogadoComSucesso.getSobrenome();
        String idioma =
                Acesso.usuarioLogadoComSucesso.getIdioma();
        String telefone =
                Acesso.usuarioLogadoComSucesso.telefone.principal;
//        Usuario.JSON.Endereco endereco =
//                Acesso.usuarioLogadoComSucesso.endereco.get();
        String cpf =
                Acesso.usuarioLogadoComSucesso.documento.cpf;
        String cnpj =
                Acesso.usuarioLogadoComSucesso.documento.cnpj;
        String passaporte =
                Acesso.usuarioLogadoComSucesso.documento.passaporte;
        {
            if (telefone == null) telefone = "";
            if (email == null) email = "";
            if (nome == null) nome = "";
//            if (sobrenome == null) sobrenome = "";
            if (idioma == null) {
                idioma = "";
            } else if (idioma.length() > 0) {
                Resposta resposta = acesso.getResposta("IDIOMA", idioma);
                if (resposta != null) idioma = resposta.getResposta();
            }
        }
//        ScrollView scrollView = new ScrollView(viewPager.getContext());
//        {
//            linearLayout.addView(scrollView);
//        }
//        {
//            linearLayout_0 = new LinearLayout(viewPager.getContext());
//            scrollView.addView(linearLayout_0);
//            linearLayout_0.setOrientation(LinearLayout.VERTICAL);
//            linearLayout_0.setPadding(5, 5, 5, 5);
//        }
        {
            {
                linearLayout_1 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_1);
                linearLayout_1.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_1.setLayoutParams(layoutParams);
                linearLayout_1.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_1.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("INFORMAÇÕES BÁSICAS");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("VISUALIZADO COMO: " + sistema);
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("NOME: " + (nome));
                textView.setPadding(5, 5, 5, 5);
            }
//            {
//                TextView textView = new TextView(viewPager.getContext());
//                linearLayout_1.addView(textView);
//                textView.setText("SOBRENOME: " + (sobrenome));
//                textView.setPadding(5, 5, 5, 5);
//            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("E-MAIL: " + (email));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("TELEFONE: " + (telefone));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("IDIOMA: " + (idioma));
                textView.setPadding(5, 5, 5, 5);
            }
        }
        {
            View view_space = new View(viewPager.getContext());
            linearLayout.addView(view_space);
            view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
        }
        {
            {
                linearLayout_4 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_4);
                linearLayout_4.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_4.setLayoutParams(layoutParams);
                linearLayout_4.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_4.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_4.addView(textView);
                textView.setText("DOCUMENTOS");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_4.addView(textView);
                textView.setText("CPF: " + (cpf == null ? "" : cpf));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_4.addView(textView);
                textView.setText("CNPJ: " + (cnpj == null ? "" : cnpj));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_4.addView(textView);
                textView.setText("PASSAPORTE: " + (passaporte == null ? "" : passaporte));
                textView.setPadding(5, 5, 5, 5);
            }
        }
        {
            View view_space = new View(viewPager.getContext());
            linearLayout.addView(view_space);
            view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
        }
        {
            {
                linearLayout_3 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_3);
                linearLayout_3.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_3.setLayoutParams(layoutParams);
                linearLayout_3.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_3.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("ENDEREÇO");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("ESTADO: " + (Acesso.usuarioLogadoComSucesso.estado == null ? "" : Acesso.usuarioLogadoComSucesso.estado));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("CIDADE: " + (Acesso.usuarioLogadoComSucesso.municipio == null ? "" : Acesso.usuarioLogadoComSucesso.municipio));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("CEP: " + (Acesso.usuarioLogadoComSucesso.endereco.cep == null ? "" : Acesso.usuarioLogadoComSucesso.endereco.cep));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("BAIRRO: " + (Acesso.usuarioLogadoComSucesso.endereco.bairro == null ? "" : Acesso.usuarioLogadoComSucesso.endereco.bairro));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("RUA: " + (Acesso.usuarioLogadoComSucesso.endereco.via == null ? "" : Acesso.usuarioLogadoComSucesso.endereco.via));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("NUMERO: " + (Acesso.usuarioLogadoComSucesso.endereco.numero == null ? "" : Acesso.usuarioLogadoComSucesso.endereco.numero));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("COMPLEMENTO: " + (Acesso.usuarioLogadoComSucesso.endereco.complemento == null ? "" : Acesso.usuarioLogadoComSucesso.endereco.complemento));
                textView.setPadding(5, 5, 5, 5);
            }
        }
        {
            View view_space = new View(viewPager.getContext());
            linearLayout.addView(view_space);
            view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
        }
        {
            {
                linearLayout_2 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_2);
                linearLayout_2.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_2.setLayoutParams(layoutParams);
                linearLayout_2.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_2.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_2.addView(textView);
                textView.setText("TROCAR SENHA");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_2.addView(textView);
                textView.setText("Clique aqui para alterar ...");
                textView.setPadding(5, 5, 5, 5);
                textView.setTypeface(null, Typeface.ITALIC);
            }
        }
        {
            linearLayout_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentStatePagerAdapter.next(Perfil_FragmentStatePagerAdapter.FRAGMENT_NAME.BASICO);
                }
            });
        }
        {
            linearLayout_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentStatePagerAdapter.next(Perfil_FragmentStatePagerAdapter.FRAGMENT_NAME.SENHA);
                }
            });
        }
        {
            linearLayout_3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentStatePagerAdapter.next(Perfil_FragmentStatePagerAdapter.FRAGMENT_NAME.ENDERECO);
                }
            });
        }
        {
            linearLayout_4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentStatePagerAdapter.next(Perfil_FragmentStatePagerAdapter.FRAGMENT_NAME.DOCUMENTO);
                }
            });
        }
        return relativeLayout;
    }

}
