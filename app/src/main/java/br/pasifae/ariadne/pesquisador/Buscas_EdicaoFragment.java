package br.pasifae.ariadne.pesquisador;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_Application_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.Pesquisador;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.pesquisador.adapters.Criterios_RecyclerViewAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by JosueResende on 09/04/2018.
 */

public class Buscas_EdicaoFragment  extends Fragment {

    Acesso acesso;
    protected Buscas_FragmentStatePagerAdapter buscasFragmentStatePagerAdapter;
    protected ViewPager viewPager;
    protected Busca busca;

    private ProgressDialog progressDialog;

    TextView textView_titulo;
    LinearLayout linearLayout_nome;
    LinearLayout linearLayout_criterios;
    LinearLayout linearLayout_input;
    EditText editText_nome;
    RecyclerView recyclerView;
    Criterios_RecyclerViewAdapter recyclerViewAdapter;
    Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;
    private ConfirmacaoFragment confirmacaoDeDelecaoFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Aguarde...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
        }

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        RelativeLayout relativeLayout = new RelativeLayout(viewPager.getContext());

        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(Color.WHITE);
            relativeLayout.setBackground(gradientDrawable);
        }
        {
            RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
            textView_titulo = new TextView(viewPager.getContext());
            relativeLayout.addView(textView_titulo);
            textView_titulo.setText("EDICAO DA BUSCA");
            textView_titulo.setTextColor(Color.WHITE);
            textView_titulo.setBackgroundColor(Color.TRANSPARENT);
            textView_titulo.setPadding(5, 5, 5, 5);
            textView_titulo.setLayoutParams(relativeLayoutParams);
            relativeLayoutParams.addRule(RelativeLayout.ALIGN_TOP, RelativeLayout.TRUE);
        }
        {
            RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
            linearLayout_nome = new LinearLayout(viewPager.getContext());
            linearLayout_nome.setId(View.generateViewId());
            relativeLayout.addView(linearLayout_nome);
            linearLayout_nome.setOrientation(LinearLayout.VERTICAL);
            linearLayout_nome.setLayoutParams(relativeLayoutParams);
            relativeLayoutParams.addRule(RelativeLayout.BELOW, textView_titulo.getId());
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_nome.addView(textView);
                textView.setText("NOME");
                textView.setBackgroundColor(Color.WHITE);
                textView.setPadding(5, 5, 5, 5);
            }
            {
                editText_nome = new EditText(viewPager.getContext());
                linearLayout_nome.addView(editText_nome);
                editText_nome.setBackgroundColor(Color.WHITE);
                editText_nome.setPadding(5, 5, 5, 5);
            }
        }
        {
            RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
            linearLayout_criterios = new LinearLayout(viewPager.getContext());
            linearLayout_criterios.setId(View.generateViewId());
            relativeLayout.addView(linearLayout_criterios);
            linearLayout_criterios.setOrientation(LinearLayout.VERTICAL);
            linearLayout_criterios.setLayoutParams(relativeLayoutParams);
            relativeLayoutParams.addRule(RelativeLayout.BELOW, linearLayout_nome.getId());
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_criterios.addView(textView);
                textView.setText("CRITÉRIOS");
                textView.setBackgroundColor(getResources().getColor(android.R.color.white));
                textView.setPadding(5, 5, 5, 5);
            }
            {
                recyclerView = new RecyclerView(viewPager.getContext());
                recyclerView.setId(View.generateViewId());
                linearLayout_criterios.addView(recyclerView);
                recyclerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            }
            {
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(viewPager.getContext(), LinearLayoutManager.VERTICAL, false)
                );
            }
            {
                recyclerViewAdapter = new Criterios_RecyclerViewAdapter(viewPager.getContext(), busca);
                recyclerView.setAdapter(recyclerViewAdapter);
            }
            {
                recyclerViewAdapter.setOnItemClickListener(new Criterios_RecyclerViewAdapter.OnItemClickListener<Criterio>() {
                    @Override
                    public void onItemClick(Criterio item) {
                        //
                    }

                    @Override
                    public void onDeleteClick(Criterio item) {
                        if (item == null) return;
                        confirmacaoDeDelecaoFragment
                                .setObject(item)
                                .show(getFragmentManager(), "CONFIRMAÇÃO");

                    }
                });
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(30);
                recyclerView.setBackground(gradientDrawable);
            }
        }
/*******/
        {
            confirmacaoDeDelecaoFragment = new ConfirmacaoFragment<Criterio>()
                    .setOnPositive(new ConfirmacaoFragment.OnPositive<Criterio>() {
                        @Override
                        public void click(Criterio o) {
                            new DeleteCriterio().execute(o);
                        }
                    });
            confirmacaoDeDelecaoFragment.setTextoConfirmacao("Confirma a exclusão do Critério?");
            confirmacaoDeDelecaoFragment.setTitulo("EXCLUSÃO");
        }
/*******/
        {
            RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(MATCH_PARENT, 2);
            View view_space = new View(viewPager.getContext());
            relativeLayout.addView(view_space);
            view_space.setId(View.generateViewId());
            view_space.setLayoutParams(relativeLayoutParams);
            relativeLayoutParams.addRule(RelativeLayout.BELOW, linearLayout_criterios.getId());
        }
/*******/
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(container.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_add_24px);
                floatingActionButton.setElevation(2);
                floatingActionButton.setColorFilter(Color.WHITE);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.setMargins(5,5,5,5);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimaryDark));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NovoCriterioPesquisadorDialogFragment dialogFragment = new NovoCriterioPesquisadorDialogFragment();
                        dialogFragment.setListener(new NovoCriterioPesquisadorDialogFragment.NovoCriterioPesquisadorDialogListener() {
                            @Override
                            public void OnUserSelectNovoCriterioPesquisadorPositive(List<Criterio> criterios) {
                                new SaveCriterio(busca).execute(criterios.toArray(new Criterio[]{ }));
                            }

                            @Override
                            public void OnUserSelectNovoCriterioPesquisadorNegative() {

                            }
                        });
                        Bundle bundle = new Bundle();
                        Collection<Questao> questoes = acesso.getQuestoes();
                        bundle.putSerializable("questoes", (Serializable) questoes);
                        dialogFragment.setArguments(bundle);
                        dialogFragment.show(getFragmentManager(), "NOVO CRITERIO");

                    }
                });
            }
        }

        if (busca != null)
        {
            editText_nome.setText(busca.getNome());
        }
        {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    new LoadData().execute();
                }
            }, new IntentFilter("NEW_SEARCH"));
        }
        return relativeLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadData().execute();
    }

    public Buscas_EdicaoFragment setFragmentStatePagerAdapter(Buscas_FragmentStatePagerAdapter buscasFragmentStatePagerAdapter) {
        this.buscasFragmentStatePagerAdapter = buscasFragmentStatePagerAdapter;
        return this;
    }

    public Buscas_EdicaoFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    public Buscas_EdicaoFragment setBusca(Busca busca) {
        this.busca = busca;
        return this;
    }

    class DeleteCriterio extends AsyncTask<Criterio, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Criterio... os) {
            for (Criterio criterio : os) {
                criterio.setBusca(busca);
                acesso.delCriterio(criterio);
            }
            return null;
        }
    }

    class SaveCriterio extends AsyncTask<Criterio, Void, Void> {

        protected Pesquisador pesquisador;
        protected Busca busca;
        List<Criterio> criterios = new LinkedList<Criterio>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        public SaveCriterio(Busca busca) {
            this.busca = busca;
            this.pesquisador = Acesso.pesquisadorLogadoComSucesso;
        }

        @Override
        protected Void doInBackground(Criterio... os) {
            for (Criterio criterio : os) {
                criterio.setBusca(busca);
                acesso.putCriterio(criterio);
            }
            return null;
        }
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        private int updated = 0;
        private List<Resposta> respostas = new LinkedList<>();

        @Override
        protected Void doInBackground(Void... voids) {
            {
                recyclerViewAdapter.clear();
            }
            {
                Cursor cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "resposta_buscada.texto AS criterio_texto, " +
                                "resposta_buscada.id_resposta AS resposta_id, " +
                                "resposta_buscada.id_questao AS questao_id, " +
                                "resposta_buscada.id AS criterio_id " +
                                "FROM resposta_buscada " +
                                "WHERE resposta_buscada.id_busca = ? " +
                                "ORDER BY resposta_buscada.id_busca " +
                                "", new String[] { busca.getId().toString() });
                while (cursor.moveToNext()) {
                    updated++;
                    respostas.add(
                            new Resposta(
                                    cursor.getLong(cursor.getColumnIndex("resposta_id")),""
                            )
                            .setTextoDaBusca(
                                    cursor.getString(cursor.getColumnIndex("criterio_texto"))
                            )
                            .setQuestao(
                                    new Questao(cursor.getLong(cursor.getColumnIndex("questao_id")), "")
                            )
                    );
                }
                cursor.close();
            }
            Database_Application_SQLiteOpenHelper application_sqLiteOpenHelper =
                    new Database_Application_SQLiteOpenHelper();
            for(Resposta resposta : respostas)
            {
                Cursor cursor = application_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "resposta.id AS resposta_id," +
                                "resposta.conteudo AS resposta_conteudo, " +
                                "resposta.texto AS resposta_texto, " +
                                "questao.texto_especifico AS questao_especifico, " +
                                "questao.texto AS questao_texto " +
                                "FROM resposta " +
                                "INNER JOIN questao ON questao.id = resposta.id_questao " +
                                "WHERE resposta.id = ? " +
                                "", new String[] { resposta.getId().toString() })
                        ;
                while (cursor.moveToNext()) {
                    Long id = cursor.getLong(cursor.getColumnIndex("resposta_id"));
                    if (resposta.getId().equals(id) == false) continue;
                    resposta.setConteudo(
                            Resposta.Conteudo.valueOf(cursor.getString(cursor.getColumnIndex("resposta_conteudo")))
                    );
                    resposta.setResposta(
                            cursor.getString(cursor.getColumnIndex("resposta_texto"))
                    );
                    resposta.getQuestao().setQuestao(
                            cursor.getString(cursor.getColumnIndex("questao_texto"))
                    );
                    resposta.getQuestao().setCriterio(
                            cursor.getString(cursor.getColumnIndex("questao_especifico"))
                    );
                }

                cursor.close();
            }
            Logger.getAnonymousLogger().info("");
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for(Resposta resposta : respostas)
            {
                recyclerViewAdapter.add(resposta);
            }
            if (progressDialog.isShowing()) progressDialog.dismiss();
        }
    }
    static
    public
    class ConfirmacaoFragment<T> extends DialogFragment {

        LinearLayout linearLayout;
        private String title = "CONFIRMAÇÃO";
        private String textoConfirmacao = "Confirma ?";

        private T object;

        ConfirmacaoFragment.OnPositive<T> onPositive;
        ConfirmacaoFragment.OnNegative<T> onNegative;

        public void setTextoConfirmacao(String textoConfirmacao) {
            this.textoConfirmacao = textoConfirmacao;
        }

        public void setTitulo(String title) {
            this.title = title;
        }
        static public interface OnPositive<T> {
            public void click(T o);
        };

        static public interface OnNegative<T> {
            public void click(T o);
        };

        public ConfirmacaoFragment setOnPositive(OnPositive<T> onPositive) {
            this.onPositive = onPositive;
            return this;
        }

        public ConfirmacaoFragment setOnNegative(OnNegative<T> onNegative) {
            this.onNegative = onNegative;
            return this;
        }

        public ConfirmacaoFragment setObject(T o) {
            this.object = o;
            return this;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText(title);
                textView.setTextSize(21);
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimaryDark));
                textView.setPadding(10,10,10,10);
            }
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText(textoConfirmacao);
                textView.setTextSize(20);
                textView.setPadding(20,20,20,20);
            }

            builder.setView(linearLayout)
                    .setPositiveButton("CONFIRMA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (onPositive != null) onPositive.click(object);
                        }
                    })
                    .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (onNegative != null) onNegative.click(object);
                        }
                    });

            return builder.create();
        }
    }

}
