package br.pasifae.ariadne.pesquisador;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.Validator;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import br.com.receitasdecodigo.utils.MaskEditUtil;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.VoluntarioService;
import retrofit2.Call;
import retrofit2.Response;

public class Perfil_EnderecoFragment extends Perfil_Fragment {

    Validator validator;

    private Spinner spinner_estado;
    private Spinner spinner_cidade;

    private EditText editText_cep;
    private EditText editText_bairro;
    private EditText editText_via;
    private EditText editText_numero;
    private EditText editText_complemento;

    private Resposta_SpinnerAdapter resposta_spinnerAdapter_estado;
    private Resposta_SpinnerAdapter resposta_spinnerAdapter_cidade;
    private Resposta resposta_cep;
    private Resposta resposta_bairro;
    private Resposta resposta_via;
    private Resposta resposta_numero;
    private Resposta resposta_complemento;

    class UpdateProfile extends AsyncTask<Void, Void, Void> {

        Map<String, Resposta> respostas = new LinkedHashMap<>();

        private void add(Object object) {
            if ((object instanceof Resposta) == false) return;
            Resposta resposta = (Resposta) object;
            Questao questao = Acesso.Database.getQuestaoById(resposta.getQuestao().getId());
            resposta.setQuestao(questao);
            if (questao.getLimiteDeRespostasMarcadas() == 1) {
                if (questao.getTag() == null) return;
                respostas.put(questao.getTag(), resposta);
            } else if (questao.getLimiteDeRespostasMarcadas() == -1) {
                if (questao.getTag() == null) return;
                if (resposta.getTag() == null) return;
                respostas.put(questao.getTag() + "|" + resposta.getTag(), resposta);
            }
        }

        private String get(String k) {
            if (respostas.containsKey(k) == false) return null;
            Resposta resposta = respostas.get(k);
            if (resposta.getTag() == null) return resposta.getResposta();
            return resposta.getTag();
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Atualizando...");
            progressDialog.show();
            add(spinner_estado.getSelectedItem());
            add(spinner_cidade.getSelectedItem());
            add(resposta_cep);
            add(resposta_bairro);
            add(resposta_via);
            add(resposta_numero);
            add(resposta_complemento);
        }

        private String getTexto(String tag) {
            if (respostas.get(tag) == null) return "";
            if (respostas.get(tag).getQuestao().getLimiteDeRespostasMarcadas() == 1) return respostas.get(tag).getTag();
            if (respostas.get(tag).doVoluntario.hasTexto()) return respostas.get(tag).doVoluntario.texto;
            return "";
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Acesso.usuarioLogadoComSucesso
                    .estado = getTexto("ESTADO");
            Acesso.usuarioLogadoComSucesso
                    .municipio = getTexto("MUNICIPIO");
            Acesso.usuarioLogadoComSucesso
                    .endereco.cep = getTexto("ENDERECO|CEP");
            Acesso.usuarioLogadoComSucesso
                    .endereco.bairro = getTexto("ENDERECO|BAIRRO");
            Acesso.usuarioLogadoComSucesso
                    .endereco.via = getTexto("ENDERECO|VIA");
            Acesso.usuarioLogadoComSucesso
                    .endereco.numero = getTexto("ENDERECO|NUMERO");
            Acesso.usuarioLogadoComSucesso
                    .endereco.complemento = getTexto("ENDERECO|COMPLEMENTO");

            fragmentStatePagerAdapter.before();
            fragmentStatePagerAdapter.reset();

            progressDialog.dismiss();
        }

        private void update(String tag) {
            Logger.getAnonymousLogger().info("UPDATE: " + tag);
            Resposta resposta = respostas.get(tag);
            if (resposta == null) return;
            Long idResposta = resposta.getId();
            Long idQuestao = resposta.getQuestao().getId();
            Logger.getAnonymousLogger().info(String.format("idQuestao: %d : idResposta: %d : texto: %s", idQuestao, idResposta, (resposta.doVoluntario.hasTexto() ? resposta.doVoluntario.texto : "")));
            try {
                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
                Call<VoluntarioService.Response> call =
                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
                                idQuestao,
                                idResposta,
                                (resposta.doVoluntario.hasTexto() ? resposta.doVoluntario.texto : "")
                        );
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (String tag : new String[] {
                    "ESTADO",
                    "MUNICIPIO",
                    "ENDERECO|CEP",
                    "ENDERECO|BAIRRO",
                    "ENDERECO|VIA",
                    "ENDERECO|NUMERO",
                    "ENDERECO|COMPLEMENTO"}) {
                update(tag);
            }
            return null;
        }
    }

    class Resposta_SpinnerAdapter extends Pasifae_SpinnerAdapter<Resposta> {

        public Resposta_SpinnerAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public boolean isEnabled(int position) {
            return (getItemId(position) > -1);
        }

        @Override
        public long getItemId(int position) {
            Object resposta = getItem(position);
            if (resposta != null && resposta instanceof Resposta) return ((Resposta)resposta).getId();
            return -1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout linearLayout = new LinearLayout(mContext);
            {
                Resposta resposta = (Resposta) getItem(position);
                TextView textView = new TextView(mContext);
                linearLayout.addView(textView);
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView.setText(resposta.getResposta());
            }
            return linearLayout;
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            validator = new Validator(this);
            validator.setValidationListener(this);
        }
        {
            resposta_spinnerAdapter_estado = new Resposta_SpinnerAdapter(viewPager.getContext());
            resposta_spinnerAdapter_cidade = new Resposta_SpinnerAdapter(viewPager.getContext());
            resposta_cep = acesso.getResposta("ENDERECO", "CEP");
            resposta_bairro = acesso.getResposta("ENDERECO", "BAIRRO");
            resposta_via = acesso.getResposta("ENDERECO", "VIA");
            resposta_numero = acesso.getResposta("ENDERECO", "NUMERO");
            resposta_complemento = acesso.getResposta("ENDERECO", "COMPLEMENTO");
        }
        {
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("ENDERECO");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("ESTADO");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                spinner_estado = new Spinner(viewPager.getContext());
                linearLayout.addView(spinner_estado);
                spinner_estado.setAdapter(resposta_spinnerAdapter_estado);

            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("CIDADE");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                spinner_cidade = new Spinner(viewPager.getContext());
                linearLayout.addView(spinner_cidade);
                spinner_cidade.setAdapter(resposta_spinnerAdapter_cidade);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("CEP");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                editText_cep = new EditText(viewPager.getContext());
                linearLayout.addView(editText_cep);
                editText_cep.setText(Acesso.usuarioLogadoComSucesso.endereco.cep);
                editText_cep.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText_cep.addTextChangedListener(MaskEditUtil.mask(editText_cep, MaskEditUtil.FORMAT_CEP));
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("BAIRRO");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                editText_bairro = new EditText(viewPager.getContext());
                linearLayout.addView(editText_bairro);
                editText_bairro.setText(Acesso.usuarioLogadoComSucesso.endereco.bairro);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("RUA");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                editText_via = new EditText(viewPager.getContext());
                linearLayout.addView(editText_via);
                editText_via.setText(Acesso.usuarioLogadoComSucesso.endereco.via);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("NÚMERO");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                editText_numero = new EditText(viewPager.getContext());
                linearLayout.addView(editText_numero);
                editText_numero.setText(Acesso.usuarioLogadoComSucesso.endereco.numero);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("COMPLEMENTO");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                editText_complemento = new EditText(viewPager.getContext());
                linearLayout.addView(editText_complemento);
                editText_complemento.setText(Acesso.usuarioLogadoComSucesso.endereco.complemento);
            }
        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_archive_24px);
                floatingActionButton.setElevation(2);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10,10,10,10);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimary));
                floatingActionButton.setImageTintList(getResources().getColorStateList(android.R.color.white));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        if (validator != null) validator.validate();
//                        if (validated)
                        {
                            resposta_cep.doVoluntario.texto = editText_cep.getText().toString();
                            resposta_bairro.doVoluntario.texto = editText_bairro.getText().toString();
                            resposta_via.doVoluntario.texto = editText_via.getText().toString();
                            resposta_numero.doVoluntario.texto = editText_numero.getText().toString();
                            resposta_complemento.doVoluntario.texto = editText_complemento.getText().toString();
                            new UpdateProfile().execute();
                        }
                    }
                });
            }

        }
        {
            Questao questao = acesso.getQuestao("ESTADO");
            List<Resposta> respostas_estado = acesso.getRespostas("ESTADO", null);

            if (respostas_estado.size() > 0) {
                Collections.sort(respostas_estado, new Comparator<Resposta>() {
                    @Override
                    public int compare(Resposta o1, Resposta o2) {
                        return o1.getResposta().compareTo(o2.getResposta());
                    }
                });
                resposta_spinnerAdapter_estado.reset();
                resposta_spinnerAdapter_estado.putItem(
                        new Resposta(-1L, Resposta.Conteudo.NENHUM, "<Selecione seu Estado>")
                                .setQuestao(questao)
                );
                int index = 0;
                int selection = -1;
                for (Resposta resposta : respostas_estado) {
                    ++index;
                    resposta_spinnerAdapter_estado.putItem(resposta);
                    if (resposta.getTag() == null) continue;
                    if (Acesso.usuarioLogadoComSucesso.estado == null) continue;
                    if (Acesso.usuarioLogadoComSucesso.estado.equalsIgnoreCase(resposta.getTag())) {
                        selection = index;
                    }
                }
                if (selection > -1) {
                    spinner_estado.setSelection(selection);
                }
            }
        }
        {
            spinner_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    resposta_spinnerAdapter_cidade.reset();
                    List<Resposta> respostas_cidade = null;
                    Questao questao = null;
                    {
                        Resposta resposta = (Resposta) spinner_estado.getSelectedItem();
                        respostas_cidade = acesso.getRespostasReferentes(resposta.getId());
                        if (respostas_cidade.size() > 0) {
                            questao = respostas_cidade.get(0).getQuestao();
                        }
                    }
                    if (respostas_cidade != null && respostas_cidade.size() > 0) {
                        Collections.sort(respostas_cidade, new Comparator<Resposta>() {
                            @Override
                            public int compare(Resposta o1, Resposta o2) {
                                return o1.getResposta().compareTo(o2.getResposta());
                            }
                        });
                        resposta_spinnerAdapter_cidade.putItem(
                                new Resposta(-1L, Resposta.Conteudo.NENHUM, "<Selecione sua Cidade>")
                                        .setQuestao(questao)
                        );
                        int index = 0;
                        int selection = -1;
                        for (Resposta resposta : respostas_cidade) {
                            ++index;
                            resposta_spinnerAdapter_cidade.putItem(resposta);
                            if (resposta.getTag() == null) continue;
                            if (Acesso.usuarioLogadoComSucesso.municipio == null) continue;
                            if (Acesso.usuarioLogadoComSucesso.municipio.equalsIgnoreCase(resposta.getTag())) {
                                selection = index;
                            }
                        }
                        if (selection > -1) {
                            spinner_cidade.setSelection(selection);
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        return relativeLayout;
    }

}
