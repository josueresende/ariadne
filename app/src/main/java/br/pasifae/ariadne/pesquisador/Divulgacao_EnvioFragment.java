package br.pasifae.ariadne.pesquisador;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Anuncio;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.Resposta;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class Divulgacao_EnvioFragment extends Divulgacao_Fragment {

    LinearLayout linearLayout_1;
    LinearLayout linearLayout_2;
    LinearLayout linearLayout_3;

    TextView textView_expiracao;
    TextView textView_caae;
    TextView textView_criterio;
    TextView textView_mensagem;

    private ConfirmacaoFragment confirmacaoDeDelecaoFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("ENVIO");
                textView.setPadding(5, 5, 5, 5);
            }
            {
                View view_space = new View(viewPager.getContext());
                linearLayout.addView(view_space);
                view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
            }
            {
                linearLayout_1 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_1);
                linearLayout_1.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_1.setLayoutParams(layoutParams);
                linearLayout_1.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_1.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("EXPIRAÇÂO");
                textView.setPadding(5, 5, 5, 5);
            }
            {
                textView_expiracao = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView_expiracao);
                textView_expiracao.setPadding(5, 5, 5, 5);
            }
            {
                if (fragmentStatePagerAdapter.anuncio.getDataDeExpiracao() == null) {
                    textView_expiracao.setText("");
                } else {
                    textView_expiracao.setText(new SimpleDateFormat("dd/MM/yyyy").format(fragmentStatePagerAdapter.anuncio.getDataDeExpiracao()));
                }
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("CAAE");
                textView.setPadding(5, 5, 5, 5);
            }
            {
                textView_caae = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView_caae);
                textView_caae.setPadding(5, 5, 5, 5);
                textView_caae.setText(fragmentStatePagerAdapter.anuncio.extras.getExtra("CAAE"));
            }
            {
                View view_space = new View(viewPager.getContext());
                linearLayout.addView(view_space);
                view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
            }
            {
                linearLayout_2 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_2);
                linearLayout_2.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_2.setLayoutParams(layoutParams);
                linearLayout_2.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_2.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_2.addView(textView);
                textView.setText("CRITÉRIOS");
                textView.setPadding(5, 5, 5, 5);
            }
            {
                textView_criterio = new TextView(viewPager.getContext());
                linearLayout_2.addView(textView_criterio);
                textView_criterio.setPadding(5, 5, 5, 5);
            }
            {
                if (fragmentStatePagerAdapter.anuncio.getCriterios().size() == 0) {
                    textView_criterio.setText("");
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (Criterio o : fragmentStatePagerAdapter.anuncio.getCriterios()) {
                        if (stringBuilder.length() > 0) stringBuilder.append("\n");
                        {
                            Resposta.FromJSON jsonObject = o.getResposta().getRespostaAsJSON();
                            Resposta.Conteudo conteudo = o.getResposta().getConteudo();
                            if (Resposta.Conteudo.DATA.equals(conteudo) && jsonObject != null) {
                                stringBuilder.append(String.format("%s ENTRE %d E %d ANOS",
                                        o.getQuestao().getTituloParaPesquisador(),
                                        jsonObject.min,
                                        jsonObject.max
                                ));
                            } else if (Resposta.Conteudo.DURACAO_MESES.equals(conteudo) && jsonObject != null) {
                                stringBuilder.append(String.format("%s ENTRE %d E %d MESES",
                                        o.getQuestao().getTituloParaPesquisador(),
                                        jsonObject.min,
                                        jsonObject.max
                                ));
                            } else {
                                stringBuilder.append(o.getQuestao().getTituloParaPesquisador() + " = " + o.getResposta().getResposta());
                            }

                        }
                    }
                    textView_criterio.setText(stringBuilder.toString());
                }
            }
            {
                View view_space = new View(viewPager.getContext());
                linearLayout.addView(view_space);
                view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
            }
            {
                linearLayout_3 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_3);
                linearLayout_3.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_3.setLayoutParams(layoutParams);
                linearLayout_3.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_3.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("MENSAGEM");
                textView.setPadding(5, 5, 5, 5);
            }
            {
                textView_mensagem = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView_mensagem);
                textView_mensagem.setPadding(5, 5, 5, 5);
                textView_mensagem.setText(fragmentStatePagerAdapter.anuncio.getConteudo());
            }
            {
                FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
                relativeLayout.addView(floatingActionButton);
                {
                    floatingActionButton.setId(View.generateViewId());

                    floatingActionButton.setImageResource(android.R.drawable.ic_menu_send);
                    floatingActionButton.setElevation(2);

                    floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                    floatingActionButton.setFocusable(true);

                    RelativeLayout.LayoutParams layoutParams =
                            new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    layoutParams.setMargins(10, 10, 10, 10);

                    floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimary));

                    floatingActionButton.setLayoutParams(layoutParams);
                }
                {
                    confirmacaoDeDelecaoFragment = new ConfirmacaoFragment<Anuncio>()
                            .setOnPositive(new ConfirmacaoFragment.OnPositive<Anuncio>() {
                                @Override
                                public void click(Anuncio item) {
                                    new Send().execute();
                                }
                            });
                    confirmacaoDeDelecaoFragment.setTextoConfirmacao("Confirma o envio desta Divulgação?");
                    confirmacaoDeDelecaoFragment.setTitulo("ENVIO");
                }
                {
                    floatingActionButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (fragmentStatePagerAdapter.anuncio.isCompletoParaEnviar()) {
                                confirmacaoDeDelecaoFragment
                                        .show(getFragmentManager(), "CONFIRMAÇÃO");
                            } else {
                                Toast.makeText(getContext(), "Preencha todos os itens para enviar", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
                }
            }
        }
        return relativeLayout;
    }
    class Send extends Loading<Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                    new Intent("WAIT_FOR_A_MESSAGE_UPDATE_ON")
            );
            fragmentStatePagerAdapter.reset();
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                    new Intent("TAB_CHANGE")
                            .putExtra("TAB", "DIVULGAR")
            );
        }

        @Override
        protected Void doInBackground(Void... strings) {
//            Logger.getAnonymousLogger().info("anuncio: " + Acesso.toJson(this.anuncio));
            acesso.putAnuncio(fragmentStatePagerAdapter.anuncio);
            return null;
        }

    }

    abstract class Loading<Param> extends AsyncTask<Param, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

}
