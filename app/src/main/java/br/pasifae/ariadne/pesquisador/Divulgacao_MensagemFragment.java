package br.pasifae.ariadne.pesquisador;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class Divulgacao_MensagemFragment  extends Divulgacao_Fragment {

    LinearLayout linearLayout_mensagem;
    EditText editText_conteudo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            {
                linearLayout_mensagem = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_mensagem);
                linearLayout_mensagem.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
                linearLayout_mensagem.setLayoutParams(layoutParams);
                linearLayout_mensagem.setOrientation(LinearLayout.VERTICAL);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_mensagem.addView(textView);
                textView.setText("MENSAGEM");
                textView.setPadding(5, 5, 5, 5);
            }
            {
                editText_conteudo = new EditText(viewPager.getContext());
                linearLayout_mensagem.addView(editText_conteudo);
                editText_conteudo.setPadding(5,5,5,5);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
                editText_conteudo.setLayoutParams(layoutParams);
                editText_conteudo.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
                editText_conteudo.setGravity(Gravity.BOTTOM);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                editText_conteudo.setBackground(gradientDrawable);
            }
            {
                if (fragmentStatePagerAdapter.anuncio.getConteudo() != null) {
                    editText_conteudo.setText(fragmentStatePagerAdapter.anuncio.getConteudo());
                }
            }
        }
        return relativeLayout;
    }

    @Override
    public void navigateToLeft(View view) {
        fragmentStatePagerAdapter.anuncio.setConteudo(editText_conteudo.getText().toString());
    }

    @Override
    public void navigateToRight(View view) {
        fragmentStatePagerAdapter.anuncio.setConteudo(editText_conteudo.getText().toString());
    }
}
