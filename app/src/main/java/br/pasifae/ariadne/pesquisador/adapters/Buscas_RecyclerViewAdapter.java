package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Busca;

public class Buscas_RecyclerViewAdapter extends RecyclerView.Adapter<Buscas_RecyclerViewAdapter.ViewHolder>  {

    final public List<Busca> buscas = new LinkedList<>();
    final private Context mContext;
    private OnItemClickListener onItemClickListener;

    static public interface OnItemClickListener {
        void onItemClick(Object item);
        void onDeleteClick(Object item);
        void onEditClick(Object item);
        void onSearchClick(Object item);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public Buscas_RecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;
        buscas.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Busca_ViewHolder(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(buscas.get(position));
    }

    @Override
    public int getItemCount() {
        return buscas.size();
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Busca o);
    }

    public Busca getItem(int position) {
        return this.buscas.get(position);
    }

    public int getPositionById(long id) {
        for (int position = 0; position < buscas.size(); position++) {
            if (buscas.get(position).getId() == null) continue;
            if (buscas.get(position).getId().longValue() == id) return position;
        }
        return -1;
    }

    public Busca getBuscaById(long id) {
        for (int position = 0; position < buscas.size(); position++) {
            if (buscas.get(position).getId() == null) continue;
            if (buscas.get(position).getId().longValue() == id) return buscas.get(position);
        }
        return null;
    }

    class Busca_ViewHolder extends Buscas_RecyclerViewAdapter.ViewHolder implements View.OnClickListener {
        TextView textView_nome;
        LinearLayout linearLayout;

        public Busca_ViewHolder(ConstraintLayout itemView) {
            super(itemView);
            {
                itemView.setOnClickListener(this);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                itemView.setPadding(5, 5, 5, 5);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.LTGRAY);
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
                itemView.setBackground(gradientDrawable);
            }
            {
                ConstraintLayout.LayoutParams constraintLayoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.MATCH_PARENT);

                textView_nome = new TextView(mContext);
                textView_nome.setId(View.generateViewId());
                itemView.addView(textView_nome);
                textView_nome.setPadding(10, 10, 10, 10);
                textView_nome.setLayoutParams(constraintLayoutParams);
            }
            {
                ConstraintLayout.LayoutParams constraintLayoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

                linearLayout = new LinearLayout(mContext);
                itemView.addView(linearLayout);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.setLayoutParams(constraintLayoutParams);
                linearLayout.setGravity(Gravity.RIGHT);
                linearLayout.setPadding(0,0,0,0);

//                {
//                    GradientDrawable gradientDrawable = new GradientDrawable();
//                    gradientDrawable.setColor(Color.GREEN);
//                    linearLayout.setBackground(gradientDrawable);
//                }

//                View[] bars = new View[4];
//                {
//                    LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(2, ViewGroup.LayoutParams.MATCH_PARENT);
//                    for (int i = 0; i < bars.length; i++) {
//                        GradientDrawable gradientDrawable = new GradientDrawable();
//                        gradientDrawable.setColor(Color.GRAY);
//                        bars[i] = new View(mContext);
//                        bars[i].setId(View.generateViewId());
//                        bars[i].setLayoutParams(linearLayoutParams);
//                        bars[i].setBackground(gradientDrawable);
//                        linearLayout.addView(bars[i]);
//                    }
//                }
                {
                    ImageButton imageButton = new ImageButton(mContext);
                    imageButton.setId(View.generateViewId());
                    linearLayout.addView(imageButton);
                    imageButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_delete_24px));
                    imageButton.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    imageButton.setColorFilter(Color.WHITE);

                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setStroke(2, Color.TRANSPARENT);
                    gradientDrawable.setColor(mContext.getResources().getColor(R.color.pesquisadorPrimary));
                    gradientDrawable.setCornerRadius(60);
                    imageButton.setBackground(gradientDrawable);

                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemClickListener != null) onItemClickListener.onDeleteClick(buscas.get(getAdapterPosition()));
                        }
                    });
                }
                {
                    ImageButton imageButton = new ImageButton(mContext);
                    imageButton.setId(View.generateViewId());
                    linearLayout.addView(imageButton);
                    imageButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_edit_24px));
                    imageButton.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    imageButton.setColorFilter(Color.WHITE);

                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setStroke(2, Color.TRANSPARENT);
                    gradientDrawable.setColor(mContext.getResources().getColor(R.color.pesquisadorPrimary));
                    gradientDrawable.setCornerRadius(60);
                    imageButton.setBackground(gradientDrawable);

                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemClickListener != null) onItemClickListener.onEditClick(buscas.get(getAdapterPosition()));
                        }
                    });
                }
                {
                    ImageButton imageButton = new ImageButton(mContext);
                    imageButton.setId(View.generateViewId());
                    linearLayout.addView(imageButton);
                    imageButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_search_24px));
                    imageButton.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    imageButton.setColorFilter(Color.WHITE);

                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setStroke(2, Color.TRANSPARENT);
                    gradientDrawable.setColor(mContext.getResources().getColor(R.color.pesquisadorPrimary));
                    gradientDrawable.setCornerRadius(60);
                    imageButton.setBackground(gradientDrawable);

                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemClickListener != null) onItemClickListener.onSearchClick(buscas.get(getAdapterPosition()));
                        }
                    });
                }

            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        textView_nome.getId(),
                        ConstraintSet.LEFT,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.LEFT,
                        5
                );
                constraintSet.connect(
                        linearLayout.getId(),
                        ConstraintSet.RIGHT,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.RIGHT,
                        5
                );

//                constraintSet.connect(
//                        linearLayout.getId(),
//                        ConstraintSet.LEFT,
//                        textView_nome.getId(),
//                        ConstraintSet.RIGHT,
//                        5
//                );


//                for (int i = 1; i < bars.length; i++) {
//                    constraintSet.connect(
//                            bars[i].getId(),
//                            ConstraintSet.RIGHT,
//                            bars[i-1].getId(),
//                            ConstraintSet.LEFT,
//                            5
//                    );
//                }

//                constraintSet.connect(
//                        textView_nome.getId(),
//                        ConstraintSet.RIGHT,
//                        linearLayout.getId(),
//                        ConstraintSet.LEFT,
//                        5
//                );

                for (View view : new View[] {textView_nome}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.LEFT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.LEFT,
                            5
                    );
                }
                for (View view : new View[] {textView_nome, linearLayout}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.TOP,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.TOP,
                            5
                    );
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.BOTTOM,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.BOTTOM,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }
        }

        @Override
        void bind(Busca o) {
            {
                textView_nome.setText(o.getNome());
            }
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) onItemClickListener.onItemClick(buscas.get(getAdapterPosition()));
        }
    }
}
