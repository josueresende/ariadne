package br.pasifae.ariadne.pesquisador;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Anuncio;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.CriterioDeRespostaMarcada;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.pesquisador.adapters.Anuncio_RecyclerViewAdapter;
import br.pasifae.ariadne.remote.MensagemService;
import br.pasifae.ariadne.remote.TeseuService;
import retrofit2.Call;
import retrofit2.Response;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class Divulgacao_ListaFragment extends Divulgacao_Fragment {

    LinearLayout linearLayout_1;
    LinearLayout linearLayout_2;
    LinearLayout linearLayout_3;

    RecyclerView recyclerView;

    TextView textView_expiracao;
    TextView textView_caae;
    TextView textView_criterio;
    TextView textView_mensagem;

    Anuncio_RecyclerViewAdapter recyclerViewAdapter;

    private ConfirmacaoFragment confirmacaoDeDelecaoFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
//            {
//                TextView textView = new TextView(viewPager.getContext());
//                linearLayout.addView(textView);
//                textView.setText("ENVIO");
//                textView.setPadding(5, 5, 5, 5);
//            }
//            {
//                View view_space = new View(viewPager.getContext());
//                linearLayout.addView(view_space);
//                view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
//            }
            {
                linearLayout_1 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_1);
                linearLayout_1.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_1.setLayoutParams(layoutParams);
                linearLayout_1.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_1.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter = new Anuncio_RecyclerViewAdapter(viewPager.getContext());
            }
            {
                confirmacaoDeDelecaoFragment = new ConfirmacaoFragment<Anuncio>()
                        .setOnPositive(new ConfirmacaoFragment.OnPositive<Anuncio>() {
                            @Override
                            public void click(Anuncio item) {
                                new DeleteData(item.getIdMensagem(), item.getId())
                                        .execute();
                            }
                        });
                confirmacaoDeDelecaoFragment.setTextoConfirmacao("Confirma a exclusão da Divulgação?");
                confirmacaoDeDelecaoFragment.setTitulo("EXCLUSÃO");
            }
            {
                recyclerView = new RecyclerView(viewPager.getContext());
                linearLayout_1.addView(recyclerView);
                {
                    recyclerView.setId(View.generateViewId());
                    recyclerView.setLayoutManager(
                            new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false)
                    );
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setPadding(5, 5, 5, 5);
                }
                {
                    recyclerViewAdapter.setOnItemClickListener(new Anuncio_RecyclerViewAdapter.OnItemClickListener<Anuncio>() {
                        @Override
                        public void onItemClick(Anuncio item) {

                        }

                        @Override
                        public void onDeleteClick(Anuncio item) {
                            confirmacaoDeDelecaoFragment
                                    .setObject(item)
                                    .show(getFragmentManager(), "CONFIRMAÇÃO");
                        }
                    });
                }
            }
        }
        {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                            new Intent("WAIT_FOR_A_MESSAGE_UPDATE_OFF")
                    );
                    new LoadData().execute();
                }
            }, new IntentFilter("NEW_MESSAGE"));
        }

        return relativeLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadData().execute();
    }

    class DeleteData extends AsyncTask<Void, Void, Void> {

        private Long idMensagem;
        private Long idAnuncio;

        public DeleteData(Long idMensagem, Long idAnuncio) {
            this.idMensagem = idMensagem;
            this.idAnuncio = idAnuncio;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                    new Intent("DASHBOARD_PROGRESSBAR_ON")
            );
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                MensagemService service = TeseuService.retrofit.create(MensagemService.class);
                {
                    Call<MensagemService.Response> call = service.removerAnuncio(idAnuncio);
                    Response<MensagemService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        MensagemService.Response response = executed.body();
                        if (response.error_code == 0) {
                            try {
                                database_user_sqLiteOpenHelper
                                        .getWritableDatabase().delete(
                                        "mensagem ",
                                        "id = ?",
                                        new String[]{ String.valueOf(idMensagem) }
                                );
                                database_user_sqLiteOpenHelper
                                        .getWritableDatabase().delete(
                                        "resposta_buscada",
                                        "id_mensagem_anuncio = ?",
                                        new String[]{ String.valueOf(idAnuncio) }
                                );
                                database_user_sqLiteOpenHelper
                                        .getWritableDatabase().delete(
                                        "mensagem_anuncio",
                                        "id = ?",
                                        new String[]{ String.valueOf(idAnuncio) }
                                );
                                LocalBroadcastManager.getInstance(Ariadne.getAppContext())
                                        .sendBroadcast(new Intent("NEW_MESSAGE"));
                            } catch (Exception e) {

                            }

                        }
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    } else {
                        Logger.getAnonymousLogger().info(executed.errorBody().string());
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        long last_id = 0;
        Map<Long, Anuncio> anuncios = new LinkedHashMap<>();

        @Override
        protected Void doInBackground(Void... voids) {
            {
                Cursor cursor = null;
                try {
                    cursor = database_user_sqLiteOpenHelper
                            .getReadableDatabase()
                            .rawQuery("" +
                                    "SELECT DISTINCT " +
                                    "mensagem_anuncio.id_mensagem AS mensagem_id, " +
                                    "mensagem_anuncio.data_expiracao AS anuncio_dataexpiracao, " +
                                    "mensagem_anuncio.id AS anuncio_id " +
                                    "FROM mensagem_anuncio " +
                                    "ORDER BY mensagem_anuncio.id " +
                                    "", null);
                    while (cursor.moveToNext()) {
                        long id =
                                cursor.getLong(cursor.getColumnIndex("anuncio_id"));
                        long timestampExpiracao =
                                cursor.getLong(cursor.getColumnIndex("anuncio_dataexpiracao"));
                        if (id > last_id) last_id = id;
                        anuncios.put(id, new Anuncio(id, "")
                                .setIdMensagem(cursor.getLong(cursor.getColumnIndex("mensagem_id")))
                                .setDataDeExpiracao(new Date(timestampExpiracao)));
                    }
                } catch (Exception e) {

                } finally {
                    if (cursor != null) cursor.close();
                }
                try {
                    cursor = database_user_sqLiteOpenHelper
                            .getReadableDatabase()
                            .rawQuery("" +
                                    "SELECT DISTINCT " +
                                    "resposta_buscada.texto AS criterio_texto, " +
                                    "resposta_buscada.id_resposta AS resposta_id, " +
                                    "resposta_buscada.id_questao AS questao_id, " +
                                    "resposta_buscada.id_mensagem_anuncio AS anuncio_id, " +
                                    "resposta_buscada.id AS criterio_id " +
                                    "FROM resposta_buscada " +
                                    "ORDER BY resposta_buscada.id " +
                                    "", new String[] { }
                            );
                    while (cursor.moveToNext()) {
                        Long idAnuncio =
                                cursor.getLong(cursor.getColumnIndex("anuncio_id"));
                        if (idAnuncio == null) continue;
                        if (anuncios.containsKey(idAnuncio)) {
                            Long idQuestao = cursor.getLong(cursor.getColumnIndex("questao_id"));
                            Long idResposta = cursor.getLong(cursor.getColumnIndex("resposta_id"));
                            Criterio criterio = new CriterioDeRespostaMarcada();
                            criterio.setId(cursor.getLong(cursor.getColumnIndex("criterio_id")));
                            criterio.setTexto(cursor.getString(cursor.getColumnIndex("criterio_texto")));
                            criterio.setQuestao(Acesso.Database.getQuestaoById(idQuestao));
                            criterio.setResposta(Acesso.Database.getRespostaById(idResposta));
                            anuncios.get(idAnuncio).getCriterios().add(criterio);
                        }
                    }
                } catch (Exception e) {

                } finally {
                    if (cursor != null) cursor.close();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                    new Intent("DASHBOARD_PROGRESSBAR_ON")
            );
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            recyclerViewAdapter.mensagens.clear();
            recyclerViewAdapter.mensagens.addAll(anuncios.values());
            recyclerViewAdapter.notifyDataSetChanged();
            if (Dashboard_PesquisadorActivity.isWaitingForAMessageUpdate()) {
                LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                        new Intent("DASHBOARD_PROGRESSBAR_ON")
                );
            } else {
                LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                        new Intent("DASHBOARD_PROGRESSBAR_OFF")
                );
            }
        }
    }
}