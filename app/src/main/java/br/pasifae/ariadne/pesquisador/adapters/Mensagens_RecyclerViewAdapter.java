package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Mensagem;

public class Mensagens_RecyclerViewAdapter extends RecyclerView.Adapter<Mensagens_RecyclerViewAdapter.ViewHolder> {

    private static final int VIEW_TYPE_ENVIADA = 1;
    private static final int VIEW_TYPE_RECEBIDA = 2;

    final public List<Mensagem> mensagens = new LinkedList<>();
    final private Context mContext;

    private View.OnLongClickListener onLongClickListener;

    public Mensagens_RecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;
        mensagens.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ENVIADA) {
            return new Enviada(new ConstraintLayout(mContext));
        }
        return new Recebida(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Mensagem mensagem = mensagens.get(position);
        holder.bind(mensagem);
    }

    @Override
    public int getItemViewType(int position) {
        Mensagem mensagem = mensagens.get(position);
        long autor = mensagem.getAutor().getId().longValue();
        if (autor == Acesso.usuarioLogadoComSucesso.getId().longValue()) {
            return VIEW_TYPE_ENVIADA;
        }
        return VIEW_TYPE_RECEBIDA;
    }

    @Override
    public int getItemCount() {
        return mensagens.size();
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    public String allText() {
        StringBuilder text = new StringBuilder();
        for (Mensagem mensagem : mensagens) {
            if (text.length() > 0) text.append("\r\n");
            text.append(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(mensagem.getDataDeEnvio()));
            text.append(" ");
            text.append(mensagem.getAutor().getNomeCompleto());
            text.append(" : ");
            text.append(mensagem.getConteudo());
        }
        return text.toString();
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Mensagem mensagem);
    }

    class Recebida extends ViewHolder {
        TextView textView_anuncio;
        TextView textView_nome;
        TextView textView_conteudo;
//        TextView textView_time;

        public Recebida(ConstraintLayout itemView) {
            super(itemView);
            {
                itemView.setLongClickable(true);
                itemView.setOnLongClickListener(onLongClickListener);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                itemView.setPadding(5,5,5,5);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
                itemView.setBackground(gradientDrawable);
            }
            {
                textView_nome = new TextView(mContext);
                textView_nome.setId(View.generateViewId());
                itemView.addView(textView_nome);
            }
            {
                textView_anuncio = new TextView(mContext);
                textView_anuncio.setId(View.generateViewId());
                itemView.addView(textView_anuncio);
                GradientDrawable gradientDrawable = new GradientDrawable();
                textView_anuncio.setBackground(gradientDrawable);
                textView_anuncio.setPadding(10,10,10,10);
            }
            {
                textView_conteudo = new TextView(mContext);
                textView_conteudo.setId(View.generateViewId());
                itemView.addView(textView_conteudo);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(mContext.getResources().getColor(R.color.voluntarioPrimaryLight));
                gradientDrawable.setCornerRadius(10);
                textView_conteudo.setBackground(gradientDrawable);
                textView_conteudo.setPadding(10,10,10,10);
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        textView_nome.getId(),
                        ConstraintSet.TOP,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.TOP,
                        5
                );

                constraintSet.connect(
                        textView_anuncio.getId(),
                        ConstraintSet.LEFT,
                        textView_nome.getId(),
                        ConstraintSet.RIGHT,
                        5
                );

                constraintSet.connect(
                        textView_conteudo.getId(),
                        ConstraintSet.TOP,
                        textView_nome.getId(),
                        ConstraintSet.BOTTOM,
                        5
                );

                for (View view : new View[] { textView_nome, textView_conteudo}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.LEFT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.LEFT,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }
        }

        @Override
        void bind(Mensagem mensagem) {
            textView_nome.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(mensagem.getDataDeEnvio()) + " - " + mensagem.getAutor().getNomeCompleto());
            textView_conteudo.setText(mensagem.getConteudo());
            textView_anuncio.setText("");
            if (mensagem.hasExtra()) {
                StringBuilder stringBuilder = new StringBuilder();
                if (mensagem.hasExtra("CAAE"))
                    stringBuilder.append(String.format("CAAE: %s", mensagem.getExtra("CAAE")));
                if (stringBuilder.length() > 0) textView_anuncio.setText(stringBuilder.toString());
            } else {
                textView_anuncio.setVisibility(View.GONE);
            }
        }
    }

    class Enviada extends ViewHolder {
        TextView textView_anuncio;
        TextView textView_conteudo;
        TextView textView_time;

        public Enviada(ConstraintLayout itemView) {
            super(itemView);
            {
                itemView.setLongClickable(true);
                itemView.setOnLongClickListener(onLongClickListener);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                itemView.setPadding(5,5,5,5);
//                itemView.setBackgroundColor(mContext.getResources().getColor(R.color.voluntarioPrimaryDark));
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
//                gradientDrawable.setStroke(2, Color.LTGRAY);
                itemView.setBackground(gradientDrawable);
            }
            {
                textView_anuncio = new TextView(mContext);
                textView_anuncio.setId(View.generateViewId());
                itemView.addView(textView_anuncio);
                GradientDrawable gradientDrawable = new GradientDrawable();
                textView_anuncio.setBackground(gradientDrawable);
                textView_anuncio.setPadding(10,10,10,10);
            }
            {
                textView_conteudo = new TextView(mContext);
                textView_conteudo.setId(View.generateViewId());
                itemView.addView(textView_conteudo);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(10);
                textView_conteudo.setBackground(gradientDrawable);
                textView_conteudo.setPadding(10,10,10,10);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(mContext.getResources().getColor(R.color.pesquisadorPrimaryLight));
                gradientDrawable.setCornerRadius(25);
                textView_conteudo.setBackground(gradientDrawable);
            }
            {
                textView_time = new TextView(mContext);
                textView_time.setId(View.generateViewId());
                itemView.addView(textView_time);
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        textView_time.getId(),
                        ConstraintSet.TOP,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.TOP,
                        5
                );

                constraintSet.connect(
                        textView_anuncio.getId(),
                        ConstraintSet.RIGHT,
                        textView_time.getId(),
                        ConstraintSet.LEFT,
                        5
                );

                constraintSet.connect(
                        textView_time.getId(),
                        ConstraintSet.BOTTOM,
                        textView_conteudo.getId(),
                        ConstraintSet.TOP,
                        5
                );

                constraintSet.connect(
                        textView_conteudo.getId(),
                        ConstraintSet.BOTTOM,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.BOTTOM,
                        5
                );

                for (View view : new View[] {textView_conteudo, textView_time}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.RIGHT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.RIGHT,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }
        }

        @Override
        void bind(Mensagem mensagem) {
            textView_conteudo.setText(mensagem.getConteudo());
            textView_time.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(mensagem.getDataDeEnvio()));
            textView_anuncio.setText("");
            if (mensagem.hasExtra()) {
                StringBuilder stringBuilder = new StringBuilder();
                if (mensagem.hasExtra("CAAE"))
                    stringBuilder.append(String.format("CAAE: %s", mensagem.getExtra("CAAE")));
                if (stringBuilder.length() > 0) textView_anuncio.setText(stringBuilder.toString());
            }
        }
    }
}
