package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.dados.modelo.Voluntario;

public class Resultados_RecyclerViewAdapter extends RecyclerView.Adapter<Resultados_RecyclerViewAdapter.ViewHolder>  {
    final public List<Voluntario> os = new LinkedList<>();
    final private Context mContext;
    private OnItemClickListener onItemClickListener;

    public void clear() {
        this.os.clear();
        notifyDataSetChanged();
    }

    public void add(Voluntario voluntario) {
        this.os.add(voluntario);
        notifyDataSetChanged();
    }

    static public interface OnItemClickListener {
        void onItemClick(Object item);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public Resultados_RecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;
        os.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Resultado_ViewHolder(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(os.get(position));
    }

    @Override
    public int getItemCount() {
        return os.size();
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Voluntario o);
    }

    public Voluntario getItem(int position) {
        return this.os.get(position);
    }

    class Resultado_ViewHolder extends Resultados_RecyclerViewAdapter.ViewHolder implements View.OnClickListener {
        TextView textView_nome;
        TextView textView_distancia;
        LinearLayout linearLayout;

        public Resultado_ViewHolder(ConstraintLayout itemView) {
            super(itemView);
            {
                itemView.setOnClickListener(this);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                itemView.setPadding(10,10,10,10);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
                itemView.setBackground(gradientDrawable);
            }
            {
                textView_nome = new TextView(mContext);
                textView_nome.setId(View.generateViewId());
                itemView.addView(textView_nome);
//                {
//                    GradientDrawable gradientDrawable = new GradientDrawable();
//                    gradientDrawable.setColor(Color.YELLOW);
//                    gradientDrawable.setCornerRadius(15);
//                    textView_nome.setBackground(gradientDrawable);
//                }
////                textView_nome.setLayoutParams(constraintLayoutParams);
//                textView_nome.setPadding(10,10,10,10);
//            }
//            {
//                ConstraintLayout.LayoutParams constraintLayoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.MATCH_PARENT);
//
//                linearLayout = new LinearLayout(mContext);
//                itemView.addView(linearLayout);
//                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
//                linearLayout.setLayoutParams(constraintLayoutParams);
////                linearLayout.setGravity(Gravity.NO_GRAVITY);
//                linearLayout.setPadding(2,2,2,2);
//                {
//                    GradientDrawable gradientDrawable = new GradientDrawable();
//                    gradientDrawable.setColor(Color.GREEN);
//                    linearLayout.setBackground(gradientDrawable);
//                }
//
//                View[] bars = new View[4];
//                {
//                    LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(2, ViewGroup.LayoutParams.MATCH_PARENT);
//                    for (int i = 0; i < bars.length; i++) {
//                        GradientDrawable gradientDrawable = new GradientDrawable();
//                        gradientDrawable.setColor(Color.GRAY);
//                        bars[i] = new View(mContext);
//                        bars[i].setId(View.generateViewId());
//                        bars[i].setLayoutParams(linearLayoutParams);
//                        bars[i].setBackground(gradientDrawable);
//                        linearLayout.addView(bars[i]);
//                    }
//                }
            }
            {
                textView_distancia = new TextView(mContext);
                textView_distancia.setId(View.generateViewId());
                itemView.addView(textView_distancia);
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        textView_nome.getId(),
                        ConstraintSet.LEFT,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.LEFT,
                        5
                );
                constraintSet.connect(
                        textView_distancia.getId(),
                        ConstraintSet.RIGHT,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.RIGHT,
                        5
                );
//
////                for (int i = 1; i < bars.length; i++) {
////                    constraintSet.connect(
////                            bars[i].getId(),
////                            ConstraintSet.RIGHT,
////                            bars[i-1].getId(),
////                            ConstraintSet.LEFT,
////                            5
////                    );
////                }
//
////                constraintSet.connect(
////                        textView_nome.getId(),
////                        ConstraintSet.RIGHT,
////                        linearLayout.getId(),
////                        ConstraintSet.LEFT,
////                        5
////                );
//
//                for (View view : new View[] {textView_nome}) {
//                    constraintSet.connect(
//                            view.getId(),
//                            ConstraintSet.LEFT,
//                            ConstraintSet.PARENT_ID,
//                            ConstraintSet.LEFT,
//                            5
//                    );
//                }
                for (View view : new View[] {textView_nome, textView_distancia}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.TOP,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.TOP,
                            5
                    );
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.BOTTOM,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.BOTTOM,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }
        }

        @Override
        void bind(Voluntario o) {
            {
                textView_nome.setText(
                        o.getUsuario().getNomeCompleto()
                );
                if (o.getDistancia() == null) {
                    textView_distancia.setText("-");
                } else if (o.getDistancia() < 1D) {
                    textView_distancia.setText(
                            ((new DecimalFormat("0.##")).format(o.getDistanciaEmMetros()) + " m")
                    );
                } else {
                    textView_distancia.setText(
                            ((new DecimalFormat("0.##")).format(o.getDistancia()) + " km")
                    );
                }

            }
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) onItemClickListener.onItemClick(os.get(getAdapterPosition()));
        }
    }
}
