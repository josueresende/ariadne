package br.pasifae.ariadne.pesquisador;

import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

import java.util.LinkedList;
import java.util.List;

abstract public class Pasifae_SpinnerAdapter<T> extends BaseAdapter implements SpinnerAdapter {
    final Context mContext;
    private List<T> mObjects = new LinkedList<>();

    public Pasifae_SpinnerAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return mObjects.get(position);
    }

    public void putItem(T o) {
        this.mObjects.add(o);
        this.notifyDataSetChanged();
    }

    public void reset() {
        this.mObjects.clear();
        this.notifyDataSetChanged();
    }
}
