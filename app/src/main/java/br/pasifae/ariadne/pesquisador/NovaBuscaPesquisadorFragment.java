package br.pasifae.ariadne.pesquisador;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;

/**
 * Created by JosueResende on 25/02/2018.
 */

public class NovaBuscaPesquisadorFragment extends DialogFragment {

    static public interface NovaBuscaDialogListener {
        public void OnUserSelectNovaBuscaPositive(String nome);
        public void OnUserSelectNovaBuscaNegative();
    }

    private Acesso acesso;
    private View layout;
    private NovaBuscaDialogListener listener;
    private ArrayAdapter adapter;
    private EditText nome;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NovaBuscaDialogListener) {
            this.listener = (NovaBuscaDialogListener)context;
        }

    }

    public NovaBuscaPesquisadorFragment setOnNovaBuscaDialogListener(NovaBuscaDialogListener novaBuscaDialogListener) {
        this.listener = novaBuscaDialogListener;
        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        acesso = new Acesso(getActivity());

        layout = inflater.inflate(R.layout.layout_dialog_nova_busca, null);

/*

        adapter = new QuestoesAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item);
//        adapter.addAll(questoes);
        adapter.addAll(acesso.getQuestoes());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ((Spinner)layout.findViewById(R.id.novoCriterioBusca_spinnerQuestoes)).setAdapter(adapter);

// */
        builder.setView(layout)
                // Add action buttons
                .setPositiveButton("CRIAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editText = layout.findViewById(R.id.novaBuscaDialog_editText);
                        if (listener != null) listener.OnUserSelectNovaBuscaPositive(editText.getText().toString());
                    }
                })
                .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (listener != null) listener.OnUserSelectNovaBuscaNegative();
                        NovaBuscaPesquisadorFragment.this.getDialog().cancel();
                    }
                });

        layout.findViewById(R.id.novaBuscaDialog_editText).requestFocus();

        return builder.create();
    }
}
