package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Usuario;

public class Autores_RecyclerViewAdapter  extends RecyclerView.Adapter<Autores_RecyclerViewAdapter.ViewHolder>  {

    final public List<Usuario> autores = new LinkedList<>();
    final private Context mContext;
    private OnItemClickListener onItemClickListener;

    static public interface OnItemClickListener {
        void onItemClick(Object item);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public Autores_RecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;
        autores.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Autoria(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(autores.get(position));
    }

    @Override
    public int getItemCount() {
        return autores.size();
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Usuario usuario);
    }

    class Autoria extends Autores_RecyclerViewAdapter.ViewHolder implements View.OnClickListener {
        TextView textView_nome;
        View view_space;

        public Autoria(ConstraintLayout itemView) {
            super(itemView);
            {
                itemView.setOnClickListener(this);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                view_space = new View(mContext);
                view_space.setId(View.generateViewId());
                itemView.addView(view_space);
                view_space.setLayoutParams(new ConstraintLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
            }
            {
                itemView.setPadding(5,5,5,5);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
                gradientDrawable.setStroke(2, Color.LTGRAY);
                itemView.setBackground(gradientDrawable);
            }
            {
                textView_nome = new TextView(mContext);
                textView_nome.setId(View.generateViewId());
                itemView.addView(textView_nome);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(15);
                textView_nome.setBackground(gradientDrawable);
                textView_nome.setPadding(10,10,10,10);
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        view_space.getId(),
                        ConstraintSet.BOTTOM,
                        textView_nome.getId(),
                        ConstraintSet.TOP,
                        1
                );

                for (View view : new View[] {view_space, textView_nome}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.LEFT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.LEFT,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }
        }

        @Override
        void bind(Usuario usuario) {
            if ("PESQUISADOR".equalsIgnoreCase(usuario.getTipo())) {
                textView_nome.setText(usuario.getNome());
            } else {
                textView_nome.setText(usuario.getNomeCompleto());
            }
//            if (usuario.getId().equals(Acesso.usuarioLogadoComSucesso.getId())) {
//                textView_nome.setTypeface(textView_nome.getTypeface(), Typeface.BOLD);
//            }
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) onItemClickListener.onItemClick(autores.get(getAdapterPosition()));
        }
    }

}
