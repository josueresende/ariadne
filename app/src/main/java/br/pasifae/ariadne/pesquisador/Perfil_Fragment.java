package br.pasifae.ariadne.pesquisador;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;

public class Perfil_Fragment extends Fragment implements Validator.ValidationListener {
    Perfil_FragmentStatePagerAdapter fragmentStatePagerAdapter;
    ViewPager viewPager;

    Acesso acesso;
    ProgressDialog progressDialog;
    Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;

    RelativeLayout relativeLayout;
    LinearLayout linearLayout_underRelative;
    ScrollView scrollView;
    LinearLayout linearLayout;

    Validator validator;
    boolean validated = false;
    @Override
    public void onValidationSucceeded() {
        this.validated = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        this.validated = false;
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(viewPager.getContext());
            // Display error messages
            if (view instanceof Spinner) {
                Spinner sp = (Spinner) view;
                view = ((LinearLayout) sp.getSelectedView()).getChildAt(0);
                // we are actually interested in the text view spinner has
            } else if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }

    }

    public Perfil_Fragment setFragmentStatePagerAdapter(Perfil_FragmentStatePagerAdapter fragmentStatePagerAdapter) {
        this.fragmentStatePagerAdapter = fragmentStatePagerAdapter;
        return this;
    }

    public Perfil_Fragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Aguarde...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
        }

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());
        {
            relativeLayout = new RelativeLayout(container.getContext());
        }
        {
            relativeLayout.setLayoutParams(
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            );
        }
        {
            linearLayout_underRelative = new LinearLayout(container.getContext());
            relativeLayout.addView(linearLayout_underRelative);
            linearLayout_underRelative.setOrientation(LinearLayout.VERTICAL);
            linearLayout_underRelative.setLayoutParams(
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            );

            linearLayout_underRelative.setPadding(0,0,0,100);
            GradientDrawable gradientDrawable = new GradientDrawable();
//            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(Color.WHITE);
            linearLayout_underRelative.setBackground(gradientDrawable);
        }
        {
            TextView textView = new TextView(container.getContext());
            linearLayout_underRelative.addView(textView);
            textView.setText("PERFIL");
            textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.WHITE);
        }
        {
            scrollView = new ScrollView(viewPager.getContext());
            linearLayout_underRelative.addView(scrollView);
            scrollView.setPadding(5, 5, 5, 5);
            scrollView.setFillViewport(true);
        }
        {
            linearLayout = new LinearLayout(container.getContext());
            scrollView.addView(linearLayout);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(
                    new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT)
            );

            GradientDrawable gradientDrawable = new GradientDrawable();
//            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(Color.WHITE);
            linearLayout.setBackground(gradientDrawable);

            linearLayout.setPadding(5,5,15,5);
            scrollView.setScrollbarFadingEnabled(false);
        }
        {
            View view_space = new View(viewPager.getContext());
            relativeLayout.addView(view_space);

            RelativeLayout.LayoutParams layoutParams =
                    new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 50);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            layoutParams.setMargins(5,5,5,5);

            view_space.setLayoutParams(layoutParams);
        }

        return relativeLayout;
    }

}
