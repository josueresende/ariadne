package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Anuncio;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;

public class Anuncio_RecyclerViewAdapter extends RecyclerView.Adapter<Anuncio_RecyclerViewAdapter.ViewHolder> {

    final public List<Anuncio> mensagens = new LinkedList<>();
    final private Context mContext;
    private OnItemClickListener onItemClickListener;

    public Anuncio_RecyclerViewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    static public interface OnItemClickListener<T> {
        void onItemClick(T item);
        void onDeleteClick(T item);
    }

    public void setOnItemClickListener(OnItemClickListener<Anuncio> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Anuncio_ViewHolder(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mensagens.get(position));
    }

    @Override
    public int getItemCount() {
        return mensagens.size();
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Anuncio mensagem);
    }

    class Anuncio_ViewHolder extends ViewHolder {

        TextView textView_dataDeExpiracao;
        TextView textView_criterios;
        TextView textView_extras;
        TextView textView_mensagem;

        LinearLayout linearLayout;

        @Override
        void bind(Anuncio mensagem) {
            textView_dataDeExpiracao.setText(
                    "EXPIRA EM: " + new SimpleDateFormat("dd/MM/yyyy").format(mensagem.getDataDeExpiracao())
            );
            if (mensagem.getCriterios() != null) {
                Map<Long, Questao> questoes = new LinkedHashMap<>();
                Map<Long, List<String>> respostas = new LinkedHashMap<>();
                //
                for (Criterio criterio : mensagem.getCriterios()) {
                    Questao questao = criterio.getQuestao();
                    Resposta resposta = criterio.getResposta();
                    if (respostas.containsKey(questao.getId()) == false) {
                        respostas.put(questao.getId(), new LinkedList<String>());
                    }
                    if (questoes.containsKey(questao.getId()) == false) {
                        questoes.put(questao.getId(), questao);
                    }
                    //
                    resposta.setResposta(criterio.getTexto());
                    //
                    Resposta.FromJSON jsonObject = resposta.getRespostaAsJSON();
                    Resposta.Conteudo conteudo = resposta.getConteudo();
                    //
                    if (Resposta.Conteudo.DATA.equals(conteudo) && jsonObject != null) {
                        respostas.get(questao.getId()).add(String.format("ENTRE %d E %d ANOS",
                            jsonObject.min,
                            jsonObject.max
                        ));
                    } else if (Resposta.Conteudo.DURACAO_MESES.equals(conteudo) && jsonObject != null) {
                        respostas.get(questao.getId()).add(String.format("ENTRE %d E %d MESES",
                            jsonObject.min,
                            jsonObject.max
                        ));
                    } else {
                        respostas.get(questao.getId()).add(" = " + resposta.getResposta());
                    }
                }
                StringBuilder stringBuilder = new StringBuilder();
                for (Questao questao : questoes.values()) {
                    if (stringBuilder.length() > 0) stringBuilder.append("\n");
                    stringBuilder.append(
                            questao.getTituloParaPesquisador() + " " +
                            TextUtils.join(",", respostas.get(questao.getId()))
                    );
                }
                textView_criterios.setText(stringBuilder.toString());
            }
        }

        public Anuncio_ViewHolder(ConstraintLayout itemView) {
            super(itemView);
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                itemView.setPadding(5,5,5,5);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
                gradientDrawable.setStroke(2, Color.LTGRAY);
                itemView.setBackground(gradientDrawable);
            }
            {
                textView_dataDeExpiracao = new TextView(mContext);
                textView_dataDeExpiracao.setId(View.generateViewId());
                itemView.addView(textView_dataDeExpiracao);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(15);
                textView_dataDeExpiracao.setBackground(gradientDrawable);
                textView_dataDeExpiracao.setPadding(10,10,10,10);
            }
            {
                textView_criterios = new TextView(mContext);
                textView_criterios.setId(View.generateViewId());
                itemView.addView(textView_criterios);
                textView_criterios.setPadding(10,10,10,10);
                textView_criterios.setTextSize(14);
            }
            {
                ConstraintLayout.LayoutParams constraintLayoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.MATCH_PARENT);

                linearLayout = new LinearLayout(mContext);
                itemView.addView(linearLayout);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.setLayoutParams(constraintLayoutParams);
                linearLayout.setGravity(Gravity.RIGHT);
                linearLayout.setPadding(0,0,0,0);
                {
                    ImageButton imageButton = new ImageButton(mContext);
                    imageButton.setId(View.generateViewId());
                    linearLayout.addView(imageButton);
                    imageButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_remove_24px));
                    imageButton.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    imageButton.setColorFilter(Color.WHITE);

                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setStroke(2, Color.TRANSPARENT);
                    gradientDrawable.setColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                    gradientDrawable.setCornerRadius(60);
                    imageButton.setBackground(gradientDrawable);

                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onDeleteClick(mensagens.get(getAdapterPosition()));
                            }
                        }
                    });
                }
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        linearLayout.getId(),
                        ConstraintSet.RIGHT,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.RIGHT,
                        5
                );

                constraintSet.connect(
                        linearLayout.getId(),
                        ConstraintSet.TOP,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.TOP,
                        5
                );

                constraintSet.connect(
                        textView_dataDeExpiracao.getId(),
                        ConstraintSet.TOP,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.TOP,
                        5
                );

                constraintSet.connect(
                        textView_criterios.getId(),
                        ConstraintSet.TOP,
                        textView_dataDeExpiracao.getId(),
                        ConstraintSet.BOTTOM,
                        5
                );

                for (View view : new View[] {textView_dataDeExpiracao, textView_criterios}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.LEFT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.LEFT,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }
        }

    }
}
