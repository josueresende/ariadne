package br.pasifae.ariadne.pesquisador;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.pesquisador.adapters.Autores_RecyclerViewAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by JosueResende on 15/04/2018.
 */

public class Conversas_ListaFragment extends Fragment {

    private Conversas_FragmentStatePagerAdapter fragmentStatePagerAdapter;
    private ViewPager viewPager;
    private Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;
    private Autores_RecyclerViewAdapter recyclerViewAdapter;

    ConstraintLayout constraintLayout;
    TextView textView_titulo;
    RecyclerView recyclerView_mensagens;

    public Conversas_ListaFragment setFragmentStatePagerAdapter(Conversas_FragmentStatePagerAdapter fragmentStatePagerAdapter) {
        this.fragmentStatePagerAdapter = fragmentStatePagerAdapter;
        return this;
    }

    public Conversas_ListaFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        private int updated = 0;

        @Override
        protected Void doInBackground(Void... voids) {
            recyclerViewAdapter.autores.clear();
            {
                Cursor cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "mensagem.tp_usuario AS autor_tipo, " +
                                "mensagem.nm_usuario AS autor_nome, " +
                                "mensagem.id_usuario AS autor_id " +
                                "FROM mensagem " +
                                "LEFT JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id " +
                                "WHERE mensagem_direta.id IS NULL " +
                                "ORDER BY mensagem.nm_usuario " +
                                "", null);
                while (cursor.moveToNext()) {
                    updated++;
                    long idAutor =
                            cursor.getLong(cursor.getColumnIndex("autor_id"));
                    String nomeAutor =
                            cursor.getString(cursor.getColumnIndex("autor_nome"));
                    String tipoAutor =
                            cursor.getString(cursor.getColumnIndex("autor_tipo"));

                    Usuario autor = new Usuario(idAutor, nomeAutor).setTipo(tipoAutor);

                    recyclerViewAdapter.autores.add(autor);
                }
                cursor.close();
            }
            {
                Long id = Acesso.usuarioLogadoComSucesso.getId();
                Cursor cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "mensagem_direta.tp_usuario AS leitor_tipo, " +
                                "mensagem_direta.nm_usuario AS leitor_nome, " +
                                "mensagem_direta.id_usuario AS leitor_id " +
                                "FROM mensagem " +
                                "INNER JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id " +
                                "WHERE " + (String.format("mensagem.id_usuario = %d", id)) + " " +
                                "ORDER BY mensagem_direta.nm_usuario " +
                                "", null);
                while (cursor.moveToNext()) {
                    updated++;
                    long idAutor =
                            cursor.getLong(cursor.getColumnIndex("leitor_id"));
                    String nomeAutor =
                            cursor.getString(cursor.getColumnIndex("leitor_nome"));
                    String tipoAutor =
                            cursor.getString(cursor.getColumnIndex("leitor_tipo"));

                    Usuario autor = new Usuario(idAutor, nomeAutor).setTipo(tipoAutor);

                    recyclerViewAdapter.autores.add(autor);
                }
                cursor.close();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (updated > 0) {
                recyclerViewAdapter.notifyDataSetChanged();
                recyclerView_mensagens.scrollToPosition(0);
            }
            if (Acesso.Tab.dadosDaMensagem != null) {
                Usuario usuario = new Usuario(Acesso.Tab.dadosDaMensagem.autor_id, "");
                Acesso.Tab.dadosDaMensagem = null;
                fragmentStatePagerAdapter.paginaDeMensagens(usuario);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        {
            // https://blog.sendbird.com/android-chat-tutorial-building-a-messaging-ui
            constraintLayout = new ConstraintLayout(viewPager.getContext());

            constraintLayout.setId(View.generateViewId());
            constraintLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
            constraintLayout.setPadding(5,5,5,5);
            constraintLayout.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
        }
        {
            recyclerView_mensagens = new RecyclerView(viewPager.getContext());
            recyclerView_mensagens.setId(View.generateViewId());
            constraintLayout.addView(recyclerView_mensagens);
            recyclerView_mensagens.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.MATCH_CONSTRAINT));

            {
                recyclerView_mensagens.setLayoutManager(
                        new LinearLayoutManager(viewPager.getContext(), LinearLayoutManager.VERTICAL, false)
                );
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
//                gradientDrawable.setCornerRadius(30);
                recyclerView_mensagens.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter = new Autores_RecyclerViewAdapter(viewPager.getContext());
                recyclerView_mensagens.setAdapter(recyclerViewAdapter);
            }
            {
                recyclerView_mensagens.addItemDecoration(new DividerItemDecoration(viewPager.getContext(), DividerItemDecoration.VERTICAL));
            }
            {
                recyclerView_mensagens.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView_mensagens.scrollToPosition(recyclerView_mensagens.getAdapter().getItemCount() - 1);
                    }
                });
            }
            {
                recyclerViewAdapter.setOnItemClickListener(new Autores_RecyclerViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Object item) {
                        if (item instanceof Usuario) {
                            fragmentStatePagerAdapter.notifyDataSetChanged();
                            fragmentStatePagerAdapter.paginaDeMensagens((Usuario) item);
                        }
                    }
                });
            }
        }
        {
            textView_titulo = new TextView(viewPager.getContext());
            textView_titulo.setId(View.generateViewId());
            constraintLayout.addView(textView_titulo);
            textView_titulo.setText("ANÚNCIOS E CONVERSAS");
            textView_titulo.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView_titulo.setTypeface(null, Typeface.BOLD);
            textView_titulo.setPadding(5,5,5,5);
            textView_titulo.setTextColor(Color.WHITE);
        }
/**************************************************************************************************/
        {
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);

            constraintSet.connect(
                    textView_titulo.getId(),
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP,
                    5
            );

            constraintSet.connect(
                    recyclerView_mensagens.getId(),
                    ConstraintSet.BOTTOM,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.BOTTOM,
                    5
            );

            constraintSet.connect(
                    recyclerView_mensagens.getId(),
                    ConstraintSet.TOP,
                    textView_titulo.getId(),
                    ConstraintSet.BOTTOM,
                    5
            );

            for (Integer pos : new Integer[] { ConstraintSet.LEFT, ConstraintSet.RIGHT }) {
                constraintSet.connect(
                        textView_titulo.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
                constraintSet.connect(
                        recyclerView_mensagens.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
            }

            constraintSet.constrainWidth(textView_titulo.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
            constraintSet.constrainWidth(recyclerView_mensagens.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);

            constraintSet.applyTo(constraintLayout);
        }
        {
            recyclerView_mensagens.setVerticalScrollBarEnabled(true);
            recyclerView_mensagens.scrollToPosition(recyclerView_mensagens.getAdapter().getItemCount() - 1);
        }
        {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    new LoadData().execute();
                }
            }, new IntentFilter("NEW_MESSAGE"));
        }
        return constraintLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        {
            new LoadData().execute();
        }
    }
}
