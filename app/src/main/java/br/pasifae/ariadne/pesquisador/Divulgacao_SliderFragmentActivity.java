package br.pasifae.ariadne.pesquisador;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Mock;
import br.pasifae.ariadne.dados.modelo.Usuario;

/**
 * Created by JosueResende on 16/04/2018.
 */

public class Divulgacao_SliderFragmentActivity extends FragmentActivity {
    ViewPager viewPager;
    Divulgacao_FragmentStatePagerAdapter pagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        {   /** ATUA EM CONJUNTO COM Main_Activity.onNewIntent **/
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    nm.cancel(R.string.app_name);
                    finish();
                }
            }, new IntentFilter("LOGOUT"));
        }
        LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String tab = intent.getStringExtra("TAB");
                if ("DIVULGAR".equalsIgnoreCase(tab)) pagerAdapter.reset();
            }
        }, new IntentFilter("TAB_CHANGE"));

        viewPager = new ViewPager(this);
        pagerAdapter = new Divulgacao_FragmentStatePagerAdapter(getSupportFragmentManager(), viewPager);
        viewPager.setId(View.generateViewId());
        viewPager.setAdapter(pagerAdapter);
        setContentView(viewPager);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        } else {
            getParent().onBackPressed();
        }
    }
}
