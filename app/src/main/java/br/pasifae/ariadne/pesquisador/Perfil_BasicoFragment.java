package br.pasifae.ariadne.pesquisador;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.logging.Logger;

import br.com.receitasdecodigo.utils.MaskEditUtil;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.VoluntarioService;
import retrofit2.Call;
import retrofit2.Response;

public class Perfil_BasicoFragment extends Perfil_Fragment {

    @NotEmpty(message = "Preencha seu nome")
    private EditText editText_nome;

//    private EditText editText_sobrenome;

    @Email(message = "Digite um e-mail válido")
    private EditText editText_email;

    private EditText editText_telefone;

    private Spinner spinner_idioma;

    private Resposta_SpinnerAdapter resposta_spinnerAdapter;

    class Resposta_SpinnerAdapter extends Pasifae_SpinnerAdapter<Resposta> {

        public Resposta_SpinnerAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public long getItemId(int position) {
            Object resposta = getItem(position);
            if (resposta != null && resposta instanceof Resposta) return ((Resposta)resposta).getId();
            return -1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout linearLayout = new LinearLayout(mContext);
            {
                Resposta resposta = (Resposta) getItem(position);
                TextView textView = new TextView(mContext);
                linearLayout.addView(textView);
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView.setText(resposta.getResposta());
            }
            return linearLayout;
        }

    }

    class UpdateProfile extends AsyncTask<Void, Void, Void> {

        Questao questaoNome;
//        Questao questaoSobrenome;
        Questao questaoIdioma;
        Questao questaoEmail;
        Questao questaoTelefone;

        String nome;
        String sobrenome;
        String email;
        String telefone;

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Atualizando...");
            progressDialog.show();
            questaoNome =
                    acesso.getQuestao("NOME");
//            questaoSobrenome =
//                    acesso.getQuestao("SOBRENOME");
            questaoIdioma =
                    acesso.getQuestao("IDIOMA");
            questaoEmail =
                    acesso.getQuestao("EMAIL");
            questaoTelefone =
                    acesso.getQuestao("TELEFONE");

            //
            nome =
                    editText_nome.getText().toString();
//            sobrenome =
//                    editText_sobrenome.getText().toString();
            email =
                    editText_email.getText().toString();
            telefone =
                    editText_telefone.getText().toString();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            fragmentStatePagerAdapter.before();
            fragmentStatePagerAdapter.reset();
            progressDialog.dismiss();
        }

        private void updateNome() {
            try {
                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
                Call<VoluntarioService.Response> call =
                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
                                questaoNome.getId(),
                                questaoNome.getRespostas().get(0).getId(),
                                nome
                        );
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    Acesso.usuarioLogadoComSucesso.setNome(nome);
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

//        private void updateSobrenome() {
//            try {
//                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
//                Call<VoluntarioService.Response> call =
//                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
//                                questaoSobrenome.getId(),
//                                questaoSobrenome.getRespostas().get(0).getId(),
//                                sobrenome
//                        );
//                Response<VoluntarioService.Response> executed = call.execute();
//                if (executed.isSuccessful()) {
//                    VoluntarioService.Response response = executed.body();
//                    Acesso.usuarioLogadoComSucesso.setSobrenome(sobrenome);
//                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
//                } else {
//                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
//                }
//            } catch(Exception e) {
//                e.printStackTrace();
//            }
//        }

        private void updateEmail() {
            try {
                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
                Call<VoluntarioService.Response> call =
                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
                                questaoEmail.getId(),
                                questaoEmail.getRespostas().get(0).getId(),
                                email
                        );
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    Acesso.usuarioLogadoComSucesso.email.principal = email;
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        private void updateTelefone() {
            try {
                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
                Call<VoluntarioService.Response> call =
                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
                                questaoTelefone.getId(),
                                questaoTelefone.getRespostas().get(0).getId(),
                                telefone
                        );
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    Acesso.usuarioLogadoComSucesso.telefone.principal = telefone;
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        private void updateIdioma() {
            try {
                Resposta resposta = (Resposta) spinner_idioma.getSelectedItem();
                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
                Call<VoluntarioService.Response> call =
                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
                                questaoIdioma.getId(),
                                resposta.getId(),
                                ""
                        );
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    Acesso.usuarioLogadoComSucesso.setIdioma(resposta.getTag());
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... os) {
            updateNome();
//            updateSobrenome();
            updateIdioma();
            updateEmail();
            updateTelefone();
            return null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            validator = new Validator(this);
            validator.setValidationListener(this);
        }
        {
            String nome = Acesso.usuarioLogadoComSucesso.getNome();
//            String sobrenome = Acesso.usuarioLogadoComSucesso.getSobrenome();
            String email = Acesso.usuarioLogadoComSucesso.email.principal;
            String telefone = Acesso.usuarioLogadoComSucesso.telefone.principal;
            String idioma = Acesso.usuarioLogadoComSucesso.getIdioma();
            {
                if (nome == null) nome = "";
//                if (sobrenome == null) sobrenome = "";
                if (email == null) email = "";
                if (telefone == null) telefone = "";
                if (idioma == null)  idioma = "";
            }
//            {
//                scrollView = new ScrollView(viewPager.getContext());
//                linearLayout.addView(scrollView);
//                scrollView.setPadding(5, 5, 5, 5);
//            }
//            {
//                linearLayout_1 = new LinearLayout(viewPager.getContext());
//                scrollView.addView(linearLayout_1);
//                linearLayout_1.setPadding(10, 10, 10, 10);
//                linearLayout_1.setOrientation(LinearLayout.VERTICAL);
//            }
//            {
//                GradientDrawable gradientDrawable = new GradientDrawable();
//                gradientDrawable.setColor(Color.WHITE);
//                gradientDrawable.setCornerRadius(20);
//                linearLayout_1.setBackground(gradientDrawable);
//            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("INFORMAÇÕES BÁSICAS");
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("NOME COMPLETO");
                textView.setPadding(5, 5, 5, 5);

                editText_nome = new EditText(viewPager.getContext());
                linearLayout.addView(editText_nome);
                editText_nome.setText(nome);
            }
//            {
//                TextView textView = new TextView(viewPager.getContext());
//                linearLayout.addView(textView);
//                textView.setText("SOBRENOME");
//                textView.setPadding(5, 5, 5, 5);
//
//                editText_sobrenome = new EditText(viewPager.getContext());
//                linearLayout.addView(editText_sobrenome);
//                editText_sobrenome.setText(sobrenome);
//            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("E-MAIL");
                textView.setPadding(5, 5, 5, 5);

                editText_email = new EditText(viewPager.getContext());
                linearLayout.addView(editText_email);
                editText_email.setText(email);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("TELEFONE");
                textView.setPadding(5, 5, 5, 5);

                editText_telefone = new EditText(viewPager.getContext());
                linearLayout.addView(editText_telefone);
                editText_telefone.setText(telefone);
                editText_telefone.setInputType(InputType.TYPE_CLASS_NUMBER);
                editText_telefone.addTextChangedListener(MaskEditUtil.mask(editText_telefone, MaskEditUtil.FORMAT_FONE));
            }
            {
                resposta_spinnerAdapter = new Resposta_SpinnerAdapter(viewPager.getContext());

                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("IDIOMA");
                textView.setPadding(5, 5, 5, 5);

                spinner_idioma = new Spinner(viewPager.getContext());
                linearLayout.addView(spinner_idioma);
                spinner_idioma.setAdapter(resposta_spinnerAdapter);

                Questao questao = acesso.getQuestao("IDIOMA");
                if (questao != null) {
                    int position = 0;
                    for (Resposta resposta : questao.getRespostas()) {
                        resposta_spinnerAdapter.putItem(resposta);
                        if (idioma.equalsIgnoreCase(resposta.getTag())) spinner_idioma.setSelection(position);
                        position++;
                    }
                }
            }
            {
                View view_space = new View(viewPager.getContext());
                linearLayout.addView(view_space);
                view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 40));
            }
            {
                //
            }
        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_archive_24px);
                floatingActionButton.setElevation(2);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10,10,10,10);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimary));
                floatingActionButton.setImageTintList(getResources().getColorStateList(android.R.color.white));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (validator != null) validator.validate();
                        if (validated) new UpdateProfile().execute();
                    }
                });
            }

        }
        return relativeLayout;
    }

}
