package br.pasifae.ariadne.pesquisador;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import br.pasifae.android.support.v4.view.Pasifae_FragmentStatePageAdapter;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Anuncio;

/**
 * Created by JosueResende on 16/04/2018.
 */

public class Divulgacao_FragmentStatePagerAdapter extends Pasifae_FragmentStatePageAdapter {

    public Anuncio anuncio;

    enum FRAGMENT_NAME {
        LISTA, DATA_DE_EXPIRACAO, CRITERIOS, MENSAGEM, ENVIO
    }

    FRAGMENT_NAME[] index = new FRAGMENT_NAME[10];

    ViewPager viewPager;

    public Divulgacao_FragmentStatePagerAdapter(FragmentManager fragmentManager, ViewPager viewPager) {
        super(fragmentManager);
        this.viewPager = viewPager;
        this.viewPager.setOffscreenPageLimit(0);
        this.reset();
    }

    public void reset() {
        this.anuncio = new Anuncio();
        this.index = new FRAGMENT_NAME[10];
        this.viewPager.setCurrentItem(0, true);
        this.index[0] = FRAGMENT_NAME.LISTA;
        notifyDataSetChanged();
        viewPager.setCurrentItem(0, true);
        viewPager.refreshDrawableState();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        Divulgacao_Fragment fragment = null;
        if (FRAGMENT_NAME.DATA_DE_EXPIRACAO.equals(index[position])) {
            if (anuncio == null) anuncio = new Anuncio();
            fragment = new Divulgacao_DataDeExpiracaoFragment();
        } else if (FRAGMENT_NAME.CRITERIOS.equals(index[position])) {
            fragment = new Divulgacao_CriteriosFragment();
        } else if (FRAGMENT_NAME.MENSAGEM.equals(index[position])) {
            fragment = new Divulgacao_MensagemFragment();
        } else if (FRAGMENT_NAME.ENVIO.equals(index[position])) {
            fragment = new Divulgacao_EnvioFragment();
        } else if (FRAGMENT_NAME.LISTA.equals(index[position])) {
            fragment = new Divulgacao_ListaFragment();
        }
        if (fragment != null) {
            fragment.setFragmentStatePagerAdapter(this)
                    .setViewPager(viewPager)
                    .setAnuncio(anuncio)
            ;
            final int inside_position = position;
            final Divulgacao_Fragment inside_fragment = fragment;
            fragment.onCreateView = new Divulgacao_Fragment.OnCreateView() {
                @Override
                public void Before(RelativeLayout relativeLayout) { }

                @Override
                public void After(RelativeLayout relativeLayout) {
                    if (FRAGMENT_NAME.LISTA.equals(index[inside_position]) == false)
                    {
                        FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
                        inside_fragment.relativeLayout.addView(floatingActionButton);

                        {
                            floatingActionButton.setId(View.generateViewId());

                            floatingActionButton.setImageResource(R.drawable.ic_baseline_navigate_before_24px);
                            floatingActionButton.setElevation(2);

                            floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                            floatingActionButton.setFocusable(true);

                            RelativeLayout.LayoutParams layoutParams =
                                    new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            layoutParams.setMargins(10,10,10,10);

                            floatingActionButton.setBackgroundTintList(inside_fragment.getResources().getColorStateList(R.color.pesquisadorPrimary));
                            floatingActionButton.setImageTintList(inside_fragment.getResources().getColorStateList(android.R.color.white));

                            floatingActionButton.setLayoutParams(layoutParams);
                        }
                        {
                            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    inside_fragment.navigateToLeft(view);
                                    before();
                                }
                            });
                        }
                    }
                    if (FRAGMENT_NAME.ENVIO.equals(index[inside_position]) == false)
                    {
                        FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
                        inside_fragment.relativeLayout.addView(floatingActionButton);

                        {
                            floatingActionButton.setId(View.generateViewId());

                            if (FRAGMENT_NAME.LISTA.equals(index[inside_position])) {
                                floatingActionButton.setImageResource(R.drawable.ic_baseline_add_24px);
                            } else {
                                floatingActionButton.setImageResource(R.drawable.ic_baseline_navigate_next_24px);
                            }
                            floatingActionButton.setElevation(2);

                            floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                            floatingActionButton.setFocusable(true);

                            RelativeLayout.LayoutParams layoutParams =
                                    new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                            layoutParams.setMargins(10,10,10,10);

                            floatingActionButton.setBackgroundTintList(inside_fragment.getResources().getColorStateList(R.color.pesquisadorPrimary));
                            floatingActionButton.setImageTintList(inside_fragment.getResources().getColorStateList(android.R.color.white));

                            floatingActionButton.setLayoutParams(layoutParams);
                        }
                        {
                            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    inside_fragment.navigateToRight(view);
                                    next();
                                }
                            });
                        }
                    }

                }
            };

        }
        return fragment;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (; count < index.length; count++) {
            if (index[count] == null) break;
        }
        return count;
    }

    public void next() {
        int x =  viewPager.getCurrentItem() + 1;
        if (x >= FRAGMENT_NAME.values().length) {
            return;
        }
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.values()[x];
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

    public void before() {
        int x =  viewPager.getCurrentItem() - 1;
        if (x < 0) {
            return;
        }
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.values()[x];
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

}
