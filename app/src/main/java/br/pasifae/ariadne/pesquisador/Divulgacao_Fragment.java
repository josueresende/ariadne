package br.pasifae.ariadne.pesquisador;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Anuncio;

public class Divulgacao_Fragment  extends Fragment {
    Divulgacao_FragmentStatePagerAdapter fragmentStatePagerAdapter;
    ViewPager viewPager;

    Acesso acesso;
//    ProgressDialog progressDialog;
    Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;

    RelativeLayout relativeLayout;
    LinearLayout linearLayout_underRelative;
    ScrollView scrollView;
    LinearLayout linearLayout;

    private Anuncio anuncio;

    protected boolean setaEsquerda = false;
    protected boolean setaDireita  = false;

    public OnCreateView onCreateView = new OnCreateView() {
        @Override
        public void Before(RelativeLayout relativeLayout) { }
        @Override
        public void After(RelativeLayout relativeLayout) { }
    };

    public interface OnCreateView {
        public void Before(RelativeLayout relativeLayout);
        public void After(RelativeLayout relativeLayout);
    }

    public Divulgacao_Fragment setFragmentStatePagerAdapter(Divulgacao_FragmentStatePagerAdapter fragmentStatePagerAdapter) {
        this.fragmentStatePagerAdapter = fragmentStatePagerAdapter;
        return this;
    }

    public Divulgacao_Fragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    public Divulgacao_Fragment setAnuncio(Anuncio anuncio) {
        this.anuncio = anuncio;
        return this;
    }

    static
    public
    class ConfirmacaoFragment<T> extends DialogFragment {

        LinearLayout linearLayout;
        private String title = "CONFIRMAÇÃO";
        private String textoConfirmacao = "Confirma ?";

        private T object;

        ConfirmacaoFragment.OnPositive<T> onPositive;
        ConfirmacaoFragment.OnNegative<T> onNegative;

        public void setTextoConfirmacao(String textoConfirmacao) {
            this.textoConfirmacao = textoConfirmacao;
        }

        public void setTitulo(String title) {
            this.title = title;
        }

        static public interface OnPositive<T> {
            public void click(T o);
        };

        static public interface OnNegative<T> {
            public void click(T o);
        };

        public ConfirmacaoFragment setOnPositive(ConfirmacaoFragment.OnPositive<T> onPositive) {
            this.onPositive = onPositive;
            return this;
        }

        public ConfirmacaoFragment setOnNegative(ConfirmacaoFragment.OnNegative<T> onNegative) {
            this.onNegative = onNegative;
            return this;
        }

        public ConfirmacaoFragment setObject(T o) {
            this.object = o;
            return this;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText(title);
                textView.setTextSize(21);
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimaryDark));
                textView.setPadding(10,10,10,10);
            }
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText(textoConfirmacao);
                textView.setTextSize(20);
                textView.setPadding(20,20,20,20);
            }

            builder.setView(linearLayout)
                    .setPositiveButton("CONFIRMA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (onPositive != null) onPositive.click(object);
                        }
                    })
                    .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (onNegative != null) onNegative.click(object);
                        }
                    });

            return builder.create();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        {
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Aguarde...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//        }

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());
        {
            relativeLayout = new RelativeLayout(container.getContext());
            onCreateView.Before(relativeLayout);
        }
        {
//            GradientDrawable gradientDrawable = new GradientDrawable();
//            gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimary));
//            relativeLayout.setBackground(gradientDrawable);
            relativeLayout.setLayoutParams(
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            );
//            relativeLayout.setPadding(10,10,10,10);
        }
        {
            linearLayout_underRelative = new LinearLayout(container.getContext());
            relativeLayout.addView(linearLayout_underRelative);
            linearLayout_underRelative.setOrientation(LinearLayout.VERTICAL);
            linearLayout_underRelative.setLayoutParams(
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            );

            linearLayout_underRelative.setPadding(0,0,0,100);
            GradientDrawable gradientDrawable = new GradientDrawable();
//            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(Color.WHITE);
            linearLayout_underRelative.setBackground(gradientDrawable);
        }
        {
            TextView textView = new TextView(container.getContext());
            linearLayout_underRelative.addView(textView);
            textView.setText("DIVULGAÇÃO");
            textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.WHITE);
        }
        {
            scrollView = new ScrollView(viewPager.getContext());
            linearLayout_underRelative.addView(scrollView);
            scrollView.setLayoutParams(
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            );
            scrollView.setPadding(5, 5, 5, 5);
            scrollView.setFillViewport(true);

        }
        {
            linearLayout = new LinearLayout(container.getContext());
            scrollView.addView(linearLayout);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(
                    new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT)
            );
            linearLayout.setPadding(10,10,10,100);
            GradientDrawable gradientDrawable = new GradientDrawable();
//            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(Color.WHITE);
//            gradientDrawable.setCornerRadius(15);
            linearLayout.setBackground(gradientDrawable);
        }
        {
            onCreateView.After(relativeLayout);
        }
        return relativeLayout;
    }

    public void navigateToLeft(View view) { }

    public void navigateToRight(View view) { }
}
