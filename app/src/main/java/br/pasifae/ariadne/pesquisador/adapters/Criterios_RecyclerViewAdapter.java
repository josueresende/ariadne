package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collection;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.CriterioDeRespostaMarcada;
import br.pasifae.ariadne.dados.modelo.Resposta;

public class Criterios_RecyclerViewAdapter extends RecyclerView.Adapter<Criterios_RecyclerViewAdapter.ViewHolder>  {

    final private Context mContext;
    private Busca busca;
    private OnItemClickListener onItemClickListener;

    public Criterios_RecyclerViewAdapter(Context context, Busca busca) {
        this.mContext = context;
        this.busca = busca;
    }

    public void add(Resposta resposta) {
        this.busca.getRespostas().add(resposta);
        notifyDataSetChanged();
    }

    public void clear() {
        this.busca.getRespostas().clear();
    }

    static public interface OnItemClickListener<T> {
        void onItemClick(T item);
        void onDeleteClick(T item);
    }

    public void setOnItemClickListener(OnItemClickListener<Criterio> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Resposta o);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Criterio_ViewHolder(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(busca.getRespostas().get(position));
    }

    @Override
    public int getItemCount() {
        return busca.getRespostas().size();
    }

    public Resposta getResposta(int position) {
        return busca.getRespostas().get(position);
    }

    public Criterio getCriterio(int position) {
        Criterio criterio = null;
        {
            try {
            Resposta resposta = getResposta(position);
            criterio = new CriterioDeRespostaMarcada();
            criterio.setQuestao(resposta.getQuestao());
            criterio.setResposta(resposta);
            criterio.setPesquisador(busca.getPesquisador());
            criterio.setBusca(busca);
            criterio.setTexto("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return criterio;
    }

    class Criterio_ViewHolder extends Criterios_RecyclerViewAdapter.ViewHolder implements View.OnClickListener {

        TextView textView_nome;
        LinearLayout linearLayout;

        public Criterio_ViewHolder(ConstraintLayout itemView) {
            super(itemView);
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(25);
                gradientDrawable.setStroke(2, Color.LTGRAY);
                itemView.setBackground(gradientDrawable);

                itemView.setOnClickListener(this);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
                itemView.setPadding(10,10,10,10);
            }
            {
                textView_nome = new TextView(itemView.getContext());
                textView_nome.setId(View.generateViewId());
                textView_nome.setTextSize(18);
                itemView.addView(textView_nome);
            }
            {
                ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
                textView_nome.setLayoutParams(layoutParams);
            }
            {
                ConstraintLayout.LayoutParams constraintLayoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.MATCH_PARENT);

                linearLayout = new LinearLayout(mContext);
                itemView.addView(linearLayout);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.setLayoutParams(constraintLayoutParams);
                linearLayout.setGravity(Gravity.RIGHT);
                linearLayout.setPadding(0,0,0,0);
                {
                    ImageButton imageButton = new ImageButton(mContext);
                    imageButton.setId(View.generateViewId());
                    linearLayout.addView(imageButton);
                    imageButton.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_baseline_remove_24px));
                    imageButton.setLayoutParams(new LinearLayout.LayoutParams(100,100));
                    imageButton.setColorFilter(Color.WHITE);

                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setStroke(2, Color.TRANSPARENT);
                    gradientDrawable.setColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
                    gradientDrawable.setCornerRadius(60);
                    imageButton.setBackground(gradientDrawable);

                    imageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemClickListener != null) onItemClickListener.onDeleteClick(getCriterio(getAdapterPosition()));
                        }
                    });
                }
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                constraintSet.connect(
                        textView_nome.getId(),
                        ConstraintSet.RIGHT,
                        linearLayout.getId(),
                        ConstraintSet.LEFT,
                        5
                );
                constraintSet.connect(
                        linearLayout.getId(),
                        ConstraintSet.RIGHT,
                        ConstraintSet.PARENT_ID,
                        ConstraintSet.RIGHT,
                        5
                );

                for (View view : new View[] {textView_nome}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.RIGHT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.RIGHT,
                            5
                    );
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.LEFT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.LEFT,
                            5
                    );
                }

                constraintSet.applyTo(itemView);
            }

        }

        @Override
        void bind(Resposta o) {
            Resposta.FromJSON jsonObject = null;
            if (o.daBusca.hasJSON()) {
                jsonObject = o.daBusca.getJSON();
            }
            Resposta.Conteudo conteudo = o.getConteudo();
            if (Resposta.Conteudo.DATA.equals(conteudo) && jsonObject != null) {
                textView_nome.setText(String.format("%s ENTRE %d E %d ANOS",
                        o.getQuestao().getTituloParaPesquisador(),
                        jsonObject.min,
                        jsonObject.max
                ));
            } else if (Resposta.Conteudo.DURACAO_MESES.equals(conteudo) && jsonObject != null) {
                textView_nome.setText(String.format("%s ENTRE %d E %d MESES",
                        o.getQuestao().getTituloParaPesquisador(),
                        jsonObject.min,
                        jsonObject.max
                ));
            } else {
                textView_nome.setText(o.getQuestao().getTituloParaPesquisador() + " = " + o.getResposta());
            }
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) onItemClickListener.onItemClick(busca.getRespostas().get(getAdapterPosition()));
        }
    }

}
