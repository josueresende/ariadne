package br.pasifae.ariadne.pesquisador;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Usuario;

/**
 * Created by JosueResende on 13/04/2018.
 */

public class Conversas_SliderFragmentActivity extends FragmentActivity {
    ViewPager viewPager;
    Conversas_FragmentStatePagerAdapter pagerAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        {   /** ATUA EM CONJUNTO COM Main_Activity.onNewIntent **/
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    nm.cancel(R.string.app_name);
                    finish();
                }
            }, new IntentFilter("LOGOUT"));
        }
        {
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    long autor_id = intent.getLongExtra("AUTOR_ID", 0);
                    if (autor_id > 0) pagerAdapter.paginaDeMensagens(new Usuario(autor_id, ""));
                }
            }, new IntentFilter("MESSAGE_NOTIFICATION"));
        }
        viewPager = new ViewPager(this);
        pagerAdapter = new Conversas_FragmentStatePagerAdapter(getSupportFragmentManager(), viewPager);
        viewPager.setId(View.generateViewId());
        viewPager.setAdapter(pagerAdapter);
//        pagerAdapter.paginaDeBusca();
        viewPager.setLongClickable(true);
        viewPager.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        setContentView(viewPager);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        } else {
            getParent().onBackPressed();
        }
    }


}
