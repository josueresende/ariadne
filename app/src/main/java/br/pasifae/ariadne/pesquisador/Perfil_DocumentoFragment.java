package br.pasifae.ariadne.pesquisador;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import br.com.receitasdecodigo.utils.MaskEditUtil;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.VoluntarioService;
import retrofit2.Call;
import retrofit2.Response;

public class Perfil_DocumentoFragment extends Perfil_Fragment {

    private EditText editText_cpf;
    private EditText editText_cnpj;
    private EditText editText_passaporte;

    private Resposta resposta_cpf;
    private Resposta resposta_cnpj;
    private Resposta resposta_passaporte;

    class UpdateProfile extends AsyncTask<Void, Void, Void> {

        Map<String, Resposta> respostas = new LinkedHashMap<>();

        private void add(Object object) {
            if ((object instanceof Resposta) == false) return;
            Resposta resposta = (Resposta) object;
            Questao questao = Acesso.Database.getQuestaoById(resposta.getQuestao().getId());
            resposta.setQuestao(questao);
            if (questao.getLimiteDeRespostasMarcadas() == 1) {
                if (questao.getTag() == null) return;
                respostas.put(questao.getTag(), resposta);
            } else if (questao.getLimiteDeRespostasMarcadas() == -1) {
                if (questao.getTag() == null) return;
                if (resposta.getTag() == null) return;
                respostas.put(questao.getTag() + "|" + resposta.getTag(), resposta);
            }
        }

        private String get(String k) {
            if (respostas.containsKey(k) == false) return null;
            Resposta resposta = respostas.get(k);
            if (resposta.getTag() == null) return resposta.getResposta();
            return resposta.getTag();
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Atualizando...");
            progressDialog.show();
            add(resposta_cpf);
            add(resposta_cnpj);
            add(resposta_passaporte);
        }

        private String getTexto(String tag) {
            if (respostas.get(tag) == null) return "";
            if (respostas.get(tag).getQuestao().getLimiteDeRespostasMarcadas() == 1) return respostas.get(tag).getTag();
            if (respostas.get(tag).doVoluntario.hasTexto()) return respostas.get(tag).doVoluntario.texto;
            return "";
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Acesso.usuarioLogadoComSucesso
                    .documento.cpf = getTexto("DOCUMENTO|CPF");

            Acesso.usuarioLogadoComSucesso
                    .documento.cnpj = getTexto("DOCUMENTO|CNPJ");

            Acesso.usuarioLogadoComSucesso
                    .documento.passaporte = getTexto("DOCUMENTO|PASSAPORTE");

            fragmentStatePagerAdapter.before();
            fragmentStatePagerAdapter.reset();

            progressDialog.dismiss();
        }

        private void update(String tag) {
            Logger.getAnonymousLogger().info("UPDATE: " + tag);
            Resposta resposta = respostas.get(tag);
            if (resposta == null) return;
            Long idResposta = resposta.getId();
            Long idQuestao = resposta.getQuestao().getId();
            Logger.getAnonymousLogger().info(String.format("idQuestao: %d : idResposta: %d : texto: %s", idQuestao, idResposta, (resposta.doVoluntario.hasTexto() ? resposta.doVoluntario.texto : "")));
            try {
                VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
                Call<VoluntarioService.Response> call =
                        service.marcar(Acesso.usuarioLogadoComSucesso.getLogin(),
                                idQuestao,
                                idResposta,
                                (resposta.doVoluntario.hasTexto() ? resposta.doVoluntario.texto : "")
                        );
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            for (String tag : new String[] {
                    "DOCUMENTO|CPF",
                    "DOCUMENTO|CNPJ",
                    "DOCUMENTO|PASSAPORTE",
            }) {
                update(tag);
            }
            return null;
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            validator = new Validator(this);
            validator.setValidationListener(this);
        }
        {
            String cpf = Acesso.usuarioLogadoComSucesso.documento.cpf;
            {
                if (cpf == null) cpf = "";
                resposta_cpf = acesso.getResposta("DOCUMENTO", "CPF");
            }
            String cnpj = Acesso.usuarioLogadoComSucesso.documento.cnpj;
            {
                if (cnpj == null) cnpj = "";
                resposta_cnpj = acesso.getResposta("DOCUMENTO", "CNPJ");
            }
            String passaporte = Acesso.usuarioLogadoComSucesso.documento.passaporte;
            {
                if (passaporte == null) passaporte = "";
                resposta_passaporte = acesso.getResposta("DOCUMENTO", "PASSAPORTE");
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("DOCUMENTO");
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
                textView.setBackground(gradientDrawable);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("CPF");
                textView.setPadding(5, 5, 5, 5);

                editText_cpf = new EditText(viewPager.getContext());
                linearLayout.addView(editText_cpf);
                editText_cpf.setText(cpf);
                editText_cpf.setInputType(InputType.TYPE_CLASS_NUMBER);
//                editText_cpf.addTextChangedListener(MaskEditUtil.mask(editText_cpf, MaskEditUtil.FORMAT_CPF));
                editText_cpf.setFilters(new InputFilter[] {new InputFilter.LengthFilter(11)});
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("CNPJ");
                textView.setPadding(5, 5, 5, 5);

                editText_cnpj = new EditText(viewPager.getContext());
                linearLayout.addView(editText_cnpj);
                editText_cnpj.setText(cnpj);
                editText_cnpj.setInputType(InputType.TYPE_CLASS_NUMBER);
//                editText_cnpj.addTextChangedListener(MaskEditUtil.mask(editText_cpf, MaskEditUtil.FORMAT_CNPJ));
                editText_cpf.setFilters(new InputFilter[] {new InputFilter.LengthFilter(14)});
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("PASSAPORTE");
                textView.setPadding(5, 5, 5, 5);

                editText_passaporte = new EditText(viewPager.getContext());
                linearLayout.addView(editText_passaporte);
                editText_passaporte.setText(passaporte);
                editText_passaporte.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_archive_24px);
                floatingActionButton.setElevation(2);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10,10,10,10);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimary));
                floatingActionButton.setImageTintList(getResources().getColorStateList(android.R.color.white));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (validator != null) validator.validate();
                        if (validated)
                        {
                            resposta_cpf.doVoluntario.texto = editText_cpf.getText().toString();
                            new UpdateProfile().execute();
                        }
                    }
                });
            }

        }

        return relativeLayout;
    }
}
