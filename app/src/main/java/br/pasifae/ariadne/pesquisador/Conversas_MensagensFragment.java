package br.pasifae.ariadne.pesquisador;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.pasifae.android.support.v4.view.Pasifae_FragmentStatePageAdapter;
import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.Jin;
import br.pasifae.ariadne.dados.Mock;
import br.pasifae.ariadne.dados.SwipeRecyclerViewDatabaseAdapter;
import br.pasifae.ariadne.dados.jins.MensagensDoUsuario;
import br.pasifae.ariadne.dados.modelo.Anuncio;
import br.pasifae.ariadne.dados.modelo.Mensagem;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.pesquisador.adapters.Mensagens_RecyclerViewAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by JosueResende on 15/04/2018.
 */

public class Conversas_MensagensFragment extends Fragment {


    private ViewPager viewPager;
    private Usuario usuario;

    public Conversas_MensagensFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    public Conversas_MensagensFragment setUsuario(Usuario usuario) {
        this.usuario = usuario;

        return this;
    }

    /*****************************/
    class Send extends AsyncTask<Void, Void, Void> {

        final private Mensagem mensagem;

        public Send(Mensagem mensagem) {
            this.mensagem = mensagem;
        }

        @Override
        protected void onPreExecute() {
            editText_mensagem.setText("");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Mock.sleep(1000);
        }

        @Override
        protected Void doInBackground(Void... strings) {
            acesso.putMensagemDireta(this.mensagem);
            return null;
        }

    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        private int updated = 0;

        @Override
        protected Void doInBackground(Void... voids) {
            String currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String anunciosDoEscolhido =
                    String.format("mensagem.id_usuario = %d AND mensagem_direta.id IS NULL AND " +
                            "mensagem.id IN (SELECT id_mensagem FROM mensagem_anuncio WHERE data_expiracao <= '%s 23:59:59') ",
                            usuario.getId(), currentDate
                    );
            String mensagensDoEscolhidoParaLogado =
                    String.format("mensagem.id_usuario = %d AND mensagem_direta.id_usuario = %d",
                            usuario.getId(), Acesso.usuarioLogadoComSucesso.getId()
                    );
            String mensagensDoLogadoParaEscolhido =
                    String.format("mensagem.id_usuario = %d AND mensagem_direta.id_usuario = %d",
                            Acesso.usuarioLogadoComSucesso.getId(), usuario.getId()
                    );
            Cursor cursor = database_user_sqLiteOpenHelper
                    .getReadableDatabase()
                    .rawQuery("" +
                            "SELECT " +
                            "mensagem.id_usuario AS autor_id, " +
                            "mensagem.nm_usuario AS autor_nome, " +
                            "mensagem.tp_usuario AS autor_tipo, " +
                            "mensagem.data_criacao AS mensagem_data_criacao, " +
                            "mensagem.conteudo AS mensagem_conteudo, " +
                            "mensagem_anuncio.id AS anuncio_id, " +
                            "mensagem_anuncio.data_expiracao AS anuncio_expiracao, " +
                            "mensagem.tx_extra AS mensagem_extra, " +
                            "mensagem.id AS mensagem_id " +
                            "FROM mensagem " +
                            "LEFT JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id " +
                            "LEFT JOIN mensagem_anuncio ON mensagem_anuncio.id_mensagem = mensagem.id " +
                            "WHERE 1=1 " +
                            "AND " + String.format("mensagem.id > %d", idMensagem) + " " +
                            "AND (" +
                                "(" + anunciosDoEscolhido + ")" +
                                " OR " +
                                "(" + mensagensDoEscolhidoParaLogado + ")" +
                                " OR " +
                                "(" + mensagensDoLogadoParaEscolhido + ")" +
                            ") " +
                            "ORDER BY mensagem.data_criacao " +
                            "", null);
            while (cursor.moveToNext()) {
                updated++;
                idMensagem =
                        cursor.getLong(cursor.getColumnIndex("mensagem_id"))
                ;
                String conteudoMensagem =
                        cursor.getString(cursor.getColumnIndex("mensagem_conteudo"));
                long idAutor =
                        cursor.getLong(cursor.getColumnIndex("autor_id"));
                String nomeAutor =
                        cursor.getString(cursor.getColumnIndex("autor_nome"));
                String tipoAutor =
                        cursor.getString(cursor.getColumnIndex("autor_tipo"));
                Date dataDeCriacao =
                        new Date(cursor.getLong(cursor.getColumnIndex("mensagem_data_criacao")));
                Usuario autor =
                        new Usuario(idAutor, nomeAutor).setTipo(tipoAutor);
                String extra =
                        cursor.getString(cursor.getColumnIndex("mensagem_extra"));

                Mensagem mensagem = null;
                try {
                    if (cursor.isNull(cursor.getColumnIndex("anuncio_id"))) {
                        mensagem = new Mensagem(idMensagem, conteudoMensagem)
                                .setAutor(autor)
                                .setDataDeEnvio(dataDeCriacao)
                        ;
                    } else {
                        mensagem = new Anuncio(idMensagem, conteudoMensagem)
                                .setDataDeExpiracao(new Date(
                                        cursor.getLong(cursor.getColumnIndex("anuncio_expiracao"))
                                ))
                                .setAutor(autor)
                                .setDataDeEnvio(dataDeCriacao)
                        ;
                    }

                    if (extra != null) mensagem.extras = Mensagem.Extra.fromJSON(extra);

                } catch(Exception e) {
                    e.printStackTrace();
                }
                if (mensagem != null) recyclerViewAdapter.mensagens.add(mensagem);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (updated > 0) {
                recyclerViewAdapter.notifyDataSetChanged();
                recyclerView_mensagens.scrollToPosition(recyclerViewAdapter.mensagens.size() - 1);
            }
        }


    }
/*****************************/
    ConstraintLayout constraintLayout;
    TextView textView_titulo;
    RecyclerView recyclerView_mensagens;
    View view_space;
    LinearLayout linearLayout_input;
    Mensagens_RecyclerViewAdapter recyclerViewAdapter;
    Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;
    EditText editText_mensagem;
    Acesso acesso;

    long idMensagem = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        // https://blog.sendbird.com/android-chat-tutorial-building-a-messaging-ui
        constraintLayout = new ConstraintLayout(viewPager.getContext());

        {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimary));
            constraintLayout.setId(View.generateViewId());
            constraintLayout.setBackground(gradientDrawable);
            constraintLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
            constraintLayout.setTop(20);
            constraintLayout.setPadding(10,10,10,10);
        }
        {
            recyclerView_mensagens = new RecyclerView(viewPager.getContext());
            recyclerView_mensagens.setId(View.generateViewId());
            constraintLayout.addView(recyclerView_mensagens);

            ConstraintLayout.LayoutParams constraintLayoutParams = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, ConstraintLayout.LayoutParams.MATCH_CONSTRAINT);
            recyclerView_mensagens.setLayoutParams(constraintLayoutParams);

            {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(viewPager.getContext(), LinearLayoutManager.VERTICAL, false);
                linearLayoutManager.setSmoothScrollbarEnabled(true);
                linearLayoutManager.setStackFromEnd(true);
                recyclerView_mensagens.setLayoutManager(linearLayoutManager);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
//                gradientDrawable.setCornerRadius(30);
                recyclerView_mensagens.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter = new Mensagens_RecyclerViewAdapter(viewPager.getContext());
                recyclerView_mensagens.setAdapter(recyclerViewAdapter);
            }
            {
                recyclerView_mensagens.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView_mensagens.scrollToPosition(recyclerView_mensagens.getAdapter().getItemCount() - 1);
                    }
                });
            }

        }
        {
            textView_titulo = new TextView(viewPager.getContext());
            textView_titulo.setId(View.generateViewId());
            constraintLayout.addView(textView_titulo);
            textView_titulo.setText("MENSAGENS: " + usuario.getNomeCompleto());
            textView_titulo.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView_titulo.setTypeface(null, Typeface.BOLD);
            textView_titulo.setPadding(5,5,5,5);
            textView_titulo.setTextColor(Color.WHITE);
        }
        {
            view_space = new View(viewPager.getContext());
            view_space.setId(View.generateViewId());
            constraintLayout.addView(view_space);
            view_space.setLayoutParams(
                    new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_CONSTRAINT, 2)
            );
            view_space.setBackgroundColor(Color.LTGRAY);
        }
        {
            linearLayout_input = new LinearLayout(viewPager.getContext());
            linearLayout_input.setId(View.generateViewId());
            constraintLayout.addView(linearLayout_input);
//            linearLayout_input.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            {
                linearLayout_input.setBackgroundColor(Color.WHITE);
                linearLayout_input.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout_input.setGravity(Gravity.BOTTOM);
                linearLayout_input.setPadding(5,5,5,5);
            }
            {
                editText_mensagem = new EditText(viewPager.getContext());
                linearLayout_input.addView(editText_mensagem);
                {
                    editText_mensagem.setPadding(5,5,5,5);
                }
                {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.weight = 1;
                    layoutParams.leftMargin = 5;
                    layoutParams.rightMargin = 10;

                    editText_mensagem.setLayoutParams(layoutParams);
                    editText_mensagem.setSingleLine(false);
                    editText_mensagem.setLines(4);
                    editText_mensagem.setMinLines(1);
                    editText_mensagem.setHint("Mensagem...");
                }
                {
                    GradientDrawable gradientDrawable = new GradientDrawable();
                    gradientDrawable.setColor(Color.WHITE);
//                    gradientDrawable.setCornerRadius(20);
//                    gradientDrawable.setStroke(2, Color.LTGRAY);
                    editText_mensagem.setBackground(gradientDrawable);
                }
                {
                    Button button = new Button(viewPager.getContext());
                    linearLayout_input.addView(button);
                    button.setGravity(Gravity.CENTER);
                    button.setText("ENVIAR");
                    button.setLeft(10);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String mensagem = editText_mensagem.getText().toString();
                            if (mensagem != null && mensagem.trim().length() > 0) {
                                new Send(new Mensagem(0L, mensagem)
                                        .setAutor(Acesso.usuarioLogadoComSucesso)
                                        .setLeitor(usuario)
                                ).execute();
                            } else {
                                Toast.makeText(getContext(), "Preencha a mensagem para enviar", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        }
/**************************************************************************************************/
        {
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);

            for (Integer pos : new Integer[] { ConstraintSet.LEFT, ConstraintSet.RIGHT }) {
                constraintSet.connect(
                        textView_titulo.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
                constraintSet.connect(
                        recyclerView_mensagens.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
                constraintSet.connect(
                        view_space.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
                constraintSet.connect(
                        linearLayout_input.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
            }

            constraintSet.connect(
                    recyclerView_mensagens.getId(),
                    ConstraintSet.TOP,
                    textView_titulo.getId(),
                    ConstraintSet.BOTTOM,
                    5
            );
            constraintSet.connect(
                    recyclerView_mensagens.getId(),
                    ConstraintSet.BOTTOM,
                    view_space.getId(),
                    ConstraintSet.TOP,
                    5
            );

            constraintSet.connect(
                    view_space.getId(),
                    ConstraintSet.BOTTOM,
                    linearLayout_input.getId(),
                    ConstraintSet.TOP,
                    5
            );

            constraintSet.connect(
                    textView_titulo.getId(),
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP,
                    5
            );

            constraintSet.connect(
                    linearLayout_input.getId(),
                    ConstraintSet.BOTTOM,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.BOTTOM,
                    0
            );

            constraintSet.constrainWidth(textView_titulo.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
            constraintSet.constrainWidth(recyclerView_mensagens.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
            constraintSet.constrainMinHeight(recyclerView_mensagens.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
            constraintSet.constrainWidth(view_space.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
            constraintSet.constrainWidth(linearLayout_input.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);

            constraintSet.applyTo(constraintLayout);
        }
        {
            recyclerView_mensagens.setVerticalScrollBarEnabled(true);
            recyclerView_mensagens.scrollToPosition(recyclerView_mensagens.getAdapter().getItemCount() - 1);
        }
        {
            recyclerViewAdapter.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final CharSequence[] items = {
                            "Compartilhar conversa ...",
//                            "Apagar conversa"
                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            switch (item) {
                                case 0:
                                    Intent sendIntent = new Intent();
                                    sendIntent.setAction(Intent.ACTION_SEND);
                                    sendIntent.putExtra(Intent.EXTRA_TEXT, recyclerViewAdapter.allText());
                                    sendIntent.setType("text/plain");
                                    startActivity(Intent.createChooser(sendIntent, "Compartilhar conversa"));
                                    break;
                                case 1:
                                    // TODO implementar apagamento de conversa
                                    break;
                            }
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    return true;
                }
            });
        }
        {
            new LoadData().execute();
        }
        {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    new LoadData().execute();
                }
            }, new IntentFilter("NEW_MESSAGE"));
        }

        return constraintLayout;
    }

}
