package br.pasifae.ariadne.pesquisador;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.SwipeRecyclerViewAdapter;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Voluntario;
import br.pasifae.ariadne.pesquisador.adapters.Resultados_RecyclerViewAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by JosueResende on 08/04/2018.
 */

public class Buscas_ResultadosFragment extends Fragment {

    private Acesso acesso;

    private ProgressDialog progressDialog;

    protected SwipeRecyclerViewAdapter<Voluntario> adapter;
    protected Buscas_FragmentStatePagerAdapter buscasFragmentStatePagerAdapter;
    protected ViewPager viewPager;
    protected Busca busca;

    private Resultados_RecyclerViewAdapter recyclerViewAdapter_Resultados;
    private Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;

    class Search extends Loading<Void> {

        Busca busca;
        private List<Voluntario> os = new LinkedList<>();

        public Search(Busca busca) {
            this.busca = busca;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
            recyclerViewAdapter_Resultados.clear();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for (Voluntario o : os) recyclerViewAdapter_Resultados.add(o);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            if (os.size() == 0) {
                Toast.makeText(getContext(), "Sem resultados para a busca", Toast.LENGTH_LONG).show();

            }
        }

        @Override
        protected Void doInBackground(Void... os) {
            this.os = acesso.procuraVoluntarios(busca);
            return null;
        }

    }


    public Buscas_ResultadosFragment setFragmentStatePagerAdapter(Buscas_FragmentStatePagerAdapter buscasFragmentStatePagerAdapter) {
        this.buscasFragmentStatePagerAdapter = buscasFragmentStatePagerAdapter;
        return this;
    }

    public Buscas_ResultadosFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    public Buscas_ResultadosFragment setBusca(Busca busca) {
        this.busca = busca;
        return this;
    }

    abstract class Loading<Param> extends AsyncTask<Param, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }

    public Buscas_ResultadosFragment setResultadosAdapter(SwipeRecyclerViewAdapter<Voluntario> resultados) {
        this.adapter = resultados;
        {
//            adapter.setOnEvent(new SwipeRevealLayoutAdapter.OnInfoItem<Voluntario>() {
//                @Override
//                public void infoItem(View v, Voluntario voluntario) {
//
//                }
//            });
        }
        return this;
    }

    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Aguarde...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
        }

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        RelativeLayout relativeLayout = new RelativeLayout(container.getContext());

        {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimary));
            relativeLayout.setBackground(gradientDrawable);
//            relativeLayout.setPadding(0,20,0,0);
            relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        }

        LinearLayout linearLayout = new LinearLayout(container.getContext());
        relativeLayout.addView(linearLayout);

        {
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        }
        {
            linearLayout.setPadding(10,10,10,10);
            GradientDrawable gradientDrawable = new GradientDrawable();
//            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(Color.WHITE);
//            gradientDrawable.setCornerRadius(15);
            linearLayout.setBackground(gradientDrawable);
        }
        {
            TextView textView = new TextView(container.getContext());
            linearLayout.addView(textView);
            textView.setText("RESULTADOS DA BUSCA");
            textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.WHITE);
        }
        {
            recyclerView = new RecyclerView(container.getContext());
            linearLayout.addView(recyclerView);
            {
                recyclerView.setId(View.generateViewId());
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false)
                );
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(30);
                recyclerView.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter_Resultados = new Resultados_RecyclerViewAdapter(viewPager.getContext());
                recyclerView.setAdapter(recyclerViewAdapter_Resultados);
                recyclerView.setPadding(5,5,5,5);
            }
            {
                recyclerViewAdapter_Resultados.setOnItemClickListener(new Resultados_RecyclerViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Object item) {
                        if (item instanceof Voluntario) {
                            buscasFragmentStatePagerAdapter.paginaDoVoluntario((Voluntario) item);
                        }

                    }
                });
            }
        }
        {
            new Search(busca).execute();
        }
        return relativeLayout;

    }

    public View onCreateView_(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        acesso = new Acesso(getActivity());

        RelativeLayout relativeLayout = new RelativeLayout(getContext());

        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));

        LinearLayout linearLayout = new LinearLayout(getContext());
        relativeLayout.addView(linearLayout);
        linearLayout.setPadding(10,10,10,10);
        linearLayout.getLayoutParams().width = MATCH_PARENT;

        {
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            TextView textView = new TextView(getContext());
            linearLayout.addView(textView);
            textView.setLines(2);
            textView.setText("BUSCA: " + busca.getNome() + "\n" + adapter.getItemCount() + " items(s)");
            textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimaryLight));
            textView.setPadding(5,5,5,5);
        }
        {
            RecyclerView recyclerView = new RecyclerView(getContext());
            linearLayout.addView(recyclerView);
            {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
                recyclerView.setAdapter(adapter);
                recyclerView.setPadding(5,5,5,5);
            }
            {
                // TODO: swipe helper com icones? para conversa/informações
            }
        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_bullhorn);
                floatingActionButton.setElevation(2);

                floatingActionButton.setSize(FloatingActionButton.SIZE_AUTO);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.setMargins(5,5,5,5);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimary));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        buscasFragmentStatePagerAdapter.paginaDeAnuncios(busca);
                    }
                });
            }
        }

        return relativeLayout;
    }
}
