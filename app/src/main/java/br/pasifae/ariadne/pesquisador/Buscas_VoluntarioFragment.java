package br.pasifae.ariadne.pesquisador;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.dados.modelo.Voluntario;
import br.pasifae.ariadne.pesquisador.adapters.Voluntario_RecyclerViewAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class Buscas_VoluntarioFragment extends Fragment {

    Acesso acesso;

    private Buscas_FragmentStatePagerAdapter fragmentStatePagerAdapter;
    private Usuario usuario;
    private Voluntario voluntario;
    private ViewPager viewPager;

    protected Busca busca;

    private Voluntario_RecyclerViewAdapter recyclerViewAdapter_Voluntario;

    Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;
    private ProgressDialog progressDialog;

    RelativeLayout relativeLayout;
    LinearLayout linearLayout_1;
    LinearLayout linearLayout_2;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Aguarde...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
        }

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        relativeLayout = new RelativeLayout(viewPager.getContext());

        {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimary));
            relativeLayout.setBackground(gradientDrawable);
            relativeLayout.setPadding(0,20,0,0);
            relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        }

        linearLayout_1 = new LinearLayout(container.getContext());
        relativeLayout.addView(linearLayout_1);
        linearLayout_1.setOrientation(LinearLayout.VERTICAL);
        {
            linearLayout_1.setPadding(10,10,10,10);
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setStroke(2, Color.BLACK);
            gradientDrawable.setColor(Color.WHITE);
            gradientDrawable.setCornerRadius(15);
            linearLayout_1.setBackground(gradientDrawable);
        }
        {
            TextView textView = new TextView(container.getContext());
            linearLayout_1.addView(textView);
            textView.setText("RESPOSTAS DO VOLUNTARIO");
            textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.WHITE);
        }
        {
            TextView textView = new TextView(container.getContext());
            linearLayout_1.addView(textView);
            textView.setText("IDENTIFICACAO: " + usuario.getNomeCompleto());
            textView.setBackgroundColor(Color.WHITE);
            textView.setPadding(5,5,5,5);
            {
                textView.setPadding(10,10,10,10);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(15);
                textView.setBackground(gradientDrawable);
            }
        }
        {
            linearLayout_1.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        }
        {
            recyclerView = new RecyclerView(container.getContext());
            linearLayout_1.addView(recyclerView);
            {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                recyclerView.setId(View.generateViewId());
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false)
                );
                recyclerView.setLayoutParams(layoutParams);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(30);
                recyclerView.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter_Voluntario = new Voluntario_RecyclerViewAdapter(viewPager.getContext(), voluntario);
                recyclerView.setAdapter(recyclerViewAdapter_Voluntario);
                recyclerView.setPadding(5,5,5,5);
            }
            {
                for (Questao questao : voluntario.perfil.values()) {
                    if (questao.getCriterioDeBusca() == false) continue;
                    recyclerViewAdapter_Voluntario.add(questao);
                }
            }
        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(relativeLayout.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_forum_black_48dp);
                floatingActionButton.setElevation(2);
                floatingActionButton.setColorFilter(Color.WHITE);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimaryDark));

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.setMargins(20,20,20,20);

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fragmentStatePagerAdapter.paginaDeMensagem(usuario);
                    }
                });
            }
        }

        return relativeLayout;
    }

    public Buscas_VoluntarioFragment setFragmentStatePagerAdapter(Buscas_FragmentStatePagerAdapter fragmentStatePagerAdapter) {
        this.fragmentStatePagerAdapter = fragmentStatePagerAdapter;
        return this;
    }

    public Buscas_VoluntarioFragment setUsuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public Buscas_VoluntarioFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    public Buscas_VoluntarioFragment setVoluntario(Voluntario voluntario) {
        this.voluntario = voluntario;
        return this;
    }
}
