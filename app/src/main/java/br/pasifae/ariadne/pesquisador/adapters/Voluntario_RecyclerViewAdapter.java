package br.pasifae.ariadne.pesquisador.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.Voluntario;

public class Voluntario_RecyclerViewAdapter extends RecyclerView.Adapter<Voluntario_RecyclerViewAdapter.ViewHolder>  {

    final private Context mContext;
    private OnItemClickListener onItemClickListener;
    final public List<Questao> questoes = new LinkedList<>();

    public Voluntario_RecyclerViewAdapter(Context mContext, Voluntario voluntario) {
        this.mContext = mContext;

    }

    static public interface OnItemClickListener {
        void onItemClick(Object item);
    }

    abstract class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ConstraintLayout itemView) { super(itemView); }
        abstract void bind(Questao o);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Perfil(new ConstraintLayout(mContext));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(questoes.get(position));
    }

    @Override
    public int getItemCount() { return questoes.size(); }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void add(Questao questao) {
        this.questoes.add(questao);
        notifyDataSetChanged();
    }

    class Perfil extends ViewHolder implements View.OnClickListener {
        TextView textView;

        public Perfil(ConstraintLayout itemView) {
            super(itemView);
            {
//                itemView.setOnClickListener(this);
            }
            {
                RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                itemView.setLayoutParams(layoutParams);
            }
            {
                itemView.setPadding(5,5,5,5);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.TRANSPARENT);
                gradientDrawable.setCornerRadius(15);
                itemView.setBackground(gradientDrawable);
            }
            {
                textView = new TextView(mContext);
                textView.setId(View.generateViewId());
                itemView.addView(textView);
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setCornerRadius(15);
                textView.setBackground(gradientDrawable);
                textView.setPadding(10,10,10,10);
            }
            {
                ConstraintSet constraintSet = new ConstraintSet();
                constraintSet.clone(itemView);

                for (View view : new View[] {textView}) {
                    constraintSet.connect(
                            view.getId(),
                            ConstraintSet.LEFT,
                            ConstraintSet.PARENT_ID,
                            ConstraintSet.LEFT,
                            5
                    );
                }
                constraintSet.applyTo(itemView);
            }
        }

        @Override
        void bind(Questao questao) {

            StringBuilder stringBuilder = new StringBuilder();

            for (Resposta o : questao.getRespostas()) {

                if (stringBuilder.length() > 0) stringBuilder.append("; ");

                Resposta.Conteudo conteudo = o.getConteudo();

                String resposta = (o.getResposta() != null ? o.getResposta() : o.doVoluntario.texto);

                if (Resposta.Conteudo.DATA.equals(conteudo)) {
                    try {
                        Date born = new SimpleDateFormat("yyyy-MM-dd").parse(resposta);
                        LocalDate bornDate = new LocalDate(born);
                        stringBuilder.append(String.format("%d ano(s)",
                                Years.yearsBetween(bornDate, new LocalDate()).getYears()
                        ));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (Resposta.Conteudo.DURACAO_MESES.equals(conteudo)) {
                    stringBuilder.append(String.format("%s mes(es)",
                            resposta
                    ));
                } else {
                    stringBuilder.append(resposta);
                }
            }
            textView.setText(String.format("%s: %s",
                    questao.getTituloParaPesquisador(), stringBuilder.toString()
            ));

        }

        @Override
        public void onClick(View v) {
            //if (onItemClickListener != null) onItemClickListener.onItemClick(autores.get(getAdapterPosition()));
        }
    }

}
