package br.pasifae.ariadne.pesquisador;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonPrimitive;

import org.florescu.android.rangeseekbar.RangeSeekBar;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.QuestoesAdapter;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.CriterioDeRespostaMarcada;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;

/**
 * Created by JosueResende on 18/04/2018.
 */

public class Dialog_EscolherCriterioDialogFragment extends DialogFragment {

    private ProgressDialog progressDialog;

    private Acesso acesso;
    private View layout;
    private ArrayAdapter questoesAdapter;
    private ViewGroup viewGroup;

    private Dialog_EscolherCriterioDialogFragment_Listener listener;

    List<Resposta> respostasMarcadas = new LinkedList<>();
    Collection<Questao> questoes;

    public Dialog_EscolherCriterioDialogFragment setListener(Dialog_EscolherCriterioDialogFragment_Listener listener) {
        this.listener = listener;
        return this;
    }

    static public interface Dialog_EscolherCriterioDialogFragment_Listener {
        public void OnUserSelectCriterioEscolhido_Positive(List<Criterio> criterios);
        public void OnUserSelectCriterioEscolhido_Negative();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Dialog_EscolherCriterioDialogFragment_Listener) {
            this.setListener((Dialog_EscolherCriterioDialogFragment_Listener)context);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();

            acesso = new Acesso(getActivity());

            layout = inflater.inflate(R.layout.layout_dialog_novo_criterio, null);
            {
                questoes = acesso.getCriterios();
            }
            {
//                if (getArguments() != null && getArguments().containsKey("questoes")) {
//                    questoes = (Collection<Questao>) getArguments().getSerializable("questoes");
//                } else {
//                    questoes = acesso.getQuestoes();
//                }

                questoesAdapter = new QuestoesAdapter(getContext(), R.layout.multiline_spinner_dropdown_list);
                questoesAdapter.addAll(questoes);
                questoesAdapter.setDropDownViewResource(R.layout.multiline_spinner_dropdown_list);

                ((Spinner) layout.findViewById(R.id.novoCriterioBusca_spinnerQuestoes)).setAdapter(questoesAdapter);
            }
            {

            }
            {
                viewGroup = (ViewGroup) layout.findViewById(R.id.novoCriterioBusca_linearLayoutRespostas);

                ((Spinner) layout.findViewById(R.id.novoCriterioBusca_spinnerQuestoes))
                        .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                          @Override
                          public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                              new Respostas(viewGroup, respostasMarcadas).execute((Questao)adapterView.getItemAtPosition(position));
                          }

                          @Override
                          public void onNothingSelected(AdapterView<?> adapterView) {

                          }
                      }
                );
            }

            builder.setView(layout)
                    // Add action buttons
                    .setPositiveButton("ADICIONAR", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            List<Criterio> criterios = new LinkedList<Criterio>();
                            for (Resposta resposta : respostasMarcadas) {
                                Criterio criterio = new CriterioDeRespostaMarcada();
                                criterio.setQuestao(resposta.getQuestao());
                                criterio.setResposta(resposta);
                                criterios.add(criterio);
                            }
                            if (listener != null) listener.OnUserSelectCriterioEscolhido_Positive(criterios);
                        }
                    })
                    .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (listener != null) listener.OnUserSelectCriterioEscolhido_Negative();
                            Dialog_EscolherCriterioDialogFragment.this.getDialog().cancel();
                        }
                    });
            return builder.create();

        }
    }
}
//
class Respostas extends AsyncTask<Questao, Void, List<Resposta>> {

    final private ViewGroup container;
    final private List<Resposta> respostasMarcadas;
    private Questao questaoEscolhida;

    public Respostas(ViewGroup container, List<Resposta> respostasMarcadas) {
        this.container = container;
        this.respostasMarcadas = respostasMarcadas;
    }

    private void marca(final Resposta resposta) {
        resposta.setQuestao(questaoEscolhida);
        resposta.setMarcado(true);
        if (questaoEscolhida.getLimiteDeRespostasMarcadas() == 1) respostasMarcadas.clear();
        respostasMarcadas.add(resposta);
    }

    private void desmarca(final Resposta resposta) {
        respostasMarcadas.remove(resposta);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (container != null) container.removeAllViews();
    }

    @Override
    protected void onPostExecute(List<Resposta> respostas) {
        super.onPostExecute(respostas);
        {   // UMA RESPOSTA_
            if (questaoEscolhida.getLimiteDeRespostasMarcadas() == 1) { // uma resposta possivel

                if (respostas.size() == 1) { // UMA RESPOSTA_/ UMA OPCAO

                    Resposta resposta = respostas.get(0);

                    LinearLayout linearLayoutForOptions = new LinearLayout(container.getContext());
                    container.addView(linearLayoutForOptions);

                    linearLayoutForOptions.setOrientation(LinearLayout.VERTICAL);

                    if (resposta.getResposta() != null && resposta.getResposta().length() > 0) {
                        TextView textView = new TextView(container.getContext());
                        linearLayoutForOptions.addView(textView);

                        textView.setText(resposta.getResposta());
                    }

                    if (Resposta.Conteudo.DATA.equals(resposta.getConteudo())) {

                        this.resposta_1_date(linearLayoutForOptions, resposta);

                    } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {

                        this.resposta_1_text(linearLayoutForOptions, resposta);

                    }

                } else { // UMA RESPOSTA_/ VARIAS OPCOES

                    // NAO TRATA RADIOBUTTON COMM TEXTO

                    RadioGroup radioGroup = new RadioGroup(container.getContext());
                    container.addView(radioGroup);

                    radioGroup.setOrientation(RadioGroup.VERTICAL);

                    for (Resposta resposta : respostas) {

                        RadioButton radioButton = new RadioButton(container.getContext());
                        radioGroup.addView(radioButton);

                        radioButton.setTag(resposta);
                        radioButton.setText(resposta.getResposta());

                        radioButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                marca((Resposta) view.getTag());
                            }
                        });
                    }
                }
            }
        }
        {   // VARIAS RESPOSTAS
            if (questaoEscolhida.getLimiteDeRespostasMarcadas() != 1) {

                LinearLayout linearLayoutForOptions = new LinearLayout(container.getContext());
                container.addView(linearLayoutForOptions);

                linearLayoutForOptions.setOrientation(LinearLayout.VERTICAL);

                for (final Resposta resposta : respostas) {

                    LinearLayout linearLayoutForLine = new LinearLayout(container.getContext());
                    linearLayoutForOptions.addView(linearLayoutForLine);

                    CheckBox checkBox = new CheckBox(container.getContext());
                    linearLayoutForLine.addView(checkBox);

                    checkBox.setTag(resposta);
                    checkBox.setText(resposta.getResposta());


                    if (Resposta.Conteudo.MARCACAO.equals(resposta.getConteudo())) {

                        checkBox.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                boolean marcado = ((CheckBox) view).isChecked();
                                if (marcado) {
                                    marca((Resposta) view.getTag());
                                } else {
                                    desmarca((Resposta) view.getTag());
                                }

                            }
                        });
                    } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {

                        EditText editText = new EditText(container.getContext());
                        linearLayoutForLine.addView(editText);

                        editText.setTag(checkBox);

                        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                            @Override
                            public boolean onEditorAction(TextView view, int actionId, KeyEvent keyEvent) {
                                if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                        actionId == EditorInfo.IME_ACTION_DONE ||
                                        keyEvent != null &&
                                                keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
                                                keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                                    if (keyEvent == null || !keyEvent.isShiftPressed()) {
                                        // the user is done typing.
                                        CheckBox checkBox = (CheckBox) view.getTag();

                                        boolean marcado = checkBox.isChecked();
                                        if (marcado) {
                                            marca((Resposta) checkBox.getTag());
                                        } else {
                                            desmarca((Resposta) checkBox.getTag());
                                        }
                                        return true; // consume.
                                    }
                                }
                                return false; // pass on to other listeners.
                            }
                        });
                    }
                }
            }
        }
    }
    private void resposta_1_text(LinearLayout linearLayoutForOptions, Resposta resposta) {

        EditText editText = new EditText(container.getContext());
        linearLayoutForOptions.addView(editText);
    }

    private void resposta_1_date(LinearLayout linearLayoutForOptions, final Resposta resposta) {
        RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<Integer>(container.getContext());
        linearLayoutForOptions.addView(rangeSeekBar);

        rangeSeekBar.setTextAboveThumbsColorResource(R.color.black_overlay);
        rangeSeekBar.setTag(resposta);

        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                JSONObject jsonObject = new JSONObject();
                try { jsonObject.put("min", new JsonPrimitive(minValue)); } catch (JSONException e) { e.printStackTrace(); }
                try { jsonObject.put("max", new JsonPrimitive(maxValue)); } catch (JSONException e) { e.printStackTrace(); }
                if (bar.getTag() instanceof Resposta) {
                    Resposta respondido = (Resposta) bar.getTag();
                    desmarca(respondido);
                    respondido.setResposta(jsonObject.toString());
                    marca(respondido);
                }

            }
        });
    } // resposta_1_date

    protected List<Resposta> doInBackground(Questao... params) {
        questaoEscolhida = params[0];
        respostasMarcadas.clear();
        return questaoEscolhida.getRespostas();
    }

}

