package br.pasifae.ariadne.pesquisador;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.receitasdecodigo.utils.MaskEditUtil;

public class Divulgacao_DataDeExpiracaoFragment extends Divulgacao_Fragment {

    private EditText editText_dataDeExpiracao;
    private EditText editText_caae;

    private DatePickerDialog.OnDateSetListener onDateSetListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container, savedInstanceState);
        this.setaDireita = true;
        {
            TextView textView = new TextView(container.getContext());
            linearLayout.addView(textView);
            textView.setText("DATA DE EXPIRAÇÃO");
            textView.setPadding(5,5,5,5);
        }
        /*/ -------------------------------------------------------------------------------------------- /*/
        {   // DATE
            editText_dataDeExpiracao = new EditText(getContext());
            linearLayout.addView(editText_dataDeExpiracao);

            editText_dataDeExpiracao.setClickable(true);
            editText_dataDeExpiracao.setFocusable(false);
            editText_dataDeExpiracao.setHint("dd/mm/yyyy");
            editText_dataDeExpiracao.setTextColor(getResources().getColor(android.R.color.black));
            editText_dataDeExpiracao.setTextSize(21);

            try {
                if (fragmentStatePagerAdapter.anuncio.getDataDeExpiracao() != null) {
                    editText_dataDeExpiracao.setText(new SimpleDateFormat("dd/MM/yyyy")
                            .format(fragmentStatePagerAdapter.anuncio.getDataDeExpiracao()));
                }
            } catch (Exception e) {
            }

            onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, monthOfYear);
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    Object o = view.getTag();
                    if (o instanceof EditText) {
                        ((EditText) o).setText(new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime()));
                    }
                    fragmentStatePagerAdapter.anuncio.setDataDeExpiracao(calendar.getTime());
                }
            };
            editText_dataDeExpiracao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Calendar calendar = Calendar.getInstance();
                    try {
                        calendar.setTime(fragmentStatePagerAdapter.anuncio.getDataDeExpiracao());
                    } catch (Exception e) {
                    }
                    DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                            onDateSetListener,
                            calendar.get(Calendar.YEAR),
                            calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)
                    );
                    datePickerDialog.getDatePicker().setTag(view);
                    datePickerDialog.show();
                }
            });
        }
        /*/ -------------------------------------------------------------------------------------------- /*/
        {
            TextView textView = new TextView(viewPager.getContext());
            linearLayout.addView(textView);
            textView.setText("CAAE");
            textView.setPadding(5, 5, 5, 5);

            editText_caae = new EditText(viewPager.getContext());
            linearLayout.addView(editText_caae);
            editText_caae.setInputType(InputType.TYPE_CLASS_NUMBER);
            editText_caae.addTextChangedListener(MaskEditUtil.mask(editText_caae, "####.#.###.####-##"));
        }

        return relativeLayout;
    }

    @Override
    public void navigateToRight(View view) {
        fragmentStatePagerAdapter.anuncio.extras.putExtra("CAAE", editText_caae.getText().toString());
    }
}
