package br.pasifae.ariadne.pesquisador;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.SwipeRecyclerViewAdapter;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.Resposta;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class Divulgacao_CriteriosFragment extends Divulgacao_Fragment {

    private SwipeRecyclerViewAdapter<Criterio> criteriosAdapter;
    private FragmentManager fragmentManager;

    LinearLayout linearLayout_criterio;
    RecyclerView recyclerView;
    RelativeLayout relativeLayout_criterio;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        fragmentManager = getFragmentManager();
        {
            linearLayout_criterio = new LinearLayout(container.getContext());

            linearLayout.addView(linearLayout_criterio);

            linearLayout_criterio.setPadding(10,10,10,10);
            linearLayout_criterio.getLayoutParams().width = MATCH_PARENT;
            linearLayout_criterio.setOrientation(LinearLayout.VERTICAL);

            {
                TextView textView = new TextView(container.getContext());
                linearLayout_criterio.addView(textView);
                textView.setText("CRITÉRIOS DO PÚBLICO");
                textView.setPadding(5,5,5,5);
            }
            {
                recyclerView = new RecyclerView(container.getContext());
                linearLayout_criterio.addView(recyclerView);
                {
                    criteriosAdapter =
                            new SwipeRecyclerViewAdapter<Criterio>(viewPager.getContext(),
                                    new SwipeRecyclerViewAdapter.OnCreateCardView() {
                                        @Override
                                        public SwipeRecyclerViewAdapter.SwipeRecyclerViewViewHolder set(View cardView) {
                                            return new SwipeRecyclerViewAdapter.SwipeRecyclerRelativeLayoutViewHolder<Criterio>(cardView) {
                                                TextView textView_nome;
                                                @Override
                                                public void write(CardView o) {
                                                    textView_nome = new TextView(o.getContext());
                                                    o.addView(textView_nome);
                                                    textView_nome.setTextSize(18);
                                                    CardView.LayoutParams layoutParams = new CardView.LayoutParams(CardView.LayoutParams.MATCH_PARENT, CardView.LayoutParams.WRAP_CONTENT);
                                                    textView_nome.setLayoutParams(layoutParams);
                                                }

                                                @Override
                                                public void onClick(View v) {

                                                }

                                                @Override
                                                public void use(Criterio o) {
//                                                    textView_nome.setText(
//                                                            o.getQuestao().getTituloParaPesquisador() + " = " + o.getResposta().getResposta()
//                                                    );
                                                    Resposta.FromJSON jsonObject = o.getResposta().getRespostaAsJSON();
                                                    Resposta.Conteudo conteudo = o.getResposta().getConteudo();
                                                    if (Resposta.Conteudo.DATA.equals(conteudo) && jsonObject != null) {
                                                        textView_nome.setText(String.format("%s ENTRE %d E %d ANOS",
                                                                o.getQuestao().getTituloParaPesquisador(),
                                                                jsonObject.min,
                                                                jsonObject.max
                                                        ));
                                                    } else if (Resposta.Conteudo.DURACAO_MESES.equals(conteudo) && jsonObject != null) {
                                                        textView_nome.setText(String.format("%s ENTRE %d E %d MESES",
                                                                o.getQuestao().getTituloParaPesquisador(),
                                                                jsonObject.min,
                                                                jsonObject.max
                                                        ));
                                                    } else {
                                                        textView_nome.setText(o.getQuestao().getTituloParaPesquisador() + " = " + o.getResposta().getResposta());
                                                    }

                                                }
                                            };
                                        }
                                    }, fragmentStatePagerAdapter.anuncio.getCriterios()
                            )
                    ;
                }
                {
                    recyclerView.setId(View.generateViewId());
                    recyclerView.setLayoutManager(
                            new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false)
                    );
                    recyclerView.setAdapter(criteriosAdapter);
                    recyclerView.setPadding(5,5,5,5);
                }

            }

            relativeLayout_criterio = new RelativeLayout(container.getContext());
            relativeLayout_criterio.setId(View.generateViewId());
            linearLayout_criterio.addView(relativeLayout_criterio);
            {
                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.setMargins(5,75,5,5);

                ImageButton imageButton = new ImageButton(container.getContext());
                relativeLayout.addView(imageButton);
                imageButton.setImageResource(R.drawable.ic_baseline_add_24px);
                imageButton.setLayoutParams(layoutParams);
                imageButton.setColorFilter(Color.WHITE);

                imageButton.setBackgroundResource(R.drawable.background_rounded_corners);
                GradientDrawable gradientDrawable = (GradientDrawable) imageButton.getBackground();
                gradientDrawable.setColor(getResources().getColor(R.color.pesquisadorPrimary));
                gradientDrawable.setCornerRadius(60);

                imageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog_EscolherCriterioDialogFragment dialogFragment =
                                (new Dialog_EscolherCriterioDialogFragment())
                                        .setListener(new Dialog_EscolherCriterioDialogFragment.Dialog_EscolherCriterioDialogFragment_Listener() {
                                            @Override
                                            public void OnUserSelectCriterioEscolhido_Positive(List<Criterio> criterios) {
                                                for (Criterio criterio : criterios) {
                                                    criteriosAdapter.add(criterio);
                                                }
                                                criteriosAdapter.notifyDataSetChanged();
                                            }

                                            @Override
                                            public void OnUserSelectCriterioEscolhido_Negative() {

                                            }
                                        });
                        dialogFragment.show(fragmentManager, "NOVO CRITERIO");
                    }
                });
            }
        }
        return relativeLayout;
    }

}
