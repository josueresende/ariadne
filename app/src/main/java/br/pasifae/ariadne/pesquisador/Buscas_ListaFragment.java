package br.pasifae.ariadne.pesquisador;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Estudo;
import br.pasifae.ariadne.dados.modelo.Pesquisador;
import br.pasifae.ariadne.pesquisador.adapters.Buscas_RecyclerViewAdapter;
import br.pasifae.ariadne.remote.PesquisadorService;
import br.pasifae.ariadne.remote.TeseuService;
import retrofit2.Call;
import retrofit2.Response;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by JosueResende on 08/04/2018.
 */

public class Buscas_ListaFragment extends Fragment {

    private Acesso acesso;

    private ProgressDialog progressDialog;

    private Buscas_FragmentStatePagerAdapter buscasFragmentStatePagerAdapter;
    private ViewPager viewPager;

    private ConfirmacaoFragment confirmacaoDeDelecaoFragment;

    private RecyclerView recyclerView;
    private Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;

    private Buscas_RecyclerViewAdapter recyclerViewAdapter_Buscas;

    private boolean creating = false;

    public Buscas_ListaFragment setFragmentStatePagerAdapter(Buscas_FragmentStatePagerAdapter buscasFragmentStatePagerAdapter) {
        this.buscasFragmentStatePagerAdapter = buscasFragmentStatePagerAdapter;
        return this;
    }

    public Buscas_ListaFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Aguarde...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
        }

        acesso = new Acesso(getActivity());

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        RelativeLayout relativeLayout = new RelativeLayout(container.getContext());

        {
            relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        }
        LinearLayout linearLayout = new LinearLayout(container.getContext());
        relativeLayout.addView(linearLayout);
        {
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        }
        {
            TextView textView = new TextView(container.getContext());
            linearLayout.addView(textView);
            textView.setText("BUSCAS CADASTRADAS");
            textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimary));
            textView.setTypeface(null, Typeface.BOLD);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.WHITE);
        }
        {
            recyclerView = new RecyclerView(container.getContext());
            linearLayout.addView(recyclerView);
            {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                recyclerView.setId(View.generateViewId());
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false)
                );
                recyclerView.setLayoutParams(layoutParams);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(30);
                recyclerView.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter_Buscas = new Buscas_RecyclerViewAdapter(viewPager.getContext());
                recyclerView.setAdapter(recyclerViewAdapter_Buscas);
                recyclerView.setPadding(5,5,5,5);
            }
            {
                recyclerViewAdapter_Buscas.setOnItemClickListener(new Buscas_RecyclerViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Object item) {
//                        if (item instanceof Busca) {
//                            buscasFragmentStatePagerAdapter.paginaDeBusca((Busca) item);
//                        }
                    }

                    @Override
                    public void onDeleteClick(Object item) {
                        confirmacaoDeDelecaoFragment
                                .setObject(item)
                                .show(getFragmentManager(), "CONFIRMAÇÃO");
                    }

                    @Override
                    public void onEditClick(Object item) {
                        buscasFragmentStatePagerAdapter.paginaDeBusca((Busca) item);

                    }

                    @Override
                    public void onSearchClick(Object item) {
                        buscasFragmentStatePagerAdapter.paginaDeResultados((Busca) item);
                    }
                });
            }
            {
                confirmacaoDeDelecaoFragment = new ConfirmacaoFragment<Busca>()
                        .setOnPositive(new ConfirmacaoFragment.OnPositive<Busca>() {
                            @Override
                            public void click(Busca o) {
                                new RemoverBusca(o).execute();
                            }
                        });
                confirmacaoDeDelecaoFragment.setTextoConfirmacao("Confirma a exclusão da Busca?");
                confirmacaoDeDelecaoFragment.setTitulo("EXCLUSÃO");
            }
        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(container.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_add_24px);
                floatingActionButton.setElevation(2);
                floatingActionButton.setColorFilter(Color.WHITE);


                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.setMargins(5,5,5,5);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.pesquisadorPrimaryDark));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new NovaBuscaPesquisadorFragment()
                                .setOnNovaBuscaDialogListener(new NovaBuscaPesquisadorFragment.NovaBuscaDialogListener() {
                                    @Override
                                    public void OnUserSelectNovaBuscaPositive(String nome) {
                                        creating = true;
                                        new SaveBusca(null, Acesso.pesquisadorLogadoComSucesso).execute(nome);
                                    }

                                    @Override
                                    public void OnUserSelectNovaBuscaNegative() {

                                    }
                                })
                                .show(getFragmentManager(), "NOVA BUSCA")
                        ;
                    }
                });
            }
        }
        {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (creating) {
                        creating = false;
                        new LoadData(true).execute();
                    } else {
                        new LoadData().execute();
                    }

                }
            }, new IntentFilter("NEW_SEARCH"));
        }
        return relativeLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadData().execute();
    }

    static
    public
    class ConfirmacaoFragment<T> extends DialogFragment {

        LinearLayout linearLayout;
        private String title = "CONFIRMAÇÃO";
        private String textoConfirmacao = "Confirma ?";

        private T object;

        ConfirmacaoFragment.OnPositive<T> onPositive;
        ConfirmacaoFragment.OnNegative<T> onNegative;

        public void setTextoConfirmacao(String textoConfirmacao) {
            this.textoConfirmacao = textoConfirmacao;
        }

        public void setTitulo(String title) {
            this.title = title;
        }

        static public interface OnPositive<T> {
            public void click(T o);
        };

        static public interface OnNegative<T> {
            public void click(T o);
        };

        public ConfirmacaoFragment setOnPositive(ConfirmacaoFragment.OnPositive<T> onPositive) {
            this.onPositive = onPositive;
            return this;
        }

        public ConfirmacaoFragment setOnNegative(ConfirmacaoFragment.OnNegative<T> onNegative) {
            this.onNegative = onNegative;
            return this;
        }

        public ConfirmacaoFragment setObject(T o) {
            this.object = o;
            return this;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText(title);
                textView.setTextSize(21);
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                textView.setBackgroundColor(getResources().getColor(R.color.pesquisadorPrimaryDark));
                textView.setPadding(10,10,10,10);
            }
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText(textoConfirmacao);
                textView.setTextSize(20);
                textView.setPadding(20,20,20,20);
            }

            builder.setView(linearLayout)
                    .setPositiveButton("CONFIRMA", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if (onPositive != null) onPositive.click(object);
                        }
                    })
                    .setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (onNegative != null) onNegative.click(object);
                        }
                    });

            return builder.create();
        }
    }
    class SaveBusca extends AsyncTask<String, Void, Void> {

        final private Estudo estudo;
        final private Pesquisador pesquisador;

        public SaveBusca(Estudo estudo, Pesquisador pesquisador) {
            this.estudo = estudo;
            this.pesquisador = pesquisador;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(String... strings) {
            Busca busca = new Busca(null, strings[0]);
            if (estudo == null && pesquisador != null) {
                busca.setPesquisador(pesquisador);
                acesso.putBusca(pesquisador, busca);
            } else if (estudo != null) {
                acesso.putBusca(estudo, busca);
            }
            return null;
        }

    }

    class RemoverBusca extends AsyncTask<Void, Void, Void> {

        private Busca busca;

        public RemoverBusca(Busca busca) {
            this.busca = busca;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... strings) {
            acesso.popBusca(busca);
            return null;
        }

    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        boolean novaBusca = false;
        long last_id = 0;
        List<Busca> buscas = new LinkedList<>();

        public LoadData() { }

        public LoadData(boolean novaBusca) {
            this.novaBusca = novaBusca;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Cursor cursor = null;
            try {
                cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "busca.nm_busca AS busca_nome, " +
                                "busca.id AS busca_id " +
                                "FROM busca " +
                                "ORDER BY busca.id " +
                                "", null);
                while (cursor.moveToNext()) {
                    long idBusca =
                            cursor.getLong(cursor.getColumnIndex("busca_id"));
                    String nomeBusca =
                            cursor.getString(cursor.getColumnIndex("busca_nome"));
                    if (idBusca > last_id) last_id = idBusca;
                    buscas.add(new Busca(idBusca, nomeBusca));
                }
            } catch (Exception e) {

            } finally {
                if (cursor != null) cursor.close();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            total = recyclerViewAdapter_Buscas.buscas.size();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (buscas.size() == 0) return;
                recyclerViewAdapter_Buscas.buscas.clear();
                recyclerViewAdapter_Buscas.buscas.addAll(buscas);
                recyclerViewAdapter_Buscas.notifyDataSetChanged();
                if (novaBusca && last_id > 0) {
                    Busca busca = recyclerViewAdapter_Buscas.getBuscaById(last_id);
                    if (busca != null) buscasFragmentStatePagerAdapter.paginaDeBusca(busca);
                }
            } catch(Exception e) {
            } finally {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
        }
    }

}
