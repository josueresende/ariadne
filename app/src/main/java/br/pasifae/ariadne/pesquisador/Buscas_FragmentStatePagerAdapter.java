package br.pasifae.ariadne.pesquisador;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import br.pasifae.android.support.v4.view.Pasifae_FragmentStatePageAdapter;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.dados.modelo.Voluntario;

/**
 * Created by JosueResende on 08/04/2018.
 */

public class Buscas_FragmentStatePagerAdapter extends Pasifae_FragmentStatePageAdapter {

    enum FRAGMENT_NAME {
        LISTA, EDICAO, RESULTADOS, MENSAGEM, PERFIL
    }

    FRAGMENT_NAME[] index = new FRAGMENT_NAME[10];
    ViewPager viewPager;
    Busca busca;
    Usuario usuario;
    Voluntario voluntario;

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (; count < index.length; count++) {
            if (index[count] == null) break;
        }
        return count;
    }

    public Buscas_FragmentStatePagerAdapter(FragmentManager fragmentManager, ViewPager viewPager) {
        super(fragmentManager);
        this.viewPager = viewPager;
        this.viewPager.setOffscreenPageLimit(0);
        index[0] = FRAGMENT_NAME.LISTA;
    }

    @Override
    public Fragment getItem(int position) {
        if (FRAGMENT_NAME.LISTA.equals(index[position])) {
            return new Buscas_ListaFragment()
                    .setViewPager(viewPager)
                    .setFragmentStatePagerAdapter(this)
                    ;
        } else  if (FRAGMENT_NAME.RESULTADOS.equals(index[position])) {
            return new Buscas_ResultadosFragment()
                    .setViewPager(viewPager)
                    .setFragmentStatePagerAdapter(this)
                    .setBusca(busca)
                    ;
        } else  if (FRAGMENT_NAME.EDICAO.equals(index[position])) {
            return new Buscas_EdicaoFragment()
                    .setViewPager(viewPager)
                    .setFragmentStatePagerAdapter(this)
                    .setBusca(busca)
                    ;
        } else  if (FRAGMENT_NAME.MENSAGEM.equals(index[position])) {
            return new Conversas_MensagensFragment()
                    .setViewPager(viewPager)
                    .setUsuario(usuario)
                    ;
        } else  if (FRAGMENT_NAME.PERFIL.equals(index[position])) {
            return new Buscas_VoluntarioFragment()
                    .setFragmentStatePagerAdapter(this)
                    .setViewPager(viewPager)
                    .setUsuario(usuario)
                    .setVoluntario(voluntario)
                    ;
        }
        return null;
    }

    public void paginaDoVoluntario(Voluntario item) {
        int x = 2;
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.PERFIL; // TODO pagina do perfil
        this.usuario = item.getUsuario();
        this.voluntario = item;
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

    public void paginaDeMensagem(Usuario item) {
        int x = 3;
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.MENSAGEM;
        this.usuario = item;
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

    public void paginaDeBusca(Busca busca) {
        int x = 1;
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.EDICAO;
        this.busca = busca;
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

    public void paginaDeResultados(Busca busca) {
        int x = 1;
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.RESULTADOS;
        this.busca = busca;
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }
    public void paginaDeAnuncios(Busca busca) {

    }
    public void removerPaginaDeAnuncios() {

    }

}
