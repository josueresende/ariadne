package br.pasifae.ariadne;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.Jin;
import br.pasifae.ariadne.dados.Mock;
import br.pasifae.ariadne.dados.jins.BuscasDoPesquisador;
import br.pasifae.ariadne.dados.jins.MensagensDoUsuario;
import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.pesquisador.Dashboard_PesquisadorActivity;
import br.pasifae.ariadne.remote.PesquisadorService;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.UsuarioService;
import br.pasifae.ariadne.remote.VoluntarioService;
import br.pasifae.ariadne.security.Keyset;
import br.pasifae.ariadne.voluntario.Dashboard_VoluntarioActivity;
import retrofit2.Call;
import retrofit2.Response;

public class Main_Activity extends AppCompatActivity implements Validator.ValidationListener, TermoCienciaFragment.TermoCienciaDialogListener {
    Acesso acesso;
    Keyset keyset;

    Stack<Integer> layouts = new Stack<Integer>();

    Integer lastLayout;

    ProgressDialog progressDialog;
    Activity mActivity;
    Context mContext;

    boolean cargaCompleta = false;

    @NotEmpty(trim = true, message = "Campo obrigatório")
    @Length(min = 6, trim = true, message = "Seu nome deve ter no mínimo 6 letras")
    private EditText editText_username;
    @NotEmpty(trim = true, message = "Campo obrigatório")
    @Password(min = 6, scheme = Password.Scheme.ANY, message = "Preencha com o mínimo de 6 caracteres")
    private EditText editText_password;
    private RadioGroup radioGroup_usertype;
    private CheckBox checkBox_remember;

    private Button button_login;
    private Button button_register;

    Validator validator;
    boolean failed = false;

    @Override
    public void onValidationSucceeded() {
        this.failed = false;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        this.failed = true;
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages
            if (view instanceof Spinner) {
                Spinner sp = (Spinner) view;
                view = ((LinearLayout) sp.getSelectedView()).getChildAt(0);
                // we are actually interested in the text view spinner has
            } else if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }
    }

    @Override
    public void OnUserSelectPositive() {
        new TermoDeCienciaAsync(
                Acesso.usuarioLogadoComSucesso.getLogin(),
                -1L,
                -1L,
                "true"
        ).execute();
    }

    @Override
    public void OnUserSelectNegative() {
        if (Acesso.usuarioLogadoComSucesso != null) acesso.deslogarUsuario(Acesso.usuarioLogadoComSucesso.getLogin());
        if (progressDialog.isShowing()) progressDialog.dismiss();
        toggleFields(true);
    }

    private View.OnClickListener clickOnLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            //
            if (validator != null) {
                validator.validate();
                if (failed) return;
            }
            //
            new LoginAsyncTask(
                    editText_username.getText().toString(),
                    editText_password.getText().toString(),
                    checkBox_remember.isChecked()
            ).execute();
        }
    };

    private View.OnClickListener clickOnRegister = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            layouts.push(R.layout.activity_registro);
            new FragmentRotate(mActivity).execute();
        }
    };


    private View.OnClickListener clickOnRegistration = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (validator != null) {
                validator.validate();
                if (failed) return;
            }

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            //
            Acesso.TIPO_DE_USUARIO usertype = Acesso.TIPO_DE_USUARIO.VOLUNTARIO;
            if (radioGroup_usertype.getCheckedRadioButtonId() == R.id.radioButtonPesquisador) usertype = Acesso.TIPO_DE_USUARIO.PESQUISADOR;
            //
            new RegisterAsyncTask(
                    usertype.name(),
                    editText_username.getText().toString(),
                    editText_password.getText().toString(),
                    checkBox_remember.isChecked()
            ).execute();
        }
    };

    @SuppressLint("ResourceType")
    @Override
    public void onBackPressed() {
        if (lastLayout == R.layout.activity_registro) {
            layouts.push(R.layout.activity_login);
            new FragmentRotate(this).execute();
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("LOGOUT"));
        if (progressDialog.isShowing()) progressDialog.dismiss();
        toggleFields(true);
    }

    /** **/
    private DatabaseReference firebaseDatabaseReference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        {
            acesso = new Acesso(this);
        }
        {
            mActivity = this;
            mContext = getApplicationContext();
        }
        if (savedInstanceState != null) {
            return;
        }
        {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Aguarde...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
        }
        {
            keyset = Keyset.load();
            if (keyset.noSign()) {
                Logger.getAnonymousLogger().info("Gerando chaves de primeiro acesso");
                keyset.generate();
                Logger.getAnonymousLogger().info("Chaveiro criado: " + keyset.getUID());
                /** AINDA FALTARA PEGAR A CHAVE PUBLICA REMOTA **/
                /** NO MOMENTO DO LOGIN EH FEITO UMA ABERTURA DE SESSAO ADQUIRINDO PUBLICA REMOTA **/
            } else {
                Logger.getAnonymousLogger().info("Chaveiro encontrado: " + keyset.getUID());
            }
        }
        {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new android.transition.Fade());
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        {
            layouts.push(R.layout.activity_login);
            layouts.push(R.layout.activity_fullscreen);
            layouts.push(R.layout.layout_logo_edital);
            layouts.push(R.layout.activity_logo_rebec);
        }
        {
            Ariadne.createNotificationChannel();
        }
        {
            firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            // Read from the database
            DatabaseReference teseu = firebaseDatabaseReference.child("teseu");
            teseu.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild("host") && dataSnapshot.child("host").getValue() != null) {
                        TeseuService.retrofit.setHost(dataSnapshot.child("host").getValue().toString());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
        {
            AsyncTask asyncTask = new DatabaseCache_Update();
            Object[] params = new Void[]{};
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                Logger.getAnonymousLogger().info("*** THREAD_POOL_EXECUTOR ***");
                asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
            } else {
                Logger.getAnonymousLogger().info("*** EXECUTE ***");
                asyncTask.execute(params);
            }
        }
        {
            new FragmentRotate(this).execute();
        }
    }

    private void start_Dashboard_Voluntario() {
        Intent intent = null;
        {
            if (Acesso.usuarioLogadoComSucesso.hasApagarMensagens()) {
                Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper =
                        new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());
                for (String tablename: new String[] {"mensagem_anuncio","mensagem_direta","mensagem"}) {
                    database_user_sqLiteOpenHelper.getWritableDatabase().delete(tablename, "", null);
                }
            }
            if (Acesso.voluntarioLogadoComSucesso.isCienteDoTermo()) {
                intent = new Intent(mContext, Dashboard_VoluntarioActivity.class);
                {
                    try {
                        Jin.newInstance(6000, new Jin.Jin_Param[]{
                                new MensagensDoUsuario()
                        });
                    } catch(Throwable e) {
                        Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
                        intent = null;
                    }
                }
            } else {
                TermoCienciaFragment dialogFragment = new TermoCienciaFragment();
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                dialogFragment.setTextTermo("" +
                    "Entendo que este aplicativo permite que eu encontre pesquisas e pesquisadores do meu interesse " +
                    "mas que minha participação em qualquer pesquisa somente ocorrerá depois de assinar o " +
                    "Termo de Consentimento Livre e Esclarecido que o pesquisador de cada estudo está obrigado a me " +
                    "apresentar com todas as informações que tenho direito."
                );
                dialogFragment.show(getSupportFragmentManager(), "TERMO DE CIÊNCIA");
            }

        }
        if (intent != null) mContext.startActivity(intent);
    }

    private void start_Dashboard_Pesquisador() {
        Intent intent = null;
        {
            if (Acesso.usuarioLogadoComSucesso.hasApagarMensagens()) {
                Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper =
                        new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());
                for (String tablename: new String[] {"mensagem_anuncio","mensagem_direta","mensagem"}) {
                    database_user_sqLiteOpenHelper.getWritableDatabase().delete(tablename, "", null);
                }
            }
            if (Acesso.usuarioLogadoComSucesso.hasApagarBuscas()) {
                Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper =
                        new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());
                for (String tablename: new String[] {"resposta_buscada","busca"}) {
                    database_user_sqLiteOpenHelper.getWritableDatabase().delete(tablename, "", null);
                }
            }

            if (Acesso.pesquisadorLogadoComSucesso.isCienteDoTermo()) {
                intent = new Intent(mContext, Dashboard_PesquisadorActivity.class);

                try {
                    Jin.newInstance(10000, new Jin.Jin_Param[] {
                            new BuscasDoPesquisador(),
                            new MensagensDoUsuario()
                    });
                } catch(Throwable e) {
                    Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
                    intent = null;
                }
            } else {
                TermoCienciaFragment dialogFragment = new TermoCienciaFragment();
                dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialog);
                dialogFragment.setTextTermo("" +
                        "Declaro que todas as informações prestadas, bem com todas as mensagens trocadas com os voluntários " +
                        "referem-se a pesquisas devidamente autorizadas pelos Comitês de Ética em Pesquisa e demais orgãos " +
                        "envolvidos, sendo de minha total responsabilidade quaisquer prejuízos que vier a causar."
                );
                dialogFragment.show(getSupportFragmentManager(), "TERMO DE CIÊNCIA");
            }
        }
//        if (progressDialog.isShowing()) progressDialog.dismiss();
        if (intent != null) mActivity.startActivity(intent);
    }

    class FragmentRotate extends AsyncTask<Void, Void, Void> {

        Activity activity;

        public FragmentRotate(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Mock.sleep(3500);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            View contentView = null;
            if (layouts.isEmpty() == false) {
                if (cargaCompleta == false && layouts.size() == 1 && layouts.get(0) == R.layout.activity_login) {
                    layouts.push(R.layout.activity_fullscreen);
                    layouts.push(R.layout.layout_logo_edital);
                }
                {
                    contentView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
                    contentView.setBackgroundColor(Color.WHITE);
                    if (contentView instanceof ViewGroup) {
                        ((ViewGroup)contentView).removeAllViews();
                    }
                }
                lastLayout = layouts.pop();
                {
                    activity.setContentView(lastLayout);
                    contentView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
                    contentView.setBackgroundColor(Color.WHITE);
                }
                if ((lastLayout == R.layout.activity_login) || (lastLayout == R.layout.activity_registro)) {
                    toggleSaripaarValidator(true);
                } else {
                    toggleSaripaarValidator(false);
                }
                if (lastLayout == R.layout.activity_login) {

                    if (progressDialog.isShowing()) progressDialog.dismiss();

                    editText_username = (AutoCompleteTextView) contentView.findViewById(R.id.email);
                    editText_password = (EditText) contentView.findViewById(R.id.password);
                    checkBox_remember = (CheckBox) contentView.findViewById(R.id.remember);
                    button_login = (Button) contentView.findViewById(R.id.email_sign_in_button);
                    button_register = (Button) contentView.findViewById(R.id.buttonRegistrar);

                    boolean remember = (acesso.relembrarNomeDoUsuario() != null && acesso.relembrarSenhaDoUsuario() != null);

                    checkBox_remember.setChecked(remember);

                    if (remember) {
                        editText_username.setText(acesso.relembrarNomeDoUsuario());
                        editText_password.setText(acesso.relembrarSenhaDoUsuario());
                    }

                    button_login.setOnClickListener(clickOnLogin);
                    button_register.setOnClickListener(clickOnRegister);

                    editText_username.setFocusable(true);
                    editText_username.requestFocus();
                } else if (lastLayout == R.layout.activity_registro) {
                    editText_username = findViewById(R.id.editTextUsername);
                    editText_password  = findViewById(R.id.editTextPassword);
                    checkBox_remember = findViewById(R.id.checkBoxLembrar);
                    button_register = findViewById(R.id.buttonRegistrar);
                    radioGroup_usertype = findViewById(R.id.radioGroupClassificacao);

                    button_register.setOnClickListener(clickOnRegistration);
                }
            }
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (layouts.isEmpty()) {
                if (cargaCompleta) {
                    if (progressDialog.isShowing()) progressDialog.dismiss();
                    return;
                } else if (progressDialog.isShowing() == false) {
                    progressDialog.show();
                }
            }
            new FragmentRotate(activity).execute();
        }
    }

    private void toggleSaripaarValidator(boolean b) {
        if (b) {
            validator = new Validator(this);
            validator.setValidationListener(this);
        } else {
            validator = null;
        }
    }

    class DatabaseCache_Progress {
        int total;
        int position = 0;

        public DatabaseCache_Progress(int total) {
            this.total = total;
        }

        public DatabaseCache_Progress setPosition(int position) {
            this.position = position;
            return this;
        }

        public DatabaseCache_Progress addPosition() {
            this.position++;
            return this;
        }

        public int getPercentual() {
            try {
                return (position / (total / 100));
            } catch(Exception e) {
            }
            return 0;
        }
    }

    class DatabaseCache_Update extends AsyncTask<Void, DatabaseCache_Progress, Void> {
        @Override
        protected void onPreExecute() {
            Logger.getAnonymousLogger().info("*** DATABASE CACHE UPDATE ***");
            Logger.getAnonymousLogger().info("***          START        ***");
            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.ic_baseline_system_update_24px, null));
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Atualizando ...");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.setMessage("Completo ... ");
            Logger.getAnonymousLogger().info("*** DATABASE CACHE UPDATE ***");
            Logger.getAnonymousLogger().info("***          ENDS         ***");
            cargaCompleta = true;
        }

        @Override
        protected void onProgressUpdate(DatabaseCache_Progress... values) {
            DatabaseCache_Progress databaseCache_progress = values[0];
            progressDialog.setMessage(String.format("Atualizando dados ... %d%%", databaseCache_progress.getPercentual()));
            if (databaseCache_progress.getPercentual() > 0 && progressDialog.isShowing() == false) progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            do {
                Logger.getAnonymousLogger().info("*** WAITING TESEU HOST ***");
                Mock.sleep(1000);
            } while (TeseuService.retrofit.hasHost() == false);

            Logger.getAnonymousLogger().info("*** DATABASE CACHE UPDATE DOING ***");

//            Acesso.Database.execDelete("resposta", "", new String[]{});
//            Acesso.Database.execDelete("questao", "", new String[]{});
//            Acesso.Database.execDelete("formulario", "", new String[]{});
//            Acesso.Database.execDelete("questao_formulario", "", new String[]{});

            int idVersaoQuestao = -1;
            int idVersaoResposta = -1;
            int idVersaoFormulario = -1;
            int idVersaoQuestaoFormulario = -1;

            Cursor cursor = null;
            try {
                cursor = Acesso.Database.execSelect("SELECT COUNT(id),MAX(id_versao) FROM questao", null);
                if (cursor.moveToNext()) {
                    if (cursor.getInt(0) > 0) idVersaoQuestao = cursor.getInt(1);
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            } finally {
                try { if (cursor != null) cursor.close(); } catch(Throwable e) { }
            }
            try {
                cursor = Acesso.Database.execSelect("SELECT COUNT(id), MAX(id_versao) FROM resposta", null);
                if (cursor.moveToNext()) {
                    if (cursor.getInt(0) > 0) idVersaoResposta = cursor.getInt(1);
                }
                cursor.close();
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            } finally {
                try { if (cursor != null) cursor.close(); } catch(Throwable e) { }
            }
            try {
                cursor = Acesso.Database.execSelect("SELECT COUNT(id),MAX(id_versao) FROM formulario ", null);
                if (cursor.moveToNext()) {
                    if (cursor.getInt(0) > 0) idVersaoFormulario = cursor.getInt(1);
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            } finally {
                try { if (cursor != null) cursor.close(); } catch(Throwable e) { }
            }
            try {
                cursor = Acesso.Database.execSelect("SELECT COUNT(id),MAX(id_versao) FROM questao_formulario ", null);
                if (cursor.moveToNext()) {
                    if (cursor.getInt(0) > 0) idVersaoQuestaoFormulario = cursor.getInt(1);
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            } finally {
                try { if (cursor != null) cursor.close(); } catch(Throwable e) { }
            }
            try {
                PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);

                List<PesquisadorService.DadosDaQuestao> dadosDasQuestoes = new LinkedList<>();
                List<PesquisadorService.DadosDeResposta> dadosDasRespostas = new LinkedList<>();
                List<PesquisadorService.DadosDeFormulario> dadosDosFormularios = new LinkedList<>();
                List<PesquisadorService.DadosDeQuestaoFormulario> dadosDasQuestoesFormularios = new LinkedList<>();

                {
                    Logger.getAnonymousLogger().info(String.format("\n\n-----------------------------> \tversao da questao > %d\n", idVersaoQuestao));
                    Call<PesquisadorService.Response> call = service.questoes(idVersaoQuestao);// TODO enviar a versao
                    Response<PesquisadorService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        PesquisadorService.Response response = executed.body();
                        if (response.error_code == 0) dadosDasQuestoes = response.questoes;
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    }
                }
                {
                    Logger.getAnonymousLogger().info(String.format("\n\n-----------------------------> \tversao da resposta > %d\n", idVersaoResposta));
                    Call<PesquisadorService.Response> call = service.respostas(idVersaoResposta);
                    Response<PesquisadorService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        PesquisadorService.Response response = executed.body();
                        if (response.error_code == 0) dadosDasRespostas = response.respostas;
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    } else if (executed.errorBody() == null) {
                        Logger.getAnonymousLogger().info(String.format("error_body=%s", new String(executed.errorBody().bytes())));
                    }
                }
                {
                    Logger.getAnonymousLogger().info(String.format("\n\n-----------------------------> \tversao do formulario > %d\n", idVersaoFormulario));
                    Call<PesquisadorService.Response> call = service.formularios(idVersaoFormulario);
                    Response<PesquisadorService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        PesquisadorService.Response response = executed.body();
                        if (response.error_code == 0) {
                            dadosDosFormularios = response.formularios;
                            dadosDasQuestoesFormularios = response.questoesformularios;
                        }
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    } else if (executed.errorBody() == null) {
                        Logger.getAnonymousLogger().info(String.format("error_body=%s", new String(executed.errorBody().bytes())));
                    }
                }
                if (dadosDasQuestoes.size() > 0 || dadosDasRespostas.size() > 0 || dadosDosFormularios.size() > 0 || dadosDasQuestoesFormularios.size() > 0)
                {
                    DatabaseCache_Progress databaseCache_progress = new DatabaseCache_Progress(
                            (dadosDasQuestoes.size() + dadosDasRespostas.size() + dadosDosFormularios.size() + dadosDasQuestoesFormularios.size())
                    );
                    this.publishProgress(databaseCache_progress);
                    {
                        for (PesquisadorService.DadosDaQuestao dados : dadosDasQuestoes) {
                            Logger.getAnonymousLogger().info(String.format("questao_id=%d", dados.questao_id));
                            // TODO verificar se existe a questao pelo id
                            // TODO se _deleted então remover

                            this.publishProgress(databaseCache_progress.addPosition());

                            try {

                                Questao questao = Acesso.Database.getQuestaoById(dados.questao_id);

                                ContentValues contentValues = new ContentValues();
                                contentValues.put("texto", dados.questao_texto);
                                contentValues.put("texto_especifico", dados.questao_especifico);
                                contentValues.put("limite_marcadas", dados.questao_limite_marcadas);
                                contentValues.put("is_criterio", dados.questao_iscriterio);
                                contentValues.put("tag", dados.questao_tag);
                                contentValues.put("config", dados.questao_config);
                                contentValues.put("id_versao", dados._versao);

                                if (questao == null) {
                                    contentValues.put("id", dados.questao_id);
                                    Acesso.Database.execInsert("questao", contentValues);
                                } else {
                                    Acesso.Database.execUpdate("questao", contentValues, String.format("id = %d", dados.questao_id), null);
                                }

                            } catch(Throwable e) {
                                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                    }
                    {
                        for (PesquisadorService.DadosDeResposta dados : dadosDasRespostas) {
                            Logger.getAnonymousLogger().info(String.format("resposta_id=%d", dados.resposta_id));
                            this.publishProgress(databaseCache_progress.addPosition());

                            try {
                                Resposta resposta = Acesso.Database.getRespostaById(dados.resposta_id);

                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id_questao", dados.questao_id);
                                contentValues.put("conteudo", dados.resposta_conteudo);
                                contentValues.put("texto", dados.resposta_texto);
                                contentValues.put("id_versao", dados._versao);
                                contentValues.put("tag", dados.resposta_tag);

                                if (resposta == null) {
                                    contentValues.put("id", dados.resposta_id);
                                    Acesso.Database.execInsert("resposta", contentValues);
                                } else {
                                    Acesso.Database.execUpdate("resposta", contentValues, String.format("id = %d", dados.resposta_id), null);
                                }
                            } catch(Throwable e) {
                                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                    }
                    {
                        for (PesquisadorService.DadosDeFormulario dados : dadosDosFormularios) {
                            Logger.getAnonymousLogger().info(String.format("formulario_id=%d", dados.formulario_id));
                            this.publishProgress(databaseCache_progress.addPosition());

                            try {
                                Formulario formulario = Acesso.Database.getFormularioById(dados.formulario_id);

                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id_versao", dados._versao);
                                contentValues.put("id_resposta_referencia", dados.formulario_id_resposta);
//                                contentValues.put("id_resposta_conclusiva", dados.);
//                                contentValues.put("resposta_texto_livre", dados.);
                                contentValues.put("titulo", dados.formulario_titulo);

                                if (formulario == null) {
                                    contentValues.put("id", dados.formulario_id);
                                    Acesso.Database.execInsert("formulario", contentValues);
                                } else {
                                    Acesso.Database.execUpdate("formulario", contentValues, String.format("id = %d", dados.formulario_id), null);
                                }
                            } catch(Throwable e) {
                                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
                            }
                        }
                    }
                    {
                        for (PesquisadorService.DadosDeQuestaoFormulario dados : dadosDasQuestoesFormularios) {
                            Logger.getAnonymousLogger().info(String.format("questaoformulario_id=%d", dados.questaoformulario_id));
                            this.publishProgress(databaseCache_progress.addPosition());

                            try {
                                cursor = Acesso.Database.execSelect(
                                        "SELECT * FROM questao_formulario WHERE id = ?",
                                        new String[]{String.valueOf(dados.questaoformulario_id)}
                                );
                                boolean update = false;
                                if (cursor.moveToFirst()) update = true;

                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id_versao", dados._versao);
                                contentValues.put("id_questao", dados.questaoformulario_id_questao);
                                contentValues.put("id_formulario", dados.questaoformulario_id_formulario);
                                contentValues.put("ordem_exibicao", dados.questaoformulario_ordem_exibicao);
                                contentValues.put("id", dados.questaoformulario_id);

                                if (update) {
                                    Acesso.Database.execUpdate("questao_formulario", contentValues, String.format("id = %d", dados.questaoformulario_id), null);
                                } else {
                                    Acesso.Database.execInsert("questao_formulario", contentValues);
                                }
                            } catch(Throwable e) {
                                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
                            } finally {
                                try { if (cursor != null) cursor.close(); } catch(Throwable e) { }
                            }
                        }
                    }
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            }
            Logger.getAnonymousLogger().info("*** DATABASE CACHE UPDATE DONE ***");
            return null;
        }
    }

    class LoginAsyncTask extends AsyncTask<Void, Void, Void> {

        private final String username;
        private final String password;
        private final boolean remember;

        Acesso.TIPO_DE_USUARIO tipoDeUsuario;

        private int error_code = 999;
        private String error_description;

        public LoginAsyncTask(String username, String password, boolean remember) {
            this.username = username;
            this.password = password;
            this.remember = remember;
            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.ic_baseline_person_24px, null));
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Entrando...");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            toggleFields(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (error_code == 0) {
                if (Acesso.usuarioLogadoComSucesso.isPesquisador()) {
                    start_Dashboard_Pesquisador();
                } else if (Acesso.usuarioLogadoComSucesso.isVoluntario()) {
                    start_Dashboard_Voluntario();
                }
            } else {
                if (progressDialog.isShowing()) progressDialog.dismiss();
                toggleFields(true);
            }
            if (error_code == 0) {
            } else if (error_code < 100) {
                editText_username.setError(error_description);
            } else if (error_code < 200) {
                editText_password.setError(error_description);
            } else if (error_code < 300) {
                Toast.makeText(mContext, "Não foi possível logar.\n" + error_description, Toast.LENGTH_LONG).show();
            } else if (error_code == 1000) {
                Toast.makeText(mContext, "Tempo de resposta excedido.\nTente mais tarde.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mContext, "Não foi possível logar.\nPor favor, tente mais tarde.", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Keyset keyset = Keyset.load();
                /** SEM ENCRYPT - NAO TEM CHAVE PUBLICA REMOTA **/
                if (keyset.noEncrypt()) {
                    TeseuService service = TeseuService.retrofit.create(TeseuService.class);
                    Call<TeseuService.Response> call = service.openSession(keyset.getPublicKey());
                    Response<TeseuService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        TeseuService.Response response = executed.body();
                        if (response != null && response.pubk != null) {
                            keyset.loadRemotePublicKey(response.pubk);
                        }
                    }
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            }
            try {
                Keyset keyset = Keyset.load();
                if (keyset.noEncrypt()) {
                    Logger.getAnonymousLogger().log(Level.SEVERE, "NO ENCRYPT");
                    return null;
                }
                UsuarioService service = TeseuService.retrofit.create(UsuarioService.class);
                Call<UsuarioService.Response> call = service.loginCriptografado(
                        keyset.encrypt(username),
                        keyset.encrypt(password)
                );
                Response<UsuarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    UsuarioService.Response response = executed.body();
                    this.error_code = response.error_code;
                    this.error_description = response.error_description;
                    if (response.debug != null) {
                        Logger.getAnonymousLogger().info(
                                "\n\tLOGIN\n" + (new String(Base64.decode(response.debug, Base64.DEFAULT)))
                        );
                    }
                    if (response.error_code == 0) {
                        acesso.logarUsuario(response);
                        {
                            this.tipoDeUsuario = Acesso.TIPO_DE_USUARIO.valueOf(Acesso.usuarioLogadoComSucesso.getTipo());
                        }
                        response.usuario.setSenha(password);
                        if (remember) {
                            acesso.lembrarUsuario(response.usuario);
                        } else {
                            acesso.esquecerUsuario();
                        }
                        acesso.cancelaRegistroDeUsuario();
                        acesso.setNomeDoUsuarioLogadoComSucesso(username);
                        return null;
                    } else {
                        Logger.getAnonymousLogger().severe(response.error_description);
                        Logger.getAnonymousLogger().severe(response.error_code + "");
                    }
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(SocketTimeoutException e) {
                this.error_code = 1000;
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            } catch(Throwable e) {
                this.error_code = 999;
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            }
            return null;
        }
    }

    class RegisterAsyncTask extends AsyncTask<Void, Void, Void> {

        private final String usertype;
        private final String username;
        private final String password;
        private final boolean remember;

        private int error_code = 999;
        private String error_description;

        public RegisterAsyncTask(String usertype, String username, String password, boolean remember) {
            this.usertype = usertype;
            this.username = username;
            this.password = password;
            this.remember = remember;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.ic_baseline_person_add_24px, null));
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Registrando...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            Intent intent = null;
            if (error_code == 0) {
                Toast.makeText(mContext, "Registrado com sucesso.", Toast.LENGTH_LONG).show();
//                if (Acesso.TIPO_DE_USUARIO.PESQUISADOR.equals(usertype)){
//                    start_Dashboard_Pesquisador();
//                } else if (Acesso.TIPO_DE_USUARIO.VOLUNTARIO.equals(usertype)) {
//                    start_Dashboard_Voluntario();
//                }
                layouts.push(R.layout.activity_login);
                new FragmentRotate(mActivity).execute();
            } else if (error_code < 100) {
                editText_username.setError(error_description);
            } else if (error_code < 200) {
                editText_password.setError(error_description);
            } else if (error_code < 300) {
                Toast.makeText(mContext, "Não foi possível registrar.\n" + error_description, Toast.LENGTH_LONG).show();
            } else if (error_code == 1000) {
                Toast.makeText(mContext, "Tempo de resposta excedido.\nTente mais tarde.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mContext, "Não foi possível registrar.\nPor favor, tente mais tarde.", Toast.LENGTH_LONG).show();
            }
            if (intent != null) mActivity.startActivity(intent);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Keyset keyset = Keyset.load();

                /** SEM ENCRYPT - NAO TEM CHAVE PUBLICA REMOTA **/
                if (keyset.noEncrypt()) {
                    TeseuService service = TeseuService.retrofit.create(TeseuService.class);
                    Call<TeseuService.Response> call = service.openSession(keyset.getPublicKey());
                    Response<TeseuService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        TeseuService.Response response = executed.body();
                        if (response != null && response.pubk != null) {
                            keyset.loadRemotePublicKey(response.pubk);
                        }
                    }
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            }
            try {
                Keyset keyset = Keyset.load();
                if (keyset.noEncrypt()) {
                    Logger.getAnonymousLogger().log(Level.SEVERE, "NO ENCRYPT");
                    return null;
                }
                UsuarioService service = TeseuService.retrofit.create(UsuarioService.class);
                String mUsername = keyset.encrypt(username);
                String mPassword = keyset.encrypt(password);
                Call<UsuarioService.Response> call = service.registrarCriptografado(usertype, mUsername, mPassword);
                Response<UsuarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    UsuarioService.Response response = executed.body();
                    this.error_code = response.error_code;
                    this.error_description = response.error_description;
                    if (response.error_code == 0) {
                        if (remember) {
                            acesso.lembrarUsuario(new Usuario().setTipo(usertype).setLogin(username).setNome(username).setSenha(password));
                        } else {
                            acesso.esquecerUsuario();
                        }
                        acesso.cancelaRegistroDeUsuario();
                        acesso.setNomeDoUsuarioLogadoComSucesso(username);
                        Acesso.usuarioLogadoComSucesso = null;
                    }
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(SocketTimeoutException e) {
                this.error_code = 1000;
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            }
            return null;
        }
    }
    /***/
    class TermoDeCienciaAsync extends AsyncTask<Void, Void, Void> {

        private Collection<Formulario> formularios;
        String username;
        Long idQuestao;
        Long idResposta;
        String texto;

        int error_code = 1000;
        String error_description = "";

        public TermoDeCienciaAsync(String username, Long idQuestao, Long idResposta, String texto) {
            this.username = username;
            this.idQuestao = idQuestao;
            this.idResposta = idResposta;
            this.texto = texto;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Validando...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            if (error_code == 0) {
                if (Acesso.usuarioLogadoComSucesso.isPesquisador()) {
                    Acesso.pesquisadorLogadoComSucesso.setCienteDoTermo(true);
                    start_Dashboard_Pesquisador();
                } else if (Acesso.usuarioLogadoComSucesso.isVoluntario()) {
                    Acesso.voluntarioLogadoComSucesso.setCienteDoTermo(true);
                    start_Dashboard_Voluntario();
                }
            } else if (error_code < 100) {
                editText_username.setError(error_description);
            } else if (error_code < 200) {
                editText_password.setError(error_description);
            } else if (error_code < 300) {
                Toast.makeText(mContext, "Não foi possível validar.\n" + error_description, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mContext, "Não foi possível validar.\nPor favor, tente mais tarde.", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
            try {
                Call<VoluntarioService.Response> call = service.marcar(username, idQuestao, idResposta, texto);

                Response<VoluntarioService.Response> executed = call.execute();

                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    this.error_code = response.error_code;
                    this.error_description = response.error_description;
                    if (response.debug != null) {
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    }
                }
            } catch(Throwable e) {
                Logger.getAnonymousLogger().log(Level.SEVERE, e.getMessage(), e);
            }
            return null;
        }
    }

    private void toggleFields(boolean toggle) {
        if (editText_username != null) editText_username.setEnabled(toggle);
        if (editText_password != null) editText_password.setEnabled(toggle);
        if (checkBox_remember != null) checkBox_remember.setEnabled(toggle);
        if (button_login != null) button_login.setEnabled(toggle);
        if (button_register != null) button_register.setEnabled(toggle);

    }
}
