package br.pasifae.ariadne.voluntario;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Questionario_FinalFragment extends Questionario_Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            TextView textView = new TextView(getContext());
            linearLayout.addView(textView);

            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout.setBackground(gradientDrawable);
            }

            textView.setText("Parabéns!\n" +
                    "Você atingiu o final das questões.\n\n" +
                    "O preenchimento correto é muito importante.\n" +
                    "Se quiser, revise as questões a qualquer momento.\n\n" +
                    "Aguarde o contato de um pesquisador através das mensagens."
            );
            textView.setTextSize(21);
            textView.setTextColor(getResources().getColor(android.R.color.black));
            textView.setPadding(10,10,10,10);
        }

        return relativeLayout;
    }
}
