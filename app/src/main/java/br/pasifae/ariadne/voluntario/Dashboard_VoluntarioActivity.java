package br.pasifae.ariadne.voluntario;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import java.nio.ByteBuffer;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.Main_Activity;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Jin;
import br.pasifae.ariadne.dados.Mock;
import br.pasifae.ariadne.dados.modelo.Voluntario;

public class Dashboard_VoluntarioActivity extends TabActivity {

    private Acesso acesso;
    private String username;
    private ProgressDialog progressDialog;
    private ListView listViewPrincipal;
    private ImageView imageViewPhoto;
    private Voluntario voluntario;

    private ClickOn clickOn;

    @Override
    protected void onResume() {
        super.onResume();
        Acesso.iniciarLocationListener(this);
        Acesso.verificarPermissoes(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) Acesso.iniciarLocationListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
//        if (getParent() != null) getParent().onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onRestart() {
        // comes back from child activity
        super.onRestart();
        if (clickOn != null && AsyncTask.Status.PENDING.equals(clickOn.getStatus())) {
            clickOn.execute();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dashboard_voluntario);

        {   /** ATUA EM CONJUNTO COM Main_Activity.onNewIntent **/
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.cancel(R.string.app_name);
                finish();
                }
            }, new IntentFilter("LOGOUT"));
        }
        {
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String tab = intent.getStringExtra("TAB");
                    Acesso.Tab.tabHost.setCurrentTabByTag(tab);
                    Mock.sleep(300);
                    long autor_id = intent.getLongExtra("MESSAGE_AUTOR_ID", 0);
                    if (autor_id > 0) {
                        LocalBroadcastManager.getInstance(Ariadne.getAppContext()).sendBroadcast(
                                new Intent("MESSAGE_NOTIFICATION").putExtra("AUTOR_ID", autor_id)
                        );
                    }
                }
            }, new IntentFilter("TAB_CHANGE"));
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);

        acesso = new Acesso(this);

        username = Acesso.usuarioLogadoComSucesso.getLogin();

        voluntario = Acesso.voluntarioLogadoComSucesso; //acesso.getVoluntario(username);

        ((TextView)findViewById(R.id.textViewNome)).setText(
                voluntario.getUsuario().getNome() == null ? voluntario.getUsuario().getLogin() : voluntario.getUsuario().getNome()
        );

        imageViewPhoto = findViewById(R.id.dashBoardVoluntario_imageView);

        if (voluntario.getFoto() != null) {
            Bitmap bitmap_tmp = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
            ByteBuffer buffer = ByteBuffer.wrap(voluntario.getFoto());
            bitmap_tmp.copyPixelsFromBuffer(buffer);
            imageViewPhoto.setImageBitmap(bitmap_tmp);
            imageViewPhoto.invalidate();
        }

        imageViewPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, 0x1000);
            }
        });

        Acesso.Tab.tabHost = findViewById(android.R.id.tabhost);
        Acesso.Tab.tabSpecs.clear();
        {
            TabHost.TabSpec tabSpec = Acesso.Tab.tabHost.newTabSpec("QUESTOES");
            tabSpec.setIndicator("", getDrawable(R.drawable.ic_baseline_list_alt_24px));
            Intent intent = new Intent(getApplicationContext(), Questionario_VoluntarioSliderActivity.class);
//            Intent intent = new Intent(getApplicationContext(), Questionario_SliderFragmentActivity.class);
            intent.putExtra("voluntario", voluntario);
            tabSpec.setContent(intent);
            Acesso.Tab.tabHost.addTab(tabSpec);
            Acesso.Tab.tabSpecs.add(tabSpec);
        }
        {
            TabHost.TabSpec tabSpec = Acesso.Tab.tabHost.newTabSpec("PERFIL");
            tabSpec.setIndicator("", getDrawable(R.drawable.ic_baseline_person_24px));
            Intent intent = new Intent(getApplicationContext(), Perfil_SliderFragmentActivity.class);
            intent.putExtra("voluntario", voluntario);
            tabSpec.setContent(intent);
            Acesso.Tab.tabHost.addTab(tabSpec);
            Acesso.Tab.tabSpecs.add(tabSpec);
        }
        {
            TabHost.TabSpec tabSpec = Acesso.Tab.tabHost.newTabSpec("MENSAGENS");
            tabSpec.setIndicator("", getDrawable(R.drawable.ic_baseline_message_24px));
            Intent intent = new Intent(getApplicationContext(), Conversas_SliderFragmentActivity.class);
            intent.putExtra("voluntario", voluntario);
            tabSpec.setContent(intent);
            Acesso.Tab.tabHost.addTab(tabSpec);
            Acesso.Tab.tabSpecs.add(tabSpec);
        }
        {
            RelativeLayout dashBoardVoluntario_main = findViewById(R.id.dashBoardVoluntario_main);

            FloatingActionButton floatingActionButton = new FloatingActionButton(dashBoardVoluntario_main.getContext());
            dashBoardVoluntario_main.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_close_24px);
                floatingActionButton.setElevation(2);
                floatingActionButton.setColorFilter(Color.WHITE);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.voluntarioPrimaryDark));

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.setMargins(20,50,40,20);

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String username = acesso.relembrarNomeDoUsuario();
                        acesso.deslogarUsuario(username);
                        Jin.getInstance().shutdown();
                        //
                        Intent intent = new Intent(Ariadne.getAppContext(), Main_Activity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0x1000 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

            bitmap =  getResizedBitmap(bitmap, 100, 100);

            Bitmap.Config config = bitmap.getConfig();

            imageViewPhoto.setImageBitmap(bitmap);

            imageViewPhoto.postInvalidate();

            {
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();

                int size = bitmap.getRowBytes() * bitmap.getHeight();
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                bitmap.copyPixelsToBuffer(byteBuffer);
                new ClickOnUpdatePhoto(byteBuffer.array()).execute();
            }
        }
    }
    /** getResizedBitmap method is used to Resized the Image according to custom width and height
     * @param image
     * @param newHeight (new desired height)
     * @param newWidth (new desired Width)
     * @return image (new resized image)
     * */
    public static Bitmap getResizedBitmap(Bitmap image, int newHeight, int newWidth) {
        int width = image.getWidth();
        int height = image.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(image, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }
    abstract class ClickOn extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null) progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null) progressDialog.dismiss();
        }

    }
    class ClickOnUpdatePhoto extends AsyncTask<Void, Void, Void> {

        private byte[] buffer;

        public ClickOnUpdatePhoto(byte[] buffer) {
            this.buffer = buffer;
        }

        @Override
        protected Void doInBackground(Void... voids) {
//            int start = 0;
//            int end = 512;
//            int part = 0;
//            do {
//                byte[] copy = Arrays.copyOfRange(buffer, start, end);
//                Logger.getAnonymousLogger().info("base64 " + (++part) + ": " + Base64.encodeToString(copy, Base64.URL_SAFE | Base64.NO_PADDING));
//                start = end + 1;
//                end = end + 512;
//                if (end > buffer.length) end = buffer.length - 1;
//            } while (start > end);

            String base64 = Base64.encodeToString(buffer, Base64.URL_SAFE | Base64.NO_WRAP);
            acesso.putPhoto(username, base64);
            return null;
        }
    }

}
