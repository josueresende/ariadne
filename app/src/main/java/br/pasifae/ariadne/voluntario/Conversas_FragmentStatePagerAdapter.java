package br.pasifae.ariadne.voluntario;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import br.pasifae.android.support.v4.view.Pasifae_FragmentStatePageAdapter;
import br.pasifae.ariadne.dados.SwipeRecyclerViewAdapter;
import br.pasifae.ariadne.dados.modelo.Mensagem;
import br.pasifae.ariadne.dados.modelo.Usuario;

/**
 * Created by JosueResende on 13/04/2018.
 */

public class Conversas_FragmentStatePagerAdapter extends Pasifae_FragmentStatePageAdapter {

    enum FRAGMENT_NAME {
        LISTA, MENSAGENS
    }

    FRAGMENT_NAME[] index = new FRAGMENT_NAME[10];

    ViewPager viewPager;
    SwipeRecyclerViewAdapter<Usuario> autores;
    SwipeRecyclerViewAdapter<Mensagem> mensagens;

    Usuario usuario;

    public Conversas_FragmentStatePagerAdapter(FragmentManager fragmentManager, ViewPager viewPager) {
        super(fragmentManager);
        this.viewPager = viewPager;
        this.viewPager.setOffscreenPageLimit(0);
        index[0] = FRAGMENT_NAME.LISTA;
    }

    public void paginaDeMensagens(Usuario usuario) {
        for (int i = index.length; i > 1; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[1] = FRAGMENT_NAME.MENSAGENS;
        this.usuario = usuario;
        notifyDataSetChanged();
        viewPager.setCurrentItem(1, true);
        viewPager.refreshDrawableState();
    }

    @Override
    /**
     * Somente execute se nao encontrar o Item
     */
    public Fragment getItem(int position) {
        if (FRAGMENT_NAME.LISTA.equals(index[position])) {
            return new Conversas_ListaFragment()
                    .setFragmentStatePagerAdapter(this)
                    .setViewPager(viewPager)
                    ;
        } else if (FRAGMENT_NAME.MENSAGENS.equals(index[position])) {
            return new Conversas_MensagensFragment()
                    .setFragmentStatePagerAdapter(this)
                    .setViewPager(viewPager)
                    .setUsuario(usuario)
                    ;
        }
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (; count < index.length; count++) {
            if (index[count] == null) break;
        }
        return count;
    }
}
