package br.pasifae.ariadne.voluntario;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import br.com.receitasdecodigo.utils.MaskEditUtil;
import br.pasifae.ariadne.ScreenSlidePagerFragmentAdapter;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.dados.modelo.Voluntario;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.VoluntarioService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by JosueResende on 05/04/2018.
 */

public class Questionario_QuestaoFragment extends Questionario_Fragment {

    final private static int FONT_SIZE = 23;

    private ScrollView scrollView;
    private Questao questao;

    @Override
    public Questionario_QuestaoFragment setPosition(int position) {
        return (Questionario_QuestaoFragment) super.setPosition(position);
    }

    @Override
    public Questionario_QuestaoFragment setPagerAdapter(ScreenSlidePagerFragmentAdapter pageAdapter) {
        return (Questionario_QuestaoFragment) super.setPagerAdapter(pageAdapter);
    }

    abstract class ClickOn extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

    }
    class ClickOnMarcar extends ClickOn {

        private Collection<Formulario> formularios;
        String usuario;
        Long idQuestao;
        Long idResposta;
        String texto;

        public ClickOnMarcar(String usuario, Long idQuestao, Long idResposta, String texto) {
            this.usuario = usuario;
            this.idQuestao = idQuestao;
            this.idResposta = idResposta;
            this.texto = texto;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
            try {
                Call<VoluntarioService.Response> call = service.marcar(usuario, idQuestao, idResposta, texto);
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    if (response.error_code == 0) {
                    }
                    if (response.debug != null) {
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
            try {
                do {
                    Questao questao = Acesso.Database.getQuestaoById(idQuestao);
                    if (questao.getTag() != null && questao.getLimiteDeRespostasMarcadas() == 1) {
                        Field field = Usuario.class.getDeclaredField(questao.getTag().toLowerCase());
                        if (field == null) break;
                        field.setAccessible(true);
                        Resposta resposta = Acesso.Database.getRespostaById(idResposta);
                        if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo()) == false) {
                            texto = (resposta.getTag() == null ? "" : resposta.getTag());
                        }
                        Logger.getAnonymousLogger().info(String.format("Resposta: %s : %s", field.getName(), texto));
                        field.set(Acesso.usuarioLogadoComSucesso, texto);
                    }
                    break;
                } while (true);
            } catch(NoSuchFieldException e) {
            } catch(Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public Questionario_QuestaoFragment setQuestao(Questao questao) {
        this.questao = questao;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (questao != null) {
            {
                TextView textView = new TextView(getContext());
                linearLayout.addView(textView);

                textView.setText((this.position + 1) + ") " + questao.getQuestao());
                textView.setTextSize(FONT_SIZE);
                textView.setTextColor(getResources().getColor(android.R.color.black));
            }
            {
                scrollView = new ScrollView(getContext());
                linearLayout.addView(scrollView);
            }
            {
                if (questao.getLimiteDeRespostasMarcadas() == 1) {
                    montarResposta();
                }
                if (questao.getLimiteDeRespostasMarcadas() != 1) {
                    montarRespostas();
                }
            }
        }
        return relativeLayout;
    }

    private void marca(Long id, Questao questao, Resposta resposta, String texto) {
        Voluntario voluntario = Acesso.voluntarioLogadoComSucesso;

        Logger.getAnonymousLogger().info("voluntario: " + voluntario.getUsername() + " MARCA");
        Logger.getAnonymousLogger().info("questao: " + (questao == null ? "" : questao.getQuestao()));
        Logger.getAnonymousLogger().info("resposta: " + resposta.getResposta());
        Logger.getAnonymousLogger().info("texto: " + texto);

        if (questao.getLimiteDeRespostasMarcadas() == 1) {
            for (Resposta r : questao.getRespostas()) {
                if (r.equals(resposta)) continue;
                if (Resposta.Conteudo.MARCACAO.equals(r.getConteudo())) r.doVoluntario.texto = "false";
                if (Resposta.Conteudo.DURACAO_MESES.equals(r.getConteudo())) r.doVoluntario.texto = "";
            }
        }

        new ClickOnMarcar(voluntario.getUsername(), (questao == null ? null : questao.getId()), resposta.getId(), texto).execute();
    }

    private void desmarca(Long id, Questao questao, Resposta resposta, String texto) {
        Voluntario voluntario = Acesso.voluntarioLogadoComSucesso;

        Logger.getAnonymousLogger().info("voluntario: " + voluntario.getUsername() + " DESMARCA");
        Logger.getAnonymousLogger().info("questao: " + (questao == null ? "" : questao.getQuestao()));
        Logger.getAnonymousLogger().info("resposta: " + resposta.getResposta());
        Logger.getAnonymousLogger().info("texto: " + texto);

        new ClickOnMarcar(voluntario.getUsername(), (questao == null ? null : questao.getId()), resposta.getId(), texto).execute();
    }

    private void montarRespostas() {
        LinearLayout linearLayoutForOptions = new LinearLayout(getContext());
        scrollView.addView(linearLayoutForOptions);

        linearLayoutForOptions.setTag(questao);
        linearLayoutForOptions.setOrientation(LinearLayout.VERTICAL);

        for(final Resposta resposta : questao.getRespostas()) {

            LinearLayout linearLayoutForLine = new LinearLayout(getContext());
            linearLayoutForOptions.addView(linearLayoutForLine);

            CheckBox checkBox = new CheckBox(getContext());
            linearLayoutForLine.addView(checkBox);

            checkBox.setTag(resposta);
            checkBox.setText(resposta.getResposta());
            checkBox.setTextSize(FONT_SIZE);
            checkBox.setTextColor(getResources().getColor(android.R.color.black));

            if (Resposta.Conteudo.MARCACAO.equals(resposta.getConteudo())) {
                montarRespostaDeMarcacao(checkBox, resposta);
            } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
                checkBox.setChecked(resposta.doVoluntario.hasTexto());
                montarRespostaDeTexto(linearLayoutForLine, resposta);
            }

        }
    }

    private void montarRespostaDeMarcacao(CheckBox checkBox, Resposta resposta) {
        checkBox.setChecked(resposta.doVoluntario.isTrue());

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Resposta resposta = (Resposta) view.getTag();
                boolean marcado = ((CheckBox)view).isChecked();
                if (marcado) {
                    resposta.doVoluntario.texto = "true";
                    marca(resposta.getId(),
                            questao,
                            resposta,
                            "true"
                    );
                } else {
                    resposta.doVoluntario.texto = "false";
                    desmarca(resposta.getId(),
                            questao,
                            resposta,
                            "false"
                    );

                }

            }
        });

    }

    private void montarResposta() {
        if (questao.getRespostas().size() == 1) {
            montarUmaOpcao(questao.getRespostas().get(0));
        } else {
            montarVariasOpcoes(questao.getRespostas());
        }
    }

    private void montarVariasOpcoes(List<Resposta> respostas) {
        RadioGroup radioGroup = new RadioGroup(getContext());
        scrollView.addView(radioGroup);

        radioGroup.setOrientation(RadioGroup.VERTICAL);

        for(Resposta resposta : respostas) {

            RadioButton radioButton = new RadioButton(getContext());
            radioGroup.addView(radioButton);

            radioButton.setTag(resposta);
            radioButton.setText(resposta.getResposta());
            radioButton.setTextColor(getResources().getColor(android.R.color.black));

            radioButton.setChecked(resposta.doVoluntario.isTrue());

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Resposta resposta = (Resposta) view.getTag();

                    if (Resposta.Conteudo.DURACAO_MESES.equals(resposta.getConteudo())) {
                        try {
                            Calendar calendar = Calendar.getInstance();
                            int mes = Integer.parseInt(resposta.getResposta().replaceAll("ˆ[0-9]", "")) * -1;
                            calendar.add(Calendar.MONTH, mes);
                            String text = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                            marca(resposta.getId(),
                                questao,
                                resposta,
                                text
                            );
                        } catch (Exception e) {
                        }
                    } else {
                        resposta.doVoluntario.texto = (((RadioButton)view).isChecked() ? "true" : "false");
                        marca(resposta.getId(),
                                questao,
                                resposta,
                                (((RadioButton)view).isChecked() ? "true" : "false")
                        );
                    }
                }
            });
        }
    }

    private void montarUmaOpcao(final Resposta resposta) {

        LinearLayout linearLayoutForOptions = new LinearLayout(getContext());
        scrollView.addView(linearLayoutForOptions);

        linearLayoutForOptions.setTag(questao);
        linearLayoutForOptions.setOrientation(LinearLayout.VERTICAL);

        if (resposta.getResposta() != null && resposta.getResposta().length() > 0) {
            TextView textView = new TextView(getContext());
            linearLayoutForOptions.addView(textView);
            textView.setText(resposta.getResposta());
        }

        if (Resposta.Conteudo.DATA.equals(resposta.getConteudo())) {
            montarRespostaDeData(linearLayoutForOptions, resposta);
        } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
            montarRespostaDeTexto(linearLayoutForOptions, resposta);
        }
    }

    private void montarRespostaDeTexto(LinearLayout linearLayoutForOptions, Resposta resposta) {
        EditText editText = new EditText(getContext());
        linearLayoutForOptions.addView(editText);

        editText.setTag(resposta);
        editText.setTextSize(FONT_SIZE);

        if (resposta.doVoluntario.hasTexto()) {
            editText.setText(resposta.doVoluntario.texto);
        }

        editText.setSingleLine();
        editText.setTextColor(getResources().getColor(android.R.color.black));

        if (resposta.getTag() != null && "CEP".equals(resposta.getTag())) {
            editText.addTextChangedListener(MaskEditUtil.mask(editText, "#####-###"));
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent keyEvent) {
                if ((actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) &&
//                                                keyEvent != null &&
//                                                keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
//                                                keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER &&
                        true
                        ) {

                    if (keyEvent == null || !keyEvent.isShiftPressed()) {
                        // the user is done typing.
                        Resposta resposta = (Resposta) view.getTag();
                        if (view.getText().toString().length() == 0) {
                            resposta.doVoluntario.texto = "";
                            desmarca(resposta.getId(),
                                    questao,
                                    resposta,
                                    view.getText().toString()
                            );
                        } else {
                            resposta.doVoluntario.texto = view.getText().toString();
                            marca(resposta.getId(),
                                    questao,
                                    resposta,
                                    view.getText().toString()
                            );
                        }
                        return true; // consume.
                    }
                }
                return false; // pass on to other listeners.
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                EditText editText = (EditText) view;
                if (hasFocus == false) {
                    Resposta resposta = (Resposta) editText.getTag();
                    if (editText.getText().toString().length() == 0) {
                        resposta.doVoluntario.texto = "";
                        desmarca(resposta.getId(),
                                questao,
                                resposta,
                                editText.getText().toString()
                        );
                    } else {
                        resposta.doVoluntario.texto = editText.getText().toString();
                        marca(resposta.getId(),
                                questao,
                                resposta,
                                editText.getText().toString()
                        );
                    }
                }
            }
        });
    }

    private void montarRespostaDeData(LinearLayout linearLayoutForOptions, Resposta resposta) {
        EditText editText = new EditText(getContext());
        linearLayoutForOptions.addView(editText);

        editText.setTag(resposta);
        editText.setClickable(true);
        editText.setFocusable(false);
        editText.setHint("dd/mm/yyyy");
        editText.setTextColor(getResources().getColor(android.R.color.black));
        editText.setTextSize(FONT_SIZE);

        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(resposta.doVoluntario.texto);
            editText.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        } catch (Exception e) {
        }

        final DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Object o = view.getTag();
                if (o instanceof EditText) {
                    EditText editText = (EditText) o;
                    ((EditText) o).setText(new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime()));
                    if (editText.getTag() instanceof Resposta) {
                        Resposta resposta = (Resposta) editText.getTag();
                        resposta.doVoluntario.texto = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        marca(resposta.getId(),
                                questao,
                                resposta,
                                new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime())
                        );
                    }
                }
            }
        };
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                try {
                    Resposta resposta = (Resposta) view.getTag();
                    calendar.setTime(
                            new SimpleDateFormat("yyyy-MM-dd").parse(resposta.doVoluntario.texto)
                    );
                } catch (Exception e) {
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        onDateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                datePickerDialog.getDatePicker().setTag(view);
                datePickerDialog.show();
            }
        });
    }

}
