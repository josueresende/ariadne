package br.pasifae.ariadne.voluntario;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import br.pasifae.ariadne.ScreenSlidePagerFragmentAdapter;

public class Questionario_Fragment extends Fragment {

    protected ScreenSlidePagerFragmentAdapter pageAdapter;
    protected RelativeLayout relativeLayout;
    protected LinearLayout linearLayout;
    protected int position;

    public Questionario_Fragment setPosition(int position) {
        this.position = position;
        return this;
    }

    public Questionario_Fragment setPagerAdapter(ScreenSlidePagerFragmentAdapter pageAdapter) {
        this.pageAdapter = pageAdapter;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        float density = getResources().getDisplayMetrics().density;
        int paddingPixel = (int) (25 * density);
        {
            relativeLayout = new RelativeLayout(getContext());
            relativeLayout.setPadding(10,10,10,50);
        }
        {
            linearLayout = new LinearLayout(getContext());
            relativeLayout.addView(linearLayout);
            linearLayout.setPadding(0, paddingPixel, 10, 5);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            );
        }
        return relativeLayout;
    }

}
