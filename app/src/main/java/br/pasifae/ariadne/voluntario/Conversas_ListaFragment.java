package br.pasifae.ariadne.voluntario;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.voluntario.mensagens.Autores_RecyclerViewAdapter;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by JosueResende on 23/04/2018.
 */

public class Conversas_ListaFragment extends Fragment {
    private Conversas_FragmentStatePagerAdapter fragmentStatePagerAdapter;
    private ViewPager viewPager;

    public Conversas_ListaFragment setFragmentStatePagerAdapter(Conversas_FragmentStatePagerAdapter fragmentStatePagerAdapter) {
        this.fragmentStatePagerAdapter = fragmentStatePagerAdapter;
        return this;
    }

    public Conversas_ListaFragment setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        return this;
    }

    class LoadData extends AsyncTask<Void, Void, Void> {

        private int updated = 0;

        @Override
        protected Void doInBackground(Void... voids) {
            Long id = Acesso.usuarioLogadoComSucesso.getId();
            {
                // TODO o serviço web se encarregará de fazer o match para buscas
            }
            Map<Long, Usuario> autores = new LinkedHashMap<>();
            try {   /** MENSAGENS DIRETAS **/
                Cursor cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "mensagem.tp_usuario AS autor_tipo, " +
                                "mensagem.nm_usuario AS autor_nome, " +
                                "mensagem.id_usuario AS autor_id " +
                                "FROM mensagem " +
                                "INNER JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id " +
                                "WHERE " + (String.format("mensagem_direta.id_usuario = %d", id)) + " " +
                                "ORDER BY mensagem.nm_usuario " +
                                "", null);
                while (cursor.moveToNext()) {
                    updated++;
                    long idAutor =
                            cursor.getLong(cursor.getColumnIndex("autor_id"));
                    String nomeAutor =
                            cursor.getString(cursor.getColumnIndex("autor_nome"));
                    String tipoAutor =
                            cursor.getString(cursor.getColumnIndex("autor_tipo"));

                    Usuario autor = new Usuario(idAutor, nomeAutor).setTipo(tipoAutor);

                    autores.put(autor.getId(), autor);
                }
                cursor.close();
            } catch (Throwable e) { e.printStackTrace(); }
            try {   /** MENSAGENS ENVIADAS **/
                Cursor cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "mensagem_direta.tp_usuario AS leitor_tipo, " +
                                "mensagem_direta.nm_usuario AS leitor_nome, " +
                                "mensagem_direta.id_usuario AS leitor_id " +
                                "FROM mensagem " +
                                "INNER JOIN mensagem_direta ON mensagem_direta.id_mensagem = mensagem.id " +
                                "WHERE " + (String.format("mensagem.id_usuario = %d", id)) + " " +
                                "ORDER BY mensagem_direta.nm_usuario " +
                                "", null);
                while (cursor.moveToNext()) {
                    updated++;
                    long idAutor =
                            cursor.getLong(cursor.getColumnIndex("leitor_id"));
                    String nomeAutor =
                            cursor.getString(cursor.getColumnIndex("leitor_nome"));
                    String tipoAutor =
                            cursor.getString(cursor.getColumnIndex("leitor_tipo"));

                    Usuario autor = new Usuario(idAutor, nomeAutor).setTipo(tipoAutor);

                    autores.put(autor.getId(), autor);
                }
                cursor.close();
            } catch (Throwable e) { e.printStackTrace(); }
            try {   /** ANUNCIOS **/
                String hoje = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                Cursor cursor = database_user_sqLiteOpenHelper
                        .getReadableDatabase()
                        .rawQuery("" +
                                "SELECT DISTINCT " +
                                "mensagem.tp_usuario AS autor_tipo, " +
                                "mensagem.nm_usuario AS autor_nome, " +
                                "mensagem.id_usuario AS autor_id, " +
                                "mensagem_anuncio.data_expiracao AS expiracao " +
                                "FROM mensagem " +
                                "INNER JOIN mensagem_anuncio ON mensagem_anuncio.id_mensagem = mensagem.id " +
//                                "WHERE mensagem_anuncio.data_expiracao >= '" + hoje + " 23:59:59' " +
                                "ORDER BY mensagem.nm_usuario " +
                                "", null);
                while (cursor.moveToNext()) {
                    long idAutor =
                            cursor.getLong(cursor.getColumnIndex("autor_id"));
                    String nomeAutor =
                            cursor.getString(cursor.getColumnIndex("autor_nome"));
                    String tipoAutor =
                            cursor.getString(cursor.getColumnIndex("autor_tipo"));

                    if (cursor.isNull(cursor.getColumnIndex("expiracao")) == false) {
                        Date dataDeExpiracao = new Date(cursor.getLong(cursor.getColumnIndex("expiracao")));
                        if (dataDeExpiracao.before(new Date())) continue;
                    }

                    Usuario autor = new Usuario(idAutor, nomeAutor).setTipo(tipoAutor);

                    autores.put(autor.getId(), autor);
                }
                cursor.close();
            } catch (Throwable e) { e.printStackTrace(); }
            recyclerViewAdapter.autores.clear();
            for (Usuario autor : autores.values()) {
                recyclerViewAdapter.autores.add(autor);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (updated > 0) {
                recyclerViewAdapter.notifyDataSetChanged();
            }
            if (Acesso.Tab.dadosDaMensagem != null) {
                Usuario usuario = new Usuario(Acesso.Tab.dadosDaMensagem.autor_id, "");
                Acesso.Tab.dadosDaMensagem = null;
                fragmentStatePagerAdapter.paginaDeMensagens(usuario);
            }
        }


    }

    ConstraintLayout constraintLayout;
    TextView textView_titulo;
    RecyclerView recyclerView_mensagens;
    View view_space;
    LinearLayout linearLayout_input;
    Autores_RecyclerViewAdapter recyclerViewAdapter;
    Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper;
    long idMensagem = -1;

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        {
            // https://blog.sendbird.com/android-chat-tutorial-building-a-messaging-ui
            constraintLayout = new ConstraintLayout(viewPager.getContext());

            constraintLayout.setId(View.generateViewId());
            constraintLayout.setLayoutParams(new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
            constraintLayout.setPadding(5,5,5,5);
            constraintLayout.setBackgroundColor(getResources().getColor(R.color.voluntarioPrimary));
        }
        {
            recyclerView_mensagens = new RecyclerView(viewPager.getContext());
            recyclerView_mensagens.setId(View.generateViewId());
            constraintLayout.addView(recyclerView_mensagens);
            recyclerView_mensagens.setLayoutParams(new ConstraintLayout.LayoutParams(0, 0));

            {
                recyclerView_mensagens.setLayoutManager(
                        new LinearLayoutManager(viewPager.getContext(), LinearLayoutManager.VERTICAL, false)
                );
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(30);
                recyclerView_mensagens.setBackground(gradientDrawable);
            }
            {
                recyclerViewAdapter = new Autores_RecyclerViewAdapter(viewPager.getContext());
                recyclerView_mensagens.setAdapter(recyclerViewAdapter);
            }
            {
                recyclerView_mensagens.addItemDecoration(new DividerItemDecoration(viewPager.getContext(), DividerItemDecoration.VERTICAL));
            }
            {
                recyclerView_mensagens.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView_mensagens.scrollToPosition(recyclerView_mensagens.getAdapter().getItemCount() - 1);
                    }
                });
            }
            {
                recyclerViewAdapter.setOnItemClickListener(new Autores_RecyclerViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(Object item) {
                        if (item instanceof Usuario) {
                            fragmentStatePagerAdapter.notifyDataSetChanged();
                            fragmentStatePagerAdapter.paginaDeMensagens((Usuario) item);
                        }
                    }
                });
            }
        }
        {
            textView_titulo = new TextView(viewPager.getContext());
            textView_titulo.setId(View.generateViewId());
            constraintLayout.addView(textView_titulo);
            textView_titulo.setText("ANÚNCIOS E CONVERSAS");
            textView_titulo.setBackgroundColor(getResources().getColor(R.color.voluntarioPrimary));
            textView_titulo.setTypeface(null, Typeface.BOLD);
            textView_titulo.setPadding(5,5,5,5);
            textView_titulo.setTextColor(Color.WHITE);
        }
/**************************************************************************************************/
        {
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(constraintLayout);

            constraintSet.connect(
                    textView_titulo.getId(),
                    ConstraintSet.TOP,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.TOP,
                    5
            );

//            constraintSet.connect(
//                    linearLayout_input.getId(),
//                    ConstraintSet.BOTTOM,
//                    ConstraintSet.PARENT_ID,
//                    ConstraintSet.BOTTOM,
//                    5
//            );

//            constraintSet.connect(
//                    view_space.getId(),
//                    ConstraintSet.BOTTOM,
//                    linearLayout_input.getId(),
//                    ConstraintSet.TOP,
//                    5
//            );

            constraintSet.connect(
                    recyclerView_mensagens.getId(),
                    ConstraintSet.BOTTOM,
                    ConstraintSet.PARENT_ID,
                    ConstraintSet.BOTTOM,
                    5
            );

            constraintSet.connect(
                    recyclerView_mensagens.getId(),
                    ConstraintSet.TOP,
                    textView_titulo.getId(),
                    ConstraintSet.BOTTOM,
                    5
            );

            for (Integer pos : new Integer[] { ConstraintSet.LEFT, ConstraintSet.RIGHT }) {
                constraintSet.connect(
                        textView_titulo.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
                constraintSet.connect(
                        recyclerView_mensagens.getId(),
                        pos,
                        ConstraintSet.PARENT_ID,
                        pos,
                        5
                );
//                constraintSet.connect(
//                        view_space.getId(),
//                        pos,
//                        ConstraintSet.PARENT_ID,
//                        pos,
//                        5
//                );
//                constraintSet.connect(
//                        linearLayout_input.getId(),
//                        pos,
//                        ConstraintSet.PARENT_ID,
//                        pos,
//                        5
//                );
            }

            constraintSet.constrainWidth(textView_titulo.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
            constraintSet.constrainWidth(recyclerView_mensagens.getId(),
                    ConstraintSet.MATCH_CONSTRAINT);
//            constraintSet.constrainWidth(view_space.getId(),
//                    ConstraintSet.MATCH_CONSTRAINT);
//            constraintSet.constrainWidth(linearLayout_input.getId(),
//                    ConstraintSet.MATCH_CONSTRAINT);

            constraintSet.applyTo(constraintLayout);
        }
        {
            recyclerView_mensagens.setVerticalScrollBarEnabled(true);
            recyclerView_mensagens.scrollToPosition(recyclerView_mensagens.getAdapter().getItemCount() - 1);
        }
/**************************************************************************************************/
        {
            LocalBroadcastManager.getInstance(Ariadne.getAppContext()).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    new LoadData().execute();
                }
            }, new IntentFilter("NEW_MESSAGE"));
        }
        return constraintLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadData().execute();
    }
}
