package br.pasifae.ariadne.voluntario;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.Acesso;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class Perfil_ApresentacaoFragment extends Perfil_Fragment {
    LinearLayout linearLayout_1;
    LinearLayout linearLayout_2;
    LinearLayout linearLayout_3;

    TextView textView_sistema;
    TextView textView_nome;
    TextView textView_estado;
    TextView textView_cidade;
    TextView textView_cep;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            {
                linearLayout_1 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_1);
                linearLayout_1.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_1.setLayoutParams(layoutParams);
                linearLayout_1.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_1.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView);
                textView.setText("INFORMAÇÕES BÁSICAS");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.voluntarioPrimaryLight));
                textView.setBackground(gradientDrawable);
            }
            {
                textView_sistema = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView_sistema);
                textView_sistema.setPadding(5, 5, 5, 5);
            }
            {
                textView_nome = new TextView(viewPager.getContext());
                linearLayout_1.addView(textView_nome);
                textView_nome.setPadding(5, 5, 5, 5);
            }
        }
        {
            View view_space = new View(viewPager.getContext());
            linearLayout.addView(view_space);
            view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
        }
        {
            {
                linearLayout_3 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_3);
                linearLayout_3.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_3.setLayoutParams(layoutParams);
                linearLayout_3.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_3.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView);
                textView.setText("LOCALIDADE");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.voluntarioPrimaryLight));
                textView.setBackground(gradientDrawable);
            }
            {
                textView_estado = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView_estado);
                textView_estado.setPadding(5, 5, 5, 5);
            }
            {
                textView_cidade = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView_cidade);
                textView_cidade.setPadding(5, 5, 5, 5);
            }
            {
                textView_cep = new TextView(viewPager.getContext());
                linearLayout_3.addView(textView_cep);
                textView_cep.setPadding(5, 5, 5, 5);
            }
        }
        {
            View view_space = new View(viewPager.getContext());
            linearLayout.addView(view_space);
            view_space.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 20));
        }
        {
            {
                linearLayout_2 = new LinearLayout(viewPager.getContext());
                linearLayout.addView(linearLayout_2);
                linearLayout_2.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
                linearLayout_2.setLayoutParams(layoutParams);
                linearLayout_2.setOrientation(LinearLayout.VERTICAL);
            }
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setStroke(2, Color.BLACK);
                gradientDrawable.setColor(Color.WHITE);
                gradientDrawable.setCornerRadius(20);
                linearLayout_2.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_2.addView(textView);
                textView.setText("SENHA");
                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.voluntarioPrimaryLight));
                textView.setBackground(gradientDrawable);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout_2.addView(textView);
                textView.setText("Alterar a senha ...");
                textView.setPadding(5, 5, 5, 5);
                textView.setTypeface(null, Typeface.ITALIC);
            }
        }
        {
            linearLayout_2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentStatePagerAdapter.next(Perfil_FragmentStatePagerAdapter.FRAGMENT_NAME.SENHA);
                }
            });
        }
        return relativeLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        String nome =
                Acesso.usuarioLogadoComSucesso.getNome();
        if (nome == null) nome = "";

        {
            textView_sistema.setText("VISUALIZADO COMO: " + Acesso.usuarioLogadoComSucesso.getNomeDeExibicao());
            textView_nome.setText("NOME: " + (nome));
        }
        {
            textView_estado.setText("ESTADO: " + (Acesso.usuarioLogadoComSucesso.estado == null ? "" : Acesso.usuarioLogadoComSucesso.estado));
            textView_cidade.setText("CIDADE: " + (Acesso.usuarioLogadoComSucesso.municipio == null ? "" : Acesso.usuarioLogadoComSucesso.municipio));
            textView_cep.setText("CEP: " + (Acesso.usuarioLogadoComSucesso.cep == null ? "" : Acesso.usuarioLogadoComSucesso.cep));
        }
    }
}
