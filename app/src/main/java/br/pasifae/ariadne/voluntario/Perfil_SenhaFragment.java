package br.pasifae.ariadne.voluntario;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.logging.Logger;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.UsuarioService;
import br.pasifae.ariadne.security.Keyset;
import retrofit2.Call;
import retrofit2.Response;

public class Perfil_SenhaFragment extends Perfil_Fragment {

    @Password
    private EditText editText_senha;

    @ConfirmPassword(message = "As senhas não são iguais")
    private EditText editText_confirmacao;

    class UpdateProfile extends AsyncTask<Void, Void, Void> {
        // TODO similar ao fragmento básico

        String password;

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Atualizando...");
            progressDialog.show();
            Keyset keyset = Keyset.load();
            password = keyset.encrypt(editText_senha.getText().toString());
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            fragmentStatePagerAdapter.before();
            fragmentStatePagerAdapter.reset();

            progressDialog.dismiss();
        }
        private void update() {
            try {
                UsuarioService service = TeseuService.retrofit.create(UsuarioService.class);
                Call<UsuarioService.Response> call =
                        service.atualizarSenha(password);
                Response<UsuarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    UsuarioService.Response response = executed.body();
                    if (response.debug != null) Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().severe(executed.errorBody().string());
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            update();
            return null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        {
            validator = new Validator(this);
            validator.setValidationListener(this);
        }
        {
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("ALTERAR SENHA");
                textView.setPadding(5, 5, 5, 5);

                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.setColor(getResources().getColor(R.color.voluntarioPrimaryLight));
                textView.setBackground(gradientDrawable);

                textView.setTypeface(null, Typeface.BOLD);
                textView.setPadding(5,5,5,5);
                textView.setTextColor(Color.WHITE);
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("NOVA SENHA");
                textView.setPadding(5, 5, 5, 5);

                editText_senha = new EditText(viewPager.getContext());
                linearLayout.addView(editText_senha);
                editText_senha
                        .setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText_senha.setText("");
            }
            {
                TextView textView = new TextView(viewPager.getContext());
                linearLayout.addView(textView);
                textView.setText("CONFIRMAÇÃO");
                textView.setPadding(5, 5, 5, 5);

                editText_confirmacao = new EditText(viewPager.getContext());
                linearLayout.addView(editText_confirmacao);
                editText_confirmacao
                        .setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText_confirmacao.setText("");
            }

        }
        {
            FloatingActionButton floatingActionButton = new FloatingActionButton(viewPager.getContext());
            relativeLayout.addView(floatingActionButton);

            {
                floatingActionButton.setId(View.generateViewId());

                floatingActionButton.setImageResource(R.drawable.ic_baseline_archive_24px);
                floatingActionButton.setElevation(2);

                floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10,10,10,10);

                floatingActionButton.setBackgroundTintList(getResources().getColorStateList(R.color.voluntarioPrimary));
                floatingActionButton.setImageTintList(getResources().getColorStateList(android.R.color.white));

                floatingActionButton.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (validator != null) validator.validate();
                        if (validated)
                        {
                            new UpdateProfile().execute();
                        }
                    }
                });
            }

        }

        return relativeLayout;
    }
}
