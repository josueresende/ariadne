package br.pasifae.ariadne.voluntario;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import br.pasifae.android.support.v4.view.Pasifae_FragmentStatePageAdapter;

public class Perfil_FragmentStatePagerAdapter extends Pasifae_FragmentStatePageAdapter {

    ViewPager viewPager;
    public enum FRAGMENT_NAME {
        APRESENTACAO, SENHA
    }

    FRAGMENT_NAME[] index = new FRAGMENT_NAME[10];

    public void reset() {
        this.index = new FRAGMENT_NAME[10];
        index[0] = FRAGMENT_NAME.APRESENTACAO;
        viewPager.setCurrentItem(0, true);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public Perfil_FragmentStatePagerAdapter(FragmentManager fm, ViewPager viewPager) {
        super(fm);
        this.viewPager = viewPager;
        this.viewPager.setOffscreenPageLimit(0);
        this.reset();
    }

    @Override
    public Fragment getItem(int position) {
        Perfil_Fragment fragment = null;
        if (FRAGMENT_NAME.APRESENTACAO.equals(index[position])) {
            fragment = new Perfil_ApresentacaoFragment();
//        } else if (FRAGMENT_NAME.BASICO.equals(index[position])) {
//            fragment = new Perfil_BasicoFragment();
//        } else if (FRAGMENT_NAME.DOCUMENTO.equals(index[position])) {
//            fragment = new Perfil_DocumentoFragment();
//        } else if (FRAGMENT_NAME.ENDERECO.equals(index[position])) {
//            fragment = new Perfil_EnderecoFragment();
        } else if (FRAGMENT_NAME.SENHA.equals(index[position])) {
            fragment = new Perfil_SenhaFragment();
        }
        if (fragment != null) {
            fragment.setFragmentStatePagerAdapter(this)
                    .setViewPager(viewPager)
            ;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        int count = 0;
        for (; count < index.length; count++) {
            if (index[count] == null) break;
        }
        return count;
    }

    public void next() {
        next(null);
    }

    public void next(Perfil_FragmentStatePagerAdapter.FRAGMENT_NAME fragment) {
        int x =  viewPager.getCurrentItem() + 1;
        if (x >= FRAGMENT_NAME.values().length) {
            return;
        }
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = (fragment == null ? FRAGMENT_NAME.values()[x] : fragment);
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

    public void before() {
        int x =  viewPager.getCurrentItem() - 1;
        if (x < 0) {
            return;
        }
        for (int i = index.length; i > x; i--) {
            if (index[i-1] == null) continue;
            index[i-1] = null;
            destroyItem(viewPager, i);
        }
        index[x] = FRAGMENT_NAME.values()[x];
        notifyDataSetChanged();
        viewPager.setCurrentItem(x, true);
        viewPager.refreshDrawableState();
    }

}
