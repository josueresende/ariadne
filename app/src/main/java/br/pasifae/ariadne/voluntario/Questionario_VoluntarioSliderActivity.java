package br.pasifae.ariadne.voluntario;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.logging.Logger;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.ScreenSlidePagerFragmentAdapter;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.Questao;

public class Questionario_VoluntarioSliderActivity extends FragmentActivity {

    RelativeLayout relativeLayout_questionario;
    ViewPager viewPager;
    FloatingActionButton floatingActionButton_before;
    FloatingActionButton floatingActionButton_next;

    ScreenSlidePagerFragmentAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questoes_voluntario_slider);
        {   /** ATUA EM CONJUNTO COM Main_Activity.onNewIntent **/
            LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    nm.cancel(R.string.app_name);
                    finish();
                }
            }, new IntentFilter("LOGOUT"));
        }

        pagerAdapter = new ScreenSlidePagerFragmentAdapter(getSupportFragmentManager());

        {   // HELP
/**
            select
            questao_formulario.ordem_exibicao,
            formulario.id as id_formulario,
            formulario.titulo,
            resposta.id_questao as id_questao_resposta,
            questao.id as id_questao,
            questao.tag,
            questao.texto,
            'dummy' as dummy
            from questao
            inner join questao_formulario on questao_formulario.id_questao = questao.id
            inner join formulario on formulario.id = questao_formulario.id_formulario
            left join resposta on resposta.id = formulario.id_resposta_referencia
            order by questao_formulario.ordem_exibicao
**/
        }
        {
            for(Formulario formulario : Acesso.voluntarioLogadoComSucesso.formularios.values()) {
                ((ScreenSlidePagerFragmentAdapter)pagerAdapter).add(formulario);
                if (formulario.getResposta() != null) continue;
                for (Questao questao : formulario.getQuestoes()) {
                    ((ScreenSlidePagerFragmentAdapter)pagerAdapter).add(questao);
                }
            }
        }

        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
//        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        viewPager.setPadding(10,0, 10, 90);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Logger.getAnonymousLogger().info("onPageScrolled: " + position);
            }

            @Override
            public void onPageSelected(int position) {
                Logger.getAnonymousLogger().info("onPageSelected: " + position);
                pagerAdapter.checarQuestao(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Logger.getAnonymousLogger().info("onPageScrollStateChanged: " + state);
            }
        });

        relativeLayout_questionario = findViewById(R.id.relativeLayout_questionario);

        {
            floatingActionButton_before = new FloatingActionButton(this);
            relativeLayout_questionario.addView(floatingActionButton_before);

            {
                floatingActionButton_before.setId(View.generateViewId());

                floatingActionButton_before.setImageResource(R.drawable.ic_baseline_navigate_before_24px);
                floatingActionButton_before.setElevation(2);

                floatingActionButton_before.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton_before.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10,10,10,10);

                floatingActionButton_before.setBackgroundTintList(getResources().getColorStateList(R.color.voluntarioPrimary));
                floatingActionButton_before.setImageTintList(getResources().getColorStateList(android.R.color.white));

                floatingActionButton_before.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton_before.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (viewPager.getCurrentItem() == 0) {
                            Toast.makeText(view.getContext(), "Início das questões", Toast.LENGTH_SHORT).show();
                        }
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    }
                });
            }
        }
        {
            floatingActionButton_next = new FloatingActionButton(this);
            relativeLayout_questionario.addView(floatingActionButton_next);

            {
                floatingActionButton_next.setId(View.generateViewId());

                floatingActionButton_next.setImageResource(R.drawable.ic_baseline_navigate_next_24px);
                floatingActionButton_next.setElevation(2);

                floatingActionButton_next.setSize(FloatingActionButton.SIZE_MINI);
                floatingActionButton_next.setFocusable(true);

                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10, 10, 10, 10);

                floatingActionButton_next.setBackgroundTintList(getResources().getColorStateList(R.color.voluntarioPrimary));
                floatingActionButton_next.setImageTintList(getResources().getColorStateList(android.R.color.white));

                floatingActionButton_next.setLayoutParams(layoutParams);
            }
            {
                floatingActionButton_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ((viewPager.getCurrentItem() + 1) >= pagerAdapter.getCount()) {
                            Toast.makeText(view.getContext(), "Fim das questões.\nAguarde o contato do pesquisador", Toast.LENGTH_SHORT).show();
                        }
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    }
                });
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        } else {
            getParent().onBackPressed();
        }
    }

}
