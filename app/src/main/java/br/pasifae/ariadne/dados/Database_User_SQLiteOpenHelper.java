package br.pasifae.ariadne.dados;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.pasifae.ariadne.Ariadne;

/**
 * Created by JosueResende on 23/04/2018.
 */

public class Database_User_SQLiteOpenHelper extends SQLiteOpenHelper {

    public Database_User_SQLiteOpenHelper(String username) {
        super(Ariadne.getAppContext(), ("db-" + username), null, 13);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS mensagem " +
                "(" +
                "id_usuario INT," +
                "nm_usuario TEXT," +
                "tp_usuario TEXT," +
                "data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "conteudo TEXT," +
                "dt_envio TEXT," +
                "tx_extra TEXT," +
//                "uid TEXT PRIMARY KEY," +
                "id INT PRIMARY KEY" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS mensagem_anuncio " +
                "(" +
                "id_mensagem INT," +
                "id_busca INT," +
                "data_expiracao DATE," +
                "dt_expiracao TEXT," +
                "id INT PRIMARY KEY" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS mensagem_direta " +
                "(" +
                "id_mensagem INT," +
                "id_mensagem_referencia INT," +
                "id_usuario INT," +
                "nm_usuario TEXT," +
                "tp_usuario TEXT," +
                "dt_entregue TEXT," +
                "dt_lida TEXT," +
//                "uid_mensagem TEXT," +
//                "uid TEXT PRIMARY KEY," +
                "id INT PRIMARY KEY" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS busca " +
                "(" +
                "id_estudo INT," +
                "id_usuario INT," +
                "nm_usuario TEXT," +
                "tp_usuario TEXT," +
                "data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                "nm_busca TEXT," +
//                "uid TEXT PRIMARY KEY," +
                "id INT PRIMARY KEY" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS resposta_buscada " +
                "(" +
                "id_questao INT," +
                "id_resposta INT," +
                "id_busca INT," +
                "id_mensagem_anuncio INT," +
                "texto TEXT," +
                "id INT PRIMARY KEY" +
                ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (String table : new String[] {
                "resposta_buscada",
                "busca",
                "mensagem_direta",
                "mensagem_anuncio",
                "mensagem"
        }) {
            db.execSQL(String.format("DROP TABLE IF EXISTS %s;", table));
        }
        onCreate(db);
    }

}
