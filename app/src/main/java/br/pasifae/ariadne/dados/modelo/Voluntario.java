package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Voluntario implements HasId, Serializable {
    Long id;
    String username;
    String password;
    String nome;
    Date nascimento;
    private Double distancia;
    private byte[] foto;
    private Usuario usuario;
    public Map<Long, Formulario> formularios = new LinkedHashMap<>();
    final public Map<Long, Questao> perfil = new LinkedHashMap<Long, Questao>();

    public Voluntario() {
    }

    public Voluntario(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Voluntario setUsuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public Double getDistancia() {
        return distancia;
    }

    public Double getDistanciaEmMetros() {
        return (distancia == null ? null : distancia * 1000D);
    }

    public Voluntario setDistancia(Double distancia) {
        this.distancia = distancia;
        return this;
    }

    private boolean cienteDoTermo;
    public void setCienteDoTermo(boolean cienteDoTermo) {
        this.cienteDoTermo = cienteDoTermo;
    }
    public boolean isCienteDoTermo() {
        return cienteDoTermo;
    }
}
