package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;

/**
 * Created by JosueResende on 18/02/2018.
 */

public class RespostaDoVoluntario implements HasId,Serializable {
    private Long id;
    private Voluntario voluntario;
    private Questao questao;
    private Resposta resposta;
    private String texto;

    public Voluntario getVoluntario() {
        return voluntario;
    }

    public void setVoluntario(Voluntario voluntario) {
        this.voluntario = voluntario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RespostaDoVoluntario(Voluntario voluntario, Questao questao, Resposta resposta) {
        this.voluntario = voluntario;
        this.questao = questao;
        this.resposta = resposta;
    }

    public RespostaDoVoluntario(Voluntario voluntario, Questao questao, Resposta resposta, String texto) {
        this.voluntario = voluntario;
        this.questao = questao;
        this.resposta = resposta;
        this.texto = texto;
    }

    public Questao getQuestao() {
        return questao;
    }

    public void setQuestao(Questao questao) {
        this.questao = questao;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public void setBoolean(boolean bool) {
        this.texto = (bool ? "true" : "false");
    }

    public boolean isTrue() { return texto != null && "true".equalsIgnoreCase(texto); }

    public boolean isFalse() { return !isTrue(); }

    public boolean hasTexto() { return texto != null; }

}
