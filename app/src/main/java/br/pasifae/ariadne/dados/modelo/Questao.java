package br.pasifae.ariadne.dados.modelo;

import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Questao implements HasId, Serializable {

    private Long id;
    private Long idFormulario;
    protected String type;
    private String questao;
    private String criterio;
    private Integer limiteDeRespostasMarcadas = 1;
    private List<Resposta> respostas = new LinkedList<Resposta>();
    private String config;
    private Boolean isCriterioDeBusca;
    private String tag;

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof Questao) return ((Questao)o).id.equals(id);
        return super.equals(o);
    }

    public Questao() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Questao(Long id) {
        this.id = id;
    }

    public Questao(Long id, String questao) {
        this.id = id;
        this.questao = questao;
    }

    public Integer getLimiteDeRespostasMarcadas() {
        return limiteDeRespostasMarcadas;
    }

    public Questao setLimiteDeRespostasMarcadas(Integer limiteDeRespostasMarcadas) {
        this.limiteDeRespostasMarcadas = limiteDeRespostasMarcadas;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestao() {
        return questao;
    }

    public void setQuestao(String questao) {
        this.questao = questao;
    }

    @Deprecated
    public List<Resposta> getRespostas() {
        return respostas;
    }

    public void setRespostas(List<Resposta> respostas) {
        this.respostas = respostas;
    }

    public Questao reset() {
        this.respostas.clear();
        return this;
    }

    public Questao addRespostaLivre() {
        addRespostaLivre(Long.valueOf(this.respostas.size()));
        return this;
    }

    public Questao addRespostaFixa(String resposta) {
        addRespostaFixa(Long.valueOf(this.respostas.size()), resposta);
        return this;
    }

    public Questao addRespostaLivre(Long id) {
        addResposta(new Resposta(id, Resposta.Conteudo.TEXTO));
        return this;
    }

    public Questao addRespostaFixa(Long id, String resposta) {
        addResposta(new Resposta(id, Resposta.Conteudo.MARCACAO, resposta));
        return this;
    }

    public Questao addResposta(Resposta resposta) {
        this.respostas.add(resposta);
        return this;
    }

    @Override
    public String toString() {
        return getTituloParaPesquisador();
    }

    public boolean isRespondida() {
        for (Resposta resposta : respostas) {
            if (Resposta.Conteudo.MARCACAO.equals(resposta.getConteudo())) {
                if(resposta.doVoluntario.isTrue()) return true;
            } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
                if(resposta.doVoluntario.hasTexto()) return true;
            } else if (Resposta.Conteudo.DATA.equals(resposta.getConteudo())) {
                if(resposta.doVoluntario.hasTexto()) return true;
            }
        }
        return false;
    }
    public List<Resposta> getRespondidas() {
        List<Resposta> respondidas = new LinkedList<>();
        for (Resposta resposta : respostas) {
            if (Resposta.Conteudo.MARCACAO.equals(resposta.getConteudo())) {
                if(resposta.doVoluntario.isTrue()) respondidas.add(resposta);
            } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
                if(resposta.doVoluntario.hasTexto()) respondidas.add(resposta);
            } else if (Resposta.Conteudo.DATA.equals(resposta.getConteudo())) {
                if(resposta.doVoluntario.hasTexto()) respondidas.add(resposta);
            }
        }
        return respondidas;
    }

    public void desmarcar() {
        for (Resposta resposta : respostas) {
            if (Resposta.Conteudo.MARCACAO.equals(resposta.getConteudo())) {
                resposta.doVoluntario.texto = "false";
            } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
                resposta.doVoluntario.texto = "";
            } else if (Resposta.Conteudo.DATA.equals(resposta.getConteudo())) {
                resposta.doVoluntario.texto = "";
            }
        }
    }

    public String getCriterio() {
        return criterio;
    }

    public Questao setCriterio(String criterio) {
        this.criterio = criterio;
        return this;
    }

    public String getTituloParaPesquisador() {
        if (this.criterio == null) {
            return this.questao;
        }
        return this.criterio;
    }

    public Questao setConfig(String config) {
        this.config = config;
        return this;
    }

    public ConfiguracaoDaQuestao getConfig() {
        if (
            config == null ||
            ((config.startsWith("{") == false || config.endsWith("}") == false))
        ) return new ConfiguracaoDaQuestao();
        return new GsonBuilder().create().fromJson(config, ConfiguracaoDaQuestao.class);
    }

    public String getTag() {
        return tag;
    }

    public Questao setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public Boolean getCriterioDeBusca() {
        return isCriterioDeBusca;
    }

    public Questao setCriterioDeBusca(Boolean criterioDeBusca) {
        isCriterioDeBusca = criterioDeBusca;
        return this;
    }

    public Long getIdFormulario() {
        return idFormulario;
    }

    public Questao setIdFormulario(Long idFormulario) {
        this.idFormulario = idFormulario;
        return this;
    }
}
