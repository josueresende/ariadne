package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Busca;

/**
 * Created by JosueResende on 24/02/2018.
 */

public class BuscasDoPesquisadorAdapter extends ArrayAdapter<Busca> {

    private View.OnClickListener onEdit, onDelete;

    public BuscasDoPesquisadorAdapter(@NonNull Context context, View.OnClickListener onEdit, View.OnClickListener onDelete) {
        super(context, R.layout.item_lista_swipe_editar_apagar);
        this.onEdit = onEdit;
        this.onDelete = onDelete;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(R.layout.item_lista_swipe_editar_apagar, parent, false);

            {
                SwipeRevealLayout swipeRevealLayout = layout.findViewById(R.id.listaBuscas_swipe_id);
            }
            {
                Object tag = new Object[]{
                        this,
                        new Integer(position),
                        getItem(position)
                };

                TextView textView_item = layout.findViewById(R.id.textView_item);
                textView_item.setTag(tag);
                textView_item.setText(getItem(position).getNome());

                if (onEdit != null) {
                    TextView textView_editar = layout.findViewById(R.id.textView_editar);
                    textView_editar.setTag(tag);
                    if (textView_editar.hasOnClickListeners() == false) {
                        textView_editar.setOnClickListener(onEdit);
                    }
                }

                if (onDelete != null) {
                    TextView textView_apagar = layout.findViewById(R.id.textView_apagar);
                    textView_apagar.setTag(tag);
                    if (textView_apagar.hasOnClickListeners() == false) {
                        textView_apagar.setOnClickListener(onDelete);
                    }
                }
            }
        }

        return layout;
    }
}
