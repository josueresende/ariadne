package br.pasifae.ariadne.dados.modelo;

/**
 * Created by JosueResende on 28/02/2018.
 */

public class CriterioDeRespostaMarcada implements Criterio {

    private Long id;
    private Questao questao;
    private Resposta resposta;
    private Busca busca;
    private Pesquisador pesquisador;

    private String texto;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Questao getQuestao() {
        return questao;
    }

    @Override
    public void setQuestao(Questao questao) {
        this.questao = questao;
    }

    @Override
    public Resposta getResposta() {
        return resposta;
    }

    @Override
    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }

    @Override
    public Busca getBusca() {
        return busca;
    }

    @Override
    public void setBusca(Busca busca) {
        this.busca = busca;
    }

    @Override
    public Pesquisador getPesquisador() {
        return pesquisador;
    }

    @Override
    public void setPesquisador(Pesquisador pesquisador) {
        this.pesquisador = pesquisador;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
