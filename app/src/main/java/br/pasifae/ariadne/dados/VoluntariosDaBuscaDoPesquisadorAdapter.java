package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Voluntario;

/**
 * Created by JosueResende on 14/03/2018.
 */

public class VoluntariosDaBuscaDoPesquisadorAdapter extends RecyclerView.Adapter<VoluntariosDaBuscaDoPesquisadorAdapter.VoluntariosDaBuscaDoPesquisadorHolder> implements Serializable{

    private Context context;
    private List<Voluntario> cartList;

    public VoluntariosDaBuscaDoPesquisadorAdapter(Context context, List<Voluntario> cartList) {
        this.context = context;
        this.cartList = cartList;
        notifyDataSetChanged();
    }

    public void addAll(List<Voluntario> all) {
        cartList.clear();
        cartList.addAll(all);
        notifyDataSetChanged();
    }

    @Override
    public VoluntariosDaBuscaDoPesquisadorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_voluntario_da_busca_do_pesquisador_item, parent, false);
        return new VoluntariosDaBuscaDoPesquisadorHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VoluntariosDaBuscaDoPesquisadorHolder holder, int position) {
        Voluntario voluntario = cartList.get(position);
        holder.textViewNome.setText(voluntario.getNome());
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public void removeItem(int position) {
        cartList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Voluntario voluntario, int position) {
        cartList.add(voluntario);
        notifyItemInserted(position);
    }

    public class VoluntariosDaBuscaDoPesquisadorHolder extends RecyclerView.ViewHolder {
        private TextView textViewNome;
        public RelativeLayout viewBackground, viewForeground;

        public VoluntariosDaBuscaDoPesquisadorHolder(View itemView) {
            super(itemView);
            textViewNome = itemView.findViewById(R.id.item_voluntarioDaBusca_textView);
            viewBackground = itemView.findViewById(R.id.relativeLayout_background);
            viewForeground = itemView.findViewById(R.id.relativeLayout_foreground);
        }
    }

}
