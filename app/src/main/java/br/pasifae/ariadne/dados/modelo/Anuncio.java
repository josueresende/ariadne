package br.pasifae.ariadne.dados.modelo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 14/04/2018.
 */

public class Anuncio extends Mensagem {

    private Long idMensagem;
    private Busca busca;
    private Date dataDeExpiracao;
    private List<Criterio> criterios = new LinkedList<>();

    public Anuncio() { }

    public Anuncio(Long id, String conteudo) {
        super(id, conteudo);
    }

    public Busca getBusca() {
        return busca;
    }

    public void setBusca(Busca busca) {
        this.busca = busca;
    }

    public Date getDataDeExpiracao() {
        return dataDeExpiracao;
    }

    public void setDataDeExpiracao(String dataDeExpiracao) {
        try {
            this.dataDeExpiracao = new SimpleDateFormat("dd/MM/yyyy").parse(dataDeExpiracao);
            return;
        } catch (ParseException e) {
        }
        try {
            this.dataDeExpiracao = new SimpleDateFormat("yyyy-MM-dd").parse(dataDeExpiracao);
            return;
        } catch (ParseException e) {
        }
        this.dataDeExpiracao = null;
    }

    public Anuncio setDataDeExpiracao(Date dataDeExpiracao) {
        this.dataDeExpiracao = dataDeExpiracao;
        return this;
    }

    public List<Criterio> getCriterios() {
        return criterios;
    }

    public void setCriterios(List<Criterio> criterios) {
        this.criterios = criterios;
    }

    public boolean isCompletoParaEnviar() {
        return (
            this.dataDeExpiracao != null &&
            (this.hasExtra() && this.getExtra("CAAE") != null) &&
            this.criterios.size() > 0 &&
            (this.conteudo != null && this.conteudo.trim().length() > 0)
        );
    }

    public Long getIdMensagem() {
        return idMensagem;
    }

    public Anuncio setIdMensagem(Long idMensagem) {
        this.idMensagem = idMensagem;
        return this;
    }
}
