package br.pasifae.ariadne.dados;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.pasifae.ariadne.R;

/**
 * Created by JosueResende on 25/03/2018.
 */

public class SwipeRevealLayoutAdapter<T> extends RecyclerView.Adapter<SwipeRevealLayoutViewHolder> {

//    private OnSetObjectToItem onSetObjectToItem;
//    private OnEditItem onEditItem;
//    private OnRemoveItem onRemoveItem;
//    private View.OnClickListener onEdit, onDelete;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
    List<T> items = new LinkedList<T>();
    Map<Class, Object> onClickListenerMap = new LinkedHashMap<>();

    public interface OnSetObjectToItem<T> {
        void setItem(View v, T t);
    }

    public interface OnSearchItem<T> {
        void searchItem(View v, T t);
    }

    public interface OnInfoItem<T> {
        void infoItem(View v, T t);
    }

    public interface OnEditItem<T> {
        void editItem(View v, T t);
    }

    public interface OnRemoveItem<T> {
        void removeItem(View v, T t);
    }

    public interface OnShoutItem<T> {
        void shoutItem(View v, T t);
    }

    public SwipeRevealLayoutAdapter(OnSetObjectToItem onSetObjectToItem) {
        viewBinderHelper.setOpenOnlyOne(true);
        onClickListenerMap.put(OnSetObjectToItem.class,
                onSetObjectToItem);
    }

    public SwipeRevealLayoutAdapter(OnSetObjectToItem onSetObjectToItem, OnEditItem onEdit, OnRemoveItem onDelete) {
        viewBinderHelper.setOpenOnlyOne(true);
        onClickListenerMap.put(OnSetObjectToItem.class,
                onSetObjectToItem);
        onClickListenerMap.put(OnEditItem.class,
                onEdit);
        onClickListenerMap.put(OnRemoveItem.class,
                onDelete);
    }

    public <T> SwipeRevealLayoutAdapter setOnEvent(T onEvent) {
        Class[] classes = onEvent.getClass().getInterfaces();
        if (classes.length == 1) onClickListenerMap.put(classes[0], onEvent);
        return this;
    }

    @Override
    public SwipeRevealLayoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_lista_swipe_editar_apagar, parent, false);
        if (onClickListenerMap.containsKey(OnSearchItem.class))  view.findViewById(R.id.textView_procurar).setVisibility(View.VISIBLE);
        if (onClickListenerMap.containsKey(OnEditItem.class))  view.findViewById(R.id.imageView_editar).setVisibility(View.VISIBLE);
        if (onClickListenerMap.containsKey(OnRemoveItem.class))  view.findViewById(R.id.imageView_apagar).setVisibility(View.VISIBLE);
        if (onClickListenerMap.containsKey(OnInfoItem.class))  view.findViewById(R.id.imageView_informar).setVisibility(View.VISIBLE);
        if (onClickListenerMap.containsKey(OnShoutItem.class))  view.findViewById(R.id.imageView_shout).setVisibility(View.VISIBLE);
        view.refreshDrawableState();
        return new SwipeRevealLayoutViewHolder(view);
    }

    private <T> T get(Class<T> clazz) throws IllegalAccessException, InstantiationException {
        if (onClickListenerMap.containsKey(clazz)) {
            return (T) onClickListenerMap.get(clazz);
        }
        return clazz.newInstance();
    }

    @Override
    public void onBindViewHolder(SwipeRevealLayoutViewHolder holder, int position) {
        viewBinderHelper.bind(holder.swipeRevealLayout, String.valueOf(position));

        Object[] tag = new Object[] {
            items.get(position),
            holder
        };

        if (onClickListenerMap.containsKey(OnSetObjectToItem.class)) {
            try {
                get(OnSetObjectToItem.class).setItem(holder.itemView, items.get(position));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }

        if (onClickListenerMap.containsKey(OnEditItem.class)) {
            ImageView imageView = holder.itemView.findViewById(R.id.imageView_editar);
            imageView.setTag(tag);
            if (imageView.hasOnClickListeners() == false) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Object[] tag = (Object[]) v.getTag();
                        ((SwipeRevealLayoutViewHolder)tag[1]).swipeRevealLayout.close(true);
                        try {
                            get(OnEditItem.class).editItem(v, tag[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
        if (onClickListenerMap.containsKey(OnRemoveItem.class)) {
            ImageView imageView = holder.itemView.findViewById(R.id.imageView_apagar);
            imageView.setTag(tag);
            if (imageView.hasOnClickListeners() == false) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Object[] tag = (Object[]) v.getTag();
                        ((SwipeRevealLayoutViewHolder)tag[1]).swipeRevealLayout.close(true);
                        try {
                            get(OnRemoveItem.class).removeItem(v, tag[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
        if (onClickListenerMap.containsKey(OnSearchItem.class)) {
            TextView textView = holder.itemView.findViewById(R.id.textView_procurar);
            textView.setTag(tag);
            if (textView.hasOnClickListeners() == false) {
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Object[] tag = (Object[]) v.getTag();
                        ((SwipeRevealLayoutViewHolder)tag[1]).swipeRevealLayout.close(true);
                        try {
                            get(OnSearchItem.class).searchItem(v, tag[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
//            ImageView imageView = holder.itemView.findViewById(R.id.imageView_procurar);
//            imageView.setTag(tag);
//            if (imageView.hasOnClickListeners() == false) {
//                imageView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Object[] tag = (Object[]) v.getTag();
//                        ((SwipeRevealLayoutViewHolder)tag[1]).swipeRevealLayout.close(true);
//                        try {
//                            get(OnSearchItem.class).searchItem(v, tag[0]);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }
        }
        if (onClickListenerMap.containsKey(OnShoutItem.class)) {
            ImageView imageView = holder.itemView.findViewById(R.id.imageView_shout);
            imageView.setTag(tag);
            if (imageView.hasOnClickListeners() == false) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Object[] tag = (Object[]) v.getTag();
                        ((SwipeRevealLayoutViewHolder)tag[1]).swipeRevealLayout.close(true);
                        try {
                            get(OnShoutItem.class).shoutItem(v, tag[0]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(T t) {
        this.items.add(t);
        notifyItemInserted(items.size() - 1);
    }

    public void remove(T t) {
        int position = items.indexOf(t);
        this.items.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        items.clear();
    }
}
