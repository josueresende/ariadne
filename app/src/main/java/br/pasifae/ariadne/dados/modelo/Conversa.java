package br.pasifae.ariadne.dados.modelo;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Conversa {
    Long id;
    Pesquisador pesquisador;
    Voluntario voluntario;
    List<Mensagem> mensagens = new LinkedList<Mensagem>();

    public Conversa() {
    }

    public Conversa(Long id, Pesquisador pesquisador, Voluntario voluntario, Mensagem[] mensagens) {
        this.id = id;
        this.pesquisador = pesquisador;
        this.voluntario = voluntario;
        if (mensagens != null) {
            for (Mensagem mensagem: mensagens) {
                this.mensagens.add(mensagem);
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pesquisador getPesquisador() {
        return pesquisador;
    }

    public void setPesquisador(Pesquisador pesquisador) {
        this.pesquisador = pesquisador;
    }

    public Voluntario getVoluntario() {
        return voluntario;
    }

    public void setVoluntario(Voluntario voluntario) {
        this.voluntario = voluntario;
    }

    public List<Mensagem> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<Mensagem> mensagens) {
        this.mensagens = mensagens;
    }
}
