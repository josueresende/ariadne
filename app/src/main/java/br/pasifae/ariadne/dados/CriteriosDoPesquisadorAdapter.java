package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Criterio;

/**
 * Created by JosueResende on 25/02/2018.
 */

public class CriteriosDoPesquisadorAdapter extends ArrayAdapter<Criterio> {

    public CriteriosDoPesquisadorAdapter(@NonNull Context context) {
        super(context, R.layout.layout_lista_criterios);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(R.layout.layout_lista_criterios, parent, false);
        }

        Criterio criterio = getItem(position);

        ((TextView)layout.findViewById(R.id.listaCriterios_textView)).setText(
                criterio.getQuestao().getQuestao() + " = " + criterio.getResposta().getResposta()
        );

        return layout;

    }
}
