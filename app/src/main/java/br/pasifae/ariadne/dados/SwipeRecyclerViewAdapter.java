package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 08/04/2018.
 */

public class SwipeRecyclerViewAdapter<T> extends RecyclerView.Adapter<SwipeRecyclerViewAdapter.SwipeRecyclerViewViewHolder> {

    final private List<T> list;
    private final Context context;
    private final OnCreateCardView onCreateCardView;

    public static abstract class SwipeRecyclerViewViewHolder<T> extends RecyclerView.ViewHolder {
        public SwipeRecyclerViewViewHolder(View view) {
            super(view);
            final SwipeRecyclerViewViewHolder me = this;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    me.onClick(v);
                }
            });
        }

        public abstract void onClick(View v);

        public abstract void use(T o);

    }

    public static abstract class SwipeRecyclerCardViewViewHolder<T> extends SwipeRecyclerViewViewHolder<T> {
        public SwipeRecyclerCardViewViewHolder(View view) {
            super(new CardView(view.getContext()));
            CardView cardView = (CardView) itemView;
            cardView.setLayoutParams(new CardView.LayoutParams(CardView.LayoutParams.MATCH_PARENT, 90));
            write(cardView);
        }
        public abstract void write(CardView o);
    }

    public static abstract class SwipeRecyclerRelativeLayoutViewHolder<T> extends SwipeRecyclerViewViewHolder<T> {
        public SwipeRecyclerRelativeLayoutViewHolder(View view) {
            super(new RelativeLayout(view.getContext()));
            RelativeLayout relativeLayout = (RelativeLayout) itemView;
            int i = 0;
            for (; i < 2; i++)
            {
                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(3, RelativeLayout.LayoutParams.MATCH_PARENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10, 10, (i * 5), 10);
                CardView cardView = new CardView(view.getContext());
                relativeLayout.addView(cardView);
                cardView.setLayoutParams(layoutParams);
                cardView.setBackgroundColor(view.getResources().getColor(android.R.color.darker_gray));
            }
            {
                RelativeLayout.LayoutParams layoutParams =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                layoutParams.setMargins(10, 10, (10 + (i * 5)), 10);
                CardView cardView = new CardView(view.getContext());
                relativeLayout.addView(cardView);
                cardView.setLayoutParams(layoutParams);
                cardView.setCardElevation(4);
                write(cardView);
            }
        }
        public abstract void write(CardView o);

    }

    public static interface OnCreateCardView {
        SwipeRecyclerViewAdapter.SwipeRecyclerViewViewHolder set(View view);
    }

    public SwipeRecyclerViewAdapter(Context context, OnCreateCardView onCreateCardView) {
        this(context, onCreateCardView, new LinkedList<T>());
    }

    public SwipeRecyclerViewAdapter(Context context, OnCreateCardView onCreateCardView, List<T> list) {
        this.context = context;
        this.onCreateCardView = onCreateCardView;
        this.list = list;
    }

    public T get(int position) {
        return list.get(position);
    }

    public void add(T o) {
        list.add(o);
        notifyDataSetChanged();
    }

    @Override
    public SwipeRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        RelativeLayout relativeLayout = new RelativeLayout(context);
//
//        RelativeLayout.LayoutParams layoutParams =
//                new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 90);
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//        layoutParams.setMargins(10,10,10,10);

//        CardView cardView = new CardView(context);
//        relativeLayout.addView(cardView);

//        cardView.setLayoutParams(new CardView.LayoutParams(CardView.LayoutParams.MATCH_PARENT, 90));
//        cardView.setLayoutParams(layoutParams);

        if (onCreateCardView != null) {
            return onCreateCardView.set(parent);
        }
        return new SwipeRecyclerViewViewHolder<Object>(parent) {
            @Override
            public void onClick(View v) {

            }

            @Override
            public void use(Object o) {

            }
        };
    }

    @Override
    public void onBindViewHolder(SwipeRecyclerViewViewHolder holder, int position) {
        holder.use(this.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clear() {
        list.clear();
    }
}
