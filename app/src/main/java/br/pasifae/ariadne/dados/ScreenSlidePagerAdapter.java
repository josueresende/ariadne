package br.pasifae.ariadne.dados;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;

/**
 * Created by JosueResende on 06/04/2018.
 */

public class ScreenSlidePagerAdapter extends PagerAdapter {

    final private static int FONT_SIZE = 23;
    
    private Map<Long, List<Formulario>> questoesVinculadas = new LinkedHashMap<>();
    private List<Questao> questoes = new LinkedList<Questao>();

    @Override
    public int getCount() {
        return questoes.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        
        Questao questao = questoes.get(position);
        
        float density = container.getResources().getDisplayMetrics().density;
        int paddingPixel = (int)(25 * density);
        LinearLayout linearLayout = new LinearLayout(container.getContext());
        linearLayout.setPadding(0, paddingPixel, 0, 0);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        {
            TextView textView = new TextView(container.getContext());
            linearLayout.addView(textView);

            textView.setText((position + 1) + ") " + questao.getQuestao());
            textView.setTextSize(FONT_SIZE);
            textView.setTextColor(container.getResources().getColor(android.R.color.black));
        }
        ScrollView scrollView = new ScrollView(container.getContext());
        {
            linearLayout.addView(scrollView);
        }
        {
            if (questao.getLimiteDeRespostasMarcadas() == 1) {
                montarResposta(container.getContext(), scrollView, questao);
            }
            if (questao.getLimiteDeRespostasMarcadas() != 1) {
                montarRespostas(container.getContext(), scrollView, questao);
            }
        }
        return linearLayout;
    }

    public void add(Formulario formulario) {
        if (formulario.getResposta() == null) return;
        if (questoesVinculadas.containsKey(formulario.getResposta().getId()) == false) {
            questoesVinculadas.put(formulario.getResposta().getId(), new LinkedList<Formulario>());
        }
        questoesVinculadas.get(formulario.getResposta().getId()).add(formulario);
    }

    public void add(Questao questao) {
        this.questoes.add(questao);
    }

    private void marca(Long id, Questao questao, Resposta resposta, String texto) {

    }

    private void desmarca(Long id, Questao questao, Resposta resposta, String texto) {

    }

    private void montarRespostas(Context context, ScrollView scrollView, Questao questao) {
        LinearLayout linearLayoutForOptions = new LinearLayout(context);
        scrollView.addView(linearLayoutForOptions);

        linearLayoutForOptions.setTag(questao);
        linearLayoutForOptions.setOrientation(LinearLayout.VERTICAL);

        for(final Resposta resposta : questao.getRespostas()) {

            LinearLayout linearLayoutForLine = new LinearLayout(context);
            linearLayoutForOptions.addView(linearLayoutForLine);

            CheckBox checkBox = new CheckBox(context);
            linearLayoutForLine.addView(checkBox);

            checkBox.setTag(resposta);
            checkBox.setText(resposta.getResposta());
            checkBox.setTextSize(FONT_SIZE);
            checkBox.setTextColor(context.getResources().getColor(android.R.color.black));

            if (Resposta.Conteudo.MARCACAO.equals(resposta.getConteudo())) {
                montarRespostaDeMarcacao(checkBox, questao, resposta);
            } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
                checkBox.setChecked(resposta.doVoluntario.hasTexto());
                montarRespostaDeTexto(context, scrollView, questao, linearLayoutForLine, resposta);
            }

        }
    }

    private void montarRespostaDeMarcacao(CheckBox checkBox, final Questao questao, Resposta resposta) {
        checkBox.setChecked(resposta.doVoluntario.isTrue());

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Resposta resposta = (Resposta) view.getTag();
                boolean marcado = ((CheckBox)view).isChecked();
                if (marcado) {
                    resposta.doVoluntario.texto = "true";
                    marca(resposta.getId(),
                            questao,
                            resposta,
                            "true"
                    );
                } else {
                    resposta.doVoluntario.texto = "false";
                    desmarca(resposta.getId(),
                            questao,
                            resposta,
                            "false"
                    );

                }

            }
        });

    }

    private void montarResposta(Context context, ScrollView scrollView, Questao questao) {
        if (questao.getRespostas().size() == 1) {
            montarUmaOpcao(context, scrollView, questao, questao.getRespostas().get(0));
        } else {
            montarVariasOpcoes(context, scrollView, questao, questao.getRespostas());
        }
    }

    private void montarVariasOpcoes(Context context, ScrollView scrollView, final Questao questao, List<Resposta> respostas) {
        RadioGroup radioGroup = new RadioGroup(context);
        scrollView.addView(radioGroup);

        radioGroup.setOrientation(RadioGroup.VERTICAL);

        for(Resposta resposta : respostas) {

            RadioButton radioButton = new RadioButton(context);
            radioGroup.addView(radioButton);

            radioButton.setTag(resposta);
            radioButton.setText(resposta.getResposta());
            radioButton.setTextColor(context.getResources().getColor(android.R.color.black));

            radioButton.setChecked(resposta.doVoluntario.isTrue());

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Resposta resposta = (Resposta) view.getTag();
                    resposta.doVoluntario.texto = (((RadioButton)view).isChecked() ? "true" : "false");
                    marca(resposta.getId(),
                            questao,
                            resposta,
                            (((RadioButton)view).isChecked() ? "true" : "false")
                    );
                }
            });
        }
    }

    private void montarUmaOpcao(Context context, ScrollView scrollView, final Questao questao, final Resposta resposta) {

        LinearLayout linearLayoutForOptions = new LinearLayout(context);
        scrollView.addView(linearLayoutForOptions);

        linearLayoutForOptions.setTag(questao);
        linearLayoutForOptions.setOrientation(LinearLayout.VERTICAL);

        if (resposta.getResposta() != null && resposta.getResposta().length() > 0) {
            TextView textView = new TextView(context);
            linearLayoutForOptions.addView(textView);
            textView.setText(resposta.getResposta());
        }

        if (Resposta.Conteudo.DATA.equals(resposta.getConteudo())) {
            montarRespostaDeData(context, scrollView, questao, linearLayoutForOptions, resposta);
        } else if (Resposta.Conteudo.TEXTO.equals(resposta.getConteudo())) {
            montarRespostaDeTexto(context, scrollView, questao, linearLayoutForOptions, resposta);
        }
    }

    private void montarRespostaDeTexto(Context context, ScrollView scrollView, final Questao questao, LinearLayout linearLayoutForOptions, Resposta resposta) {
        EditText editText = new EditText(context);
        linearLayoutForOptions.addView(editText);

        editText.setTag(resposta);
        editText.setTextSize(FONT_SIZE);

        if (resposta.doVoluntario.hasTexto()) {
            editText.setText(resposta.doVoluntario.texto);
        }

        editText.setSingleLine();
        editText.setTextColor(context.getResources().getColor(android.R.color.black));

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent keyEvent) {
                if ((actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) &&
//                                                keyEvent != null &&
//                                                keyEvent.getAction() == KeyEvent.ACTION_DOWN &&
//                                                keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER &&
                        true
                        ) {

                    if (keyEvent == null || !keyEvent.isShiftPressed()) {
                        // the user is done typing.
                        Resposta resposta = (Resposta) view.getTag();
//                        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
//                                .hideSoftInputFromWindow(view.getWindowToken(), 0);
                        if (view.getText().toString().length() == 0) {
                            resposta.doVoluntario.texto = "";
                            desmarca(resposta.getId(),
                                    questao,
                                    resposta,
                                    view.getText().toString()
                            );
                        } else {
                            resposta.doVoluntario.texto = view.getText().toString();
                            marca(resposta.getId(),
                                    questao,
                                    resposta,
                                    view.getText().toString()
                            );
                        }
                        return true; // consume.
                    }
                }
                return false; // pass on to other listeners.
            }
        });

    }

    private void montarRespostaDeData(final Context context, ScrollView scrollView, final Questao questao, LinearLayout linearLayoutForOptions, Resposta resposta) {
        EditText editText = new EditText(context);
        linearLayoutForOptions.addView(editText);

        editText.setTag(resposta);
        editText.setClickable(true);
        editText.setFocusable(false);
        editText.setHint("dd/mm/yyyy");
        editText.setTextColor(context.getResources().getColor(android.R.color.black));
        editText.setTextSize(FONT_SIZE);

        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(resposta.doVoluntario.texto);
            editText.setText(new SimpleDateFormat("dd/MM/yyyy").format(date));
        } catch (Exception e) {
        }

        final DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Object o = view.getTag();
                if (o instanceof EditText) {
                    EditText editText = (EditText) o;
                    ((EditText) o).setText(new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime()));
                    if (editText.getTag() instanceof Resposta) {
                        Resposta resposta = (Resposta) editText.getTag();
                        resposta.doVoluntario.texto = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                        marca(resposta.getId(),
                                questao,
                                resposta,
                                new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime())
                        );
                    }
                }
            }
        };
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                try {
                    Resposta resposta = (Resposta) view.getTag();
                    calendar.setTime(
                            new SimpleDateFormat("yyyy-MM-dd").parse(resposta.doVoluntario.texto)
                    );
                } catch (Exception e) {
                }
                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        onDateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                datePickerDialog.getDatePicker().setTag(view);
                datePickerDialog.show();
            }
        });

    }

}
