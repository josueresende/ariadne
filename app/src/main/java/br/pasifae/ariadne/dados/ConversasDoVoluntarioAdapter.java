package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Conversa;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class ConversasDoVoluntarioAdapter extends ArrayAdapter<Conversa> {
    private int resource;

    public ConversasDoVoluntarioAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    @Override
    public void add(@Nullable Conversa object) {
        super.add(object);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(this.resource, parent, false);
            layout.setTag(new ConversasDoVoluntarioAdapter.DataHandler(layout));
        } else {
            //
        }

        if (layout.getTag() instanceof ConversasDoVoluntarioAdapter.DataHandler) {
            ConversasDoVoluntarioAdapter.DataHandler dataHandler = (ConversasDoVoluntarioAdapter.DataHandler) layout.getTag();
            Conversa conversa = getItem(position);
            dataHandler.titulo.setText(conversa.getPesquisador() == null ? " ": conversa.getPesquisador().getNome());
        }

        return layout;
    }

    private class DataHandler {
        TextView titulo;
        public DataHandler(View layout) {
            this.titulo = layout.findViewById(R.id.textViewTitulo);
        }
    }

    private class DataProvider {

    }

}
