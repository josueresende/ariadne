package br.pasifae.ariadne.dados.modelo;

import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Mensagem implements Serializable{

    public static class Extra implements Serializable {

        private Map<String, String> extra = new LinkedHashMap<>();

        static public Extra fromJSON(String extra) {
            try { return new GsonBuilder().create().fromJson(extra, Extra.class); } catch(Exception e) { return null; }
        }
        public String toJSON() {
            try { return new GsonBuilder().create().toJson(this); } catch(Exception e) { return ""; }
        }
        public String getExtra(String s) {
            if (s == null) return "";
            if (extra.containsKey(s.toLowerCase())) return extra.get(s.toLowerCase());
            return "";
        }
        public void putExtra(String s, String x) {
            if (s == null) return;
            if (x == null) return;
            extra.put(s.toLowerCase(), x);
        }

        public boolean hasExtra(String s) {
            if (s == null || s.trim().length() > 0) return false;
            return (extra.containsKey(s.toLowerCase()));
        }
    }

    Long id;
    String conteudo;
    Date dataDeEnvio;
    Date dataDeRecebimento;
    Date dataDeLeitura;
    Interlocutor autor;
    Interlocutor leitor;

    public Extra extras = new Extra();

    public boolean hasExtra() {
        if (extras == null) return false;
        return (extras.extra.size() > 0);
    }

    public boolean hasExtra(String extra) {
        if (extras == null) return false;
        return extras.hasExtra(extra);
    }

    public String getExtra(String extra) {
        if (hasExtra(extra)) return extras.getExtra(extra);
        return "";
    }

    public Mensagem() { }

    public Mensagem(Long id, String conteudo) {
        this.id = id;
        this.conteudo = conteudo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public Date getDataDeEnvio() {
        if (dataDeEnvio == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataDeEnvio);
        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(dataDeEnvio.getTime()));;
        return calendar.getTime();
    }

    public Mensagem setDataDeEnvio(Date dataDeEnvio) {
        this.dataDeEnvio = dataDeEnvio;
        return this;
    }

    public Date getDataDeRecebimento() {
        return dataDeRecebimento;
    }

    public void setDataDeRecebimento(Date dataDeRecebimento) {
        this.dataDeRecebimento = dataDeRecebimento;
    }

    public Date getDataDeLeitura() {
        return dataDeLeitura;
    }

    public void setDataDeLeitura(Date dataDeLeitura) {
        this.dataDeLeitura = dataDeLeitura;
    }

    public Interlocutor getAutor() {
        return autor;
    }

    public Mensagem setAutor(Interlocutor autor) {
        this.autor = autor;
        return this;
    }

    public Interlocutor getLeitor() {
        return leitor;
    }

    public Mensagem setLeitor(Interlocutor leitor) {
        this.leitor = leitor;
        return this;
    }
}
