package br.pasifae.ariadne.dados;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

/**
 * Created by JosueResende on 26/02/2018.
 */

public class OperadoresAdapter extends ArrayAdapter<String> {
    public OperadoresAdapter(@NonNull Context context, int resource) {
        super(context, android.R.layout.simple_spinner_dropdown_item);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout = convertView;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);

            ((CheckedTextView) layout.findViewById(android.R.id.text1)).setText(getItem(position));
        }
        return layout;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout = convertView;
//        if (convertView == null) {
//            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            layout = layoutInflater.inflate(this.resource, parent, false);
//        }
        if (position == 0) {
            layout = super.getDropDownView(position, convertView, parent);
            layout.setBackgroundColor(Color.LTGRAY);
        } else {
            layout = super.getDropDownView(position, null, parent);
        }
        return layout;
    }
}
