package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;

/**
 * Created by JosueResende on 14/02/2018.
 */

public class RespostasDoVoluntarioAdapter extends ArrayAdapter<Resposta> {

    final int resource;
    Questao questao;

    public RespostasDoVoluntarioAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.resource = R.layout.layout_lista_respostas;
    }

    public RespostasDoVoluntarioAdapter setQuestao(Questao questao, List<Resposta> respostas) {
        this.questao = questao;
        for (Resposta resposta : respostas) add(resposta);
        return this;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            if (questao instanceof QuestaoUnica) {
//                layout = layoutInflater.inflate(R.layout.layout_lista_respostas_unica, parent, false);
//                RadioGroup radioGroup = layout.findViewById(R.id.radioGroupRespostaUnica);
//                RadioButton radioButton = new RadioButton(getContext());
//                radioButton.setText(getItem(position).getResposta());
//                radioGroup.addView(radioButton);
//            }
        }

        return layout;
    }
}
