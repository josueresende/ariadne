package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Formulario;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class FormulariosDoVoluntarioAdapter extends ArrayAdapter<Formulario> {

    private int resource;

    public FormulariosDoVoluntarioAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    @Override
    public void add(@Nullable Formulario object) {
        super.add(object);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(this.resource, parent, false);
            layout.setTag(new DataHandler(layout));
        } else {
            //
        }

        if (layout.getTag() instanceof  DataHandler) {
            DataHandler dataHandler = (DataHandler) layout.getTag();
            Formulario formulario = getItem(position);
            dataHandler.titulo.setText(formulario.getTitulo());
        }

        return layout;
    }

    private class DataHandler {
        TextView titulo;
        public DataHandler(View layout) {
            this.titulo = layout.findViewById(R.id.textViewTitulo);
        }
    }

    private class DataProvider {

    }
}
