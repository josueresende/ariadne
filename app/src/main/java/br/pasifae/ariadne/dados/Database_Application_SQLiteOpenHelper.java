package br.pasifae.ariadne.dados;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.pasifae.ariadne.Ariadne;

/**
 * Created by JosueResende on 22/04/2018.
 */

public class Database_Application_SQLiteOpenHelper extends SQLiteOpenHelper {

    public Database_Application_SQLiteOpenHelper() {
        super(Ariadne.getAppContext(), "PARSIFAE", null, 13);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS keyset " +
                "(" +
                "uid TEXT," +
                "local_privateKey BLOB," +
                "local_publicKey BLOB," +
                "remote_publicKey BLOB," +
                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS questao " +
                "(" +
                "id_versao INTEGER DEFAULT 0," +
                "is_criterio INTEGER DEFAULT 1," +
                "tag TEXT," +
                "config TEXT," +
                "texto TEXT," +
                "texto_especifico TEXT," +
                "limite_marcadas INTEGER," +
                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS resposta " +
                "(" +
                "id_versao INTEGER DEFAULT 0," +
                "id_questao INTEGER," +
                "tag TEXT," +
                "conteudo TEXT," +
                "texto TEXT," +
                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS formulario " +
                "(" +
                "id_versao INTEGER DEFAULT 0," +
                "id_resposta_referencia INTEGER DEFAULT NULL," +
                "id_resposta_conclusiva INTEGER DEFAULT NULL," +
                "resposta_texto_livre TEXT," +
                "titulo TEXT," +
                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ");"
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS questao_formulario " +
                "(" +
                "id_versao INTEGER DEFAULT 0," +
                "id_questao INTEGER DEFAULT NULL," +
                "id_formulario INTEGER DEFAULT NULL," +
                "ordem_exibicao TEXT," +
                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (String table: new String[]{"keyset", "resposta","questao","questao_formulario","formulario"}) {
            db.execSQL(
                    String.format("DROP TABLE IF EXISTS %s;", table)
            );
        }
        onCreate(db);
    }
}
