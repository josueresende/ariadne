package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;

/**
 * Created by JosueResende on 15/02/2018.
 */

public interface HasId extends Serializable {
    Long getId();
    void setId(Long id);
}
