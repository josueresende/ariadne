package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import br.pasifae.ariadne.dados.Column;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Formulario implements Serializable, HasId {
    /**
     "id_versao INTEGER DEFAULT 0," +
     "id_resposta_referencia INTEGER DEFAULT NULL," +
     "id_resposta_conclusiva INTEGER DEFAULT NULL," +
     "resposta_texto_livre TEXT," +
     */
    private Long id;
    public Long id_resposta_referencia;
    public Long id_resposta_conclusiva;
    private String titulo;
    private Resposta resposta;
    @Column(name = "resposta_texto_livre")
    private String textoParaRespostaLivre;
    private Resposta conclusao;
    private List<Questao> questoes = new LinkedList<Questao>();

    public Formulario() {

    }

    public Formulario(Long id) {
        this.id = id;
    }

    public Formulario(String titulo) {
        this.titulo = titulo;
    }

    public Formulario(Long id, String titulo, Questao[] questoes) {
        this.id = id;
        this.titulo = titulo;
        if (questoes != null) {
            for (Questao questao: questoes) {
                this.questoes.add(questao);
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Questao> getQuestoes() {
        return questoes;
    }

    public void setQuestoes(List<Questao> questoes) {
        this.questoes = questoes;
    }

    public Formulario clear() {
        this.questoes.clear();
        return this;
    }

    public Formulario addQuestao(String questao, Resposta[] respostas) {
        return addQuestao(Long.valueOf(this.questoes.size()), questao, respostas);
    }

    public Formulario addQuestaoMultipla(String questao, Resposta[] respostas) {
        return addQuestaoMultipla(Long.valueOf(this.questoes.size()), questao, respostas);
    }

    public Formulario addQuestao(Long id, String questao, Resposta[] respostas) {
        return addQuestao(new Questao(id, questao), respostas);
    }

    public Formulario addQuestaoMultipla(Long id, String questao, Resposta[] respostas) {
        return addQuestao(new Questao(id, questao).setLimiteDeRespostasMarcadas(-1), respostas);
    }

    public Formulario addQuestao(Questao questao, Resposta[] respostas) {
        if (respostas != null) {
            for (Resposta resposta : respostas) questao.addResposta(resposta);
        }
        this.questoes.add(questao);
        return this;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public Formulario setResposta(Resposta resposta) {
        this.resposta = resposta;
        return this;
    }

    public String getTextoParaRespostaLivre() {
        return textoParaRespostaLivre;
    }

    public void setTextoParaRespostaLivre(String textoParaRespostaLivre) {
        this.textoParaRespostaLivre = textoParaRespostaLivre;
    }

    public Resposta getConclusao() {
        return conclusao;
    }

    public void setConclusao(Resposta conclusao) {
        this.conclusao = conclusao;
    }

    public Questao getQuestao(long questao_id) {
        if (questoes != null) {
            for (Questao questao: questoes) {
                if (questao.getId().longValue() == questao_id) return questao;
            }
        }
        return null;
    }

    public Formulario setResposta(long questao_referencia_id, long resposta_referencia_id) {
        if (questao_referencia_id > 0 && resposta_referencia_id > 0) {
            this.setResposta(new Resposta(resposta_referencia_id).setQuestao(new Questao(questao_referencia_id)));
        }
        return this;
    }
}
