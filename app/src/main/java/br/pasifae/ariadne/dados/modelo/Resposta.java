package br.pasifae.ariadne.dados.modelo;

import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Resposta implements Serializable,HasId {

    public static enum Conteudo {
        MARCACAO,
        TEXTO,
        DATA,
        DURACAO_MESES,
        GEOLOCALIZACAO,
        NENHUM
    }

    public static class FromJSON {
        public int min;
        public int max;
    }

    public static class DaBusca implements Serializable {
        public String texto;

        public boolean isTrue() { return texto != null && "true".equalsIgnoreCase(texto); }
        public boolean isFalse() { return !isTrue(); }
        public boolean hasTexto() { return texto != null; }
        public boolean hasJSON() { return hasTexto() && texto.startsWith("{") && texto.endsWith("}"); }
        public FromJSON getJSON() {
            return new GsonBuilder().create().fromJson(texto, FromJSON.class);
        }
    }
    public static class DoVoluntario implements Serializable {
        public String texto;

        public boolean isTrue() { return texto != null && "true".equalsIgnoreCase(texto); }
        public boolean isFalse() { return !isTrue(); }
        public boolean hasTexto() { return texto != null; }
    }

    private Long id;
    private String resposta;
    private Conteudo conteudo;
    private Boolean marcado;
    private Questao questao;
    public DoVoluntario doVoluntario = new DoVoluntario();
    public DaBusca daBusca = new DaBusca();
    private String tag;

    public Resposta() { }

    public Resposta(Long id) {
        this.id = id;
        this.conteudo = Conteudo.MARCACAO;
        this.resposta = "";
    }

    public Resposta(Long id, Conteudo conteudo) {
        this.id = id;
        this.conteudo = conteudo;
        this.resposta = "";
    }

    public Resposta(Long id, String resposta) {
        this.id = id;
        this.conteudo = Conteudo.MARCACAO;
        this.resposta = resposta;
    }

    public Resposta(Long id, Conteudo conteudo, String resposta) {
        this.id = id;
        this.conteudo = conteudo;
        this.resposta = resposta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public Boolean getMarcado() {
        return marcado;
    }

    public Resposta setMarcado(Boolean marcado) {
        this.marcado = marcado;
        return this;
    }

    public Resposta setTextoDoVoluntario(String texto) {
        this.doVoluntario.texto = texto;
        return this;
    }

    public Resposta setTextoDaBusca(String texto) {
        this.daBusca.texto = texto;
        return this;
    }

    public Conteudo getConteudo() {
        return conteudo;
    }

    public void setConteudo(Conteudo conteudo) {
        this.conteudo = conteudo;
    }

    public Questao getQuestao() {
        return questao;
    }

    public Resposta setQuestao(Questao questao) {
        this.questao = questao;
        return this;
    }

    public FromJSON getRespostaAsJSON() {
        if (resposta != null && resposta.trim().startsWith("{") && resposta.trim().endsWith("}")) {
            return new GsonBuilder().create().fromJson(resposta.trim(), FromJSON.class);
        }
        return null;
    }

    public String getTag() {
        return tag;
    }

    public Resposta setTag(String tag) {
        this.tag = tag;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof  Resposta) return ((Resposta)o).id.equals(id);
        return false;
    }
}
