package br.pasifae.ariadne.dados.jins;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.Jin;
import br.pasifae.ariadne.remote.MensagemService;
import br.pasifae.ariadne.remote.PesquisadorService;
import br.pasifae.ariadne.remote.TeseuService;
import retrofit2.Call;
import retrofit2.Response;

public class MensagensDoUsuario implements Jin.Jin_Param {

        @Override
        public void execute() throws Exception {

            Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper =
                    new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

            Cursor cursor = database_user_sqLiteOpenHelper.getReadableDatabase().rawQuery("SELECT MAX(id) FROM mensagem", null);

            long last_id = 0;
            if (cursor.moveToFirst()) last_id = cursor.getLong(0);
            Logger.getAnonymousLogger().info(String.format("id = %d", last_id));

            MensagemService service = TeseuService.retrofit.create(MensagemService.class);

            // TODO enviar ultimo id para somente atualizar
            Call<MensagemService.Response> call = service.listarMensagens(last_id);

            Response<MensagemService.Response> executed = call.execute();

            if (executed.isSuccessful()) {
                MensagemService.Response response = executed.body();
                if (response.debug != null) {
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
                if (response != null && response.error_code == 0) {
                    Map<Long, Ariadne.Notificacao> notificacoes = new LinkedHashMap<>();
                    int updates = 0;
                    for (MensagemService.DadosDaMensagem dados : response.mensagens) {
                        if (dados.mensagem_id > 0)
                        {
                            try {
                                Logger.getAnonymousLogger().info(
                                        String.format("[%d:%s]", dados.mensagem_id, dados.mensagem_extra)
                                );

                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id", dados.mensagem_id);
                                contentValues.put("id_usuario", dados.autor_id);
                                contentValues.put("nm_usuario", dados.autor_nome);
                                contentValues.put("tp_usuario", dados.autor_tipo);
                                contentValues.put("data_criacao", dados.mensagem_data_criacao.getTime());
                                contentValues.put("conteudo", dados.mensagem_conteudo);
                                contentValues.put("tx_extra", dados.mensagem_extra);

                                long i =
                                        database_user_sqLiteOpenHelper
                                        .getWritableDatabase()
                                        .insertWithOnConflict("mensagem", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE)
                                ;
                                updates += i < 1 ? 0 : 1
                                ;

                                if ((i > 0) && (dados.autor_id != Acesso.usuarioLogadoComSucesso.getId())) {
                                    Long autor_id = new Long(dados.autor_id);
                                    String autor_nome = String.format("%s %d", dados.autor_tipo.toLowerCase(), dados.autor_id);
                                    if (notificacoes.containsKey(autor_id) == false) notificacoes.put(autor_id, new Ariadne.Notificacao(dados.autor_id, autor_nome));
                                    notificacoes.get(autor_id).add(dados.mensagem_data_criacao.getTime(), dados.mensagem_conteudo);
                                }


                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                        if (dados.anuncio_id > 0)
                        {
                            try {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id", dados.anuncio_id);
                                contentValues.put("id_mensagem", dados.mensagem_id);
                                contentValues.put("id_busca", dados.busca_id);
                                contentValues.put("data_expiracao", dados.anuncio_data_expiracao.getTime());

//                                updates +=
//                                (
                                        database_user_sqLiteOpenHelper
                                        .getWritableDatabase()
                                        .insertWithOnConflict("mensagem_anuncio", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE)
//                                ) < 1 ? 0 : 1
                                ;

                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                        if (dados.mensagem_direta_id > 0)
                        {
                            try {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id", dados.mensagem_direta_id);
                                contentValues.put("id_mensagem", dados.mensagem_id);
                                contentValues.put("id_mensagem_referencia", dados.mensagem_id_referencia);
                                contentValues.put("id_usuario", dados.leitor_id);
                                contentValues.put("nm_usuario", dados.leitor_nome);
                                contentValues.put("tp_usuario", dados.leitor_tipo);

//                                updates +=
//                                (
                                        database_user_sqLiteOpenHelper
                                        .getWritableDatabase()
                                        .insertWithOnConflict("mensagem_direta", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE)
//                                ) < 1 ? 0 : 1
                                ;

                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    for (PesquisadorService.DadosDeResposta dados : response.respostas) {
                        if (dados.anuncio_id == 0) continue;
                        if ("S".equalsIgnoreCase(dados.buscada_deleted))
                        {
                            try {
                                updates += (
                                        database_user_sqLiteOpenHelper
                                                .getWritableDatabase()
                                                .delete("resposta_buscada", String.format("resposta_buscada.id = %d", dados.buscada_id), null)
                                );
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            try {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id", dados.buscada_id);
                                contentValues.put("id_questao", dados.questao_id);
                                contentValues.put("id_resposta", dados.resposta_id);
                                contentValues.put("id_busca", dados.busca_id);
                                contentValues.put("id_mensagem_anuncio", dados.anuncio_id);
                                contentValues.put("texto", dados.buscada_texto);
                                updates += (
                                        database_user_sqLiteOpenHelper
                                                .getWritableDatabase()
                                                .insertWithOnConflict("resposta_buscada", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE)
                                ) < 1 ? 0 : 1
                                ;
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    if (updates > 0) {
                        LocalBroadcastManager.getInstance(Ariadne.getAppContext())
                                .sendBroadcast(new Intent("NEW_MESSAGE"));
                    }

                    if (notificacoes.size() > 0) {
                        for (Ariadne.Notificacao notificacao : notificacoes.values()) {
                            Ariadne.bundleNotification(notificacao);
                        }
                    }
                }
            } else {
                Logger.getAnonymousLogger().info(executed.errorBody().string());
            }
        }

}
