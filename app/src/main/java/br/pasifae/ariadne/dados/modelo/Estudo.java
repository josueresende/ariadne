package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 19/02/2018.
 */

public class Estudo implements Serializable, HasId {
    Long id;
    String nome;
    Pesquisador pesquisador;
    List<Pesquisador> pesquisadores = new LinkedList<Pesquisador>();
    List<Busca> buscas = new LinkedList<Busca>();

    public Estudo(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Estudo(String nome, Pesquisador pesquisador) {
        this.nome = nome;
        this.pesquisador = pesquisador;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pesquisador getPesquisador() {
        return pesquisador;
    }

    public void setPesquisador(Pesquisador pesquisador) {
        this.pesquisador = pesquisador;
    }

    public List<Pesquisador> getPesquisadores() {
        return pesquisadores;
    }

    public void setPesquisadores(List<Pesquisador> pesquisadores) {
        this.pesquisadores = pesquisadores;
    }

    public List<Busca> getBuscas() {
        return buscas;
    }

    public void setBuscas(List<Busca> buscas) {
        this.buscas = buscas;
    }

    public Estudo addBusca(Busca o) {
        if (buscas == null) buscas = new LinkedList<Busca>();
        buscas.add(o);
        return this;
    }
}
