package br.pasifae.ariadne.dados.jins;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;

import java.util.logging.Logger;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.dados.Acesso;
import br.pasifae.ariadne.dados.Database_User_SQLiteOpenHelper;
import br.pasifae.ariadne.dados.Jin;
import br.pasifae.ariadne.remote.PesquisadorService;
import br.pasifae.ariadne.remote.TeseuService;
import retrofit2.Call;
import retrofit2.Response;

public class BuscasDoPesquisador implements Jin.Jin_Param {

    @Override
    public void execute() throws Exception {

        Database_User_SQLiteOpenHelper database_user_sqLiteOpenHelper =
                new Database_User_SQLiteOpenHelper(Acesso.usuarioLogadoComSucesso.getLogin());

        Cursor cursor = database_user_sqLiteOpenHelper.getReadableDatabase()
                .rawQuery("SELECT MAX(id) FROM busca", null);

        long last_id = 0;
        if (cursor.moveToFirst()) last_id = cursor.getLong(0);
        Logger.getAnonymousLogger().info(String.format("id = %d", last_id));

        PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);

        // TODO enviar ultimo id para somente atualizar
        Call<PesquisadorService.Response> call = service.updateBuscas(last_id);

        Response<PesquisadorService.Response> executed = call.execute();
        if (executed.isSuccessful()) {
            PesquisadorService.Response response = executed.body();
            if (response != null) {
                if (response.error_code == 0) {
                    int updates = 0;
                    for (PesquisadorService.DadosDaBusca dados : response.buscas) {
                        if (dados.busca_deleted == null) continue;
                        if (dados.busca_id == 0) continue;
                        if ("S".equalsIgnoreCase(dados.busca_deleted))
                        {
                            try {
                                database_user_sqLiteOpenHelper
                                        .getWritableDatabase()
                                        .delete("resposta_buscada", String.format("resposta_buscada.id_busca = %d", dados.busca_id), null);
                                updates += database_user_sqLiteOpenHelper
                                        .getWritableDatabase()
                                        .delete("busca", String.format("busca.id = %d", dados.busca_id), null);
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            try {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id", dados.busca_id);
                                contentValues.put("nm_busca", dados.busca_nome);
                                updates +=
                                        (database_user_sqLiteOpenHelper
                                                        .getWritableDatabase()
                                                        .insertWithOnConflict("busca", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE)
                                        ) < 1 ? 0 : 1
                                ;
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    for (PesquisadorService.DadosDeResposta dados : response.respostas) {
                        if (dados.busca_deleted == null) continue;
                        if (dados.busca_id == 0) continue;
                        if ("S".equalsIgnoreCase(dados.buscada_deleted))
                        {
                            try {
                                updates += (
                                    database_user_sqLiteOpenHelper
                                            .getWritableDatabase()
                                            .delete("resposta_buscada", String.format("resposta_buscada.id = %d", dados.buscada_id), null)
                                );
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            try {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("id", dados.buscada_id);
                                contentValues.put("id_questao", dados.questao_id);
                                contentValues.put("id_resposta", dados.resposta_id);
                                contentValues.put("id_busca", dados.busca_id);
                                contentValues.put("texto", dados.buscada_texto);
                                updates += (
                                        database_user_sqLiteOpenHelper
                                                .getWritableDatabase()
                                                .insertWithOnConflict("resposta_buscada", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE)
                                        ) < 1 ? 0 : 1
                                ;
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (updates > 0) {
                        LocalBroadcastManager.getInstance(Ariadne.getAppContext())
                                .sendBroadcast(new Intent("NEW_SEARCH"));
                    }
                }
                Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
            }
        } else {
            Logger.getAnonymousLogger().info(executed.errorBody().string());
        }
    }
}
