package br.pasifae.ariadne.dados;

import android.os.AsyncTask;
import android.os.Build;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by JosueResende on 23/04/2018.
 */


public class Jin extends AsyncTask<Jin.Jin_Param, Jin.Jin_Progress, Jin.Jin_Result>  {

    static private Jin INSTANCE;

    private long interval_millis = 1000;
    private Jin_Param[] jin_params;
    private boolean run = true;

    static public Jin getInstance() {
        return INSTANCE;
    }

    static public Jin newInstance(long interval_millis, Jin_Param... jin_params) {
        if (INSTANCE != null) INSTANCE.shutdown();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            Logger.getAnonymousLogger().info("*** THREAD_POOL_EXECUTOR ***");
            INSTANCE = new Jin(interval_millis, jin_params);
            INSTANCE.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jin_params);
        } else {
            throw new ExceptionInInitializerError("NEED A SDK VERSION EQUALS OR OVER HONEYCOMB");
        }
        return INSTANCE;
    }

    private Jin(long interval_millis, Jin_Param... jin_params) {
        this.interval_millis = interval_millis;
        this.jin_params = jin_params;
    }

    @Override
    protected Jin_Result doInBackground(Jin_Param... jin_params) {
        this.jin_params = jin_params;
        while (run) {
            Logger.getAnonymousLogger().info("*** JIN RUNNING ***");
            try {
                for (Jin_Param param : jin_params) {
                    try {
                        Logger.getAnonymousLogger().info("*** JIN " + param.getClass().getSimpleName() + " ***");
                        param.execute();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                Mock.sleep(interval_millis);
            }
        }
        Logger.getAnonymousLogger().info("*** JIN ENDS ***");
        return null;
    }

    public void shutdown() {
        this.run = false;
    }

    static public interface Jin_Param {
        void execute() throws Exception;
    }
    static public class Jin_Progress {

    }
    static public class Jin_Result {

    }

}