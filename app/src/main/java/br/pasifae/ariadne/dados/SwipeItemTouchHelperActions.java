package br.pasifae.ariadne.dados;

/**
 * Created by JosueResende on 09/04/2018.
 */

public abstract class SwipeItemTouchHelperActions {
    public void onLeftClicked(int position) {}
    public void onRightClicked(int position) {}
}
