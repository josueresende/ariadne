package br.pasifae.ariadne.dados;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chauthai.swipereveallayout.SwipeRevealLayout;

import br.pasifae.ariadne.R;

/**
 * Created by JosueResende on 25/03/2018.
 */

public class SwipeRevealLayoutViewHolder extends RecyclerView.ViewHolder {

    SwipeRevealLayout swipeRevealLayout;

    public SwipeRevealLayoutViewHolder(View itemView) {
        super(itemView);
        swipeRevealLayout = itemView.findViewById(R.id.listaBuscas_swipe_id);
    }

}
