package br.pasifae.ariadne.dados.modelo;

/**
 * Created by JosueResende on 19/02/2018.
 */

public interface Criterio extends HasId {
    Busca getBusca();
    void setBusca(Busca busca);
    Pesquisador getPesquisador();
    void setPesquisador(Pesquisador pequisador);
    Questao getQuestao();
    void setQuestao(Questao questao);
    Resposta getResposta();
    void setResposta(Resposta resposta);
    String getTexto();
    void setTexto(String texto);
}
