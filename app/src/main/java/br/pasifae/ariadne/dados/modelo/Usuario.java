package br.pasifae.ariadne.dados.modelo;

import com.google.gson.GsonBuilder;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by JosueResende on 16/03/2018.
 */
public class Usuario implements Interlocutor, Serializable {

    public boolean hasApagarMensagens() {
        return (tester != null && tester.contains("APAGAR_MENSAGENS"));
    }

    public boolean hasApagarBuscas() {
        return (tester != null && tester.contains("APAGAR_BUSCAS"));
    }

    public String getNomeDeExibicao() {
        if (isVoluntario()) return "voluntario " + getId();
        if (isPesquisador()) return "pesquisador " + getId();
        return login;
    }

//    public static class JSON {
//
//        public static class Endereco {
//
//            public String cep;
//            public String via;
//            public String numero;
//            public String complemento;
//            public String bairro;
//            public String cidade;
//            public String estado;
//            public String pais;
//        }
//    }

    public static class Email {
        public String principal;
    }
    public static class Telefone {
        public String principal;
    }
    public static class Endereco {
        public String cep;
        public String via;
        public String numero;
        public String complemento;
        public String bairro;

//        private JSON.Endereco _json = new JSON.Endereco();
//        public String principal;
//        public JSON.Endereco get() {
//            if (principal != null && principal.startsWith("{") && principal.endsWith("}")) {
//                this._json = new GsonBuilder().create().fromJson(principal, JSON.Endereco.class);
//            } else {
//                this._json = new JSON.Endereco();
//            }
//            return this._json;
//        }
//        public void set(JSON.Endereco json) {
//            this._json = json;
//            this.principal = new GsonBuilder().create().toJson(this._json);
//        }
    }
    public static class Documento {
        public String cpf;
        public String cnpj;
        public String passaporte;
    }
    private long id = 0;
    private String tipo;
    private String login;
    public Email email = new Email();
    private String senha;
    private String nome;
    private String sobrenome;
    private String sexo;
    private String data_nascimento;
    private String foto_base64;
    private String termo_ciencia;

    public String regiao;
    public String estado;
    public String municipio;
    public String idioma;

    public String cep;

    public Telefone telefone = new Telefone();
    public Endereco endereco = new Endereco();
    public Documento documento = new Documento();

    public Map<Long, Mensagem> mensagens = new LinkedHashMap<>();

    public String tester;

    public Usuario() {
    }

    public Usuario(Long id, String nome) {
        this.setId(id);
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public Usuario setTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public String getNomeCompleto() {
        if (tipo != null && "voluntario".equalsIgnoreCase(tipo)) {
            return String.format("voluntario %d", id);
        }
        return (nome == null) ? login : ((sobrenome == null) ? nome : nome + " " + sobrenome);
    }

    public String getNome() {
        return nome;
    }

    public Usuario setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getSenha() {
        return senha;
    }

    public Usuario setSenha(String senha) {
        this.senha = senha;
        return this;
    }

    public String getFoto_base64() {
        return foto_base64;
    }

    public void setFoto_base64(String foto_base64) {
        this.foto_base64 = foto_base64;
    }

    public String getLogin() {
        return login;
    }

    public Usuario setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDataNascimento() {
        return data_nascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.data_nascimento = dataNascimento;
    }

    public String getTermoCiencia() {
        return termo_ciencia;
    }

    public void setTermoCiencia(String termoCiencia) {
        this.termo_ciencia = termoCiencia;
    }

    public String getRegiao() {
        return regiao;
    }

    public void setRegiao(String regiao) {
        this.regiao = regiao;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public boolean isVoluntario() {
        return "VOLUNTARIO".equalsIgnoreCase(tipo);
    }

    public boolean isPesquisador() {
        return "PESQUISADOR".equalsIgnoreCase(tipo);
    }

}
