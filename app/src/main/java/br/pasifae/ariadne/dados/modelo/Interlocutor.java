package br.pasifae.ariadne.dados.modelo;

/**
 * Created by JosueResende on 13/02/2018.
 */

public interface Interlocutor {
    Long getId();
    String getNome();
    String getTipo();
    String getNomeCompleto();
}
