package br.pasifae.ariadne.dados;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Logger;

import br.pasifae.ariadne.remote.TeseuService;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by JosueResende on 19/03/2018.
 */

public class AriadneLocationListener implements LocationListener {

    final
    private LocationManager locationManager;
    final
    private Context context;

    private Location location;
    private String lastProvider;

    BigDecimal accuracy;
    BigDecimal altitude;
    BigDecimal latitude;
    BigDecimal longitude;

    class UpdateLocation extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                TeseuService service = TeseuService.retrofit.create(TeseuService.class);
                {
                    Call<TeseuService.Response> call = service.setLocation(
                            Acesso.getNomeDoUsuarioLogadoComSucesso(),
                            accuracy.doubleValue(),
                            altitude.doubleValue(),
                            latitude.doubleValue(),
                            longitude.doubleValue()
                    );
                    Response<TeseuService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        TeseuService.Response response = executed.body();
                        if (response.error_code == 0) {
                            Logger.getAnonymousLogger().info("LOCATION CHANGED ACC " + location.getAccuracy());
                            Logger.getAnonymousLogger().info("LOCATION CHANGED ALT " + location.getAltitude());
                            Logger.getAnonymousLogger().info("LOCATION CHANGED LAT " + location.getLatitude());
                            Logger.getAnonymousLogger().info("LOCATION CHANGED LON " + location.getLongitude());
                            //toastLocation();
                        } else {
                            Logger.getAnonymousLogger().info("erro: " + response.error_description);
                        }
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    }
                }

            } catch(Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    @SuppressLint("MissingPermission")
    public AriadneLocationListener(Context context, LocationManager locationManager, String provider) {
        super();
        this.context = context;
        this.lastProvider = provider;
        this.locationManager = locationManager;
        this.location = locationManager.getLastKnownLocation(lastProvider);
        load(this.location);
    }

    private void load(Location location) {
        if (location == null) return;
        this.accuracy = BigDecimal.valueOf(location.getAccuracy());
        this.altitude = BigDecimal.valueOf(location.getAltitude()).setScale(3, RoundingMode.HALF_UP);
        this.latitude = BigDecimal.valueOf(location.getLatitude()).setScale(3, RoundingMode.HALF_UP);
        this.longitude = BigDecimal.valueOf(location.getLongitude()).setScale(3, RoundingMode.HALF_UP);
    }

    private void toastLocation() {
        Toast.makeText(context,
            "alt:" + altitude.toString() + "\nlat:" + latitude.toString() + "\nlon:" + longitude.toString(),
            Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location == null) return; // nothing to do
        load(location);
        if (Acesso.getNomeDoUsuarioLogadoComSucesso() == null) return;
        new UpdateLocation().execute();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        this.lastProvider = provider;
    }

    @Override
    public void onProviderEnabled(String provider) {
        this.lastProvider = provider;
    }

    @Override
    public void onProviderDisabled(String provider) {
        this.lastProvider = provider;
    }
}
