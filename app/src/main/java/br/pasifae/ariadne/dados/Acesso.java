package br.pasifae.ariadne.dados;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import br.pasifae.ariadne.Ariadne;
import br.pasifae.ariadne.dados.modelo.Anuncio;
import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.Estudo;
import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.Mensagem;
import br.pasifae.ariadne.dados.modelo.Pesquisador;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.RespostaDoVoluntario;
import br.pasifae.ariadne.dados.modelo.Usuario;
import br.pasifae.ariadne.dados.modelo.Voluntario;
import br.pasifae.ariadne.remote.MensagemService;
import br.pasifae.ariadne.remote.PesquisadorService;
import br.pasifae.ariadne.remote.TeseuService;
import br.pasifae.ariadne.remote.UsuarioService;
import br.pasifae.ariadne.remote.VoluntarioService;
import br.pasifae.ariadne.security.Keyset;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by JosueResende on 12/02/2018.
 */

public class Acesso {

    static public boolean CLOSE = false;

    static public Usuario usuarioLogadoComSucesso;
    static public Voluntario voluntarioLogadoComSucesso;
    static public Pesquisador pesquisadorLogadoComSucesso;

    private static String PREFERENCIAS = "PreferenciasDoAriadne";

    final public static boolean ENGESSADO = true;

    final public static String KEY_TYPE = "type";
    final public static String KEY_USERNAME = "username";
    final public static String KEY_PASSWORD = "password";
    final public static String KEY_TOKEN = "token";
    final public static String KEY_KEYS = "keys";

    private SharedPreferences sharedPreferences;
    private static String nomeDoUsuarioLogadoComSucesso;
    private static String usuarioRegistrando;

    private static LocationManager locationManager;

    static public class Database {
        private static Database_Application_SQLiteOpenHelper database_Application_sqLiteOpenHelper;

        static private Database_Application_SQLiteOpenHelper getDatabase() {
            if (database_Application_sqLiteOpenHelper == null) database_Application_sqLiteOpenHelper = new Database_Application_SQLiteOpenHelper();
            return database_Application_sqLiteOpenHelper;
        }

        static public Cursor execSelect(String sql, String[] selectionArgs) {
            return getDatabase().getReadableDatabase().rawQuery(sql, selectionArgs);
        }

        static public long execInsert(String table, ContentValues contentValues) {
            return getDatabase().getWritableDatabase().insert(table, null, contentValues);
        }

        static public int execUpdate(String table, ContentValues contentValues, String whereClause, String[] whereArgs) {
            return getDatabase().getWritableDatabase().update(table, contentValues, whereClause, whereArgs);
        }

        static public int execDelete(String table, String whereClause, String[] whereArgs) {
            return getDatabase().getWritableDatabase().delete(table, whereClause, whereArgs);
        }

        static public Questao getQuestaoById(long questao_id) {
            Cursor cursor = execSelect(
                    String.format("" +
                            "SELECT * " +
                            "FROM questao " +
                            "WHERE id = %d " +
                            "", questao_id),
                    null
            );
            try {
//                "id_versao INTEGER DEFAULT 0," +
//                "is_criterio INTEGER DEFAULT 1," +
//                "tag TEXT," +
//                "config TEXT," +
//                "texto TEXT," +
//                "texto_especifico TEXT," +
//                "limite_marcadas INTEGER," +
//                "id INTEGER PRIMARY KEY AUTOINCREMENT" +
                if (cursor.moveToFirst()) {
                    Questao questao = new Questao(questao_id);
                    questao.setQuestao(
                            cursor.getString(cursor.getColumnIndex("texto"))
                    );
                    questao.setCriterio(
                            cursor.getString(cursor.getColumnIndex("texto_especifico"))
                    );
                    questao.setConfig(
                            cursor.getString(cursor.getColumnIndex("config"))
                    );
                    questao.setCriterioDeBusca(
                            cursor.getInt(cursor.getColumnIndex("is_criterio")) == 1
                    );
                    questao.setLimiteDeRespostasMarcadas(
                            cursor.getInt(cursor.getColumnIndex("limite_marcadas"))
                    );
                    questao.setTag(
                            cursor.getString(cursor.getColumnIndex("tag"))
                    );
                    return questao;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
            return null;
        }

        static public Resposta getRespostaById(long resposta_id) {
            Cursor cursor = execSelect(
                    String.format("" +
                            "SELECT * " +
                            "FROM resposta " +
                            "WHERE id = %d " +
                            "", resposta_id),
                    null
            );
            try {
                if (cursor.moveToFirst()) {
                    Resposta resposta = new Resposta(resposta_id);
                    resposta.setResposta(
                            cursor.getString(cursor.getColumnIndex("texto"))
                    );
                    resposta.setConteudo(
                            Resposta.Conteudo.valueOf(cursor.getString(cursor.getColumnIndex("conteudo")))
                    );
                    resposta.setTag(
                            cursor.getString(cursor.getColumnIndex("tag"))
                    );
                    resposta.setQuestao(
                            new Questao(cursor.getLong(cursor.getColumnIndex("id_questao")))
                    );
                    return resposta;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
            return null;
        }

        static public Formulario getFormularioById(long formulario_id) {
            Cursor cursor = execSelect(
                    String.format("" +
                            "SELECT * " +
                            "FROM formulario " +
                            "WHERE id = %d " +
                            "", formulario_id),
                    null
            );
            try {

                if (cursor.moveToFirst()) {
                    Formulario formulario = new Formulario(formulario_id);
                    formulario.setTitulo(
                            cursor.getString(cursor.getColumnIndex("titulo"))
                    );
                    formulario.setResposta (
                            new Resposta(cursor.getLong(cursor.getColumnIndex("id_resposta_referencia")))
                    );
                    formulario.setConclusao(
                            new Resposta(cursor.getLong(cursor.getColumnIndex("id_resposta_conclusiva")))
                    );
                    formulario.setTextoParaRespostaLivre(
                            cursor.getString(cursor.getColumnIndex("resposta_texto_livre"))
                    );
                    return formulario;
                }
            } catch (Throwable e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
            return null;
        }

        static private Field getField(Field[] declaredFields, String name) {
            try {
                for(Field field : declaredFields) {
                    Column column = field.getAnnotation(Column.class);
                    if (column == null) continue;
                    if (column.name().equals(name)) return field;
                }
            } catch(Exception e) {
            }
            try {
                return Formulario.class.getDeclaredField(name);
            } catch(Exception e) {
            }
            Logger.getAnonymousLogger().warning("FIELD_NOT_FOUND: " + name);
            return null;
        }

        static public <T> List<T> list(Class<T> clazz, String sql) {
            List<T> os = new LinkedList<>();
            Cursor cursor = Acesso.Database.execSelect(sql, new String[]{});
            try {
                Field[] fields = new Field[cursor.getColumnCount()];
                for (String column : cursor.getColumnNames()) {
                    int index = cursor.getColumnIndex(column);
                    fields[index] = getField(clazz.getDeclaredFields(), column);
                    if (fields[index] == null) continue;
                    fields[index].setAccessible(true);
                }
                if (cursor.moveToFirst()) {
                    do {
                        boolean valido = false;
                        T o = clazz.newInstance();
                        for (int i = 0; i < fields.length; i++) {
                            if (fields[i] == null) continue;
                            boolean dados = true;
                            try {
                                if (fields[i].getType().equals(String.class)) {
                                    fields[i].set(o, cursor.getString(i));
                                } else if (fields[i].getType().equals(Long.class)) {
                                    fields[i].set(o,  new Long(cursor.getLong(i)));
                                } else if (fields[i].getType().equals(Boolean.class)) {
                                    fields[i].set(o, new Boolean(cursor.getInt(i) == 1));
                                } else {
                                    dados = false;
                                }
                            } catch(Exception e) {
                                e.printStackTrace();
                            }
                            valido |= dados;
                        }
                        if (valido) os.add(o);
                    } while (cursor.moveToNext());
                }
            } catch(Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
            return os;

        }

    }

    static public class Adapters {

        static public
        SwipeRecyclerViewAdapter<Usuario> autores = new SwipeRecyclerViewAdapter<Usuario>(Ariadne.getAppContext(),
                new SwipeRecyclerViewAdapter.OnCreateCardView() {
                @Override
                public SwipeRecyclerViewAdapter.SwipeRecyclerViewViewHolder set(View view) {
                    return new SwipeRecyclerViewAdapter.SwipeRecyclerRelativeLayoutViewHolder<Usuario>(view) {
                            TextView textView_nome;
                            Usuario usuario;

                            @Override
                            public void write(CardView o) {
                                textView_nome = new TextView(o.getContext());
                                o.addView(textView_nome);
                                textView_nome.setTextSize(18);
                            }

                            @Override
                            public void onClick(View v) {
    //                                    me.paginaDeMensagens(this.usuario);
                            }

                            @Override
                            public void use(Usuario o) {
                                this.usuario = o;
                                if (usuario.getId() != Acesso.pesquisadorLogadoComSucesso.getId().longValue()) {
                                    textView_nome.setText(o.getNome());
                                } else {
                                    textView_nome.setTextSize(20);
                                    textView_nome.setTypeface(Typeface.DEFAULT_BOLD);
                                    textView_nome.setText("MINHAS MENSAGENS ");
                                }
                            }
                        };
                }

            }
        );
    }

    static public class Tab {
        static public TabHost tabHost;
        static public List<TabHost.TabSpec> tabSpecs = new LinkedList<>();
        static public MensagemService.DadosDaMensagem dadosDaMensagem;
//        static public void goTab(String name) {
//            int i = 0;
//            for (; i < tabSpecs.size(); i++) {
//                if (name.equalsIgnoreCase(tabSpecs.get(i).getTag())) break;
//            }
//            if (i < tabSpecs.size()) tabHost.setCurrentTab(i);
//        }
    }

    public static String getNomeDoUsuarioLogadoComSucesso() {
        if (nomeDoUsuarioLogadoComSucesso == null) return null;
        return new String(nomeDoUsuarioLogadoComSucesso);
    }

    public Acesso(Activity activity) {
        this.sharedPreferences = activity.getSharedPreferences(PREFERENCIAS, Context.MODE_PRIVATE);
    }

    public static void setNomeDoUsuarioLogadoComSucesso(String nomeDoUsuarioLogadoComSucesso) {
        Acesso.nomeDoUsuarioLogadoComSucesso = nomeDoUsuarioLogadoComSucesso;
    }

    public static String nomeDoUsuarioLogadoComSucesso() {
        return nomeDoUsuarioLogadoComSucesso == null ? null : new String(nomeDoUsuarioLogadoComSucesso);
    }

    public void habilitaRegistroDeUsuario(String username) {
        usuarioRegistrando = username;
    }

    public void cancelaRegistroDeUsuario() {
        usuarioRegistrando = null;
    }

    public String registroDeUsuario() {
        return usuarioRegistrando;
    }

    public boolean registrandoUsuario() {
        return usuarioRegistrando != null;
    }

    public static enum TIPO_DE_USUARIO {
        VOLUNTARIO, PESQUISADOR;
        public boolean isVoluntario() { return VOLUNTARIO.equals(this); }
        public boolean isPesquisador() { return PESQUISADOR.equals(this); }
    }

    public Keyset criarChaveLocal() {
        return criarChaveLocal(Keyset.load());
    }

    public Keyset criarChaveLocal(Keyset keyset) {
        keyset.generate();
        return keyset;
    }

    public Acesso.TIPO_DE_USUARIO relembrarTipoDoUsuario() {
        try {
            if (sharedPreferences.contains(KEY_TYPE)) {
                int type = sharedPreferences.getInt(KEY_TYPE, -1);
                if (type > -1) return TIPO_DE_USUARIO.values()[type];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String relembrarNomeDoUsuario() {
        try {
            if (sharedPreferences.contains(KEY_USERNAME)) {
                return sharedPreferences.getString(KEY_USERNAME, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String relembrarSenhaDoUsuario() {
        try {
            if (sharedPreferences.contains(KEY_PASSWORD)) {
                return sharedPreferences.getString(KEY_PASSWORD, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String relembrarTokenDoUsuario() {
        try {
            if (sharedPreferences.contains(KEY_TOKEN)) {
                return sharedPreferences.getString(KEY_TOKEN, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, Object> relembrarUsuario() {
        Map<String, Object> usuario = new HashMap<String, Object>();
        usuario.put(KEY_TYPE, relembrarTipoDoUsuario());
        usuario.put(KEY_USERNAME, relembrarNomeDoUsuario());
        usuario.put(KEY_PASSWORD, relembrarSenhaDoUsuario());
        return usuario;
    }

    public void lembrarUsuario(Usuario usuario) {
        TIPO_DE_USUARIO tipoDeUsuario = TIPO_DE_USUARIO.valueOf(usuario.getTipo());

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_TYPE, tipoDeUsuario.ordinal());
        editor.putString(KEY_USERNAME, usuario.getLogin());
        editor.putString(KEY_PASSWORD, usuario.getSenha());
        editor.commit();
    }

    public void logarUsuario(UsuarioService.Response response) {

        Usuario usuario = response.usuario;

        usuarioLogadoComSucesso = usuario;

        TIPO_DE_USUARIO tipoDeUsuario = TIPO_DE_USUARIO.valueOf(usuario.getTipo());
        byte[] foto = (usuario.getFoto_base64() == null) ? null : Base64.decode(usuario.getFoto_base64(), Base64.URL_SAFE | Base64.NO_WRAP);
        if (TIPO_DE_USUARIO.PESQUISADOR.equals(tipoDeUsuario)) {

            pesquisadorLogadoComSucesso = new Pesquisador();
            pesquisadorLogadoComSucesso.setId(usuario.getId());
            pesquisadorLogadoComSucesso.setNome(usuario.getLogin());
            pesquisadorLogadoComSucesso.setUsername(usuario.getLogin());
            pesquisadorLogadoComSucesso.setFoto(foto);
            pesquisadorLogadoComSucesso.setUsuario(usuario);

            pesquisadorLogadoComSucesso.setCienteDoTermo((usuario.getTermoCiencia() != null));

            for (PesquisadorService.DadosDaBusca dados : response.buscas) {
                pesquisadorLogadoComSucesso.buscas.put(
                        dados.busca_id,
                        new Busca(dados.busca_id, dados.busca_nome)
                );
            }
            for (MensagemService.DadosDaMensagem dados : response.mensagens) {
                Usuario interlocutor = null;
                if (dados.leitor_id > 0) {
                    if (pesquisadorLogadoComSucesso.mensagens.containsKey(dados.leitor_id) == false) {
                        pesquisadorLogadoComSucesso.mensagens.put(dados.leitor_id, new Usuario(dados.leitor_id, dados.leitor_nome));
                    }
                    interlocutor = pesquisadorLogadoComSucesso.mensagens.get(dados.leitor_id);
                } else {
                    if (pesquisadorLogadoComSucesso.mensagens.containsKey(dados.autor_id) == false) {
                        pesquisadorLogadoComSucesso.mensagens.put(dados.autor_id, new Usuario(dados.autor_id, dados.autor_nome));
                    }
                    interlocutor = pesquisadorLogadoComSucesso.mensagens.get(dados.autor_id);
                }
                interlocutor.mensagens
                        .put(dados.mensagem_id,
                            new Mensagem(dados.mensagem_id, dados.mensagem_conteudo)
                                .setDataDeEnvio(dados.mensagem_data_criacao)
                                .setAutor(new Usuario(dados.autor_id, dados.autor_nome))
                        );
            }
            for (Usuario u : Acesso.pesquisadorLogadoComSucesso.mensagens.values()) {
                Adapters.autores.add(usuario);
            }
        }
        if (TIPO_DE_USUARIO.VOLUNTARIO.equals(tipoDeUsuario)) {

            voluntarioLogadoComSucesso = new Voluntario();
            voluntarioLogadoComSucesso.setId(usuario.getId());
            voluntarioLogadoComSucesso.setNome(usuario.getNome() == null ? usuario.getLogin() : usuario.getNome());
            voluntarioLogadoComSucesso.setUsername(usuario.getLogin());
            voluntarioLogadoComSucesso.setFoto(foto);
            voluntarioLogadoComSucesso.setUsuario(usuario);
            voluntarioLogadoComSucesso.setCienteDoTermo((usuario.getTermoCiencia() != null));
            //
            for (VoluntarioService.DadosDoFormulario dados : response.formularios) {
                voluntarioLogadoComSucesso.formularios.put(
                        dados.formulario_id,
                        new Formulario(dados.formulario_id, dados.formulario_titulo, null)
                            .setResposta(dados.questao_referencia_id, dados.resposta_referencia_id)
                );
            }
            for (VoluntarioService.DadosDaQuestao dados : response.questoes) {
                if (voluntarioLogadoComSucesso.formularios.containsKey(dados.formulario_id)) {
                    voluntarioLogadoComSucesso.formularios.get(dados.formulario_id)
                            .getQuestoes()
                            .add(new Questao(dados.questao_id, dados.questao_texto).
                                    setLimiteDeRespostasMarcadas(dados.questao_limite_marcadas)
                            );
                }
            }
            for (VoluntarioService.DadosDeResposta dados : response.respostas) {
                if (voluntarioLogadoComSucesso.formularios.containsKey(dados.formulario_id)) {
                    Questao questao = voluntarioLogadoComSucesso.formularios.get(dados.formulario_id).getQuestao(dados.questao_id);
                    if (questao == null) continue;
                    if (dados.resposta_id == 0) continue;
                    if (dados.resposta_conteudo == null) continue;
                    questao.addResposta(
                            new Resposta(dados.resposta_id,
                                    Resposta.Conteudo.valueOf(dados.resposta_conteudo),
                                    dados.resposta_texto
                            )
                            .setTextoDoVoluntario(dados.marcada_texto)
                            .setTag(dados.resposta_tag)
                    );
                }
            }

        }
    }

    public void esquecerUsuario() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(KEY_TYPE);
        editor.remove(KEY_USERNAME);
        editor.remove(KEY_PASSWORD);
        editor.remove(KEY_TOKEN);
        editor.commit();
    }

    public boolean deslogarUsuario(String email) {
        nomeDoUsuarioLogadoComSucesso = null;
        return true;
    }

//    @Deprecated
//    public boolean logarUsuario(TIPO_DE_USUARIO tipo_de_usuario, String email, String senha) {
//        Keyset keyset = relembrarChaveiro();
//        if (keyset.noVerify()) {
//            try {
//                JsonRestService jsonRestService = new JsonRestService(keyset.getUID());
//                Map<String, String> request = new LinkedHashMap<String, String>();
//                request.put("publickey", keyset.getPublicKey());
//                JSONObject jsonObject = jsonRestService.makeHttpRequest(JsonRestService.Method.POST,"http://parsifae.scienceontheweb.net/session/open", request);
//                if (jsonObject != null && jsonObject.has("publickey")) {
//                        keyset.loadRemotePublicKey(jsonObject.getString("publickey"));
//                        lembrarChaveiro(keyset);
//                }
//            } catch(Exception e) {
//                e.printStackTrace();
//            }
//        }
//        if (tipo_de_usuario.isVoluntario()) {
//            Voluntario voluntario = Mock.getVoluntario(email);
//            if (voluntario == null) return false;
//            if (voluntario.getPassword() == null) {
//                voluntario.setPassword(senha);
//                Mock.putVoluntario(voluntario);
//            }
//            if (voluntario.getPassword().equals(senha) == false) return false;
//        } else if (tipo_de_usuario.isPesquisador()) {
//            Pesquisador pesquisador = Mock.getPesquisador(email);
//            if (pesquisador == null) return false;
//            if (pesquisador.getPassword() == null) {
//                pesquisador.setPassword(senha);
//                Mock.putPesquisador(pesquisador);
//            }
//            if (pesquisador.getPassword().equals(senha) == false) return false;
//
//        }
//        nomeDoUsuarioLogadoComSucesso = email;
//        return true;
//    }

//    public boolean registrarUsuario(TIPO_DE_USUARIO tipo_de_usuario, String email, String senha) {
//        if (tipo_de_usuario.isVoluntario()) {
//            Voluntario voluntario = Mock.getVoluntario(email);
//            if (voluntario != null) return false;
//            voluntario = new Voluntario();
//            voluntario.setUsername(email);
//            voluntario.setPassword(senha);
//            Mock.putVoluntario(voluntario);
//        } else if (tipo_de_usuario.isPesquisador()) {
//            Pesquisador pesquisador = Mock.getPesquisador(email);
//            if (pesquisador != null) return false;
//            pesquisador = new Pesquisador();
//            pesquisador.setUsername(email);
//            pesquisador.setPassword(senha);
//            Mock.putPesquisador(pesquisador);
//        }
//        nomeDoUsuarioLogadoComSucesso = email;
//        return true;
//    }

    public String tokenUsuario(TIPO_DE_USUARIO tipo_de_usuario, String email, String senha) {
        return "_MY_TOKEN_";
    }

    public Voluntario getVoluntario(String username) {
        return Mock.getVoluntario(username);
    }

    public Pesquisador getPesquisador(String username) {
        return Mock.getPesquisador(username);
    }

//    public List<Conversa> getConversasDoVoluntario(String username) {
//
//        Voluntario voluntario = getVoluntario("voluntario");
//
//        List<Conversa> conversas = Mock.getConversas(voluntario);
//
//        return conversas;
//    }
    public void putMensagemDireta(Mensagem mensagem) {
        try {
            MensagemService service = TeseuService.retrofit.create(MensagemService.class);
            {
                String uid = UUID.randomUUID().toString();
                String extra = "";
                Call<MensagemService.Response> call = service.enviarMensagemDireta(uid,
                        mensagem.getLeitor().getNome(),
                        mensagem.getConteudo(),
                        extra
                );
                Response<MensagemService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    MensagemService.Response response = executed.body();
                    if (response.error_code == 0) {
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().info(executed.errorBody().string());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void putAnuncio(Anuncio anuncio) {
        try {
            MensagemService service = TeseuService.retrofit.create(MensagemService.class);
            {
                Call<MensagemService.Response> call = null;
                if (anuncio.getBusca() != null) {
                    String uid = UUID.randomUUID().toString();
                    call = service.anunciarNaBusca(uid,
                            anuncio.getBusca().getId(),
                            new SimpleDateFormat("yyyy-MM-dd").format(anuncio.getDataDeExpiracao()),
                            anuncio.getConteudo(),
                            anuncio.extras.toJSON()
                    );
                } else if (anuncio.getCriterios() != null) {
                    JsonArray jsonArray = new JsonArray();
                    for (Criterio criterio : anuncio.getCriterios()) {
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.add("id_questao", new JsonPrimitive(criterio.getQuestao().getId()));
                            jsonObject.add("id_resposta", new JsonPrimitive(criterio.getResposta().getId()));
                            jsonObject.add("texto", new JsonPrimitive(criterio.getResposta().getResposta()));
                            jsonArray.add(jsonObject);
                    }
                    String uid = UUID.randomUUID().toString();

                    call = service.anunciarSemBusca(uid,
                            jsonArray.toString(),
                            new SimpleDateFormat("yyyy-MM-dd").format(anuncio.getDataDeExpiracao()),
                            anuncio.getConteudo(),
                            anuncio.extras.toJSON()
                    );
                }
                Response<MensagemService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    MensagemService.Response response = executed.body();
                    if (response.error_code == 0) {
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().info(executed.errorBody().string());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void putPhoto(String username, String base64) {
        try {
            UsuarioService service = TeseuService.retrofit.create(UsuarioService.class);
            {
                Call<UsuarioService.Response> call = service.atualizarFoto(username, base64);
                Response<UsuarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    UsuarioService.Response response = executed.body();
                    if (response.error_code == 0) {
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Collection<Formulario> getFormulariosDoVoluntario(String username) {
        Map<Long, Formulario> formularios = new LinkedHashMap<Long, Formulario>();
        try {
            VoluntarioService service = TeseuService.retrofit.create(VoluntarioService.class);
            {
                Call<VoluntarioService.Response> call = service.formularios(username);
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (VoluntarioService.DadosDoFormulario dados : response.formularios) {
                            formularios.put(dados.formulario_id, new Formulario(dados.formulario_id, dados.formulario_titulo, null));
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
            {
                Call<VoluntarioService.Response> call = service.questoes(username);
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (VoluntarioService.DadosDaQuestao dados : response.questoes) {
                            if (formularios.containsKey(dados.formulario_id)) {
                                formularios.get(dados.formulario_id).getQuestoes()
                                        .add(new Questao(dados.questao_id, dados.questao_texto).setLimiteDeRespostasMarcadas(dados.questao_limite_marcadas));
                            }
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
            {
                Call<VoluntarioService.Response> call = service.respostas(username);
                Response<VoluntarioService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    VoluntarioService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (VoluntarioService.DadosDeResposta dados : response.respostas) {
                            if (formularios.containsKey(dados.formulario_id)) {
                                Questao questao = formularios.get(dados.formulario_id).getQuestao(dados.questao_id);
                                if (questao == null) continue;
                                questao.addResposta(
                                        new Resposta(dados.resposta_id, Resposta.Conteudo.valueOf(dados.resposta_conteudo), dados.resposta_texto)
                                                .setTextoDoVoluntario(dados.marcada_texto)
                                );
                            }
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        /*
        List<Formulario> formularios = Mock.getFormularios();
        List<RespostaDoVoluntario> buscadas = Mock.listRespostaDoVoluntario(username);
        for (RespostaDoVoluntario respostaDoVoluntario : buscadas) {
            List<Formulario> formulariosPorResposta = Mock.getFormularios(respostaDoVoluntario);
            if (formulariosPorResposta == null) continue;
            if (formulariosPorResposta.size() == 0) continue;
            formularios.addAll(formulariosPorResposta);
        }
        Mock.sleep(100);
        // */
        return formularios.values();
    }

    public List<RespostaDoVoluntario> getRespostasDoVoluntario(String username) {
        List<RespostaDoVoluntario> respostas = null;
//        buscadas = Mock.listRespostaDoVoluntario(username);
        respostas = new LinkedList<RespostaDoVoluntario>();
        return respostas;
    }

    public Collection<Estudo> getEstudosDoPesquisador(String username) {
        Map<Long, Estudo> estudos = new LinkedHashMap<Long, Estudo>();
        try {
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.estudos(username);
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (PesquisadorService.DadosDoEstudo dados : response.estudos) {
                            estudos.put(dados.estudo_id, new Estudo(dados.estudo_id, dados.estudo_nome));
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
            {
//                Call<VoluntarioService.Response> call = service.questoes(username);
//                Response<VoluntarioService.Response> executed = call.execute();
//                if (executed.isSuccessful()) {
//                    VoluntarioService.Response response = executed.body();
//                    if (response.error_code == 0) {
//                        for (VoluntarioService.DadosDaQuestao dados : response.questoes) {
//                            if (formularios.containsKey(dados.formulario_id)) {
//                                formularios.get(dados.formulario_id).getQuestoes()
//                                        .add(new Questao(dados.questao_id, dados.questao_texto).setLimiteDeRespostasMarcadas(dados.questao_limite_marcadas));
//                            }
//                        }
//                    }
//                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
//                }
            }
            {
//                Call<VoluntarioService.Response> call = service.respostas(username);
//                Response<VoluntarioService.Response> executed = call.execute();
//                if (executed.isSuccessful()) {
//                    VoluntarioService.Response response = executed.body();
//                    if (response.error_code == 0) {
//                        for (VoluntarioService.DadosDeResposta dados : response.respostas) {
//                            if (formularios.containsKey(dados.formulario_id)) {
//                                Questao questao = formularios.get(dados.formulario_id).getQuestao(dados.questao_id);
//                                if (questao == null) continue;
//                                questao.addResposta(
//                                        new Resposta(dados.resposta_id, Resposta.Conteudo.valueOf(dados.resposta_conteudo), dados.resposta_texto)
//                                                .setTextoDoVoluntario(dados.marcada_texto)
//                                );
//                            }
//                        }
//                    }
//                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
//                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return estudos.values();
//        return Mock.getEstudosDoPesquisador(username);
    }

//    public List<Resposta> getRespostas() {
//        return Mock.getRespostas();
//    }
//
//    public List<Resposta> getRespostas(Questao questao) {
//        return Mock.getRespostas(questao);
//    }

//    public Resposta putResposta(Resposta o) {
//        return Mock.putResposta(o);
//    }
    public Resposta getResposta(String tagQuestao, String tagResposta) {
        List<Resposta> respostas = getRespostas(tagQuestao, tagResposta);
        if (respostas.size() == 0) return null;
        if (respostas.size() == 1) return respostas.get(0);
        return null;

    }
    public List<Resposta> getRespostasReferentes(Long idResposta) {
        List<Resposta> respostas = new LinkedList<>();
        try {
            {
                String sql = null;
                Cursor cursor = Database.execSelect(
                        "SELECT resposta.*, questao.limite_marcadas, questao.is_criterio  " +
                        "FROM formulario " +
                        "INNER JOIN questao_formulario ON questao_formulario.id_formulario = formulario.id " +
                        "INNER JOIN resposta ON resposta.id_questao = questao_formulario.id_questao " +
                        "INNER JOIN questao ON questao.id = questao_formulario.id_questao " +
                        "WHERE formulario.id_resposta_referencia = ?",
                        new String[]{ String.valueOf(idResposta)}
                );
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexIdQuestao = cursor.getColumnIndex("id_questao");
                    int indexConteudo = cursor.getColumnIndex("conteudo");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTag = cursor.getColumnIndex("tag");
                    int indexLimiteMarcadas = cursor.getColumnIndex("limite_marcadas");
                    int indexIsCriterio = cursor.getColumnIndex("is_criterio");
                    do {
                        respostas.add(
                                new Resposta(cursor.getLong(indexId),
                                        Resposta.Conteudo.valueOf(cursor.getString(indexConteudo)),
                                        cursor.getString(indexTexto)
                                )
                                .setTag(cursor.getString(indexTag))
                                .setQuestao(
                                        new Questao(cursor.getLong(indexIdQuestao))
                                            .setLimiteDeRespostasMarcadas(cursor.getInt(indexLimiteMarcadas))
                                            .setCriterioDeBusca(cursor.getInt(indexIsCriterio) == 1)
                                )
                        );
                    } while (cursor.moveToNext());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return respostas;
    }

    public List<Resposta> getRespostas(String tagQuestao, String tagResposta) {
        List<Resposta> respostas = new LinkedList<>();
        try {
            {
                String sql = null;
                if (tagResposta == null) {
                    sql = String.format("" +
                            "SELECT resposta.id, resposta.id_questao, resposta.conteudo, resposta.texto, resposta.tag, questao.limite_marcadas, questao.is_criterio " +
                            "FROM resposta " +
                            "INNER JOIN questao ON questao.id = resposta.id_questao " +
                            "WHERE questao.tag = '%s' ", tagQuestao);
                } else {
                    sql = String.format("" +
                            "SELECT resposta.id, resposta.id_questao, resposta.conteudo, resposta.texto, resposta.tag, questao.limite_marcadas, questao.is_criterio " +
                            "FROM resposta " +
                            "INNER JOIN questao ON questao.id = resposta.id_questao " +
                            "WHERE questao.tag = '%s' AND resposta.tag = '%s' ", tagQuestao, tagResposta);
                }
                Cursor cursor = Database.execSelect(sql, null);
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexIdQuestao = cursor.getColumnIndex("id_questao");
                    int indexConteudo = cursor.getColumnIndex("conteudo");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTag = cursor.getColumnIndex("tag");
                    int indexLimiteMarcadas = cursor.getColumnIndex("limite_marcadas");
                    int indexIsCriterio = cursor.getColumnIndex("is_criterio");
                    do {
                        respostas.add(
                            new Resposta(cursor.getLong(indexId),
                                Resposta.Conteudo.valueOf(cursor.getString(indexConteudo)),
                                cursor.getString(indexTexto)
                            )
                            .setTag(cursor.getString(indexTag))
                            .setQuestao(
                                    new Questao(cursor.getLong(indexIdQuestao))
                                    .setLimiteDeRespostasMarcadas(cursor.getInt(indexLimiteMarcadas))
                                    .setCriterioDeBusca(cursor.getInt(indexIsCriterio) == 1)
                            )
                        );
                    } while (cursor.moveToNext());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return respostas;
    }
    public Questao getQuestao(String tag) {
        List<Questao> questoes = getQuestoes(tag);
        if (questoes.size() == 0) return null;
        if (questoes.size() == 1) return questoes.get(0);
        return null;
    }
    public List<Questao> getQuestoes(String tag) {
        try {
            List<Questao> questoes = new LinkedList<>();
            {
                Cursor cursor = Database.execSelect(
                        String.format("" +
                                "SELECT id, texto, texto_especifico, limite_marcadas, is_criterio, tag, config " +
                                "FROM questao " +
                                "WHERE questao.tag LIKE '%s' ", tag), null);
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTextoEspecifico = cursor.getColumnIndex("texto_especifico");
                    int indexLimiteMarcadas = cursor.getColumnIndex("limite_marcadas");
                    int indexConfig = cursor.getColumnIndex("config");
                    int indexTag = cursor.getColumnIndex("tag");
                    int indexIsCriterio = cursor.getColumnIndex("is_criterio");
                    do {
                        questoes.add(
                                new Questao(cursor.getLong(indexId), cursor.getString(indexTexto))
                                        .setCriterio(cursor.getString(indexTextoEspecifico))
                                        .setLimiteDeRespostasMarcadas(cursor.getInt(indexLimiteMarcadas))
                                        .setConfig(cursor.getString(indexConfig))
                                        .setCriterioDeBusca(cursor.getInt(indexIsCriterio) == 1)
                                        .setTag(cursor.getString(indexTag))
                        );

                    } while (cursor.moveToNext());
                }
            }
            if (questoes.size() > 0)
            {
                for (Questao questao : questoes) {
                    questao.setRespostas(getRespostas(tag, null));
                }
            }
            return questoes;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection<Questao> getQuestoes() {
        Map<Long, Questao> questoes = new LinkedHashMap<Long, Questao>();
        try {
            {
                Cursor cursor = Database.execSelect("SELECT id, texto, texto_especifico, limite_marcadas, is_criterio, tag, config FROM questao", null);
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTextoEspecifico = cursor.getColumnIndex("texto_especifico");
                    int indexLimiteMarcadas = cursor.getColumnIndex("limite_marcadas");
                    int indexConfig = cursor.getColumnIndex("config");
                    int indexTag = cursor.getColumnIndex("tag");
                    int indexIsCriterio = cursor.getColumnIndex("is_criterio");
                    do {
                        Questao questao =
                            new Questao(cursor.getLong(indexId), cursor.getString(indexTexto))
                                    .setCriterio(cursor.getString(indexTextoEspecifico))
                                    .setLimiteDeRespostasMarcadas(cursor.getInt(indexLimiteMarcadas))
                                    .setConfig(cursor.getString(indexConfig))
                                    .setCriterioDeBusca(cursor.getInt(indexIsCriterio) == 1)
                                    .setTag(cursor.getString(indexTag))
                        ;
                        questoes.put(cursor.getLong(indexId), questao);
                    } while (cursor.moveToNext());
                }
            }
            {
                Cursor cursor = Database.execSelect("SELECT id, id_questao, conteudo, texto, tag FROM resposta", null);
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexIdQuestao = cursor.getColumnIndex("id_questao");
                    int indexConteudo = cursor.getColumnIndex("conteudo");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTag = cursor.getColumnIndex("tag");
                    do {
                        if (questoes.containsKey(cursor.getLong(indexIdQuestao)) == false) continue;
                        questoes.get(cursor.getLong(indexIdQuestao)).getRespostas()
                                .add(
                                    new Resposta(
                                        cursor.getLong(indexId),
                                        Resposta.Conteudo.valueOf(cursor.getString(indexConteudo)),
                                        cursor.getString(indexTexto)
                                    )
                                    .setTag(cursor.getString(indexTag))
                                );
                    } while (cursor.moveToNext());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return new LinkedList<Questao>(questoes.values());
    }
    public Collection<Questao> getCriterios() {
        Map<Long, Questao> questoes = new LinkedHashMap<Long, Questao>();
        try {
            {
                Cursor cursor = Database.execSelect("SELECT id, texto, texto_especifico, limite_marcadas, is_criterio, tag, config FROM questao WHERE is_criterio = 1", null);
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTextoEsspecifico = cursor.getColumnIndex("texto_especifico");
                    int indexLimiteMarcadas = cursor.getColumnIndex("limite_marcadas");
                    do {
                        questoes.put(cursor.getLong(indexId),
                                new Questao(cursor.getLong(indexId), cursor.getString(indexTexto))
                                        .setLimiteDeRespostasMarcadas(cursor.getInt(indexLimiteMarcadas))
                                        .setCriterio(cursor.getString(indexTextoEsspecifico))
                        );
                    } while (cursor.moveToNext());
                }
            }
            {
                Cursor cursor = Database.execSelect("" +
                        "SELECT resposta.id, resposta.id_questao, resposta.conteudo, resposta.texto, resposta.tag " +
                        "FROM resposta " +
                        "INNER JOIN questao ON questao.id = resposta.id_questao " +
                        "WHERE questao.is_criterio = 1", null);
                if (cursor.moveToFirst()) {
                    int indexId = cursor.getColumnIndex("id");
                    int indexIdQuestao = cursor.getColumnIndex("id_questao");
                    int indexConteudo = cursor.getColumnIndex("conteudo");
                    int indexTexto = cursor.getColumnIndex("texto");
                    int indexTag = cursor.getColumnIndex("tag");
                    do {
                        if (questoes.containsKey(cursor.getLong(indexIdQuestao)) == false) continue;
                        questoes.get(cursor.getLong(indexIdQuestao)).getRespostas()
                                .add(
                                    new Resposta(cursor.getLong(indexId),
                                        Resposta.Conteudo.valueOf(cursor.getString(indexConteudo)),
                                        cursor.getString(indexTexto)
                                    )
                                    .setTag(cursor.getString(indexTag))
                                );
                    } while (cursor.moveToNext());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return new LinkedList<Questao>(questoes.values());
    }

    public Questao putQuestao(Questao o) {
        return Mock.putQuestao(o);
    }

    public void putEstudo(Estudo estudo) {
        Mock.putEstudo(estudo);
    }

    public Formulario putFormulario(Formulario o) {
        return Mock.putFormulario(o);
    }

    public List<Busca> getBuscasDoEstudo(Estudo estudo) {
        return Mock.getBuscasDoEstudo(estudo);
    }

    public Collection<Busca> getBuscasDoPesquisador(String username) {
        Map<Long, Busca> buscas = new LinkedHashMap<Long, Busca>();
        Map<Long, Questao> questoes = new LinkedHashMap<Long, Questao>();
        try {
            PesquisadorService service = TeseuService.retrofit
                    .create(PesquisadorService.class);

            {
                Call<PesquisadorService.Response> call = service.buscas(username);
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (PesquisadorService.DadosDaBusca dados : response.buscas) {
                            buscas.put(dados.busca_id, new Busca(dados.busca_id, dados.busca_nome));
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
            {
                Call<PesquisadorService.Response> call = service.questoes(-1);
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (PesquisadorService.DadosDaQuestao dados : response.questoes) {
                            questoes.put(dados.questao_id, new Questao(dados.questao_id, dados.questao_texto).setLimiteDeRespostasMarcadas(dados.questao_limite_marcadas));
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
            {
                Call<PesquisadorService.Response> call = service.buscadas(username);
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (PesquisadorService.DadosDeResposta dados : response.respostas) {
                            //buscas.put(dados.busca_id, new Busca(dados.busca_id, dados.busca_nome));
                            if (buscas.containsKey(dados.busca_id) == false) continue;
                            buscas.get(dados.busca_id).getRespostas()
                                    .add(new Resposta(dados.resposta_id, Resposta.Conteudo.valueOf(dados.resposta_conteudo), dados.resposta_texto)
                                            .setQuestao((dados.questao_id > 0) ? questoes.get(dados.questao_id) : null)
                                            .setTextoDaBusca(dados.buscada_texto));
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        //return Mock.getBuscasDoPesquisador(username);
        return buscas.values();
    }

    public void putBusca(Pesquisador pesquisador, Busca busca) {
        try {
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.novaBusca(pesquisador.getUsername(), null, busca.getNome());
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
//                        for (PesquisadorService.DadosDaBusca dados : response.buscas) {
//                            buscas.put(dados.busca_id, new Busca(dados.busca_id, dados.busca_nome));
//                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        //Mock.putBusca(busca);
    }

    public void putBusca(Estudo estudo, Busca busca) {
        Mock.putBusca(estudo, busca);
    }

    public void popBusca(Busca busca) {
        try {
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.removerBusca(busca.getId());
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
    public void delCriterio(Criterio criterio) {
        try {
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.removerResposta(criterio.getBusca().getId(),
                        (criterio.getQuestao() == null ? null : criterio.getQuestao().getId()),
                        criterio.getResposta().getId()
                );
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().info(executed.errorBody().string());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
    public void putCriterio(Criterio criterio) {
        try {
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.novaResposta(
                        pesquisadorLogadoComSucesso.getUsername(),
                        criterio.getBusca().getId(),
                        (criterio.getQuestao() == null ? null : criterio.getQuestao().getId()),
                        criterio.getResposta().getId(),
                        criterio.getResposta().getResposta()
                );
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
//                        for (PesquisadorService.DadosDaBusca dados : response.buscas) {
//                            buscas.put(dados.busca_id, new Busca(dados.busca_id, dados.busca_nome));
//                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                } else {
                    Logger.getAnonymousLogger().info(executed.errorBody().string());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
//        Mock.putCriterio(criterio);
    }

    public Busca getBusca(Long idBusca) {
        Busca busca = null;
        Map<Long, Questao> questoes = new LinkedHashMap<Long, Questao>();
        try {
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.trazerBusca(idBusca);
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (PesquisadorService.DadosDaBusca dados : response.buscas) {
                            busca = new Busca(dados.busca_id, dados.busca_nome);
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
            if (busca != null)
            {
                {
                    Call<PesquisadorService.Response> call = service.trazerQuestoes(idBusca);
                    Response<PesquisadorService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        PesquisadorService.Response response = executed.body();
                        if (response.error_code == 0) {
                            for (PesquisadorService.DadosDaQuestao dados : response.questoes) {
                                questoes.put(dados.questao_id, new Questao(dados.questao_id, dados.questao_texto).setLimiteDeRespostasMarcadas(dados.questao_limite_marcadas));
                            }
                        }
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    }
                }
                {
                    Call<PesquisadorService.Response> call = service.trazerBuscadas(idBusca);
                    Response<PesquisadorService.Response> executed = call.execute();
                    if (executed.isSuccessful()) {
                        PesquisadorService.Response response = executed.body();
                        if (response.error_code == 0) {
                            for (PesquisadorService.DadosDeResposta dados : response.respostas) {
                                busca.getRespostas()
                                        .add(new Resposta(dados.resposta_id, Resposta.Conteudo.valueOf(dados.resposta_conteudo), dados.resposta_texto)
                                                .setQuestao((dados.questao_id > 0) ? questoes.get(dados.questao_id) : null)
                                                .setTextoDaBusca(dados.buscada_texto));
                            }
                        }
                        Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        //return Mock.getCriteriosDaBusca(busca);
        return busca;
    }

//    public RespostaDoVoluntario putRespostaDoVoluntario(RespostaDoVoluntario respostaDoVoluntario) {
//        return Mock.putRespostaDoVoluntario(respostaDoVoluntario);
//    }
//
//    public void delRespostaDoVoluntario(RespostaDoVoluntario respostaDoVoluntario) {
//        Mock.delRespostaDoVoluntario(respostaDoVoluntario);
//    }

    public List<Voluntario> procuraVoluntarios(Busca busca) {
        Map<Long, Voluntario> voluntarios = new LinkedHashMap<Long, Voluntario>();
        try {
            long idBusca = busca.getId();
            PesquisadorService service = TeseuService.retrofit.create(PesquisadorService.class);
            {
                Call<PesquisadorService.Response> call = service.trazerVoluntarios(pesquisadorLogadoComSucesso.getUsername(), idBusca);
                Response<PesquisadorService.Response> executed = call.execute();
                if (executed.isSuccessful()) {
                    PesquisadorService.Response response = executed.body();
                    if (response.error_code == 0) {
                        for (PesquisadorService.DadosDoVoluntario dados : response.voluntarios) {
                            Voluntario voluntario =
                                new Voluntario(dados.voluntario_id, dados.voluntario_nome)
                                    .setDistancia(dados.distancia)
                                    .setUsuario(new Usuario(dados.voluntario_id, dados.voluntario_nome).setTipo("VOLUNTARIO"))
                            ;
                            voluntarios.put(
                                    dados.voluntario_id,
                                    voluntario
                            );
                            for (VoluntarioService.DadosDeResposta dadosDeResposta : dados.respostas) {

                                Questao questao = Database.getQuestaoById(dadosDeResposta.questao_id);

                                if (questao == null) continue;

                                Resposta resposta = Database.getRespostaById(dadosDeResposta.resposta_id);

                                if (resposta == null) continue;

                                if (voluntario.perfil.containsKey(dadosDeResposta.questao_id) == false) {
                                    voluntario.perfil.put(
                                            dadosDeResposta.questao_id,
                                            questao
                                    );
                                } else {
                                    questao = voluntario.perfil.get(dadosDeResposta.questao_id);
                                }

                                resposta.setQuestao(questao);

                                resposta.doVoluntario.texto = dadosDeResposta.marcada_texto;

                                questao.addResposta(resposta);
                            }
                        }
                    }
                    Logger.getAnonymousLogger().info(new String(Base64.decode(response.debug, Base64.DEFAULT)));
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return new LinkedList<Voluntario>(voluntarios.values());
        // return Mock.procuraVoluntarios(busca);
    }

    static private Gson getGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.registerTypeAdapter(QuestaoUnica.class, new JSONAbstractElementAdapter<QuestaoMultipla>());
//        gsonBuilder.registerTypeAdapter(QuestaoMultipla.class, new JSONAbstractElementAdapter<QuestaoMultipla>());
        return gsonBuilder.create();
    }

    public void voluntarioResponde(Voluntario voluntario, Questao q, Resposta r) {
        //Mock.sleep(500);
    }

    static public String toJson(Object o) {
        return getGson().toJson(o);
    }

    static public <T> T fromJson(String s, Class<T> t) {
        return getGson().fromJson(s, t);
    }


    static public boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    static public void verificarPermissoes(Activity activity) {
        {
            String[] permissions = new String[] {
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            };
            if (hasPermissions(activity, permissions) == false) {
                ActivityCompat.requestPermissions(activity, permissions,  100);
            }
        }
        {
            String[] permissions = new String[] {
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
            };
            if (hasPermissions(activity, permissions) == false) {
                ActivityCompat.requestPermissions(activity, permissions,  200);
            }
        }
    }

    static public void iniciarLocationListener(Activity activity) {
        try {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                locationManager = null;
                return;
            }
            if (locationManager == null) {
                locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        0,
                        0,
                        new AriadneLocationListener(activity, locationManager, LocationManager.NETWORK_PROVIDER)
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
