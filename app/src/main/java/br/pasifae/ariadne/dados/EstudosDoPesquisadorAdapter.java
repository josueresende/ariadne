package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Estudo;

/**
 * Created by JosueResende on 19/02/2018.
 */

public class EstudosDoPesquisadorAdapter extends ArrayAdapter<Estudo> {

    private int resource;

    public EstudosDoPesquisadorAdapter(@NonNull Context context, List<Estudo> estudos) {
        super(context, R.layout.layout_lista_estudos);
        this.resource = R.layout.layout_lista_estudos;
        if (estudos != null) super.addAll(estudos);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(this.resource, parent, false);
        }

        ((TextView)layout.findViewById(R.id.textViewNomeEstudo)).setText(getItem(position).getNome());

        return layout;
    }

}
