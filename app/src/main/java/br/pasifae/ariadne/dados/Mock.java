package br.pasifae.ariadne.dados;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import br.pasifae.ariadne.dados.modelo.Busca;
import br.pasifae.ariadne.dados.modelo.Conversa;
import br.pasifae.ariadne.dados.modelo.Criterio;
import br.pasifae.ariadne.dados.modelo.Estudo;
import br.pasifae.ariadne.dados.modelo.Formulario;
import br.pasifae.ariadne.dados.modelo.HasId;
import br.pasifae.ariadne.dados.modelo.Pesquisador;
import br.pasifae.ariadne.dados.modelo.Questao;
import br.pasifae.ariadne.dados.modelo.Resposta;
import br.pasifae.ariadne.dados.modelo.RespostaDoVoluntario;
import br.pasifae.ariadne.dados.modelo.Voluntario;

/**
 * Created by JosueResende on 14/02/2018.
 */

public class Mock {

    final private static int millis = 100;

    private static SharedPreferences mockPreferences;
    private static Mock INSTANCE = new Mock();

    public static class Table<T extends HasId> implements Serializable {
        private Long lastInsertId = 0L;
        private Map<Long, T> records = new LinkedHashMap<Long, T>();
        public T search(Long id) {
            if (records.containsKey(id)) return records.get(id);
            return null;
        }
        public List<T> search(Comparable<T> comparable) {
            List<T> search = new LinkedList<T>();
            for (T o : records.values()) {
                if (comparable.compareTo(o) != 0) continue;
                search.add(o);
            }
            return search;
        }
        public List<T> list() {
            List<T> list = new LinkedList<T>();
            for (T o : records.values()) {
                list.add(o);
            }
            return list;
        }
        public int count() {
            return records.size();
        }
        public int count(Comparable<T> comparable) {
            int count = 0;
            for (T o : records.values()) {
                count += comparable.compareTo(o);
            }
            return count;
        }
        public T save(T o) {
            if (o.getId() == null) o.setId(++lastInsertId);
            records.put(o.getId(), o);
            return o;
        }
        public boolean delete(T o) {
            if (o.getId() == null) return false;
            if (records.containsKey(o.getId())) records.remove(o.getId());
            return !records.containsKey(o.getId());
        }
    }

    public static class ObjectWithId implements HasId {
        Long id;
        public Long getId() { return id; }
        public void setId(Long id) { this.id = id; }
    }

//    public static class RespostaDoVoluntario extends ObjectWithId implements Serializable {
//        Voluntario voluntario;
//        Questao questao;
//        Resposta resposta;
//        String texto;
//        Boolean marcado;
//
//        public RespostaDoVoluntario(Voluntario voluntario, Questao questao, Resposta resposta, String texto, Boolean marcado) {
//            this.voluntario = voluntario;
//            this.questao = questao;
//            this.resposta = resposta;
//            this.texto = texto;
//            this.marcado = marcado;
//        }
//    }

    public static void load(Activity activity) {
        mockPreferences = activity.getSharedPreferences("Mock", Context.MODE_PRIVATE);
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void truncate(String key) {
        try {
            SharedPreferences.Editor editor = mockPreferences.edit();
            editor.remove(key);
            editor.commit();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private <T> T load(String key, T def) {
        try {
            if (mockPreferences.contains(key)) {
                byte[] byteArray = Base64.decode(mockPreferences.getString(key, null), Base64.DEFAULT);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                T o = (T) objectInputStream.readObject();
                objectInputStream.close();
                byteArrayInputStream.close();
                return o;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return def;
    }

    private <T> void save(String key, T o) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(o);
            objectOutputStream.flush();
            objectOutputStream.close();
            byteArrayOutputStream.toByteArray();
            SharedPreferences.Editor editor = mockPreferences.edit();
            editor.putString(key, Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
            editor.commit();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static Voluntario putVoluntario(Voluntario o) {
        String key = "VOLUNTARIOS";
        Table<Voluntario> table = INSTANCE.load(key, new Table<Voluntario>());
        Mock.sleep(millis);
        o = table.save(o);
        INSTANCE.save(key, table);
        return o;
    }

    public static Voluntario getVoluntario(final String username) {
        String key = "VOLUNTARIOS";
        Table<Voluntario> table = INSTANCE.load(key, new Table<Voluntario>());
        Mock.sleep(millis);
        List<Voluntario> os = table.search(new Comparable<Voluntario>() {
            @Override
            public int compareTo(@NonNull Voluntario o) {
                if (o.getUsername() == null) return -1;
                return o.getUsername().toLowerCase().contains(username) ? 0 : 1;
            }
        });
        return (os.size() > 0 ? os.get(0) : null);
    }

    public static Pesquisador putPesquisador(Pesquisador o) {
        String key = "PESQUISADORES";
        Table<Pesquisador> table = INSTANCE.load(key, new Table<Pesquisador>());
        Mock.sleep(millis);
        o = table.save(o);
        INSTANCE.save(key, table);
        return o;
    }

    public static Pesquisador getPesquisador(final String username) {
        String key = "PESQUISADORES";
        Table<Pesquisador> table = INSTANCE.load(key, new Table<Pesquisador>());
        Mock.sleep(millis);
        List<Pesquisador> os = table.search(new Comparable<Pesquisador>() {
            @Override
            public int compareTo(@NonNull Pesquisador o) {
                if (o.getUsername() == null) return -1;
                return o.getUsername().toLowerCase().contains(username) ? 0 : 1;
            }
        });
        return (os.size() > 0 ? os.get(0) : null);
    }

    public static List<RespostaDoVoluntario> listRespostaDoVoluntario(final String username) {
        String key = "RESPOSTASDOSVOLUNTARIOS";
        Table<RespostaDoVoluntario> table = INSTANCE.load(key, new Table<RespostaDoVoluntario>());
        Mock.sleep(millis);
        Logger.getAnonymousLogger().info("[" + key + "] search: " + username);
        List<RespostaDoVoluntario> os = table.search(new Comparable<RespostaDoVoluntario>() {
            @Override
            public int compareTo(@NonNull RespostaDoVoluntario o) {
                if (o.getVoluntario() == null) return -1;
                return o.getVoluntario().getUsername().toLowerCase().contains(username) ? 0 : 1;
            }
        });
        return os;
    }

    public static RespostaDoVoluntario putRespostaDoVoluntario(final RespostaDoVoluntario o) {
        String key = "RESPOSTASDOSVOLUNTARIOS";
        Table<RespostaDoVoluntario> table = INSTANCE.load(key, new Table<RespostaDoVoluntario>());
        Mock.sleep(millis);
        if (o.getQuestao() != null && o.getQuestao().getLimiteDeRespostasMarcadas() == 1) {
            List<RespostaDoVoluntario> toRemove = table.search(new Comparable<RespostaDoVoluntario>() {
                @Override
                public int compareTo(@NonNull RespostaDoVoluntario toCompare) {
                    if (o.getId() != null && toCompare.getId().longValue() == o.getId().longValue()) return -1;
                    boolean voluntario = toCompare.getVoluntario().getId().longValue() == o.getVoluntario().getId().longValue();
                    boolean questao = toCompare.getQuestao() != null && toCompare.getQuestao().getId().longValue() == o.getQuestao().getId().longValue();
                    return (voluntario && questao) ? 0 : 1;
                }
            });
            Logger.getAnonymousLogger().info("removing: " + toRemove.size());
            for (RespostaDoVoluntario respostaDoVoluntario : toRemove) {
                table.delete(respostaDoVoluntario);
            }
        }
        RespostaDoVoluntario respostaDoVoluntario = table.save(o);
        INSTANCE.save(key, table);
        return respostaDoVoluntario;
    }

    public static int delRespostaDoVoluntario(final RespostaDoVoluntario o) {
        String key = "RESPOSTASDOSVOLUNTARIOS";
        Table<RespostaDoVoluntario> table = INSTANCE.load(key, new Table<RespostaDoVoluntario>());
        Mock.sleep(millis);
        int i = (table.delete(o) ? 1 : 0);
        INSTANCE.save(key, table);
        return i;
    }

    public static Estudo putEstudo(Estudo o) {
        return putObject("ESTUDOS", o);
    }

    public static Formulario putFormulario(Formulario o) {
        return putObject("FORMULARIOS", o);
    }

    public static <T extends HasId> T putObject(String key, T o) {
        Table<T> table = INSTANCE.load(key, new Table<T>());
        Mock.sleep(millis);
        o = table.save(o);
        INSTANCE.save(key, table);
        return o;
    }

    public static List<Estudo> getEstudosDoPesquisador(final String username) {
        String key = "ESTUDOS";
        Table<Estudo> table = INSTANCE.load(key, new Table<Estudo>());
        Mock.sleep(millis);
        Logger.getAnonymousLogger().info("[" + key + "] search: " + username);
        return table.search(new Comparable<Estudo>() {
            @Override
            public int compareTo(@NonNull Estudo o) {
                if (o.getPesquisador() == null) return -1;
                return o.getPesquisador().getUsername().toLowerCase().contains(username) ? 0 : 1;
            }
        });
    }

    private static long getLastInsertId_Busca() {
        long last_insert_id = 0;
        Mock.sleep(millis);
        {
            String key = "ESTUDOS";
            Table<Estudo> table = INSTANCE.load(key, new Table<Estudo>());
            last_insert_id = 1 + table.count(new Comparable<Estudo>() {
                @Override
                public int compareTo(@NonNull Estudo estudo) {
                    return estudo.getBuscas() == null ? 0 : estudo.getBuscas().size();
                }
            });
        }
        {
            String key = "BUSCAS";
            Table<Busca> table = INSTANCE.load(key, new Table<Busca>());
            last_insert_id += table.count();
        }
        return last_insert_id;
    }

    public static Busca putBusca(Estudo estudo, Busca o) {
        String key = "ESTUDOS";
        Table<Estudo> table = INSTANCE.load(key, new Table<Estudo>());
        Mock.sleep(millis);
        o.setId( getLastInsertId_Busca() );
        table.save(estudo.addBusca(o));
        INSTANCE.save(key, table);
        return o;
    }

    public static List<Busca> getBuscasDoEstudo(Estudo o) {
        String key = "ESTUDOS";
        Table<Estudo> table = INSTANCE.load(key, new Table<Estudo>());
        Mock.sleep(millis);
        o = table.search(o.getId());
        return o.getBuscas();
    }

    public static Busca putBusca(Busca o) {
        String key = "BUSCAS";
        Table<Busca> table = INSTANCE.load(key, new Table<Busca>());
        Mock.sleep(millis);
        o.setId( getLastInsertId_Busca() );
        table.save(o);
        INSTANCE.save(key, table);
        return o;
    }

    public static List<Busca> getBuscasDoPesquisador(final String username) {
        String key = "BUSCAS";
        Table<Busca> table = INSTANCE.load(key, new Table<Busca>());
        Mock.sleep(millis);
        Logger.getAnonymousLogger().info("[" + key + "] search: " + username);
        return table.search(new Comparable<Busca>() {
            @Override
            public int compareTo(@NonNull Busca o) {
                if (o.getPesquisador() == null) return -1;
                return o.getPesquisador().getUsername().toLowerCase().contains(username) ? 0 : 1;
            }
        });
    }

    public static Questao putQuestao(Questao o) {
        String key = "QUESTOES";
        Table<Questao> table = INSTANCE.load(key, new Table<Questao>());
        Mock.sleep(millis);
        o = table.save(o);
        INSTANCE.save(key, table);
        return o;
    }

    public static List<Questao> getQuestoes() {
        String key = "QUESTOES";
        Table<Questao> table = INSTANCE.load(key, new Table<Questao>());
        Mock.sleep(millis);
        return new LinkedList<Questao>(table.records.values());
    }

    public static List<Criterio> getCriteriosDaBusca(final Busca busca) {
        String key = "CRITERIOS";
        Table<Criterio> table = INSTANCE.load(key, new Table<Criterio>());
        Mock.sleep(millis);
        return table.search(new Comparable<Criterio>() {
            @Override
            public int compareTo(@NonNull Criterio o) {
                if (busca == null && o.getBusca() == null) return 0;
                if (o.getBusca() == null) return 1;
                return o.getBusca().getId().compareTo(busca.getId());
            }
        });
    }

    public static List<Criterio> getCriteriosDaBuscaEQuestao(final Busca busca, final Questao questao) {
        String key = "CRITERIOS";
        Table<Criterio> table = INSTANCE.load(key, new Table<Criterio>());
        Mock.sleep(millis);
        return table.search(new Comparable<Criterio>() {
            @Override
            public int compareTo(@NonNull Criterio o) {
                if (busca == null) return -1;
                if (o.getBusca() == null) return 1;
                if (o.getBusca().getId().compareTo(busca.getId()) != 0) return -1;
                if (o.getQuestao() == null) return -1;
                return (o.getQuestao().getId().compareTo(questao.getId()));
            }
        });
    }

    public static Resposta putResposta(Resposta o) {
        String key = "RESPOSTAS";
        Table<Resposta> table = INSTANCE.load(key, new Table<Resposta>());
        Mock.sleep(millis);
        o = table.save(o);
        INSTANCE.save(key, table);
        return o;
    }

    public static List<Resposta> getRespostas() {
        String key = "RESPOSTAS";
        Table<Resposta> table = INSTANCE.load(key, new Table<Resposta>());
        Mock.sleep(millis);
        return new LinkedList<Resposta>(table.records.values());
    }

    public static List<Resposta> getRespostas(final Questao questao) {
        String key = "RESPOSTAS";
        Table<Resposta> table = INSTANCE.load(key, new Table<Resposta>());
        Mock.sleep(millis);
        List<Resposta> respostas = table.search(new Comparable<Resposta>() {
            @Override
            public int compareTo(@NonNull Resposta o) {
                if (o == null) return 1;
                if (o.getQuestao() == null) return 1;
                return o.getQuestao().getId().compareTo(questao.getId());
            }
        });
        return respostas;
    }

    public static List<Conversa> getConversas(Voluntario voluntario) {
        Pesquisador pesquisador1 = getPesquisador("pesquisador1");
        Pesquisador pesquisador2 = getPesquisador("pesquisador2");
        Pesquisador pesquisador3 = getPesquisador("pesquisador3");

        List<Conversa> conversas = new LinkedList<Conversa>();

        conversas.add(
                new Conversa(Long.valueOf(conversas.size()), pesquisador1, voluntario, null)
        );
        conversas.add(
                new Conversa(Long.valueOf(conversas.size()), pesquisador3, voluntario, null)
        );
        conversas.add(
                new Conversa(Long.valueOf(conversas.size()), pesquisador2, voluntario, null)
        );
        return conversas;
    }

    public static List<Formulario> getFormularios() {
        String key = "FORMULARIOS";
        Table<Formulario> table = INSTANCE.load(key, new Table<Formulario>());
        Mock.sleep(millis);
        List<Formulario> formularios = null;
        formularios = table.search(new Comparable<Formulario>() {
            @Override
            public int compareTo(@NonNull Formulario o) {
                if (o == null) return 1;
                if (o.getResposta() != null) return 1;
                return 0;
            }
        });
        return formularios;
    }

    public static List<Formulario> getFormularios(final RespostaDoVoluntario respostaDoVoluntario) {
        String key = "FORMULARIOS";
        Table<Formulario> table = INSTANCE.load(key, new Table<Formulario>());
        Mock.sleep(millis);
        List<Formulario> formularios = table.search(new Comparable<Formulario>() {
            @Override
            public int compareTo(@NonNull Formulario o) {
                if (o == null) return 1;
                if (o.getResposta() == null) return 1;
                if (respostaDoVoluntario.getResposta().getId().equals(o.getResposta().getId()) == false) return 1;
                if (o.getTextoParaRespostaLivre() == null) return 0;
                if (respostaDoVoluntario.hasTexto() &&
                    respostaDoVoluntario.getTexto().toLowerCase().contains(o.getTextoParaRespostaLivre().toLowerCase())) {
                    return 0;
                }
                return 1;
            }
        });
        return formularios;
    }

    public static Criterio putCriterio(Criterio o) {
        String key = "CRITERIOS";
        Table<Criterio> table = INSTANCE.load(key, new Table<Criterio>());
        Mock.sleep(millis);
        o = table.save(o);
        INSTANCE.save(key, table);
        return o;
    }
/*
    public static List<Formulario> getFormularios() {
        List<Formulario> formularios = new LinkedList<Formulario>();

        long idFormulario = 1L;
        long idQuestao = 1L;
        long idResposta = 1L;

        sleep(500);

        List<Questao> questoes = Mock.getQuestoes();

        Formulario formulario = null;

        formulario = new Formulario(idFormulario++,
            "CADASTRO",
            questoes.toArray(new Questao[] {}))
        ;

        formularios.add(formulario);

//        formularios.add(
//                new Formulario(idFormulario++, "** testes **", null)
//                        .addQuestao(idQuestao++,
//                                "Você está vendo uma marcação de um só?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Sim"),
//                                        new Resposta(idResposta++, "Não"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Data de nascimento",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, Resposta.Conteudo.DATA, ""),
//                                }
//                        )
//                        .addQuestaoMultipla(idQuestao++,
//                                "Qual sua opinião sobre aplicativos móveis?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Tudo igual"),
//                                        new Resposta(idResposta++, "Apenas alguns são bons"),
//                                        new Resposta(idResposta++, "Desapeguei dessas coisas"),
//                                        new Resposta(idResposta++, Resposta.Conteudo.TEXTO, "Outros"),
//                                }
//                        )
//                        .addQuestaoMultipla(idQuestao++,
//                                "Quanto itens tem esta marcação múltipla?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Um"),
//                                        new Resposta(idResposta++, "Dois"),
//                                        new Resposta(idResposta++, "Três"),
//                                        new Resposta(idResposta++, "Parei de contar..."),
//                                }
//                        )
//        );
//        formularios.add(
//                new Formulario(idFormulario++, "Dados - pessoais", null)
//                        .addQuestao(idQuestao++,
//                                "Gênero",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Masculino"),
//                                        new Resposta(idResposta++, "Feminino"),
//                                }
//                        )
//                        .addQuestaoMultipla(idQuestao++,
//                                "Alergia",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Lactose"),
//                                        new Resposta(idResposta++, "Sulfa"),
//                                        new Resposta(idResposta++, "Camarão"),
//                                        new Resposta(idResposta++, "Frutos do Mar"),
//                                        new Resposta(idResposta++, "Ovo"),
//                                        new Resposta(idResposta++, Resposta.Conteudo.TEXTO, "Outros"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Estado do Brasil em que reside",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Região Norte"),
//                                        new Resposta(idResposta++, "Região Nordeste"),
//                                        new Resposta(idResposta++, "Região Sudeste"),
//                                        new Resposta(idResposta++, "Região Sul"),
//                                }
//                        )
//                        .addQuestaoMultipla(idQuestao++,
//                                "Doenças conhecidas",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Diabetes"),
//                                        new Resposta(idResposta++, "Lupus"),
//                                        new Resposta(idResposta++, "Artrose"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Deseja participar do estudo sobre Zika?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Sim"),
//                                        new Resposta(idResposta++, "Não"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Deseja participar do estudo sobre Chikungunya?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Sim"),
//                                        new Resposta(idResposta++, "Não"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Deseja participar do estudo sobre Malária?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Sim"),
//                                        new Resposta(idResposta++, "Não"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Deseja participar do estudo sobre Dengue?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Sim"),
//                                        new Resposta(idResposta++, "Não"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Deseja participar do estudo sobre Febre Amarela?",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Sim"),
//                                        new Resposta(idResposta++, "Não"),
//                                }
//                        )
//       );
//
//        sleep(500);
//        formularios.add(
//                new Formulario(idFormulario++, "Origem", null)
//                        .addQuestao(idQuestao++,
//                                "Região do Brasil",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Região Norte"),
//                                        new Resposta(idResposta++, "Região Nordeste"),
//                                        new Resposta(idResposta++, "Região Sudeste"),
//                                        new Resposta(idResposta++, "Região Sul"),
//                                }
//                        )
//                        .addQuestao(idQuestao++,
//                                "Etnia",
//                                new Resposta[]{
//                                        new Resposta(idResposta++, "Negro"),
//                                        new Resposta(idResposta++, "Branco"),
//                                        new Resposta(idResposta++, "Índio"),
//                                        new Resposta(idResposta++, "Asiático"),
//                                }
//                        )
//        );
        return formularios;
    }
// */

    public static List<Voluntario> procuraVoluntarios(final Busca busca) {
        String key = "RESPOSTASDOSVOLUNTARIOS";
        Table<RespostaDoVoluntario> table = INSTANCE.load(key, new Table<RespostaDoVoluntario>());

        final List<Criterio> criterios = getCriteriosDaBusca(busca);

        List<RespostaDoVoluntario> matches = table.search(new Comparable<RespostaDoVoluntario>() {
            @Override
            public int compareTo(@NonNull RespostaDoVoluntario o) {
                if (o.getQuestao() == null) return -1;
                long idQuestaoVoluntario = o.getQuestao().getId();
                long idRespostaVoluntario = o.getResposta().getId();
                boolean match = true;
                for (Criterio criterio : criterios) {
                    long idQuestaoCriterio = criterio.getQuestao().getId();
                    if (idQuestaoCriterio != idQuestaoVoluntario) return -1;
                    long idRespostaCriterio = criterio.getResposta().getId();
                    if (idRespostaCriterio != idRespostaVoluntario) return -1;
                    criterio.getResposta().getMarcado();
                    return 0;
                }
                return -1;
            }
        });

        List<Voluntario> voluntarios = new LinkedList<Voluntario>();

        for (RespostaDoVoluntario respostaDoVoluntario : matches) {
            if (voluntarios.contains(respostaDoVoluntario.getVoluntario())) continue;
            voluntarios.add(respostaDoVoluntario.getVoluntario());
        }

        return voluntarios;

    }
}
