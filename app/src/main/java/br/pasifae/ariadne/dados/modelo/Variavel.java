package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by JosueResende on 19/02/2018.
 */

public class Variavel implements Serializable, HasId {

    public static enum Operacao {
        IGUAL,
        DENTRO,
        CONTEM;
    }

    private Long id;
    private String operacao;
    private Resposta resposta;
    private List<String> valores;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public Resposta getResposta() {
        return resposta;
    }

    public void setResposta(Resposta resposta) {
        this.resposta = resposta;
    }

    public List<String> getValores() {
        return valores;
    }

    public void setValores(List<String> valores) {
        this.valores = valores;
    }
}
