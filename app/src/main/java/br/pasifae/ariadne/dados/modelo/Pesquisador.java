package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by JosueResende on 13/02/2018.
 */

public class Pesquisador implements HasId, Serializable {
    Long id;
    String username;
    String password;
    String nome;
    private byte[] foto;
    private Usuario usuario;
    public Map<Long, Busca> buscas = new LinkedHashMap<>();
    public Map<Long, Usuario> mensagens = new LinkedHashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    private boolean cienteDoTermo;

    public void setCienteDoTermo(boolean cienteDoTermo) {
        this.cienteDoTermo = cienteDoTermo;
    }

    public boolean isCienteDoTermo() {
        return cienteDoTermo;
    }
}
