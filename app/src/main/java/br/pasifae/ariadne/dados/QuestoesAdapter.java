package br.pasifae.ariadne.dados;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Questao;

/**
 * Created by JosueResende on 24/02/2018.
 */

public class QuestoesAdapter extends ArrayAdapter<Questao> {

    int resource = -1;

    public QuestoesAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout = convertView;
//        if (convertView == null) {
//            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            layout = layoutInflater.inflate(this.resource, parent, false);
//        }
        if (position == 0) {
            layout = super.getDropDownView(position, convertView, parent);
            layout.setBackgroundColor(Color.LTGRAY);
        } else {
            layout = super.getDropDownView(position, null, parent);
        }
        return layout;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View layout = convertView;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(this.resource, parent, false);

            String text = "";

            Object object = getItem(position);
            if (object instanceof Questao) {
                text = ((Questao)object).getTituloParaPesquisador();
                String[] words = text.split(" ");
                text = "";
                int tamanho = 0;
                for (String word : words) {
                    if ((tamanho + 1 + word.length()) > 20) {
                        text = text.concat("\n").concat(word);
                        tamanho = word.length();
                    } else {
                        text = text.concat(" ").concat(word);
                        tamanho += 1 + word.length();
                    }
                }
            }
            if (layout.findViewById(android.R.id.text1) != null) {
                ((CheckedTextView) layout.findViewById(android.R.id.text1)).setText(text);
            } else {
                ((TextView)layout.findViewById(R.id.textViewTextoDaQuestao)).setText(text);
            }
        }
        return layout;
    }
}
