package br.pasifae.ariadne.dados;

import android.content.Context;

/**
 * Created by JosueResende on 23/04/2018.
 */

public class SwipeRecyclerViewDatabaseAdapter<T> extends SwipeRecyclerViewAdapter<T>  {

    public SwipeRecyclerViewDatabaseAdapter(Context context, OnCreateCardView onCreateCardView, SwipeRecyclerViewDatabase swipeRecyclerViewDatabase) {
        super(context, onCreateCardView);
        this.swipeRecyclerViewDatabase = swipeRecyclerViewDatabase;
        if (swipeRecyclerViewDatabase != null) swipeRecyclerViewDatabase.init();
    }

    static public interface SwipeRecyclerViewDatabase<T> {
        int getCount();
        long getId(int position);
        T getItem(int position);
        void init();
    }

    private SwipeRecyclerViewDatabase<T> swipeRecyclerViewDatabase;

    @Override
    public long getItemId(int position) {
        if (swipeRecyclerViewDatabase != null) return swipeRecyclerViewDatabase.getId(position);
        return 0;
    }

    @Override
    public int getItemCount() {
        if (swipeRecyclerViewDatabase != null) return swipeRecyclerViewDatabase.getCount();
        return 0;
    }

    @Override
    public T get(int position) {
        if (swipeRecyclerViewDatabase != null) return swipeRecyclerViewDatabase.getItem(position);
        return null;
    }

}
