package br.pasifae.ariadne.dados.modelo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by JosueResende on 19/02/2018.
 */

public class Busca implements Serializable, HasId {
    private Long id;
    private String nome;
    private Pesquisador pesquisador;
    private List<Resposta> respostas = new LinkedList<Resposta>();

    public Busca(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pesquisador getPesquisador() {
        return pesquisador;
    }

    public void setPesquisador(Pesquisador pesquisador) {
        this.pesquisador = pesquisador;
    }

    public List<Resposta> getRespostas() {
        return respostas;
    }

    public void setRespostas(List<Resposta> respostas) {
        this.respostas = respostas;
    }
}
