package br.pasifae.ariadne.dados;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import br.pasifae.ariadne.R;
import br.pasifae.ariadne.dados.modelo.Questao;

/**
 * Created by JosueResende on 14/02/2018.
 */

public class QuestoesDoVoluntarioAdapter extends ArrayAdapter<Questao> {

    int resource = -1;
    Acesso acesso;

    public QuestoesDoVoluntarioAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.resource = resource;
//        this.acesso = new Acesso(context.get);
    }

    @Override
    public void add(@Nullable Questao object) {
        super.add(object);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View layout = convertView;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = layoutInflater.inflate(this.resource, parent, false);
//            layout.setTag(new DataHandler(layout));
            TextView titulo = layout.findViewById(R.id.textViewTextoDaQuestao);
            Questao questao = getItem(position);

            ListView respostas = layout.findViewById(R.id.listViewRespostasDaQuestao);

            titulo.setText(questao.getQuestao());
            ArrayAdapter adapter = (new RespostasDoVoluntarioAdapter(getContext(), resource)).setQuestao(questao, null);
            respostas.setAdapter(adapter);

        } else {
            //
        }

//        if (layout.getTag() instanceof DataHandler) {
//            DataHandler dataHandler = (DataHandler) layout.getTag();
//            dataHandler.titulo.setText(questao.getQuestao());
//        }

        return layout;
    }

//    private class DataHandler {
//        TextView titulo;
//        ListView buscadas;
//        public DataHandler(View layout) {
//            this.titulo = layout.findViewById(R.id.textViewTextoDaQuestao);
//            this.buscadas = layout.findViewById(R.id.listViewRespostasDaQuestao);
//            this.buscadas.setAdapter(new RespostasDoVoluntarioAdapter(getContext(), resource));
//        }
//    }
}
